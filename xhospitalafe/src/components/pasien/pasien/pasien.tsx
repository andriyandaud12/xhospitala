import React from "react";
import { config } from "../../configurations/config";

import { PaginationModel } from "../../models/paginationModel";
import { BloodGroupModel } from "../../models/bloodGroupModel";
import { Ecommand } from "../../enums/eCommand";
import { PasienModel } from "../../models/pasienModel";
import { BiodataModel } from "../../models/biodataModel";
import { PasienService } from "../../services/pasienServices/pasienService";
import { CustomerRelationModel } from "../../models/RelationModel";
import { CustomerModel } from "../../models/customerModel";
import { BloodGroupService } from "../../services/pasienServices/bloodGroupService";
import { RelationService } from "../../services/pasienServices/relationService";
import { CustomerService } from "../../services/pasienServices/customerService";
import Form from "./form";
import { BiodataService } from "../../services/pasienServices/biodataService";
import { AiOutlineClose } from "react-icons/ai";

interface IProps {}

interface IState {
  relations: CustomerRelationModel[];
  customers: CustomerModel[];
  bloodGroups: BloodGroupModel[];
  biodatas: BiodataModel[];
  pasiens: PasienModel[];
  pasien: PasienModel;
  showModal: boolean;
  command: Ecommand;
  pagination: PaginationModel;

  showSuccessModal: boolean;
  showDeleteModal: boolean;
  deleted: boolean;
}

export default class Pasien extends React.Component<IProps, IState> {
  newPagination: PaginationModel = {
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };
  newBiodata: BiodataModel = {
    id: 0,
    fullName: "",
    mobilePhone: "",
    image: "",
    imagePath: "",
    is_delete: false,
  };
  newBloodGroup: BloodGroupModel = {
    id: 0,
    code: "",
    description: "",
    is_delete: false,
  };
  newCustomerRelation: CustomerRelationModel = {
    id: 0,
    name: "",
    is_delete: false,
  };
  newCustomerModel: CustomerModel = {
    id: 0,
    biodataId: 0,
    dob: new Date(),
    gender: "",
    bloodGroupId: 0,
    rhesusType: "",
    height: 0,
    weight: 0,
    is_delete: false,
    biodata: new BiodataModel(),
    bloodGroup: new BloodGroupModel(),
  };
  newPasien: PasienModel = {
    id: 0,
    biodata: this.newBiodata,
    bloodGroup: this.newBloodGroup,
    customerRelation: this.newCustomerRelation,
    customer: this.newCustomerModel,
    parentBiodataId: 0,
    customerId: 0,
    customerRelationId: 0,
    is_delete: false,
    bloodGroupId: 0,
  };

  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      pasien: {
        ...this.state.pasien,
        [name]: event.target.checked,
      },
    });
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      relations: [],
      customers: [],
      bloodGroups: [],
      biodatas: [],
      pasiens: [],
      pasien: this.newPasien,
      showModal: false,
      command: Ecommand.create,
      pagination: this.newPagination,
      showSuccessModal: false,
      showDeleteModal: false,
      deleted: false,
    };
  }

  componentDidMount(): void {
    this.loadPasien();
  }
  setShowSuccessModal = (val: boolean) => {
    this.setState({
      showSuccessModal: val,
    });
  };
  setShowDeleteModal = (val: boolean) => {
    this.setState({
      showDeleteModal: val,
    });
  };
  onDeleteClick = (id: number) => {
    PasienService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            pasien: result.result,
            showDeleteModal: true,
          });
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  delete = (id: number) => {
    PasienService.changeStatus(id, true)
      .then((result) => {
        if (result.success) {
          this.setState({
            showDeleteModal: false,
            showSuccessModal: true,
            deleted: true,
            pasien: result.result,
          });
          this.loadPasien();
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  // loadBloodGroup=async()=>{
  //   const { pagination } = this.state;
  //   const result = await PasienService.getAll();
  //   if (result.success) {
  //     this.setState({
  //       pasiens: result.result,
  //       // pagination: {
  //       //   ...this.state.pagination,
  //       //     pages: result.pages,
  //       // },
  //     });
  //   }
  //   console.log(result);
  // }

  loadRelation = async () => {
    const { pagination } = this.state;
    const result = await RelationService.getAll(pagination);
    if (result.success) {
      this.setState({
        pasiens: result.result,
        // pagination: {
        //   ...this.state.pagination,
        //     pages: result.pages,
        // },
      });
    }
    console.log(result);
  };
  loadCustomer = async () => {
    const { pagination } = this.state;
    const result = await CustomerService.getAll(pagination);
    if (result.success) {
      this.setState({
        pasiens: result.result,
        // pagination: {
        //   ...this.state.pagination,
        //     pages: result.pages,
        // },
      });
    }
    console.log(result);
  };

  loadPasien = async () => {
    const { pagination } = this.state;
    const result = await PasienService.getAll(pagination);
    if (result.success) {
      const blResult = await BloodGroupService.getAll(pagination);
      const custResult = await CustomerService.getAll(pagination);
      const biodResult = await BiodataService.getAll(pagination);
      const rltnResult = await RelationService.getAll(pagination);
      if (blResult.success) {
        this.setState({
          bloodGroups: blResult.result,
          customers: custResult.result,
          biodatas: biodResult.result,
          relations: rltnResult.result,
        });
      }

      console.log(result.result);

      this.setState({
        pasiens: result.result,
        // pagination: {
        //   ...this.state.pagination,
        //     pages: result.pages,
        // },
      });
    }
    console.log(result);
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      pasien: {
        ...this.state.pasien,
        [name]: event.target.value,
      },
    });
  };

  createCommand = () => {
    this.setState({
      showModal: true,
      pasien: this.newPasien,
      command: Ecommand.create,
    });
    // this.setShowModal(true);
  };

  changeRowsPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadPasien();
      }, 500);
    });
  };

  //promises
  submitHandler = async () => {
    const { command } = this.state;
    if (command === Ecommand.create) {
      await PasienService.post(this.state.pasien)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              pasien: this.newPasien,
            });
            this.loadPasien();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (command === Ecommand.update) {
      // await PasienService.update(this.state.bloodGroup.id, this.state.bloodGroup)
      //   .then((result) => {
      //     if (result.success) {
      //       this.setState({
      //         showModal: false,
      //         bloodGroup: {
      //           id: 0,
      //           code: "",
      //           description:"",
      //           is_delete: false,
      //         },
      //       });
      //       this.loadPasien();
      //     } else {
      //       alert("Error result: " + result.result);
      //     }
      //   })
      //   .catch((error) => {
      //     alert("Error error: " + error);
      //   });
    } else if (command === Ecommand.ChangeStatus) {
      // await PasienService.changeStatus(
      //   this.state.bloodGroup.id,this.state.bloodGroup
      // )
      //   .then((result) => {
      //     if (result.success) {
      //       this.setState({
      //         showModal: false,
      //         bloodGroup: {
      //           id: 0,
      //           code: "",
      //           description: "",
      //           is_delete: true,
      //         },
      //       });
      //       this.loadPasien();
      //     } else {
      //       alert("Error result: " + result.result);
      //     }
      //   })
      //   .catch((error) => {
      //     alert("Error error: " + error);
      //   });
    }
  };
  // relations: CustomerRelationModel[];
  // customer: CustomerModel[];
  // bloodGroup: BloodGroupModel[];
  // biodata: BiodataModel[];
  // pasiens: PasienModel[];
  // pasien: PasienModel;
  // showModal: boolean;
  // command: Ecommand;
  // pagination: PaginationModel;
  render() {
    const {
      pasiens,
      relations,
      customers,
      bloodGroups,
      biodatas,
      pasien,
      showModal,
      command,
      showDeleteModal,
      showSuccessModal,
      deleted,
      pagination,
    } = this.state;
    return (
      <div>
        <div className="text-left text-4xl pt-5 py-5">Daftar Pasien</div>
        {/* <span>{JSON.stringify(pasiens)}</span> */}

        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 
          bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.createCommand()}
          >
            Create New
          </button>
        </div>
        {/* <span>{JSON.stringify(bloodGroups)}</span><br />
        <span>{JSON.stringify(customers)}</span><br />
        <span>{JSON.stringify(biodatas)}</span><br />
        <span>{JSON.stringify(relations)}</span> */}
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Search:
              </th>
              <th colSpan={2} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  //   readOnly={command === Ecommand.ChangeStatus}
                  type="text"
                  id="search"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  //   value={pagination.search}
                  //   onChange={this.changeSearch("search")}
                />
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
                  //   onClick={() => this.loadCategories()}
                >
                  Filter
                </button>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                {/* grow flex-none  */}
                Fullname
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                Golongan Darah
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                tanggal lahir
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                gender
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                tipe resus
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                tinggi
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                berat
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                // onClick={() => this.changeOrder("initial")}
              >
                Relasi
              </th>
              {/* <th scope="col" className="px-6 py-3 w-14 h-14">
                Active
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Action
              </th> */}
            </tr>
          </thead>
          <tbody>
            {pasiens?.map((cat: PasienModel) => {
              // console.log(cat.customer.bloodGroup.code);
              return (
                <tr
                  key={cat.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td className="px-6 py-4">{cat.biodata.fullName}</td>
                  <td className="px-6 py-4">{cat.customer.bloodGroup.code}</td>
                  <td className="px-6 py-4">
                    {new Date(cat.customer.dob).toLocaleDateString()}
                  </td>

                  {/* {new Date(cat.customer.dob).toLocaleDateString()} */}
                  {/* <td className="px-6 py-4">{cat.customer.dob.toLocaleDateString()}</td> */}
                  <td className="px-6 py-4">{cat.customer.gender}</td>
                  <td className="px-6 py-4">{cat.customer.rhesusType}</td>
                  <td className="px-6 py-4">{cat.customer.height}</td>
                  <td className="px-6 py-4">{cat.customer.weight}</td>
                  <td className="px-6 py-4">{cat.customerRelation.name}</td>
                  {/* <td className="px-6 py-4">{cat.name}</td> */}

                  <td className="px-6 py-4">
                    <div className="flex items-center">
                      <input
                        // checked={cat.active}
                        id="checked-checkbox"
                        type="checkbox"
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                    </div>
                  </td>
                  <td className="px-6 py-4">
                    <div
                      className="inline-flex"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="h-10 px-5 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                        // onClick={() => this.updateCommand(cat.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="h-10 px-5 text-red-100 transition-colors duration-150 bg-red-700 rounded-r-lg focus:shadow-outline hover:bg-red-800"
                        onClick={() => this.onDeleteClick(cat.id)}
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>

          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                  Rows per page:
                </label>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onClick={this.changeRowsPerPage("rows")}
                >
                  {config.rowsPerPage.map((cat: number) => {
                    return <option value={cat}>{cat}</option>;
                  })}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Page:
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("pageNum")}
                >
                  {/* {loopPages()} */}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl ">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {command.valueOf()}
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <Form
                    bloodGroups={bloodGroups}
                    customers={customers}
                    pasien={pasien}
                    command={command}
                    changeHandler={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                    relations={relations}
                    biodatas={biodatas}
                  />
                </div>
                <div
                  className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                  role="group"
                  aria-label="Button group"
                >
                  <button
                    className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="my-8 justify-start h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                    onClick={() => this.submitHandler()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showSuccessModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        Success
                      </h3>
                      <button onClick={() => this.setShowSuccessModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      {deleted
                        ? "Tindakan berhasil dihapus"
                        : "Tindakan berhasil ditambahkan"}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showDeleteModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        hapus
                      </h3>
                      <button onClick={() => this.setShowDeleteModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      Anda setuju untuk menghapus 
                      {pasien.biodata.fullName} ?
                    </h3>
                    <div
                      className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                        onClick={() => this.setShowDeleteModal(false)}
                      >
                        Batal
                      </button>
                      <button
                        onClick={() => this.delete(pasien.id)}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                      >
                        Hapus
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
