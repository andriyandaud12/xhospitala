import React from "react";

import { PaginationModel } from "../../models/paginationModel";
import { BloodGroupModel } from "../../models/bloodGroupModel";
import { Ecommand } from "../../enums/eCommand";
import { PasienModel } from "../../models/pasienModel";
import { BiodataModel } from "../../models/biodataModel";
import { PasienService } from "../../services/pasienServices/pasienService";
import { CustomerRelationModel } from "../../models/RelationModel";
import { CustomerModel } from "../../models/customerModel";

interface IProps {
  pasien: PasienModel;
  customers: CustomerModel[];
  bloodGroups: BloodGroupModel[];
  biodatas: BiodataModel[];
  relations: CustomerRelationModel[];
  command: Ecommand;
  changeHandler: any;
  checkBoxHandler: any;
}

interface IState {}

export default class Form extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
  }

  render() {
    const {
      bloodGroups,
      relations,
      customers,
      biodatas,
      pasien,
      command,
      changeHandler,
      checkBoxHandler,
    } = this.props;

    const biodata = biodatas.find((b) => b.id === pasien.parentBiodataId);
    return (
      <>
        <div style={{ maxHeight: "400px", overflowY: "auto" }}>
          <form style={{ maxWidth: "400px" }}>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Nama Lengkap
              </label>
              <input
                //   readOnly={command == Ecommand.ChangeStatus}
                type="text"
                id="initial"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
                value={biodata?.fullName}
                onChange={changeHandler("fullName")}
              />
            </div>
            <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Tanggal lahir
            </label>
            <input
              type="date"
              id="dob"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              // value={
              //   pasien.customer.dob instanceof Date
              //     ? pasien.customer.dob.toISOString().split("T")[0]  // Format tanggal sesuai dengan format date (YYYY-MM-DD)
              //     : ""
              // }
              onChange={changeHandler("dob")}
            />
          </div>
          
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Jenis Kelamin
              </label>
              <div className="flex items-center space-x-4">
                <label className="inline-flex items-center">
                  <input
                    type="radio"
                    className="form-radio text-blue-500"
                    name="gender"
                    value="P"
                    onChange={changeHandler("gender")}
                    // You can add checked={selectedValue === 'pria'} if you have a state to manage the selected value
                  />
                  <span className="ml-2">Pria</span>
                </label>
                <label className="inline-flex items-center">
                  <input
                    type="radio"
                    className="form-radio text-blue-500"
                    name="gender"
                    value="W"
                    onChange={changeHandler("gender")}
                    // You can add checked={selectedValue === 'wanita'} if you have a state to manage the selected value
                  />
                  <span className="ml-2">Wanita</span>
                </label>
              </div>
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Golongan Darah
              </label>
              <select
                id="countries"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value={pasien.bloodGroupId}
                onChange={changeHandler("bloodGroupId")}
              >
                <option selected value="0">
                --- Pilih ---
                </option>
                {bloodGroups.map((o: BloodGroupModel) => {
                  return <option value={o.id}>{o.code}</option>;
                })}
              </select>
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Tipe Resus
              </label>
              <div className="flex items-center space-x-4">
                <label className="inline-flex items-center">
                  <input
                    type="radio"
                    className="form-radio text-blue-500"
                    name="rhesusType"
                    value="RH-"
                    onChange={changeHandler("rhesusType")}
                    // You can add checked={selectedValue === 'pria'} if you have a state to manage the selected value
                  />
                  <span className="ml-2">RH-</span>
                </label>
                <label className="inline-flex items-center">
                  <input
                    type="radio"
                    className="form-radio text-blue-500"
                    name="rhesusType"
                    value="RH+"
                    onChange={changeHandler("rhesusType")}
                    // You can add checked={selectedValue === 'wanita'} if you have a state to manage the selected value
                  />
                  <span className="ml-2">RH+</span>
                </label>
              </div>
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Tinggi Badan
              </label>
              <input
                //   readOnly={command == Ecommand.ChangeStatus}
                type="text"
                id="weight"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
                // value={pasien.customer.height}
                onChange={changeHandler("height")}
              />
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Berat Badan
              </label>
              <input
                //   readOnly={command == Ecommand.ChangeStatus}
                type="number"
                id="name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
                // value={pasien.customer.weight}
                onChange={changeHandler("weight")}
              />
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Relasi
              </label>
              <select
                id="countries"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value={pasien.customerRelationId}
                onChange={changeHandler("customerRelationId")}
              >
                <option selected value="0">
                  --- Pilih ---
                </option>
                {relations.map((o: CustomerRelationModel) => {
                  return <option value={o.id}>{o.name}</option>;
                })}
              </select>
            </div>
            {/* <span>{JSON.stringify(pasien)}</span> */}

            {command == Ecommand.ChangeStatus ? (
              <div className="flex items-start mb-6">
                <div className="flex items-center h-5">
                  <input
                    type="checkbox"
                    value=""
                    className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                    required
                    checked={pasien.customer.is_delete}
                    onChange={checkBoxHandler("is_delete")}
                  />
                </div>
                <label className="ms-2 text-sm font-medium text-gray-900 dark:text-gray-700">
                  Is Active
                </label>
              </div>
            ) : null}
          </form>
        </div>
      </>
    );
  }
}
