import React from "react";

import { Ecommand } from "../../enums/eCommand";
import { BloodGroupModel } from "../../models/bloodGroupModel";
import { config } from "../../configurations/config";

interface IProps {
  errorAlerts: any;

  onChange: any;
  command: Ecommand;
  checkBoxHandler: any;
  bloodGroup: BloodGroupModel;
}

interface IState {
}

export default class Form extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      bloodGroup: new BloodGroupModel(),
    };
  }

  render() {
    const {  bloodGroup, onChange, checkBoxHandler, command, errorAlerts } =
      this.props;
    return (
      <>
        <form>
          {/* <span>{JSON.stringify(this.state.bloodGroup)}</span> */}

          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Kode*
            </label>
            <input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={onChange("code")}
              value={bloodGroup.code}
            />
            {!errorAlerts.Code?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Code?.message}
              </strong>
            ) : null}
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Deskripsi
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="text"
              id="description"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              
              onChange={onChange("description")}
              value={bloodGroup.description}
            />
          </div>
        </form>
      </>
    );
  }
}
