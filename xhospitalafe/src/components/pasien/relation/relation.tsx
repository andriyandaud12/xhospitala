import React from "react";
import { config } from "../../configurations/config";

import { PaginationModel } from "../../models/paginationModel";
import { Ecommand } from "../../enums/eCommand";
import { RelationService } from "../../services/pasienServices/relationService";
import { CustomerRelationModel } from "../../models/RelationModel";
import Form from "./form";
import { AiOutlineClose } from "react-icons/ai";
import { ValidationResult } from "../../../validations/validationResult";

interface IProps {}

interface IState {
  customerRelations: CustomerRelationModel[];
  customerRelation: CustomerRelationModel;
  showModal: boolean;
  command: Ecommand;
  pagination: PaginationModel;

  errorAlerts: any;
  showSuccessModal: boolean;
  showDeleteModal: boolean;
  showConfirm: boolean;
  deleted: boolean;

  oldName: string;
  newName: string;
  showEditSuccessModal: boolean;
}

export default class CustomerRelation extends React.Component<IProps, IState> {
  newPagination: PaginationModel = {
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };
  newCustomerRelation: CustomerRelationModel = {
    id: 0,
    name: "",
    is_delete: false,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      customerRelations: [],
      customerRelation: this.newCustomerRelation,
      showModal: false,
      command: Ecommand.create,
      pagination: this.newPagination,
      showSuccessModal: false,
      showDeleteModal: false,
      showConfirm: false,
      deleted: false,
      errorAlerts: { name: false },

      oldName: "", // initialize with appropriate default values
      newName: "",
      showEditSuccessModal: false,
    };
  }
  setShowSuccessModal = (val: boolean) => {
    this.setState({
      showSuccessModal: val,
    });
  };

  setShowDeleteModal = (val: boolean) => {
    this.setState({
      showDeleteModal: val,
    });
  };
  setShowConfirm = (val: boolean) => {
    this.setState({
      showConfirm: val,
    });
  };
  onDeleteClick = (id: number) => {
    RelationService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            customerRelation: result.result,
            showDeleteModal: true,
          });
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  delete = (id: number) => {
    RelationService.changeStatus(id, true)
      .then((result) => {
        if (result.success) {
          this.setState({
            showDeleteModal: false,
            showSuccessModal: true,
            deleted: true,
            customerRelation: result.result,
          });
          this.loadPasien();
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  componentDidMount(): void {
    this.loadPasien();
  }

  loadPasien = async () => {
    const { pagination } = this.state;
    const result = await RelationService.getAll(pagination);
    if (result.success) {
      this.setState({
        customerRelations: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    }
    console.log(result);
  };
  loadReset = () => {
    this.setState(
      {
        pagination: this.newPagination,
      },
      () => {
        // Callback function to ensure that the state is updated
        this.loadPasien();
      }
    );
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      customerRelation: {
        ...this.state.customerRelation,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };
  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      customerRelation: {
        ...this.state.customerRelation,
        [name]: event.target.checked,
      },
    });
  };

  createCommand = () => {
    this.setState({
      showModal: true,
      customerRelation: this.newCustomerRelation,
      command: Ecommand.create,
    });
    // this.setShowModal(true);
  };
  updateCommand = async (id: number) => {
    await RelationService.getById(id)
      .then((result) => {
        console.log(RelationService.getById(id));
        if (result.success) {
          this.setState({
            showModal: true,
            command: Ecommand.update,
            customerRelation: result.result,
            oldName: result.result.name, // store the old name
          });
          this.loadPasien();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };
  changeStatusCommand = async (id: number) => {
    await RelationService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            command: Ecommand.ChangeStatus,
            customerRelation: result.result,
          });
          this.loadPasien();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  changeRowsPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadPasien();
      }, 500);
    });
  };
  changePage = (newPage: number) => {
    if (newPage >= 1 && newPage <= this.state.pagination.pages) {
      this.setState(
        {
          pagination: {
            ...this.state.pagination,
            pageNum: newPage,
          },
        },
        () => this.loadPasien()
      );
    }
  };
  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  //menggunakan promise
  submitHandler = async (event: any) => {
    event.preventDefault();

    this.setState({
      errorAlerts: {
        Name: {
          valid: this.state.customerRelation.name.trim().length > 0,
          message: "name tidak boleh kosong",
        },
      },
    });
    if (this.state.customerRelation.name.trim().length === 0) {
      return;
    }

    console.log(
      "Lolos ",
      this.state.errorAlerts,
      this.state.customerRelation.name.length
    );
    const { command } = this.state;
    if (command === Ecommand.create) {
      await RelationService.post(this.state.customerRelation)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              customerRelation: {
                id: 0,
                name: "",
                is_delete: false,
              },
              showSuccessModal: true,
              deleted: false,
            });
            this.loadPasien();
          } else {
            console.log(ValidationResult.Validate(result.result));
            this.setState({
              errorAlerts: ValidationResult.Validate(result.result),
            });
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (command === Ecommand.update) {
      await RelationService.update(
        this.state.customerRelation.id,
        this.state.customerRelation
      )
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              customerRelation: {
                id: 0,
                name: "",
                is_delete: false,
                // showConfirm:true,
              },
              newName: this.state.customerRelation.name, // set the new name
              showEditSuccessModal: true, // Set the new property to true
            });
            this.loadPasien();
          } else {
            console.log(ValidationResult.Validate(result.result));
            this.setState({
              errorAlerts: ValidationResult.Validate(result.result),
            });
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    }
  };
  render() {
    const {
      customerRelations,
      showModal,
      customerRelation,
      command,
      pagination,
      showSuccessModal,
      showDeleteModal,
      showConfirm,
      deleted,
      errorAlerts,
      showEditSuccessModal,
    } = this.state;
    const loopPages = () => {
      const { pagination } = this.state;
      const { pageNum, pages } = pagination;
      return (
        <div className="flex items-center">
          <button
            className="mx-1 p-1 bg-gray-200 text-gray-800 rounded-md"
            onClick={() => this.changePage(pageNum - 1)}
            disabled={pageNum === 1}
          >
            Prev
          </button>
          <span className="mx-2 text-gray-800">
            Page {pageNum} of {pages}
          </span>
          <button
            className="mx-1 p-1 bg-gray-200 text-gray-800 rounded-md"
            onClick={() => this.changePage(pageNum + 1)}
            disabled={pageNum === pages}
          >
            Next
          </button>
        </div>
      );
    };
    return (
      <div>
        <div className="text-left text-4xl pt-5 py-5">Customer Relasi</div>
        {/* <span>{JSON.stringify(customerRelations)}</span> */}
        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 
          bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.createCommand()}
          >
            Create New
          </button>
        </div>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Search:
              </th>
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  type="text"
                  id="search"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  value={pagination.search}
                  onChange={this.changeSearch("search")}
                />
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadPasien()}
                >
                  Filter
                </button>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadReset()}
                >
                  Reset Filter
                </button>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                {/* grow flex-none  */}
                Nama
              </th>
            </tr>
          </thead>
          <tbody>
            {customerRelations?.map((cat: CustomerRelationModel) => {
              return (
                <tr
                  key={cat.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td className="px-6 py-4 justify-start">{cat.name}</td>
                  <td></td>
                  <td></td>
                  <td className="px-6 py-4 flex justify-end">
                    <button
                      className="h-10 px-5 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                      onClick={() => this.updateCommand(cat.id)}
                    >
                      Edit
                    </button>
                    <button
                      className="h-10 px-5 text-red-100 transition-colors duration-150 bg-red-700 rounded-r-lg focus:shadow-outline hover:bg-red-800"
                      onClick={() => this.onDeleteClick(cat.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Rows per page
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="categoryId"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("rows")}
                >
                  {config.rowsPerPage.map((o: number) => (
                    <option value={o} key={o}>
                      {o}
                    </option>
                  ))}
                </select>
              </th>
              <th> </th>
              <th scope="col" colSpan={2} className="px-6 py-3 w-14 h-14">
                {loopPages()}
              </th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="relative w-auto my-6 mx-auto max-w-3xl ">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {command.valueOf()}
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <Form
                    customerRelation={customerRelation}
                    errorAlerts={errorAlerts}
                    command={command}
                    onChange={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                  />
                </div>
                <div
                  className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                  role="group"
                  aria-label="Button group"
                >
                  <button
                    className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="my-8 justify-start h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                    onClick={(event: React.SyntheticEvent) =>
                      this.submitHandler(event)
                    }
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showSuccessModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        Success
                      </h3>
                      <button onClick={() => this.setShowSuccessModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      {deleted
                        ? "Tindakan berhasil dihapus"
                        : "Tindakan berhasil ditambahkan"}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showDeleteModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        hapus
                      </h3>
                      <button onClick={() => this.setShowDeleteModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      Anda setuju untuk menghapus {customerRelation.name} ?
                    </h3>
                    <div
                      className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                        onClick={() => this.setShowDeleteModal(false)}
                      >
                        Batal
                      </button>
                      <button
                        onClick={() => this.delete(customerRelation.id)}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                      >
                        Hapus
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showConfirm ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        Edit
                      </h3>
                      <button onClick={() => this.setShowModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      Anda setuju untuk edit {customerRelation.name} ?
                    </h3>
                    <div
                      className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                        onClick={() => this.setShowModal(false)}
                      >
                        Batal
                      </button>
                      <button
                        onClick={(event: React.SyntheticEvent) =>
                          this.submitHandler(event)
                        }
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                      >
                        Ganti
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showEditSuccessModal ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                  <div></div>
                  <h3 className="text-black text-3xl font-bold dark:text-white">
                    Edit Success
                  </h3>
                  <button
                    onClick={() =>
                      this.setState({ showEditSuccessModal: false })
                    }
                  >
                    <AiOutlineClose className="text-black text-2xl m-2" />
                  </button>
                </div>
                <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                  Congratulations! You have successfully edited from{" "}
                  {this.state.oldName} to {this.state.newName}.
                </h3>
                <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                  <button
                    className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                    onClick={() =>
                      this.setState({ showEditSuccessModal: false })
                    }
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
