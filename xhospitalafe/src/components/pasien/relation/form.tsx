import React from "react";

import { Ecommand } from "../../enums/eCommand";
import { CustomerRelationModel } from "../../models/RelationModel";
import { config } from "../../configurations/config";

interface IProps {
  errorAlerts: any;

  onChange: any;
  command: Ecommand;
  checkBoxHandler: any;
  customerRelation: CustomerRelationModel;

}

interface IState {
  // customerRelation: CustomerRelationModel;
}

export default class Form extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      customerRelation: new CustomerRelationModel(),
    };
  }

  render() {
    const { customerRelation, onChange, checkBoxHandler, command, errorAlerts } =
      this.props;
    return (
      <>
        <form>
          {/* <span>{JSON.stringify(this.state.customerRelation)}</span> */}

          <div className="mb-6">
            {/* <span>{JSON.stringify()}</span> */}
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Name
            </label>
            <input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={onChange("name")}
              value={customerRelation.name}
            />
            {!errorAlerts.Name?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
          </div>
        </form>
      </>
    );
  }
}
