import React from "react";
import { TreatmentModel } from "../../models/treatmentModel";
import { AiOutlineClose, AiOutlinePlus } from "react-icons/ai";
import FormTreatment from "./formTindakan";
import { TreatmentService } from "../../services/profilDoctor/treatmentService";
import { ValidationResult } from "../../../validations/validationResult";

interface IProps {
  tindakan: TreatmentModel[];
  doctorId: number;
  onTreatmentChange: () => void;
}
interface IState {
  tindakanState: TreatmentModel;
  showModal: boolean;
  errorAlerts: any;
  showSuccessModal: boolean;
  showDeleteModal: boolean;
  deleted: boolean;
}

export default class TindakanTab extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tindakanState: new TreatmentModel(),
      showModal: false,
      errorAlerts: { name: false },
      showSuccessModal: false,
      showDeleteModal: false,
      deleted: false,
    };
  }
  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
  };

  setShowSuccessModal = (val: boolean) => {
    this.setState({
      showSuccessModal: val,
    });
  };

  setShowDeleteModal = (val: boolean) => {
    this.setState({
      showDeleteModal: val,
    });
  };

  handleInputData = (name: any) => (event: any) => {
    this.setState({
      tindakanState: {
        ...this.state.tindakanState,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  onClickButton = () => {
    const { tindakanState } = this.state;
    const { doctorId } = this.props;
    this.setState({
      showModal: true,
    });
    tindakanState.doctorId = doctorId;
  };

  handleSubmit = async (event: any) => {
    event.preventDefault();
    this.setState({
      errorAlerts: {
        Name: {
          valid: this.state.tindakanState.name.trim().length > 0,
          message: "Nama tidak boleh kosong",
        },
      },
    });
    if (this.state.tindakanState.name.trim().length === 0) {
      return;
    }

    console.log(
      "Lolos ",
      this.state.errorAlerts,
      this.state.tindakanState.name.length
    );

    const { tindakanState } = this.state;
    await TreatmentService.post(tindakanState)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: false,
            tindakanState: new TreatmentModel(),
            showSuccessModal: true,
            deleted: false,
          });
          this.props.onTreatmentChange();
        } else {
          // alert("error");
          this.setState({
            errorAlerts: ValidationResult.Validate(result.result),
          });
        }
      })
      .catch((error) => {
        alert("error: " + error);
      });
  };

  onDeleteClick = (id: number) => {
    TreatmentService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            tindakanState: result.result,
            showDeleteModal: true,
          });
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  delete = (id: number) => {
    TreatmentService.delete(id, true)
      .then((result) => {
        if (result.success) {
          this.props.onTreatmentChange();
          this.setState({
            showDeleteModal: false,
            showSuccessModal: true,
            deleted: true,
          });
        }
        console.log("error: " + result);
      })
      .catch((error) => {
        alert("error: " + error);
        console.log("error");
      });
  };

  render() {
    const { tindakan } = this.props;
    const {
      showModal,
      tindakanState,
      errorAlerts,
      showSuccessModal,
      showDeleteModal,
      deleted,
    } = this.state;
    return (
      <div className="ms-5 flex p-5 justify-between shadow-xl rounded-3xl h-screen bg-slate-200 ">
        <div className="flex-nowrap flex-shrink-0">
          {tindakan.map((o: TreatmentModel) => (
            <div
              key={o.id}
              className="m-2 flex w-max items-center bg-blue-300 rounded-full p-1 text-blue-950"
            >
              <div className="ms-1 flex">{o.name}</div>
              <AiOutlineClose
                onClick={() => this.onDeleteClick(o.id)}
                className="m-1 cursor-pointer hover:text-red-600"
              />
            </div>
          ))}
        </div>

        <div className="flex w-max md:text-1xl sm:text-1xl text-1xl font-bold px-2 h-10 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-green-700 hover:bg-green-900">
          <button
            onClick={() => this.onClickButton()}
            style={{ whiteSpace: "nowrap" }}
            className="p-2 text-white"
          >
            Tambahkan Tindakan
          </button>
          <AiOutlinePlus className="m-3 fill-white h-30 w-30" />
        </div>
        {showModal ? (
          <div>
            <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
              <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                  <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                    <div></div>
                    <h3 className="text-black text-3xl font-bold dark:text-white">
                      Tambah Tindakan
                    </h3>
                    <button onClick={() => this.setShowModal(false)}>
                      <AiOutlineClose className=" text-black text-3xl" />
                    </button>
                  </div>
                  <div className="relative p-6 flex-auto">
                    <FormTreatment
                      errorAlerts={errorAlerts}
                      onChange={this.handleInputData}
                    />
                  </div>
                  <div
                    className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                    role="group"
                    aria-label="Button group"
                  >
                    <button
                      className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                      onClick={() => this.setShowModal(false)}
                    >
                      Batal
                    </button>
                    <button
                      onClick={(event: React.SyntheticEvent) =>
                        this.handleSubmit(event)
                      }
                      className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                    >
                      Tambah
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showSuccessModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        Success
                      </h3>
                      <button onClick={() => this.setShowSuccessModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      {deleted
                        ? "Tindakan berhasil dihapus"
                        : "Tindakan berhasil ditambahkan"}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showDeleteModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        hapus tindakan
                      </h3>
                      <button onClick={() => this.setShowDeleteModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      Anda setuju untuk menghapus {tindakanState.name} ?
                    </h3>
                    <div
                      className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                        onClick={() => this.setShowDeleteModal(false)}
                      >
                        Batal
                      </button>
                      <button
                        onClick={() => this.delete(tindakanState.id)}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                      >
                        Hapus
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
