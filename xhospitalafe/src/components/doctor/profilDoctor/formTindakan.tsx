import React from "react";
import { SearchService } from "../../services/searchService/searchService";
import { SpecializationModel } from "../../models/specializationModel";
import { TreatmentModel } from "../../models/treatmentModel";
interface IProps {
    onChange: any;
  errorAlerts: any;
}

interface IState {
  treatment: TreatmentModel;
}

export default class FormTreatment extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      treatment: new TreatmentModel(),
    };
  }

  render() {
    const { treatment } = this.state;
    const { onChange, errorAlerts } = this.props;
    return (
      <>
        <form>
          <div className="mb-6 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Nama Tindakan
            </label>
            <input
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={onChange("name")}
            />
            {!errorAlerts.Name?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
          </div>
        </form>
      </>
    );
  }
}
