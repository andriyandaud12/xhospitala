import React from "react";
import { SearchService } from "../../services/searchService/searchService";
import { SpecializationModel } from "../../models/specializationModel";
import { CurentSpecializationModel } from "../../models/profil/currentSpecializationModel";
interface IProps {
  onChange: any;
  errorAlerts: any;
  currentSpec: CurentSpecializationModel;
}

interface IState {
  specializations: SpecializationModel[];
}

export default class FormSpecialization extends React.Component<
  IProps,
  IState
> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      specializations: [],
    };
  }
  componentDidMount(): void {
    this.loadForm();
  }

  loadForm = async () => {
    SearchService.getAllSpecialization()
      .then((result) => {
        if (result.success) {
          this.setState({
            specializations: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  render() {
    const { specializations } = this.state;
    const { onChange, errorAlerts, currentSpec } = this.props;
    return (
      <>
        <form>
          <div className="mb-6 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Spesialisasi
            </label>
            <select
              id="countries"
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={onChange("specializationId")}
            >
              {}
              <option>-- Pilih --</option>
              {specializations.map((o: SpecializationModel) => {
                return (
                  <option
                    selected={currentSpec.specializationId === o.id}
                    value={o.id}
                    className="text-black"
                    key={o.name}
                  >
                    {o.name}
                  </option>
                );
              })}
            </select>
            {!errorAlerts.Name?.valid || currentSpec.specializationId === 0 ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
          </div>
        </form>
      </>
    );
  }
}
