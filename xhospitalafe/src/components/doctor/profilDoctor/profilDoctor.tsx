import React, { useState } from "react";
import { SearchResultModel } from "../../models/searchDokter/searchResultModel";
import { DetailService } from "../../services/detailService/detailService";
import { EducationModel } from "../../models/searchDokter/educationModel";
import { TreatmentModel } from "../../models/treatmentModel";
import { HistoryOfPracticeModel } from "../../models/searchDokter/historyOfPracticeModel";
import { AiFillCamera, AiFillStar, AiOutlineClose } from "react-icons/ai";
import { ProfilDoctorService } from "../../services/profilDoctor/profilDoctorService";
import { BiodataModel } from "../../models/biodataModel";
import FormChangeImage from "../formChangeImage";
import SpecializationTab from "./specialisasiTab";
import TindakanTab from "./tindakanTab";
import ActifitasTab from "./aktifitasTab";
import KonsultasiTab from "./konsultasiTab";
import PengaturanTab from "./pengaturanTab";
import { AppointmentModel } from "../../models/appointmentModel";
import { CustomerChatModel } from "../../models/profil/customerChat";

interface IProps {
  onProfileImageChange: (newImageUrl: string) => void;
}
interface IState {
  resultStates: SearchResultModel;
  biodata: BiodataModel;
  showSelectorImageModal: boolean;
  activeTab: string;
  showModalImageError: boolean;
  appointment: AppointmentModel[];
  customerChat: CustomerChatModel[];
  errorUploadImage: boolean;
  showModalSucces: boolean;
  loading: boolean;
}

export default class ProfilDoctor extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      resultStates: new SearchResultModel(),
      biodata: new BiodataModel(),
      showSelectorImageModal: false,
      activeTab: "spesialisasi",
      showModalImageError: false,
      appointment: [],
      customerChat: [],
      errorUploadImage: false,
      showModalSucces: false,
      loading: true,
    };
  }

  componentDidMount(): void {
    this.loadProfil();
    this.loadCountJanji();
    this.loadCountCustomerChat();
  }

  handleTabChange = (tab: string) => {
    this.setState({
      activeTab: tab,
    });
  };

  setShowErrorModal = (val: boolean) => {
    this.setState({
      showModalImageError: val,
    });
  };

  setShowSuccessModal = (val: boolean) => {
    this.setState({
      showModalSucces: val,
    });
  };
  loadCountJanji = () => {
    ProfilDoctorService.getAppointmentByDoctorId()
      .then((result) => {
        if (result.success) {
          this.setState({
            appointment: result.result,
            loading: false,
          });
        }
      })
      .catch((error) => {
        alert("error" + error);
      });
  };

  loadCountCustomerChat = () => {
    ProfilDoctorService.getAllCustomerChatByDoctorId()
      .then((result) => {
        if (result.success) {
          this.setState({
            customerChat: result.result,
          });
        }
      })
      .catch((error) => {
        alert("error" + error);
      });
  };

  loadProfil = () => {
    ProfilDoctorService.getDoctorByIdForProfil()
      .then((result) => {
        if (result.success) {
          this.setState({
            resultStates: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  photoUpload = (event: any) => {
    const file = event.target.files[0];
    const accepted = ["image/jpg", "image/png", "image/jpeg"];
    if (accepted.includes(file?.type)) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.resizeImage(reader.result, 512, 512)
          .then((result) => {
            this.setState({
              biodata: {
                ...this.state.biodata,
                image: result,
              },
              errorUploadImage: false,
            });
            this.resizeImage(reader.result, 128, 128)
              .then((resultSmall) => {
                this.setState({
                  biodata: {
                    ...this.state.biodata,
                    image: resultSmall,
                  },
                });
                console.log(result, resultSmall);
              })
              .catch((error) => {
                alert(error);
              });
          })
          .catch((error) => {
            alert(error);
          });
      };

      reader.onerror = (error) => {
        console.log("Error: ", error);
      };
    } else {
      this.setState({
        showModalImageError: true,
        errorUploadImage: true,
      });
    }
  };

  resizeImage = (
    base64Str: any,
    maxWidth: number = 512,
    maxHeight: number = 512
  ) => {
    return new Promise((resolve) => {
      let img = new Image();
      img.src = base64Str;
      img.onload = () => {
        let canvas = document.createElement("canvas");
        const MAX_WIDTH = maxWidth;
        const MAX_HEIGHT = maxHeight;
        let width = img.width;
        let height = img.height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;
        let ctx: any = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL());
      };
    });
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showSelectorImageModal: val,
    });
  };

  onClickImage = async (id: number) => {
    await ProfilDoctorService.getBiodataById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showSelectorImageModal: true,
            biodata: result.result,
          });
          this.loadProfil();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  submitHandlerForm = async () => {
    const { biodata } = this.state;
    await ProfilDoctorService.updateImage(biodata.id, biodata)
      .then((result) => {
        if (result.success) {
          this.setState({
            showSelectorImageModal: false,
            biodata: result.result,
            showModalSucces: true,
          });
          this.loadProfil();
          this.props.onProfileImageChange(result.result.image);
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  render() {
    const {
      resultStates,
      activeTab,
      biodata,
      showSelectorImageModal,
      showModalImageError,
      errorUploadImage,
      appointment,
      showModalSucces,
      customerChat,
      loading,
    } = this.state;
    let activeTabContent = null;

    switch (activeTab) {
      case "spesialisasi":
        activeTabContent = (
          <SpecializationTab
            doctorId={resultStates.doctorId}
            currentSpecId={resultStates.currentSpecializationId}
            spesialisasi={resultStates.specialization}
            onSpesialisasiChange={this.loadProfil}
          />
        );
        break;
      case "tindakan":
        activeTabContent = (
          <TindakanTab
            onTreatmentChange={this.loadProfil}
            doctorId={resultStates.doctorId}
            tindakan={resultStates.treatment}
          />
        );
        break;
      case "aktifitas":
        activeTabContent = <ActifitasTab />;
        break;
      case "konsultasi":
        activeTabContent = <KonsultasiTab />;
        break;
      case "pengaturan":
        activeTabContent = <PengaturanTab />;
        break;
      // Add cases for other tabs

      default:
        break;
    }
    return (
      <div className="flex overflow-auto">
        {loading ? (
          <div
            className="h-screen w-screen"
            style={{
              border: "4px solid rgba(0, 0, 0, 0.1)",
              borderLeft: "4px solid #3498db",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              animation: "spin 1s linear infinite",
              margin: "20px auto",
            }}
          ></div>
        ) : (
          <div className="flex overflow-auto">
            <div>
              {/* side bar */}
              <div className="m-4 overflow-auto">
                <div key={resultStates.doctorId} className="flex-shrink-0 mx-2">
                  {/* header dokter */}
                  <div className="shadow-xl rounded-3xl bg-slate-200 w-200">
                    <div className="p-4">
                      <div className="text-center">
                        <div
                          onClick={() =>
                            this.onClickImage(resultStates.biodataId)
                          }
                          className="flex items-center justify-center flex-shrink-0 "
                        >
                          <div className="relative">
                            <img
                              src={resultStates.image}
                              alt={`${resultStates.fullName}'s profile`}
                              className="w-32 h-32 object-cover rounded-full border border-blue-700 p-1 mb-2"
                            />
                            <div className="absolute top-0 left-0 w-full h-full flex items-center justify-center opacity-0 transition-opacity duration-300 hover:opacity-100">
                              <AiFillCamera className="h-9 w-9" />
                            </div>
                          </div>
                        </div>
                        <h3 className="text-lg justify-center font-bold flex-nowrap text-blue-800 mb-2">
                          {resultStates.fullName}
                        </h3>
                        <h4 className="text-sm text-gray-600  mb-2">
                          {resultStates.specialization === null
                            ? "Anda belum menambahkan spesialisasi"
                            : `Spesialis ${resultStates.specialization}`}
                        </h4>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            marginBottom: 6,
                          }}
                        >
                          {[...Array(5)].map((_, index) => (
                            <AiFillStar style={{ color: "darkgoldenrod" }} />
                          ))}
                        </div>
                      </div>
                      <div className="p-3 border flex border-t-slate-400 justify-between">
                        <p className="text-base font-bold text-blue-800">
                          Janji
                        </p>
                        <p className="rounded-full w-6 h-6 text-center font-bold text-blue-800 bg-blue-300">
                          {appointment === null ? "0" : appointment.length}
                        </p>
                      </div>
                      <div className="p-3 border flex border-t-slate-400 justify-between">
                        <p className="text-base font-bold text-blue-800">
                          Obrolan/Konsultasi
                        </p>
                        <p className="rounded-full w-6 h-6 text-center font-bold text-blue-800 bg-blue-300">
                          {customerChat === null ? "0" : customerChat.length}
                        </p>
                      </div>
                    </div>
                  </div>
                  {/* tentang saya */}
                  <div className="shadow-xl rounded-3xl pb-4">
                    <div className="mt-7 p-2 bg-slate-200 rounded-tr-3xl rounded-tl-3xl">
                      <h5 className="p-1 text-center font-bold text-blue-800">
                        Tentang Saya
                      </h5>
                    </div>
                    <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                      <p className="p-2 ms-2 font-bold text-left  text-blue-800">
                        Tindakan Medis
                      </p>
                      <div className="mx-6">
                        {resultStates.treatment.map((o: TreatmentModel) => {
                          return (
                            <p className="font-normal text-left  text-gray-500">
                              - {o.name}
                            </p>
                          );
                        })}
                      </div>
                    </div>
                    <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                      <p className="font-bold p-2 ms-2 text-left  text-blue-800">
                        Riwayat Praktek
                      </p>
                      <div className="mx-6">
                        {resultStates.historyOfPractice.map(
                          (o: HistoryOfPracticeModel) => {
                            return (
                              <div>
                                <p className="font-normal text-left  text-gray-500">
                                  - {o.medicalFacilityName}
                                </p>
                                <div className="flex justify-between">
                                  <p className="ms-8 font-light text-left text-sm  text-gray-500">
                                    {o.officeSpecialization}
                                  </p>
                                  <p className="ms-8 font-light text-left text-sm  text-gray-500">
                                    {o.startDate.split("-")[0]} -{" "}
                                    {o.endDate.split("-")[0] ===
                                    new Date().getFullYear().toString()
                                      ? "Sekarang"
                                      : o.endDate.split("-")[0]}
                                  </p>
                                </div>
                              </div>
                            );
                          }
                        )}
                      </div>
                    </div>

                    <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                      <p className="font-bold p-2 ms-2 text-left  text-blue-800">
                        Pendidikan
                      </p>
                      <div className="mx-6">
                        {resultStates.education.map((o: EducationModel) => {
                          return (
                            <div>
                              <p className="font-normal text-left  text-gray-500">
                                {o.institutionName}
                              </p>
                              <div className="flex justify-between">
                                <p className="ms-6 font-light text-sm text-left  text-gray-500">
                                  {o.major}
                                </p>
                                <p className="ms-6 font-light text-sm text-left  text-gray-500">
                                  {o.endYear}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Tab menu */}
            <div>
              <div className="flex flex-row m-4 rounded-3xl max-w-screen">
                {[
                  "spesialisasi",
                  "tindakan",
                  "aktifitas",
                  "konsultasi",
                  "pengaturan",
                ].map((tab) => (
                  <div
                    key={tab}
                    onClick={() => this.handleTabChange(tab)}
                    className={`m-4 shadow-lg p-1 w-40 h-10 ${
                      activeTab === tab ? "bg-blue-500" : "bg-slate-200"
                    } rounded-full cursor-pointer`}
                  >
                    <h5
                      className={`p-1 text-center font-bold ${
                        activeTab === tab ? "text-white" : "text-blue-800"
                      }`}
                    >
                      {tab.charAt(0).toUpperCase() + tab.slice(1)}
                    </h5>
                  </div>
                ))}
              </div>
              <div className="flex-grow p-4">{activeTabContent}</div>
            </div>

            {showSelectorImageModal ? (
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black ms-8 md:text-3xl sm:text-4xl text-3xl font-bold md:py-6text-3xl dark:text-white">
                        Ganti Gambar
                      </h3>
                      <button onClick={() => this.setShowModal(false)}>
                        <AiOutlineClose className="cursor-pointer text-black text-3xl" />
                      </button>
                    </div>
                    <div className="relative p-6 flex-auto">
                      <FormChangeImage
                        photoUpload={this.photoUpload}
                        biodata={biodata}
                      />
                    </div>
                    <div
                      className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        disabled={errorUploadImage}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                        onClick={() => this.submitHandlerForm()}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
            {showModalImageError ? (
              <div>
                <div>
                  <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                    <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                      <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                        <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                          <div></div>
                          <h3 className="text-black text-3xl font-bold dark:text-white">
                            Error
                          </h3>
                          <button onClick={() => this.setShowErrorModal(false)}>
                            <AiOutlineClose className=" text-black text-2xl m-2" />
                          </button>
                        </div>
                        <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                          File type Tidak diizinkan
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
            {showModalSucces ? (
              <div>
                <div>
                  <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                    <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                      <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                        <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                          <div></div>
                          <h3 className="text-black text-3xl font-bold dark:text-white">
                            Success
                          </h3>
                          <button
                            onClick={() => this.setShowSuccessModal(false)}
                          >
                            <AiOutlineClose className=" text-black text-2xl m-2" />
                          </button>
                        </div>
                        <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                          Berhasil Mengubah Foto Profil
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : null}
          </div>
        )}
      </div>
    );
  }
}
