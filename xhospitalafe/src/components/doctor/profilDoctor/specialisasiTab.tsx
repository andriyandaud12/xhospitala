import React from "react";
import {
  AiFillPlusCircle,
  AiOutlineClose,
  AiOutlinePlus,
} from "react-icons/ai";
import { BsFilePlus, BsPlusCircleFill } from "react-icons/bs";
import { SpecializationModel } from "../../models/specializationModel";
import { SearchService } from "../../services/searchService/searchService";
import { ProfilDoctorService } from "../../services/profilDoctor/profilDoctorService";
import FormSpecialization from "./formSpecialization";
import { CurentSpecializationModel } from "../../models/profil/currentSpecializationModel";
import { CurentSpecializationService } from "../../services/profilDoctor/currentSpecializationService";
import { FaPencil } from "react-icons/fa6";
interface IProps {
  currentSpecId: number;
  doctorId: number;
  spesialisasi: string;
  onSpesialisasiChange: () => void;
}
interface IState {
  showModal: boolean;
  currentSpec: CurentSpecializationModel;
  showSuccessModal: boolean;
  errorAlerts: any;
  updated: boolean;
}

export default class SpecializationTab extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showModal: false,
      currentSpec: new CurentSpecializationModel(),
      showSuccessModal: false,
      updated: false,
      errorAlerts: { Name: false },
    };
  }
  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
  };

  setShowSuccessModal = (val: boolean) => {
    this.setState({
      showSuccessModal: val,
    });
  };

  handleInputData = (name: any) => (event: any) => {
    this.setState({
      currentSpec: {
        ...this.state.currentSpec,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  getCurrentSpec = () => {
    const { currentSpec } = this.state;
    const { spesialisasi } = this.props;
    if (spesialisasi === null) {
      this.setState({
        showModal: true,
      });
      currentSpec.doctorId = this.props.doctorId;
    } else {
      CurentSpecializationService.getCurrentById(this.props.currentSpecId)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: true,
              currentSpec: result.result,
            });
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  };

  submitHandlerForm = async (event: any) => {
    event.preventDefault();
    this.setState({
      errorAlerts: {
        Name: {
          valid: this.state.currentSpec.specializationId > 0,
          message: "Spesialisasi tidak boleh kosong",
        },
      },
    });
    if (this.state.currentSpec.specializationId === 0) {
      return;
    }

    console.log(
      "Lolos ",
      this.state.errorAlerts,
      this.state.currentSpec.specializationId
    );
    const { currentSpec } = this.state;
    const { spesialisasi } = this.props;
    if (spesialisasi === null) {
      await CurentSpecializationService.post(currentSpec)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              showSuccessModal: true,
              updated: false,
            });
            this.props.onSpesialisasiChange();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else {
      await CurentSpecializationService.update(currentSpec.id, currentSpec)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              currentSpec: result.result,
              showSuccessModal: true,
              updated: true,
            });
            this.props.onSpesialisasiChange();
            console.log(result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    }
  };

  render() {
    const { spesialisasi } = this.props;
    const { showModal, showSuccessModal, updated, errorAlerts, currentSpec } =
      this.state;
    return (
      <div className="ms-5 flex p-5 justify-between shadow-xl rounded-3xl h-screen bg-slate-200 ">
        <p className="me-5">
          {spesialisasi === null ? (
            "Anda belum menambahkan spesialisasi"
          ) : (
            <div>
              <div className="m-2 flex w-max items-center px-3 bg-blue-300 rounded-full p-1 text-blue-950">
                <div className="ms-1 flex">{`Spesialis ${spesialisasi}`}</div>
              </div>
            </div>
          )}
        </p>
        <div className=" flex md:text-1xl sm:text-1xl text-1xl font-bold px-2 h-10 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-green-700 hover:bg-green-900">
          <button
            onClick={() => this.getCurrentSpec()}
            className="p-2 text-white"
          >
            {spesialisasi === null
              ? "Tambahkan spesialisasi"
              : "Ubah Spesialisasi"}
          </button>

          {spesialisasi === null ? (
            <AiOutlinePlus className="m-3 fill-white h-30 w-30" />
          ) : (
            <FaPencil className="m-3 fill-white h-30 w-30" />
          )}
        </div>
        {showModal ? (
          <div>
            <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
              <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                  <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                    <div></div>
                    <h3 className="text-black text-3xl font-bold dark:text-white">
                      {spesialisasi === null
                        ? "Tambah spesialisasi"
                        : "Ubah spesialisasi"}
                    </h3>
                    <button
                      className=""
                      onClick={() => this.setShowModal(false)}
                    >
                      <AiOutlineClose className=" text-black text-3xl" />
                    </button>
                  </div>
                  <div className="relative p-6 flex-auto">
                    <FormSpecialization
                      currentSpec={currentSpec}
                      errorAlerts={errorAlerts}
                      onChange={this.handleInputData}
                    />
                  </div>
                  <div
                    className="flex items-center justify-between p-6 border-t border-solid border-blueGray-200 rounded-b"
                    role="group"
                    aria-label="Button group"
                  >
                    <button
                      className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-green-900"
                      onClick={() => this.setShowModal(false)}
                    >
                      Batal
                    </button>
                    <button
                      onClick={this.submitHandlerForm}
                      className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                    >
                      Simpan
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showSuccessModal ? (
          <div>
            <div>
              <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-80 ">
                <div className="relative w-auto my-6 mx-auto max-w-3xl ">
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                    <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                      <div></div>
                      <h3 className="text-black text-3xl font-bold dark:text-white">
                        Success
                      </h3>
                      <button onClick={() => this.setShowSuccessModal(false)}>
                        <AiOutlineClose className=" text-black text-2xl m-2" />
                      </button>
                    </div>
                    <h3 className="mx-10 text-sm p-5 items-start flex justify-center font-normal text-gray-500">
                      {updated
                        ? "Spesialisasi berhasil diubah"
                        : "Spesialisasi berhasil ditambahkan"}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
