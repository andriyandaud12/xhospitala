import React from "react";
import { LocationModel } from "../models/locationModel";
import { SpecializationModel } from "../models/specializationModel";
import { TreatmentModel } from "../models/treatmentModel";
import { SearchService } from "../services/searchService/searchService";
import { SearchDataModel } from "../models/searchDokter/searchData";
interface IProps {
  onSearch: any;
  searchResultData: SearchDataModel;
  errorAlerts: any;
  treatmentProps: TreatmentModel[];
}

interface IState {
  locations: LocationModel[];
  specializations: SpecializationModel[];
  treatments: TreatmentModel[];
}

export default class FormSearch extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      locations: [],
      specializations: [],
      treatments: [],
    };
  }
  componentDidMount(): void {
    this.loadSearch();
  }

  loadSearch = async () => {
    const { searchResultData } = this.props;
    SearchService.getAllLoaction()
      .then((result) => {
        if (result.success) {
          this.setState({
            locations: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
    SearchService.getAllSpecialization()
      .then((result) => {
        if (result.success) {
          this.setState({
            specializations: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
    // SearchService.getAllTreatment(searchResultData.specialization)
    //   .then((result) => {
    //     if (result.success) {
    //       this.setState({
    //         treatments: result.result,
    //       });
    //     }
    //   })
    //   .catch((error) => {
    //     alert(error);
    //   });
  };

  render() {
    const { locations, specializations, treatments } = this.state;
    const { onSearch, searchResultData, errorAlerts, treatmentProps } =
      this.props;
    return (
      <>
        <form>
          <div className="mb-2 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Lokasi
            </label>
            <select
              id="countries"
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={searchResultData.location}
              onChange={onSearch("location")}
            >
              <option selected value="">
                --- Pilih Lokasi ---
              </option>
              {locations.map((o: LocationModel) => {
                const optionValue = `${o.name}, ${o?.location?.locationLevel?.abbreviation} ${o?.location?.name}`;
                return (
                  <option
                    value={optionValue}
                    className="text-black"
                    key={o.name}
                  >
                    {o.name}, {o?.location?.locationLevel?.abbreviation}{" "}
                    {o?.location?.name}
                  </option>
                );
              })}
            </select>
          </div>

          <div className="mb-2 ">
            <label className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Nama Dokter
            </label>
            <input
              type="text"
              id="initial"
              value={searchResultData.doctorName}
              className="bg-gray-50 border border-blue-800 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              onChange={onSearch("doctorName")}
            />
          </div>

          <div className="mb-2">
            <label className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Spesialisasi/Sub-Spesialisasi
            </label>
            <select
              value={searchResultData.specialization}
              className={
                errorAlerts.Name?.valid &&
                searchResultData.specialization === ""
                  ? `bg-gray-50 border border-red-600 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500`
                  : `bg-gray-50 border border-blue-800 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500`
              }
              onChange={onSearch("specialization")}
            >
              <option selected value="">
                -- Pilih Spesialisasi--
              </option>
              {specializations.map((o: SpecializationModel) => {
                return <option value={o.name}>{o.name}</option>;
              })}
            </select>
            {!errorAlerts.Name?.valid ||
            searchResultData.specialization === "" ||
            searchResultData.specialization === "0" ? (
              <strong
                className="text-red-600 text-xs text-left font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
          </div>
          <div className="mb-2">
            <label className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Tindakan Medis
            </label>
            <select
              value={searchResultData.treatment}
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
              onChange={onSearch("treatment")}
            >
              <option selected value="">
                -- Pilih Tindakan --
              </option>
              {treatmentProps?.map((o: TreatmentModel) => {
                return <option value={o?.name}>{o?.name}</option>;
              })}
            </select>
          </div>
        </form>
      </>
    );
  }
}
