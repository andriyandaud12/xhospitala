import React from "react";
import { config } from "../configurations/config";

import { PaginationModel } from "../models/paginationModel";
import { Ecommand } from "../enums/eCommand";
import { DoctorModel } from "../models/doctorModel";
import { BiodataModel } from "../models/biodataModel";
import { DoctorService } from "../services/doctorService";

interface IProps {}

interface IState {
  doctors: DoctorModel[];
  doctor: DoctorModel;
  showModal: boolean;
  command: Ecommand;
  pagination: PaginationModel;
}

export default class Doctor extends React.Component<IProps, IState> {
  newPagination: PaginationModel = {
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };
  newBiodata: BiodataModel = {
    id: 0,
    fullName: "",
    mobilePhone: "",
    image: "",
    imagePath: "",
    is_delete: false,
  };
  newDoctor: DoctorModel = {
    id: 0,
    biodata: this.newBiodata,
    str: "",
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      doctors: [],
      doctor: this.newDoctor,
      showModal: false,
      command: Ecommand.create,
      pagination: this.newPagination,
    };
  }

  componentDidMount(): void {
    this.loadDoctor();
  }

  loadDoctor = async () => {
    const { pagination } = this.state;
    const result = await DoctorService.getAll();
    if (result.success) {
      this.setState({
        doctors: result.result,
        // pagination: {
        //   ...this.state.pagination,
        //     pages: result.pages,
        // },
      });
    }
    console.log(result);
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      doctor: {
        ...this.state.doctor,
        [name]: event.target.value,
      },
    });
  };

  createCommand = () => {
    this.setState({
      showModal: true,
      doctor: this.newDoctor,
      command: Ecommand.create,
    });
    // this.setShowModal(true);
  };

  changeRowsPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadDoctor();
      }, 500);
    });
  };

  render() {
    const { doctors } = this.state;
    return (
      <div>
        <div className="text-center text-4xl pt-5 py-5">Profile Doctor</div>
        
      </div>
    );
  }
}
