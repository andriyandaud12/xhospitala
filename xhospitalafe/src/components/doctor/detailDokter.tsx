import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { SearchResultModel } from "../models/searchDokter/searchResultModel";
import { ProfilDoctorService } from "../services/profilDoctor/profilDoctorService";
import { TreatmentModel } from "../models/treatmentModel";
import { HistoryOfPracticeModel } from "../models/searchDokter/historyOfPracticeModel";
import { EducationModel } from "../models/searchDokter/educationModel";
import { GoChevronDown } from "react-icons/go";
import { GoChevronUp } from "react-icons/go";
import { DoctorOfficeTreatmentPriceModel } from "../models/doctorOfficeTreatmentPrice";
import { ScheduleModel } from "../models/searchDokter/scheduleModel";

function DetailDokter() {
  const { biodataId } = useParams();
  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };
  const [prc, setPrc] = useState<DoctorOfficeTreatmentPriceModel>({
    id: 0,
    doctorOfficeTreatmentId: 0,
    doctorOfficeTreatment: [],
    price: 0,
    priceStartFrom: 0,
    priceEndFrom: 0,
  });
  const [doc, setDoc] = useState<SearchResultModel>({
    doctorId: 0,
    biodataId: 0,
    currentSpecializationId: 0,
    fullName: "",
    specialization: "",
    pengalaman: "",
    historyOfPractice: [],
    image: "",
    treatment: [],
    education: [],
    doctorSchedule: [],
    isOnline: false,
  });
  useEffect(() => {
    ProfilDoctorService.getDoctorById(biodataId ? parseInt(biodataId) : 0)
      .then((res) => {
        setDoc(res.result);
      })
      .catch((error) => {
        setDoc({
          doctorId: 0,
          biodataId: 0,
          currentSpecializationId: 0,
          fullName: "",
          specialization: "",
          pengalaman: "",
          historyOfPractice: [],
          image: "",
          treatment: [],
          education: [],
          doctorSchedule: [],
          isOnline: false,
        });
      });

    ProfilDoctorService.getDoctorPriceById(biodataId ? parseInt(biodataId) : 0)
      .then((res) => {
        setPrc(res.result);
      })
      .catch((error) => {
        setPrc({
          id: 0,
          doctorOfficeTreatmentId: 0,
          doctorOfficeTreatment: [],
          price: 0,
          priceStartFrom: 0,
          priceEndFrom: 0,
        });
      });
    return;
  }, []);

  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  return (
    <div className="py-10 items-center max-w-full">
      {doc ? (
        <>
          <div className="w-[75%] mx-auto border-2 bg-[#FFECD6] p-4 lg:flex rounded-md shadow-md">
            <div className="lg:w-1/4">
              <div className="flex justify-start items-center">
                <img
                  className="rounded-full w-32 h-32 mx-auto lg:ml-0 object-cover"
                  src={doc.image}
                  alt=""
                />
              </div>
            </div>
            <div className="lg:w-2/4 flex flex-col items-center justify-center p-4">
              <h1 className="text-neutral-700 font-bold text-2xl text-center mb-2">
                {doc.fullName}
              </h1>
              <p className="font-normal text-base text-neutral-700 text-center mb-2">
                {doc.specialization}
              </p>
              <p className="font-normal text-base text-neutral-700 text-center">
                {doc.pengalaman} pengalaman
              </p>
            </div>
            <div className="lg:w-1/4 flex justify-center items-center py-6">
              {prc && (
                <div className="flex flex-col gap-1">
                  <button className="text-white uppercase bg-gradient-to-r hover:bg-gradient-to-l from-[#4CB9E7] to-[#3559E0] p-3 font-semibold rounded-lg w-32 h-12">
                    Chat
                  </button>
                  <p className="flex justify-center font-semibold text-blue-600">
                    Rp. {prc.price}
                  </p>
                </div>
              )}
            </div>
          </div>

          <div className="grid grid-cols-1 lg:grid-cols-2 gap-6 mx-4 lg:mx-28">
            {/* Bagian Tindakan Medis */}
            <div className="w-full mt-8 lg:mt-20">
              <p className="text-2xl font-bold text-center mb-6">
                Tindakan Medis
              </p>

              <div className="w-full border-2 border-blue-400 p-4 rounded-md shadow-md">
                {doc?.treatment?.map((treatment: TreatmentModel) => (
                  <p
                    className="text-lg font-normal ml-4 mt-2"
                    key={treatment.id}
                  >
                    <span className="bullet">&#8226;</span> {treatment.name}
                  </p>
                ))}
              </div>
            </div>

            {/* Bagian Lokasi Praktek */}
            <div className="w-full mt-8 lg:mt-20">
              <p className="text-2xl font-bold text-center mb-6">
                Lokasi Praktek
              </p>

              <div className="w-full flex flex-col border-2 border-blue-400 p-4 rounded-md shadow-md">
                {doc?.historyOfPractice?.map(
                  (practice: HistoryOfPracticeModel) => (
                    <div className="flex" key={practice.medicalFacilityId}>
                      <div className="flex flex-col w-full">
                        {practice?.endDate === null && (
                          <>
                            {prc && (
                              <div className="flex flex-row mb-4">
                                <p className="font-normal text-lg ml-4 mt-2 w-1/2">
                                  <span className="bullet">&#8226;</span>{" "}
                                  {practice.medicalFacilityName}
                                </p>
                                <div className="flex flex-col">
                                  <p className="font-normal text-lg mt-2 ml-16 text-blue-600  ">
                                    Konsultasi Mulai Dari
                                  </p>
                                  <p className="font-normal text-lg mt-2 ml-16 text-blue-600 flex justify-center">
                                    Rp. {prc.priceStartFrom}
                                  </p>
                                </div>
                              </div>
                            )}
                            {!prc && (
                              <p className="font-normal text-lg ml-4 mt-2">
                                {"- "}
                                {practice.medicalFacilityName}
                              </p>
                            )}
                            <div className="flex flex-col cursor-pointer border-2 border-blue-400 mt-4 p-4 rounded-md">
                              <div
                                onClick={handleNav}
                                className="flex text-blue-600 hover:text-blue-800 items-center cursor-pointer"
                              >
                                {nav ? (
                                  <GoChevronUp size={25} />
                                ) : (
                                  <GoChevronDown size={25} />
                                )}
                                <span className="ml-2">
                                  Lihat Jadwal Praktek
                                </span>
                              </div>

                              {nav && (
                                <div className="flex mt-4">
                                  <div className="flex flex-col p-2">
                                    <div className="grid grid-cols-2 gap-2">
                                      <div className="col-span-1">
                                        <p className="font-semibold">Hari</p>
                                      </div>
                                      <div className="col-span-1">
                                        <p className="font-semibold">Jadwal</p>
                                      </div>
                                    </div>
                                    {doc.doctorSchedule
                                      .filter(
                                        (schedule: ScheduleModel) =>
                                          practice.medicalFacilityId ===
                                          schedule.medicalFacilityId
                                      )
                                      .map((schedule: ScheduleModel) => (
                                        <div
                                          className="grid grid-cols-2 gap-2"
                                          key={schedule.medicalFacilityId}
                                        >
                                          <p>{schedule.day}</p>
                                          <p>
                                            {schedule.scheduleStart} -{" "}
                                            {schedule.scheduleEnd}
                                          </p>
                                        </div>
                                      ))}
                                  </div>
                                  <div className="flex justify-center items-center ml-4">
                                    <button className="text-white uppercase bg-gradient-to-r hover:bg-gradient-to-l from-[#4CB9E7] to-[#3559E0] p-3 font-semibold rounded-lg w-32 h-12">
                                      Buat Janji
                                    </button>
                                  </div>
                                </div>
                              )}
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  )
                )}
              </div>
            </div>

            {/* Bagian Riwayat Praktek */}
            <div className="w-full mt-8 lg:mt-20">
              <p className="text-2xl font-bold text-center mb-6">
                Riwayat Praktek
              </p>

              <div className="w-full flex flex-col border-2 border-blue-400 p-4 rounded-md shadow-md">
                {doc?.historyOfPractice?.map(
                  (o: HistoryOfPracticeModel, index: number) => (
                    <div className="font-normal text-lg ml-4 mt-4" key={index}>
                      <p className="mb-2">
                        <span className="bullet">&#8226;</span>{" "}
                        {o.medicalFacilityName}
                      </p>
                      <div className="flex justify-between items-center">
                        <p className="mr-4">{o.officeSpecialization}</p>
                        <p className="text-sm">
                          {o?.startDate.split("-")[0] ===
                          o?.endDate?.split("-")[0]
                            ? // Jika tahun sama akan mengeluarkan bulan saja
                              monthNames[
                                new Date(Date.parse(o?.startDate)).getMonth()
                              ] +
                              ` ` +
                              o?.startDate.split("-")[0]
                            : o?.startDate.split("-")[0]}{" "}
                          -{" "}
                          {o?.endDate === null ||
                          o?.endDate < Date.now().toString()
                            ? "Sekarang"
                            : o?.endDate.split("-")[0] ===
                              new Date().getFullYear().toString()
                            ? o.endDate.split("-")[2].split("T")[0] +
                              ` ` +
                              monthNames[
                                new Date(Date.parse(o?.endDate)).getMonth()
                              ] +
                              ` ` +
                              o?.endDate.split("-")[0]
                            : o?.endDate.split("-")[0]}
                        </p>
                      </div>
                    </div>
                  )
                )}
              </div>
            </div>

            {/* Bagian Pendidikan */}
            <div className="w-full mt-8 lg:mt-20">
              <p className="text-2xl font-bold text-center mb-6">Pendidikan</p>

              <div className="w-full flex flex-col border-2 border-blue-400 p-4 rounded-md shadow-md">
                {doc?.education?.map(
                  (education: EducationModel, index: number) => (
                    <div key={education.institutionName} className="mb-4">
                      <p className="font-normal text-lg ml-2 mt-2">
                        <span className="bullet">&#8226;</span>{" "}
                        {education.institutionName}
                      </p>
                      <div className="flex justify-between items-center mt-2">
                        <p className="mr-4">{education.major}</p>
                        <p className="text-sm">
                          {`${education.startYear.split("-")[0]} - ${
                            education.endYear.split("-")[0] ===
                            new Date().getFullYear().toString()
                              ? "Sekarang"
                              : education.endYear.split("-")[0]
                          }`}
                        </p>
                      </div>
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
}

export default DetailDokter;
