import React from "react";
import { BiodataModel } from "../models/biodataModel";
import { config } from "../configurations/config";

interface IProps {
  //prop memanagement data dari parent atau luar
  biodata: BiodataModel;
  photoUpload: any;
}

interface IState {
  //state itu memanagement data didalam
  imagePreview: any;
}

export default class FormChangeImage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      imagePreview: config.noImage,
    };
  }

  render() {
    const { photoUpload, biodata } = this.props;
    const { imagePreview } = this.state;
    return (
      <>
        <form>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Image
            </label>
            <div className="flex justify-center items-center">
              <img
                className="m-6"
                width="128"
                height="128"
                src={biodata.image ? biodata.image : imagePreview}
              />
            </div>

            <input
              type="file"
              name="uploadFile"
              accept=".jpg,.jpeg,.png"
              onChange={photoUpload}
              src={imagePreview}
            />
          </div>
        </form>
      </>
    );
  }
}
