import React from "react";
import { Link, Route, Routes, useParams } from "react-router-dom";
import { SearchDataModel } from "../models/searchDokter/searchData";
import { SearchResultModel } from "../models/searchDokter/searchResultModel";
import { HistoryOfPracticeModel } from "../models/searchDokter/historyOfPracticeModel";
import { BsHospital } from "react-icons/bs";
import { Ecommand } from "../enums/eCommand";
import { SearchService } from "../services/searchService/searchService";
import FormSearch from "./formSearch";
import FortAppointment from "./formAppointment";
import {
  AiFillStar,
  AiOutlineClose,
  AiOutlineLoading,
  AiOutlineLoading3Quarters,
  AiOutlineReload,
} from "react-icons/ai";
import { PaginationModelPage } from "../models/searchDokter/paginationModel";
import { TreatmentModel } from "../models/treatmentModel";
import { EducationModel } from "../models/searchDokter/educationModel";
import DetailDokter from "./detailDokter";
import { click } from "@testing-library/user-event/dist/click";
import { config } from "../configurations/config";
import FormAppointment from "./formAppointment";
import Typed from "react-typed";
import { ScheduleModel } from "../models/searchDokter/scheduleModel";
import { DoctorOfficeModel } from "../models/doctorOfficeModel";
import { AppointmentModel } from "../models/appointmentModel";
import { AppointmentService } from "../services/appointmentService";
import { ValidationResult } from "../../validations/validationResult";
import { CustomerModel } from "../models/customerModel";
import { DoctorOfficeScheduleModel } from "../models/doctorOfficeScheduleModel";
import { DoctorOfficeTreatmentModel } from "../models/doctorOfficeTreatmentModel";
import { LocationModel } from "../models/locationModel";
import { withRouter } from "../layout/withRouter";
import images from "../../assets/LogoProject.png";
import { TreatmentService } from "../services/profilDoctor/treatmentService";
import { DoctorExperienceModel } from "../models/searchDokter/experienceModel";

interface IProps {
  location: SearchDataModel;
  loadSearch: () => void;
}

interface IState {
  showModalSearch: boolean;
  showModalAppointment: boolean;
  command: Ecommand;
  searchDataState: SearchDataModel;
  doctorOffices: DoctorOfficeModel[];
  resultState: SearchResultModel;
  resultStates: SearchResultModel[];
  resultStatesAll: SearchResultModel[];
  treatmentProps: TreatmentModel[];
  paginationAllDoc: PaginationModelPage;
  errorAlerts: any;
  notFoundSearch: boolean;
  online: boolean;
  loading: boolean;
  doctorExperience: DoctorExperienceModel;
}

class SearchResult extends React.Component<IProps, IState> {
  newSearch: SearchDataModel = {
    location: localStorage.getItem("loc") ?? "",
    doctorName: localStorage.getItem("name") ?? "",
    specialization: localStorage.getItem("spec") ?? "",
    treatment: localStorage.getItem("treat") ?? "",
    pageNum: 1,
    rows: 6,
    pages: 0,
  };
  newPaginationPage: PaginationModelPage = {
    pageNum: 0,
    rows: 10,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      showModalSearch: false,
      showModalAppointment: false,
      command: Ecommand.search,
      searchDataState: this.newSearch,
      resultStates: [],
      resultState: new SearchResultModel(),
      resultStatesAll: [],
      doctorOffices: [],
      paginationAllDoc: this.newPaginationPage,
      errorAlerts: { Name: false },
      notFoundSearch: false,
      online: false,
      loading: true,
      treatmentProps: [],
      doctorExperience: { doctorId: 0, doctorExperience: 0 },
    };
  }

  componentDidMount(): void {
    if (this.state.searchDataState.specialization !== "") {
      this.loadSearch();
    }
  }
  loadSearch = () => {
    const { searchDataState, resultStates } = this.state;
    SearchService.getSearchResult(searchDataState)
      .then((result) => {
        if (result.success) {
          this.setState({
            resultStates: result.result,
            searchDataState: {
              ...this.state.searchDataState,
              pages: result.pages,
            },
            loading: false,
            notFoundSearch: false,
          });
          if (result.result.length === 0) {
            this.setState({
              notFoundSearch: true,
            });
          }
        }
        this.loadDoctorExperience(result.result);
      })
      .catch((error) => {
        alert(error);
      });
    SearchService.getAllTreatment().then((result) => {
      this.setState({
        treatmentProps: result.result,
      });
    });
  };

  setShowModalSearch = (val: boolean) => {
    this.setState({
      showModalSearch: val,
    });
  };

  setShowModalAppointment = (val: boolean) => {
    this.setState({
      showModalAppointment: val,
    });
  };

  searchCommand = () => {
    this.setState({
      command: Ecommand.search,
      // searchDataState: this.newSearch,
      errorAlerts: { Name: false },
      showModalSearch: true,
    });
  };

  appointmentCommand = async (id: number) => {
    this.setState({
      showModalAppointment: true,
    });

    // await SearchService.getDoctorById(id)
    //     .then(result => {
    //         if (result.success) {
    //             this.setState({
    //                 showModalAppointment: true,
    //                 resultState: result.result
    //             })
    //         } else {
    //             alert('Error result ' + result.result);
    //         }
    //     })
    //     .catch(error => {
    //         alert('Error error' + error);
    //     })
  };

  handleSearchData = (name: any) => (event: any) => {
    if (name == "specialization") {
      if (event.target.value !== "") {
        SearchService.getAllTreatment(event.target.value).then((result) => {
          this.setState({
            treatmentProps: result.result,
          });
        });
      } else {
        this.setState({
          treatmentProps: [],
        });
      }
    }
    this.setState({
      searchDataState: {
        ...this.state.searchDataState,
        treatment: "",
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  //   submitAppointment = async () => {
  //   this.setState({
  //     errorAlerts: {
  //       Name: {
  //         valid: this.state.appointment.doctorOfficeId != 0,
  //         message: "Nama tidak boleh kosong",
  //       },
  //     },
  //   });
  //   if (this.state.appointment.doctorOfficeId != 0) {
  //     return;
  //   }

  //   console.log(
  //     "Lolos ",
  //     this.state.errorAlerts,
  //     this.state.appointment.doctorOfficeId
  //   );
  //         await AppointmentService.Post(this.state.appointment)
  //             .then(result => {
  //                 if (result.success) {
  //                     this.setState({
  //                         showModalAppointment: false,
  //                         appointment: new AppointmentModel()
  //                     })
  //                     // this.loadLocations();
  //                 } else {
  //                     //alert('Error result ' + result.result);
  //                     this.setState({
  //                         errorAlerts: ValidationResult.Validate(result.result),
  //                       });
  //                 }
  //             })
  //             .catch(error => {
  //                 alert('Error error' + error);
  //             })

  // }

  loadDoctorExperience = async (doctors: SearchResultModel[]) => {
    const updatedDoctors: SearchResultModel[] = [];

    for (const doctor of doctors) {
      SearchService.getDoctorExperienceById(doctor.doctorId)
        .then((experience) => {
          const updatedDoctor = {
            ...doctor,
            pengalaman: experience.result.doctorExperience,
          };
          updatedDoctors.push(updatedDoctor);
          if (updatedDoctors.length === doctors.length) {
            this.setState({
              resultStates: updatedDoctors,
            });
          }
          this.loadDoctorOnline(updatedDoctors);
        })
        .catch((error) => {
          console.error(
            `Error loading experience for doctor ${doctor.doctorId}: ${error}`
          );
        });
    }
  };

  loadDoctorOnline = async (doctors: SearchResultModel[]) => {
    const updatedDoctors: SearchResultModel[] = [];

    for (const doctor of doctors) {
      SearchService.getDoctorOnlineById(doctor.doctorId)
        .then((online) => {
          const updatedDoctor = {
            ...doctor,
            isOnline: online.result.isOnline,
          };
          updatedDoctors.push(updatedDoctor);
          if (updatedDoctors.length === doctors.length) {
            this.setState({
              resultStates: updatedDoctors,
            });
          }
        })
        .catch((error) => {
          console.error(
            `Error loading online for doctor ${doctor.doctorId}: ${error}`
          );
        });
    }
  };

  submitSearch = async () => {
    this.setState({
      errorAlerts: {
        Name: {
          valid:
            this.state.searchDataState.specialization.trim().length > 0 ||
            this.state.searchDataState.specialization !== "0",
          message: "Spesialisasi tidak boleh kosong",
        },
      },
    });
    if (
      this.state.searchDataState.specialization.trim().length === 0 ||
      this.state.searchDataState.specialization === "0" ||
      this.state.searchDataState.specialization === ""
    ) {
      return;
    }
    if (this.state.command === Ecommand.search) {
      const { searchDataState } = this.state;
      SearchService.saveSearchDataInput(searchDataState);
      this.setState({
        showModalSearch: false,
      });
      this.loadSearch();
    }
  };
  resetSearchData = () => {
    // Reset the state values
    SearchService.removeLocalSearchInput();
    this.setState({
      searchDataState: new SearchDataModel(),
      errorAlerts: { Name: false },
      resultStates: [],
      notFoundSearch: false,
    });
  };
  tombolResetData = () => {
    // Reset the state values
    this.setState(
      {
        searchDataState: this.newSearch,
        errorAlerts: { Name: false },
        loading: true,
        notFoundSearch: false,
      },
      () => {
        this.loadSearch();
      }
    );
  };

  getAllDoctor = () => {
    const { paginationAllDoc } = this.state;
    SearchService.getAllDoctor(paginationAllDoc)
      .then((result) => {
        if (result.success) {
          this.setState({
            resultStatesAll: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  changePage = (direction: any) => {
    const { searchDataState } = this.state;
    let newPageNum = searchDataState.pageNum;

    if (direction === "prev") {
      newPageNum -= 1;
      if (newPageNum < 1) {
        newPageNum = 1;
      }
    } else if (direction === "next") {
      newPageNum += 1;
      if (newPageNum > searchDataState.pages) {
        newPageNum = searchDataState.pages;
      }
    }

    this.setState(
      {
        searchDataState: {
          ...searchDataState,
          pageNum: newPageNum,
        },
      },
      () => {
        this.loadSearch();
      }
    );
  };

  isTimeInRange = (currentTime: string, startTime: string, endTime: string) => {
    const [currentHour, currentMinute] = currentTime.split(":").map(Number);
    const [startHour, startMinute] = startTime.split(":").map(Number);
    const [endHour, endMinute] = endTime.split(":").map(Number);

    if (
      currentHour > startHour ||
      (currentHour === startHour && currentMinute >= startMinute)
    ) {
      if (
        currentHour < endHour ||
        (currentHour === endHour && currentMinute <= endMinute)
      ) {
        return true;
      }
    }

    return false;
  };

  render() {
    const {
      resultStates,
      resultState,
      searchDataState,
      resultStatesAll,
      showModalSearch,
      showModalAppointment,
      errorAlerts,
      notFoundSearch,
      loading,
      treatmentProps,
      doctorExperience,
    } = this.state;
    const today = new Date();
    const currentDay = today.toLocaleString("id-ID", { weekday: "long" });
    const currentHour = today.toLocaleString("id-ID", {
      hour: "numeric",
      minute: "numeric",
      hour12: false,
    });
    console.log("tesDay " + currentDay);
    console.log("tesJam " + currentHour);

    return (
      <div className="overflow-y-auto">
        {loading ? (
          <div
            className="h-screen w-screen"
            style={{
              border: "4px solid rgba(0, 0, 0, 0.1)",
              borderLeft: "4px solid #3498db",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              animation: "spin 1s linear infinite",
              margin: "20px auto",
            }}
          ></div>
        ) : (
          <div>
            <div className="text-center items-center overflow-y-scroll p-4">
              <div className="ms-10 flex">
                <Link className="text-xs text-[#00df9a]  font-bold" to={"/"}>
                  Home /{"   "}
                </Link>
                <p className="text-xs text-gray-400"> Cari Dokter</p>
              </div>
              <div className="flex items-center justify-center">
                <p className="md:text-lg text-lg font-bold text-gray-500">
                  Pencarian Berdasarkan kata kunci:{" "}
                  {searchDataState.location
                    ? `Lokasi: ${searchDataState.location},`
                    : ""}{" "}
                  {searchDataState.doctorName
                    ? `Nama: ${searchDataState.doctorName},`
                    : ""}{" "}
                  {searchDataState.specialization
                    ? `Spesialisasi/sub-spesialisasi: ${searchDataState.specialization},`
                    : ""}{" "}
                  {searchDataState.treatment
                    ? `Tindakan Medis: ${searchDataState.treatment}`
                    : ""}
                </p>
                <div className="ms-6">
                  <button
                    onClick={() => this.searchCommand()}
                    className="text-gray-700 border border-blue-800 rounded-2xl font-medium my-4 mx-auto px-6 py-3 flex-nowrap"
                  >
                    {searchDataState.specialization.length === 0
                      ? "Cari Dokter"
                      : "Ulangi Pencarian"}
                  </button>
                </div>
                {/* <div onClick={() => this.tombolResetData()} className="flex">
                  <AiOutlineReload className="ms-10 h-5 w-5 text-blue-500" />
                  <p className="ms-2 cursor-pointer flex justify-center items-center">
                    Reset
                  </p>
                </div> */}
              </div>
              {notFoundSearch ? (
                <div>
                  <Typed
                    className="text-3xl font-bold md:pl-4 pl-2"
                    strings={["Pencarian", "Tidak", "Ditemukan"]}
                    typeSpeed={40}
                    backSpeed={40}
                    loop
                  />
                </div>
              ) : null}
              {/* search result */}
              <div className="text-start flex justify-center items-center p-4">
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 gap-4 w-full max-w-full">
                  {resultStates?.map((doctor: SearchResultModel) => (
                    <div className="shadow-xl rounded-3xl bg-slate-200 max-w-lg flex flex-col justify-between">
                      <div className="p-6 flex ">
                        <div className="flex-1">
                          <h3 className="text-lg font-bold flex-nowrap text-gray-800 mb-2">
                            {doctor.fullName}
                          </h3>
                          <h4 className="text-sm text-gray-600 mb-2">
                            {doctor.specialization
                              ? `Spesialis ${doctor.specialization}`
                              : "Belum ada Spesialisasi"}
                          </h4>
                          <p className="text-sm text-gray-600 mb-4">
                            Experience: {doctor.pengalaman}
                          </p>
                          <div className="grid grid-cols-1">
                            {doctor?.historyOfPractice.map(
                              (practice: HistoryOfPracticeModel) => {
                                return (
                                  <div className="text-gray-600">
                                    {practice.medicalFacilityCategoryName !==
                                    "Online" ? (
                                      <div className="flex">
                                        <BsHospital className="text-base" />{" "}
                                        <p className="text-sm ms-3">
                                          {practice.medicalFacilityName}
                                        </p>
                                      </div>
                                    ) : null}
                                  </div>
                                );
                              }
                            )}
                          </div>
                        </div>
                        <div className="mx-auto grid-cols-1">
                          <div className="flex-shrink-0">
                            <img
                              src={doctor.image}
                              alt={`${doctor.fullName}'s profile`}
                              className="w-20 h-20 object-cover rounded-full"
                            />
                          </div>
                          {doctor.isOnline ? (
                            <button
                              className={`border text-gray-700 border-blue-800 rounded-2xl font-medium my-4 mx-auto px-6 py-1 flex-nowrap`}
                            >
                              Chat
                            </button>
                          ) : (
                            <p className="flex m-4 justify-center items-center">
                              Offline
                            </p>
                          )}
                        </div>
                      </div>
                      <div className="mx-auto flex px-6">
                        <button className="text-gray-700 border border-blue-800 rounded-2xl font-medium my-4 mx-auto px-6 py-3 flex-nowrap">
                          <Link
                            to={`/DetailsDokter/${doctor.biodataId}`}
                            className=""
                          >
                            Lihat Info Lebih Banyak
                          </Link>
                        </button>
                        <button
                          className="bg-blue-800 text-white rounded-2xl font-medium my-4 ms-4 mx-auto px-6 py-3 flex-nowrap"
                          onClick={() =>
                            this.appointmentCommand(doctor.doctorId)
                          }
                        >
                          Buat Janji
                        </button>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              {showModalSearch ? (
                <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
                  <div className="w-full h-screen flex justify-center items-center">
                    <div className="grid grid-cols-1 md:grid-cols-2 m-auto h-[550px] shadow-lg shadow-gray-600 sm:max-w-[900px] bg-[#f5f5f5] rounded-lg">
                      {/* Image logo left */}
                      <div className="w-full h-[550px] hidden md:block">
                        <img
                          className="w-full h-full rounded-s-lg"
                          src={images}
                          alt="/"
                        />
                      </div>
                      {/* form search right*/}
                      <div className="flex justify-center items-center h-auto px-4 text-center">
                        <div className="w-full max-w-md dark:bg-gray-900 rounded-lg overflow-hidden overflow-y-auto flex flex-col justify-between">
                          {/* Header Form Search */}
                          <div>
                            <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                              <div></div>
                              <h3 className="text-black ms-8 md:text-3xl sm:text-4xl text-3xl font-bold md:py-6text-3xl dark:text-white">
                                Cari Dokter
                              </h3>
                              <button
                                onClick={() => this.setShowModalSearch(false)}
                              >
                                <AiOutlineClose className=" text-black text-3xl" />
                              </button>
                            </div>
                            <div className="text-gray-600 md:text-1xl sm:text-1xl text-1xl font-light md:py-6text-3xl dark:text-white border-b ">
                              Masukkan Minimal 1 kata kunci untuk pencarian
                              dokter anda
                            </div>
                          </div>
                          {/* Form Search */}
                          <div className="p-3 overflow-y-auto max-h-96">
                            <FormSearch
                              treatmentProps={treatmentProps}
                              errorAlerts={errorAlerts}
                              searchResultData={searchDataState}
                              onSearch={this.handleSearchData}
                            />
                          </div>
                          {/* Button */}
                          <div
                            className="flex items-center justify-between border-t border-solid border-blueGray-200 rounded-b"
                            role="group"
                            aria-label="Button group"
                          >
                            <button
                              onClick={() => this.resetSearchData()}
                              className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8  text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-gray-300"
                            >
                              Atur ulang
                            </button>
                            <button
                              className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                              onClick={() => this.submitSearch()}
                            >
                              Cari
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
              {showModalAppointment ? (
                <>
                  <div className="fixed inset-0 z-50 overflow-hidden bg-gray-500 bg-opacity-80">
                    <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
                      <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
                        <div>
                          <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                            <div></div>
                            <h3 className="text-black ms-8 md:text-3xl sm:text-4xl text-3xl font-bold md:py-6text-3xl dark:text-white">
                              Buat Janji
                            </h3>
                            <button
                              onClick={() =>
                                this.setShowModalAppointment(false)
                              }
                            >
                              <AiOutlineClose className=" text-black text-3xl" />
                            </button>
                          </div>
                        </div>
                        <div className="p-6 overflow-y-auto max-h-96">
                          <FormAppointment
                            errorAlerts={errorAlerts}
                            resultStates={resultStates}
                            resultState={resultState}
                          />
                        </div>
                        <div
                          className="flex items-center justify-between border-t border-solid border-blueGray-200 rounded-b"
                          role="group"
                          aria-label="Button group"
                        >
                          <button
                            //onClick={() => this.resetSearchData()}
                            className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-8 mx-8  text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-gray-300"
                          >
                            Atur ulang
                          </button>
                          <button
                            className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-8 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                            //onClick={() => this.submitAppointment()}
                          >
                            Buat Janji
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : null}
            </div>
            <div className="flex justify-center items-center ">
              {resultStates?.length === 0 ? (
                <p className="ms-10 text-sm font-normal text-gray-500">
                  Silakan Cari Dokter di tombol diatas
                </p>
              ) : (
                ""
              )}
            </div>
            {resultStates?.length === 0 ? null : (
              <div className="p-2 flex items-center me-6 justify-end font-medium text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 my-2">
                <span className="mx-2 text-gray-800">
                  Page {searchDataState.pageNum} of {searchDataState.pages}
                </span>
                <button
                  className={`text-lg mx-2 px-2 py-1 border text-white-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
                    searchDataState.pages <= 1 ? "hidden" : "visible"
                  }`}
                  onClick={() => this.changePage("prev")}
                >
                  Previous
                </button>
                <span
                  className={`text-slate-800 ${
                    searchDataState.pages <= 1 ? "hidden" : "visible"
                  }`}
                >
                  {searchDataState.pageNum}
                </span>
                <button
                  className={`text-lg mx-2 px-2 py-1 border text-slate-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
                    searchDataState.pages !== 1 ? "visible" : "hidden"
                  }`}
                  onClick={() => this.changePage("next")}
                >
                  Next
                </button>
              </div>
            )}
          </div>
        )}

        {/* Profil dokter */}
        {/* <p className="ms-10 md:text-lg text-lg font-bold text-gray-500">
          Profil Dokter
        </p> */}
        <div>
          <div className="flex m-4 overflow-auto">
            {resultStatesAll?.map((doctor: SearchResultModel) => (
              <div key={doctor.doctorId} className="flex-shrink-0 mx-2">
                {/* header dokter */}
                {/* <div className="shadow-xl rounded-3xl bg-slate-200 w-200">
                  <div className="p-4">
                    <div className="text-center">
                      <div className="flex items-center justify-center flex-shrink-0 ">
                        <img
                          src={doctor.image}
                          alt={`${doctor.fullName}'s profile`}
                          className="w-32 h-32 object-cover rounded-full border border-blue-700 p-1 mb-2"
                        />
                      </div>
                      <h3 className="text-lg justify-center font-bold flex-nowrap text-blue-800 mb-2">
                        {doctor.fullName}
                      </h3>
                      <h4 className="text-sm text-gray-600  mb-2">
                        Spesialis {doctor.specialization}
                      </h4>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          marginBottom: 6,
                        }}
                      >
                        {[...Array(5)].map((_, index) => (
                          <AiFillStar style={{ color: "darkgoldenrod" }} />
                        ))}
                      </div>
                    </div>
                    <div className="p-3 border flex border-t-slate-400 justify-between">
                      <p className="text-base font-bold text-blue-800">Janji</p>
                      <p className="rounded-full w-6 h-6 text-center font-bold text-blue-800 bg-blue-300">
                        3
                      </p>
                    </div>
                    <div className="p-3 border flex border-t-slate-400 justify-between">
                      <p className="text-base font-bold text-blue-800">
                        Obrolan/Konsultasi
                      </p>
                      <p className="rounded-full w-6 h-6 text-center font-bold text-blue-800 "></p>
                    </div>
                  </div>
                </div> */}
                {/* tentang saya */}
                {/* <div className="shadow-2xl rounded-3xl pb-4">
                  <div className="mt-7 p-2 bg-slate-200 rounded-tr-3xl rounded-tl-3xl">
                    <h5 className="p-1 text-center font-bold text-blue-800">
                      Tentang Saya
                    </h5>
                  </div>
                  <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                    <p className="p-2 ms-2 font-bold text-left  text-blue-800">
                      Tindakan Medis
                    </p>
                    <div className="mx-6">
                      {doctor.treatment.map((o: TreatmentModel) => {
                        return (
                          <p className="font-normal text-left  text-gray-500">
                            - {o.name}
                          </p>
                        );
                      })}
                    </div>
                  </div>
                  <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                    <p className="font-bold p-2 ms-2 text-left  text-blue-800">
                      Riwayat Praktek
                    </p>
                    <div className="mx-6">
                      {doctor.historyOfPractice.map(
                        (o: HistoryOfPracticeModel) => {
                          return (
                            <p className="font-normal text-left  text-gray-500">
                              - {o.medicalFacilityName}
                            </p>
                          );
                        }
                      )}
                    </div>
                  </div>

                  <div className=" mb-7 rounded-br-3xl rounded-bl-3xl">
                    <p className="font-bold p-2 ms-2 text-left  text-blue-800">
                      Pendidikan
                    </p>
                    <div className="mx-6">
                      {doctor.education.map((o: EducationModel) => {
                        return (
                          <div>
                            <p className="font-normal text-left  text-gray-500">
                              {o.institutionName}
                            </p>
                            <div className="flex justify-between">
                              <p className="ms-6 font-light text-sm text-left  text-gray-500">
                                {o.major}
                              </p>
                              <p className="ms-6 font-light text-sm text-left  text-gray-500">
                                {o.endYear}
                              </p>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div> */}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(SearchResult);
