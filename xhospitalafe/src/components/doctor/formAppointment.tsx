import React from "react";
import { LocationModel } from "../models/locationModel";
import { LocationLevelModel } from "../models/locationLevelModel";
import { MedicalFacilityModel } from "../models/medicalFacilityModel";
import { MedicalFacilityScheduleModel } from "../models/medicalFacilityScheduleModel";
import { MedicalFacilityCategoryModel } from "../models/medicalFacilityCategoryModel";
import { TreatmentModel } from "../models/treatmentModel";
import { AppointmentService } from "../services/appointmentService";
import { SearchResultModel } from "../models/searchDokter/searchResultModel";
import { CustomerModel } from "../models/customerModel";
import { DoctorOfficeModel } from "../models/doctorOfficeModel";
import { AppointmentModel } from "../models/appointmentModel";
import { DoctorOfficeTreatmentModel } from "../models/doctorOfficeTreatmentModel";
import { DoctorOfficeScheduleModel } from "../models/doctorOfficeScheduleModel";
import { CustomerAppointmentModel } from "../models/customerAppointmentModel";
interface IProps {
    resultState: SearchResultModel;
    resultStates: SearchResultModel[];
    errorAlerts: any;
}

interface IState {
  appointment: AppointmentModel[];
  doctorOffices: DoctorOfficeModel[];
  doctorOfficeSchedules: DoctorOfficeScheduleModel[];
  doctorOfficeTreatments: DoctorOfficeTreatmentModel[];
  customers: CustomerAppointmentModel[];
  locations: LocationModel[];
  medicalFacilities: MedicalFacilityModel[];
  medicalFacility: MedicalFacilityModel;
  medicalFacilitySchedules: MedicalFacilityScheduleModel[];
  medicalFacilitySchedule: MedicalFacilityScheduleModel;
  treatments: TreatmentModel[];
  treatment: TreatmentModel;
}

export default class FormAppointment extends React.Component<IProps, IState> {
    newLocationLevel: LocationLevelModel = {
        id:0, name: '', abbreviation: '', is_delete: false
    }

    newLocation: LocationModel = {
        id:0, name: "", parentId: 0, location: {}, locationLevelId: 0, locationLevel: this.newLocationLevel, is_delete: false
    }

    newMedicalFacilityCategory: MedicalFacilityCategoryModel = {
        id:0, name: ''
    }

    newMedicalFacility: MedicalFacilityModel = {
        id:0, name: "", 
        medicalFacilityCategoryId: 0, 
        medicalFacilityCategory: this.newMedicalFacilityCategory, 
        locationId: 0, 
        location: this.newLocation,
        fullAddress: "",
        email:  "",
        phoneCode: "",
        phone: "",
        fax: "", 
        is_delete: false
    }

    newTreatment: TreatmentModel = {
        id: 0, name: '', doctorId: 0, is_delete: false
    }

    newMedicalFacilitySchedule: MedicalFacilityScheduleModel = {
      id: 0,
      medicalFacilityId: 0,
      medicalFacility: this.newMedicalFacility,
      day: "",
      timeScheduleStart: "",
      timeScheduleEnd: "",
      is_delete: false
    }

  constructor(props: IProps) {
    super(props);
    this.state = {
      appointment: [],
      doctorOffices: [],
      doctorOfficeSchedules: [],
      doctorOfficeTreatments: [],
      customers: [],
      locations: [],
      medicalFacilities: [],
      medicalFacility: this.newMedicalFacility,
      medicalFacilitySchedules: [],
      medicalFacilitySchedule: this.newMedicalFacilitySchedule,
      treatments: [],
      treatment: this.newTreatment
    };
  }

  changeHandler = (name: any) => (event: any) => {
    this.setState({
        appointment: {
            ...this.state.appointment,
            [name]: event.target.value
        }
    })
}

  componentDidMount(): void {
    this.loadAppointment();
  }

loadAppointment = async () => {
    AppointmentService.GetCustomerRelation(2)
      .then((result) => {
        if (result.success) {
          this.setState({
            customers: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
      AppointmentService.GetMedicalFacility(4)
      .then((result) => {
        if (result.success) {
          this.setState({
            doctorOffices: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
      AppointmentService.GetSchedule(4)
      .then((result) => {
        if (result.success) {
          this.setState({
            doctorOfficeSchedules: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
      AppointmentService.GetTreatment(4)
      .then((result) => {
        if (result.success) {
          this.setState({
            doctorOfficeTreatments: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };


  render() {
    const { customers, doctorOffices, doctorOfficeSchedules, doctorOfficeTreatments, locations, medicalFacilities, medicalFacility, medicalFacilitySchedule, medicalFacilitySchedules, treatments, treatment, appointment } = this.state;
    const { errorAlerts } = this.props;
    return (
      <>
        <form>
          <div className="mb-6 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Konsultasikan untuk*
            </label>
            <select
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={this.changeHandler("customerId")}
            >
              <option value="0"></option>
              {customers.map((o: CustomerAppointmentModel) => {
                return (
                  <option value={o.id} className="text-black">
                    {o.biodata.fullName}, {o.customerRelation.name}
                  </option>
                );
              })}
            </select>
          </div>

          <div className="mb-6 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Faskes*
            </label>
            <select
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              onChange={this.changeHandler("doctorOfficeId")}
            >
              <option value="0"></option>
              {doctorOffices.map((o: DoctorOfficeModel) => {
                return (
                  <option value={o.id} className="text-black">
                    {o.medicalFacility.name}
                  </option>
                );
              })}
            </select>
            {!errorAlerts.Location?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Location?.message}
              </strong>
            ) : null}
          </div>

          <div className="mb-6 ">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-left">
              Waktu kedatangan*
            </label>
            <select
              value={medicalFacilitySchedule.day}
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
              onChange={this.changeHandler("doctorOfficeScheduleId")}
            >
              <option selected value="0"></option>
              {doctorOfficeSchedules.map((o: DoctorOfficeScheduleModel) => {
                return <option value={o.id}>{o.medicalFacilitySchedule.day}, {o.medicalFacilitySchedule.timeScheduleStart} s/d {o.medicalFacilitySchedule.timeScheduleEnd}</option>;
              })}
            </select>
          </div>
          <div className="mb-6">
            <label className="text-left block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Tindakan Medis 
            </label>
            <select
              value={treatment.name}
              className="bg-gray-50 border  border-blue-800  text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 "
              onChange={this.changeHandler("doctorOfficeTreatmentId")}
            >
              <option selected value="0"></option>
              {doctorOfficeTreatments.map ((o: DoctorOfficeTreatmentModel) => {
                return <option value={o.id}>{o.doctorTreatment.name}</option>;
              })}
            </select>
            <span>{JSON.stringify(appointment)}</span>
          </div>
        </form>
      </>
    );
  }
}
