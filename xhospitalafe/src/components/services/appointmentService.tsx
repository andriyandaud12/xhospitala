import axios from "axios";
import { config } from "../configurations/config";
import { MedicalFacilityModel } from "../models/medicalFacilityModel";
import { MedicalFacilityScheduleModel } from "../models/medicalFacilityScheduleModel";
import { AppointmentModel } from "../models/appointmentModel";

export const AppointmentService = {
  GetCustomerRelation: (id: number) => {
    // https://localhost:7054/api/Appointment/GetCustomerRelation/2
    console.log(config.apiUrl + `/Appointment/GetCustomerRelation/${id}`)
    const result = axios
      .get(config.apiUrl + `/Appointment/GetCustomerRelation/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  GetMedicalFacility: (id: number) => {
    // https://localhost:7054/api/Appointment/GetMedicalFacility/4
    console.log(config.apiUrl + `/Appointment/GetMedicalFacility/${id}`)
    const result = axios
      .get(config.apiUrl + `/Appointment/GetMedicalFacility/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  GetSchedule: (id: number) => {
    // https://localhost:7054/api/Appointment/GetSchedule/4
    console.log(config.apiUrl + `/Appointment/GetSchedule/${id}`)
    const result = axios
      .get(config.apiUrl + `/Appointment/GetSchedule/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  GetTreatment: (id: number) => {
    // https://localhost:7054/api/Appointment/GetTreatment/4
    console.log(config.apiUrl + `/Appointment/GetTreatment/${id}`)
    const result = axios
      .get(config.apiUrl + `/Appointment/GetTreatment/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  Post: (appointment: AppointmentModel) => {
    console.log(config.apiUrl + `/Appointment`)
    const result = axios
    //https://localhost:7054/api/Appointment
      .post(config.apiUrl + `/Appointment`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
};
