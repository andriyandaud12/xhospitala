import axios from "axios";
import { CurentSpecializationModel } from "../../models/profil/currentSpecializationModel";
import { config } from "../../configurations/config";

export const CurentSpecializationService = {
  update: (id: number, data: CurentSpecializationModel) => {
    const { doctorId, specializationId, ...newData } = data;
    const result = axios
      .put(config.apiUrl + `/CurrentSpecialization/${id}`, {
        doctorId: doctorId,
        specializationId: specializationId,
      })
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getCurrentById: (id: number) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/CurrentSpecialization/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  post: (data: CurentSpecializationModel) => {
    const { doctorId, specializationId, ...newData } = data;
    const result = axios
      .post(config.apiUrl + `/CurrentSpecialization`, {
        doctorId: doctorId,
        specializationId: specializationId,
      })
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
};
