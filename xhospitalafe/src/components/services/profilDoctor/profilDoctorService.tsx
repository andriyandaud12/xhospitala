import axios from "axios";
import { config } from "../../configurations/config";
import { BiodataModel } from "../../models/biodataModel";
import { SearchResultModel } from "../../models/searchDokter/searchResultModel";

export const ProfilDoctorService = {
  getDoctorById: (id: number) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Doctor/GetDoctorById/${id}`)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getDoctorPriceById: (id: number) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Doctor/GetDoctorPriceById/${id}`)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getDoctorByIdForProfil: () => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(
        config.apiUrl +
          `/Doctor/GetDoctorById/${localStorage.getItem("biodataId")}`
      )
      .then((respons) => {
        const getId: SearchResultModel = respons.data;
        localStorage.setItem("doctorIdForChat", getId.doctorId.toString());
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getIdProfil: (id: number) => {
    localStorage.setItem("profilDoctorId", id.toString());
  },

  getBiodataById: (id: number) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Biodata/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },

  updateImage: (id: number, biodata: BiodataModel) => {
    const { image, ...newBio } = biodata;
    const result = axios
      .put(config.apiUrl + `/Biodata/updateimage/${id}`, { image: image })
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getAppointmentByDoctorId: () => {
    const result = axios
      .get(
        config.apiUrl +
          `/Appointment/GetAppointmentByDoctorId/${localStorage.getItem(
            "doctorIdForChat"
          )}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  getAllCustomerChatByDoctorId: () => {
    const result = axios
      .get(
        config.apiUrl +
          `/CustomerChat/GetChatByDoctorId/${localStorage.getItem(
            "doctorIdForChat"
          )}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
