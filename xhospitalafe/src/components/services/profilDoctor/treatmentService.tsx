import axios from "axios";
import { config } from "../../configurations/config";
import { TreatmentModel } from "../../models/treatmentModel";

export const TreatmentService = {
  post: (data: TreatmentModel) => {
    const { doctorId, name, ...newData } = data;
    const result = axios
      .post(config.apiUrl + `/DoctorTreatment`, {
        doctorId: doctorId,
        name: name.trim(),
      })
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          status: error.response.status,
          result: error.response.data,
          //   pages: 0,
        };
      });
    return result;
  },
  delete: (doctorId: number, status: boolean) => {
    const result = axios
      .put(
        config.apiUrl +
          `/DoctorTreatment/changestatus/${doctorId}?status=${status}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getById: (id: number) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/DoctorTreatment/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
};
