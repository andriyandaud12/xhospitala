import axios from "axios";
import { config } from "../configurations/config";
import { LocationLevelModel } from "../models/locationLevelModel";
import { PaginationModel } from "../models/paginationModel";

export const LocationLevelService = {
  changeStatus: (id:number, status: boolean) => {
    const result = axios.put(config.apiUrl + "/LocationLevel/changestatus/"+ id +"?status=" + status)
      .then(respons => {
        // console.log(respons);
        return {
          success: (respons.status === 200),
          result: respons.data,
          status: respons.status
        }
      })
      .catch(error => {
        //console.log(error);
        return {
          success: false,
          status: error.response.status,
          result: error.response.data,
          message: error.response.data,
        }
    });
  return result;
  },
  getAll: () => {
    const result = axios
      .get(config.apiUrl + `/LocationLevel/GetAll`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getById: (id: number) => {
    const result = axios.get(config.apiUrl + '/LocationLevel/' + id)
    .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
    })
    .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  getPagination: (pg: PaginationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    //https://localhost:7054/api/LocationLevel/?pageNum=0&rows=0&sort=0
    console.log(config.apiUrl + `/LocationLevel/?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&sort=${pg.sort}`)
    const result = axios.get(config.apiUrl + `/LocationLevel/?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&sort=${pg.sort}`)
        .then(respons => {
            console.log(respons);
            return {
                success: respons.data.success,
                result: respons.data.data,
                pages: respons.data.pages
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error,
                pages: 0
            }
        });
    return result;
},
  post: (locationLevel: LocationLevelModel) => {
    const result = axios.post(config.apiUrl + '/LocationLevel', locationLevel)
        .then(respons => {
            // console.log(respons);
            return {
                success: (respons.status === 200),
                result: respons.data,
                status: respons.status
            }
        })
        .catch(error => {
            //console.log(error);
            return {
              success: false,
              status: error.response.status,
              result: error.response.data,
              message: error.response.data,
            }
        });
    return result;
},
update: (id: number, locationLevel: LocationLevelModel) => {
  const result = axios.put(config.apiUrl + '/LocationLevel/'+ id , locationLevel)
      .then(respons => {
          // console.log(respons);
          return {
              success: (respons.status === 200),
              result: respons.data
          }
      })
      .catch(error => {
          //console.log(error);
          return {
              success: false,
              result: error
          }
      });
  return result;
}
};
