import axios from "axios";
import { config } from "../configurations/config";
import { DoctorModel } from "../models/doctorModel";
import { PaginationModel } from "../models/paginationModel";

export const DoctorService = {
  getAll: () => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Doctor/GetAll`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
};
