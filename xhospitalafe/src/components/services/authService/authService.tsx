import axios from "axios";
import { config } from "../../configurations/config";
import { AuthenticationModel } from "../../models/authenticationModel";
import { UserModel } from "../../models/userModel";
import { RegisterModel } from "../../models/registerModel";
import { OtpModel } from "../../models/otpModel";
import { BiodataModel } from "../../models/biodataModel";

export const AuthService = {
  login: (auth: AuthenticationModel) => {
    var result = axios
      .post(config.apiUrl + "/User", auth)
      .then((respons) => {
        const user: UserModel = respons.data;
        localStorage.setItem("email", user.email);
        localStorage.setItem("token", user.token);
        localStorage.setItem("nama", user.biodata.fullName);
        localStorage.setItem("biodataId", user.biodataId.toString());
        localStorage.setItem("roles", JSON.stringify(user.menuRole));
        console.log(JSON.stringify(user.menuRole));
        return {
          success: respons.status === 200,
          status: respons.status,
          result: {
            email: user.email,
            biodataId: user.biodataId,
          },
        };
      })
      .catch((error) => {
        return {
          success: false,
          status: error.response.status,
          result: error.response.data,
          message: error.response.data,
        };
      });
    return result;
  },
  register: (reg: RegisterModel) => {
    var result = axios
      .post(config.apiUrl + "/User/register", reg)
      .then((respons) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        console.log(JSON.stringify(error.response));
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  otp: (otp: OtpModel) => {
    var result = axios
      .post(config.apiUrl + "/User/sendotp?email=" + `${otp.email}`, otp)
      .then((respons) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        console.log(JSON.stringify(error.response));
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  getToken: () => {
    return localStorage.getItem("token");
  },

  getAccount: () => {
    let rolStr: any = localStorage.getItem("roles");
    let roles: string[] = JSON.parse(rolStr);
    let biodataId: string | null = localStorage.getItem("biodataId");

    console.log(roles);
    return {
      id: 0,
      biodataId: parseInt(biodataId as string, 10) || 0,
      roleId: 0,
      email: localStorage.getItem("email") || "",
      biodata: new BiodataModel(),
      fullName: localStorage.getItem("nama") || "",
      loginAttempt: 0,
      token: "",
      menuRole: roles || [],
    };
  },

  logout: () => {
    localStorage.clear();
  },
};
