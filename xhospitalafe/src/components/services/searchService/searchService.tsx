import axios from "axios";
import { config } from "../../configurations/config";
import { SearchDataModel } from "../../models/searchDokter/searchData";
import { PaginationModel } from "../../models/paginationModel";
import { PaginationModelPage } from "../../models/searchDokter/paginationModel";

export const SearchService = {
  getAllSpecialization: () => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Specialization/GetAll`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getAllTreatment: (name?: string) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(
        config.apiUrl +
          `/DoctorTreatment/GetAllTreatmentBySpecName/${
            name ?? localStorage.getItem("spec")
          }`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getAllLoaction: () => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Location/GetAllByLocationLevel5`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getLoactionByName: (name: string) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(config.apiUrl + `/Location/GetAllByLocationLevel5/${name}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },

  getSearchResult: (res: SearchDataModel) => {
    const spec: string = localStorage.getItem("spec") ?? res.specialization;
    const loc: string = localStorage.getItem("loc") ?? res.location;
    const treat: string = localStorage.getItem("treat") ?? res.treatment;
    const name: string = localStorage.getItem("name") ?? res.doctorName;
    const pageNum: string =
      localStorage.getItem("pageNum") ?? res.pageNum.toString();
    const rows: string = localStorage.getItem("rows") ?? res.rows.toString();

    const searchStr = spec.length > 0 ? `&specialization=${spec}` : ``;
    const locationName = loc?.split(",")[0];
    const result = axios
      .get(
        config.apiUrl +
          `/Doctor/Search?pageNum=${pageNum ?? 1}&rows=${
            rows ?? 6
          }${searchStr}&location=${locationName ?? ""}&name=${
            name ?? ""
          }&treatmentName=${treat ?? ""}`
      )
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getAllDoctor: (pg: PaginationModelPage) => {
    // const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(
        config.apiUrl +
          `/Doctor/GetAllFullDescription?pageNum=${pg.pageNum}&rows=${pg.rows}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getDoctorById: (id: number) => {
    // https://localhost:7054/api/Doctor/GetDoctorById/6
    console.log(config.apiUrl + `/Doctor/GetDoctorById/${id}`);
    const result = axios
      .get(config.apiUrl + `/Doctor/GetDoctorById/${id}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  saveSearchDataInput: (res: SearchDataModel) => {
    localStorage.setItem("spec", res.specialization.toString());
    localStorage.setItem("loc", res.location.toString());
    localStorage.setItem("treat", res.treatment.toString());
    localStorage.setItem("name", res.doctorName.toString());
    localStorage.setItem("pageNum", res.pageNum.toString());
    localStorage.setItem("pages", res.pages.toString());
    localStorage.setItem("rows", res.rows.toString());
  },
  removeLocalSearchInput: () => {
    localStorage.removeItem("loc");
    localStorage.removeItem("spec");
    localStorage.removeItem("treat");
    localStorage.removeItem("pages");
    localStorage.removeItem("rows");
    localStorage.removeItem("pageNum");
    localStorage.removeItem("name");
  },

  getDoctorExperienceById: (doctorId: number) => {
    const result = axios
      .get(config.apiUrl + `/Doctor/GetDoctorExperienceById/${doctorId}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getDoctorOnlineById: (doctorId: number) => {
    const result = axios
      .get(config.apiUrl + `/Doctor/GetDoctorOnline/${doctorId}`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
