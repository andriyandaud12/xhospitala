import axios from "axios";
import { config } from "../../configurations/config";
import { PaginationModel } from "../../models/paginationModel";
import { PasienModel } from "../../models/pasienModel";

export const PasienService = {
  getAll: (pg: PaginationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    console.log(searchStr);
    const result = axios
      .get(
        config.apiUrl +
          `/CustomerMember?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        { headers: config.headers() }
      )

      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  post: (pasien: PasienModel) => {
    const { biodata,customer,bloodGroup,customerRelation, ...newPasien } = pasien;
    // const { fullName, ...biodataWithoutFullname } = biodata; // Destructure fullname from biodata
    const payload = {
      fullName: biodata.fullName,
      dob: customer.dob,
      gender:customer.gender,
      rhesusType:customer.rhesusType,
      height:customer.height,
      weight:customer.weight,
      ...newPasien,
    };
    console.log(config.apiUrl + '/CustomerMember', payload)
    const result = axios.post(config.apiUrl + '/CustomerMember', payload)
        .then(respons => {
            console.log(respons);
            return {
                success: (respons.status == 200),
                result: respons.data
            }
        })
        .catch(error => {
            // console.log(error);
            return {
                success: false,
                result: error
            }
        });
    return result;
},getById: (id: number) => {
  const result = axios
    .get(
      config.apiUrl + "/CustomerMember/" + id
      //   , { headers: config.headers() }
    )
    .then((respons) => {
      return {
        success: respons.status === 200,
        result: respons.data,
      };
    })
    .catch((error) => {
      return {
        success: false,
        result: error,
      };
    });
  return result;
},
update: (id: number, pasien: PasienModel) => {
  const result = axios
    .put(
      config.apiUrl + "/customerMember/" + id,
      pasien
      //   ,
      //   {
      //     headers: config.headers(),
      //   }
    )
    .then((respons) => {
      return {
        success: respons.status === 200,
        result: respons.data,
      };
    })
    .catch((error) => {
      return {
        success: false,
        result: error,
      };
    });
  return result;
},
changeStatus: (id: number, status: boolean) => {
  const result = axios
    .put(
      config.apiUrl + "/CustomerMember/changestatus/" + id + `?status=${status}`
    )
    .then((respons) => {
      return {
        success: respons.status === 200,
        result: respons.data,
      };
    })
    .catch((error) => {
      return {
        success: false,
        result: error,
      };
    });

  return result;
},
};
