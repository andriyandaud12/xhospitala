import axios from "axios";
import { config } from "../../configurations/config";
import { PaginationModel } from "../../models/paginationModel";
import { BloodGroupModel } from "../../models/bloodGroupModel";

export const BloodGroupService = {
  getAll: (pg: PaginationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    console.log(searchStr);
    const result = axios
      .get(
        config.apiUrl +
          `/BloodGroup?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        { headers: config.headers() }
      )

      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getById: (id: number) => {
    const result = axios
      .get(
        config.apiUrl + "/BloodGroup/" + id
        //   , { headers: config.headers() }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  post: (bloodGroup: BloodGroupModel) => {
    console.log(config.apiUrl + "/BloodGroup",
    bloodGroup)
    const result = axios
    .post(
      config.apiUrl + "/BloodGroup",
      bloodGroup
      //   , { headers: config.headers() }
      )
      .then((respons) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        // console.log(config.apiUrl + "/BloodGroup",bloodGroup)
        console.log(JSON.stringify(error.response));
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  update: (id: number, bloodGroup: BloodGroupModel) => {
    const result = axios
      .put(
        config.apiUrl + "/BloodGroup/" + id,
        bloodGroup
        //   ,
        //   {
        //     headers: config.headers(),
        //   }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  changeStatus: (id: number, status: boolean) => {
    const result = axios
      .put(
        config.apiUrl + "/BloodGroup/changestatus/" + id + `?status=${status}`
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },
};
