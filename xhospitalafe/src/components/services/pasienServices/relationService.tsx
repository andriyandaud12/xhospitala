import axios from "axios";
import { config } from "../../configurations/config";
import { PaginationModel } from "../../models/paginationModel";
import { CustomerRelationModel } from "../../models/RelationModel";

export const RelationService = {
  getAll: (pg: PaginationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    console.log(searchStr);
    const result = axios
      .get(
        config.apiUrl +
          `/CustomerRelation?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        { headers: config.headers() }
      )

      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getById: (id: number) => {
    const result = axios
      .get(
        config.apiUrl + "/CustomerRelation/" + id
        //   , { headers: config.headers() }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },

  post: (relation: CustomerRelationModel) => {
    console.log(config.apiUrl + "/CustomerRelation", relation);
    const result = axios
      .post(
        config.apiUrl + "/CustomerRelation",
        relation
        //   , { headers: config.headers() }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  update: (id: number, relation: CustomerRelationModel) => {
    // console.log(config.apiUrl + "/CustomerRelation",relation)
    console.log(config.apiUrl + "/CustomerRelation/" + id, relation);
    const result = axios
      .put(
        config.apiUrl + "/CustomerRelation/" + id,
        relation
        //   ,
        //   {
        //     headers: config.headers(),
        //   }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
          status: respons.status,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error.response.data,
          status: error.response.status,
        };
      });
    return result;
  },
  changeStatus: (id: number, status: boolean) => {
    const result = axios
      .put(
        config.apiUrl +
          "/CustomerRelation/changestatus/" +
          id +
          `?status=${status}`
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },
};
