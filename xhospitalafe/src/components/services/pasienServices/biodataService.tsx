import axios from "axios";
import { config } from "../../configurations/config";
import { PaginationModel } from "../../models/paginationModel";
import { BloodGroupModel } from "../../models/bloodGroupModel";

export const BiodataService = {
  getAll: (pg: PaginationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    console.log(searchStr);
    const result = axios
      .get(
        config.apiUrl +
          `/Biodata?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        { headers: config.headers() }
      )

      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
};
