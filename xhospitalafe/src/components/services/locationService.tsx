import axios from "axios";
import { config } from "../configurations/config";
import { LocationModel } from "../models/locationModel";
import { PaginationLocationModel } from "../models/paginationLocationModel";

export const LocationService = {
  post: (location: LocationModel) => {
    const { locationLevel, ...newLocation } = location;
    console.log(location, newLocation);
    const result = axios.post(config.apiUrl + '/Location', newLocation)
        .then(respons => {
            console.log(respons);
            return {
              success: (respons.status === 200),
              result: respons.data,
              status: respons.status
          }
        })
        .catch(error => {
            // console.log(error);
            return {
              success: false,
              status: error.response.status,
              result: error.response.data,
              message: error.response.data,
            }
        });
    return result;
},
changeStatus: (id:number, status: boolean) => {
  const result = axios.put(config.apiUrl + "/Location/changestatus/"+ id +"?status=" + status)
      .then(respons => {
          // console.log(respons);
          return {
            success: (respons.status === 200),
            result: respons.data,
            status: respons.status
          }
      })
      .catch(error => {
          //console.log(error);
          return {
            success: false,
            status: error.response.status,
            result: error.response.data,
            message: error.response.data,
          }
      });
  return result;
},

  getAll: () => {
    const result = axios
      .get(config.apiUrl + `/Location/GetAll`)
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          //   pages: 0,
        };
      });
    return result;
  },
  getPagination: (pg: PaginationLocationModel) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const levelStr = pg.level > 0 ? `&level=${pg.level}` : ``;
    console.log(config.apiUrl + `/Location/?pageNum=${pg.pageNum}&rows=${pg.rows}${levelStr}${searchStr}&sort=${pg.sort}`)
    const result = axios.get(config.apiUrl + `/Location/?pageNum=${pg.pageNum}&rows=${pg.rows}${levelStr}${searchStr}&sort=${pg.sort}`)
        .then(respons => {
            console.log(respons);
            return {
                success: respons.data.success,
                result: respons.data.data,
                pages: respons.data.pages
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error,
                pages: 0
            }
        });
    return result;
},
  getById: (id: number) => {
    const result = axios.get(config.apiUrl + '/Location/' + id)
    .then((respons) => {
        console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
          //   pages: respons.data.pages,
        };
    })
    .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  getByParentId: (id: number) => {
    // id => parentId
    const result = axios.get(config.apiUrl + '/Location/location/' + id)
        .then(respons => {
            // console.log(respons);
            return {
                success: (respons.status == 200),
                result: respons.data
            }
        })
        .catch(error => {
            return {
                success: false,
                result: error
            }
        });
    return result;
},
update: (id: number, location: LocationModel) => {
  const result = axios
    .put(
      config.apiUrl + "/Location/" + id,
      location
      //   ,
      //   {
      //     headers: config.headers(),
      //   }
    )
    .then((respons) => {
      return {
        success: respons.status === 200,
        result: respons.data,
      };
    })
    .catch((error) => {
      return {
        success: false,
        result: error,
      };
    });
  return result;
},
getAllByLevel: (id: number) => {
  console.log(config.apiUrl + `/Location/Level/${id}`)
  const result = axios
  //https://localhost:7054/api/Location/level/5
    .get(config.apiUrl + `/Location/Level/${id}`)
    .then((respons) => {
      console.log(respons);
      return {
        success: respons.status === 200,
        result: respons.data,
        //   pages: respons.data.pages,
      };
    })
    .catch((error) => {
      return {
        success: false,
        result: error,
        //   pages: 0,
      };
    });
  return result;
},
};
