import { DoctorOfficeTreatmentModel } from "./doctorOfficeTreatmentModel";

export class DoctorOfficeTreatmentPriceModel {
  constructor() {
    this.doctorOfficeTreatment = [];
  }
  id: number = 0;
  doctorOfficeTreatmentId: number = 0;
  doctorOfficeTreatment: DoctorOfficeTreatmentModel[];
  price: number = 0;
  priceStartFrom: number = 0;
  priceEndFrom: number = 0;
}
