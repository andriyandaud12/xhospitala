export class BiodataModel {
  id: number = 0;
  fullName: string = "";
  mobilePhone: string = "";
  image: any;
  imagePath: string = "";
  is_delete: boolean = false;
}
