import { CustomerRelationModel } from "./RelationModel";
import { BiodataModel } from "./biodataModel";
import { BloodGroupModel } from "./bloodGroupModel";

export class CustomerAppointmentModel {
    constructor() {
        this.biodata = new BiodataModel;
        this.bloodGroup = new BloodGroupModel;
        this.customerRelation = new CustomerRelationModel;
    }
    id: number = 0;
    biodataId: number = 0;
    biodata: BiodataModel;
    dob: Date= new Date() ;
    gender: string = "";
    bloodGroupId: number = 0;
    bloodGroup: BloodGroupModel;
    rhesusType: string = "";
    height: number = 0;
    weight: number = 0;
    is_delete: boolean = false;
    customerRelationId: number = 0;
    customerRelation: CustomerRelationModel;
}


