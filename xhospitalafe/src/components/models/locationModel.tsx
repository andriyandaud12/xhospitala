import { LocationLevelModel } from "./locationLevelModel";

export class LocationModel {
    constructor() {
        this.locationLevel = new LocationLevelModel;
        this.location = new LocationModel;
    }
    id: number = 0;
    name: string = "";
    parentId: number = 0;
    locationLevelId: number = 0;
    location: any;
    locationLevel: LocationLevelModel;
    is_delete: boolean = false;
}
