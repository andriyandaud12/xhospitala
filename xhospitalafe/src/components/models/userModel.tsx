import { BiodataModel } from "./biodataModel";

export class UserModel {
  constructor() {
    this.biodata = new BiodataModel();
  }
  id: number = 0;
  biodataId: number = 0;
  biodata: BiodataModel;
  fullName: string = "";
  roleId: number = 0;
  email: string = "";
  loginAttempt: number = 0;
  token: string = "";
  menuRole: string[] = [];
}
