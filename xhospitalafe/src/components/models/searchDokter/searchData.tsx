export class SearchDataModel {
  pageNum: number = 1;
  rows: number = 6;
  location: string = "";
  doctorName: string = "";
  specialization: string = "";
  treatment: string = "";
  pages: number = 0;
}
