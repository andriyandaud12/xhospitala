export class EducationModel {
  id: number = 0;
  doctorId: number = 0;
  educationLevelId: number = 0;
  institutionName: string = "";
  major: string = "";
  startYear: string = "";
  endYear: string = "";
  isLastEducation: boolean = false;
  is_delete: boolean = false;
}
