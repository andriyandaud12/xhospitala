import { TreatmentModel } from "../treatmentModel";
import { EducationModel } from "./educationModel";
import { HistoryOfPracticeModel } from "./historyOfPracticeModel";
import { ScheduleModel } from "./scheduleModel";

export class SearchResultModel {
  constructor() {
    this.historyOfPractice = [];
    this.treatment = [];
    this.education = [];
    this.doctorSchedule = [];
  }
  doctorId: number = 0;
  biodataId: number = 0;
  currentSpecializationId: number = 0;
  fullName: string = "";
  specialization: string = "";
  pengalaman: string = "";
  historyOfPractice: HistoryOfPracticeModel[];
  image: string = "";
  treatment: TreatmentModel[];
  education: EducationModel[];
  doctorSchedule: ScheduleModel[];
  isOnline: boolean = false;
}
