export class HistoryOfPracticeModel {
  medicalFacilityId: number = 0;
  medicalFacilityName: string = "";
  startDate: string = "";
  endDate: string = "";
  officeSpecialization: string = "";
  medicalFacilityCategoryName: string = "";
}
