export class ScheduleModel {
  doctorId: number = 0;
  medicalFacilityId: number = 0;
  slot: number = 0;
  day: string = "";
  scheduleStart: string = "";
  scheduleEnd: string = "";
}
