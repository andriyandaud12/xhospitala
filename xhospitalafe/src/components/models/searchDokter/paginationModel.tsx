import { config } from "../../configurations/config";

export class PaginationModelPage {
  pageNum: number = 0;
  rows: number = config.rowsPerPage[0];
}
