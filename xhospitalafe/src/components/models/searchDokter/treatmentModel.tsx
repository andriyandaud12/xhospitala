export class TreatmentModel {
  id: number = 0;
  doctorId: number = 0;
  name: string = "";
  is_delete: boolean = false;
}
