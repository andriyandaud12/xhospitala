import { BiodataModel } from "./biodataModel";
import { BloodGroupModel } from "./bloodGroupModel";

export class CustomerModel {
    constructor() {
        this.biodata = new BiodataModel;
        this.bloodGroup = new BloodGroupModel;
    }
    id: number = 0;
    biodataId: number = 0;
    biodata: BiodataModel;
    dob: Date= new Date() ;
    gender: string = "";
    bloodGroupId: number = 0;
    bloodGroup: BloodGroupModel;
    rhesusType: string = "";
    height: number = 0;
    weight: number = 0;
    is_delete: boolean = false;
}


