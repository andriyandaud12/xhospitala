export class PaginationModel {
  pageNum: number = 0;
  rows: number = 0;
  search: string = "";
  orderBy: string = "";
  sort: number = 0;
  pages: number = 0;
}
