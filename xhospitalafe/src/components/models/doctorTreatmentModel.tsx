import { DoctorModel } from "./doctorModel";

export class DoctorTreatmentModel {
    constructor() {
        this.doctorModel = new DoctorModel;;
    }
    id: number = 0;
    doctorModelId: number = 0;
    doctorModel: DoctorModel;
    name: string = "";
    is_delete: boolean = false;
}