import { DoctorModel } from "./doctorModel";
import { MedicalFacilityModel } from "./medicalFacilityModel";

export class DoctorOfficeModel {
    constructor() {
        this.doctor = new DoctorModel;
        this.medicalFacility = new MedicalFacilityModel;
    }
    id: number = 0;
    doctorId: number = 0;
    doctor: DoctorModel;
    medicalFacilityId: number = 0;
    medicalFacility: MedicalFacilityModel;
    specialization: string = "";
    startDate: Date | null = null;
    endDate: Date | null = null;
    is_delete: boolean = false;
}
