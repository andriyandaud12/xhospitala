import { LocationModel } from "./locationModel";
import { MedicalFacilityCategoryModel } from "./medicalFacilityCategoryModel";

export class MedicalFacilityModel {
    constructor() {
        this.location = new LocationModel;
        this.medicalFacilityCategory = new MedicalFacilityCategoryModel;
    }
    id: number = 0;
    name: string = "";
    medicalFacilityCategoryId: number = 0;
    medicalFacilityCategory: MedicalFacilityCategoryModel;
    locationId: number = 0;
    location: LocationModel;
    fullAddress: string = "";
    email: string = "";
    phoneCode: string = "";
    phone: string = "";
    fax: string = "";
    is_delete: boolean = false;
}
