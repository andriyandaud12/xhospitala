import { DoctorModel } from "./doctorModel";
import { MedicalFacilityScheduleModel } from "./medicalFacilityScheduleModel";

export class DoctorOfficeScheduleModel {
    constructor() {
        this.doctor = new DoctorModel;
        this.medicalFacilitySchedule = new MedicalFacilityScheduleModel;
    }

    id: number = 0;
    doctorId: number = 0;
    doctor: DoctorModel;
    medicalFacilityScheduleId: number = 0;
    medicalFacilitySchedule: MedicalFacilityScheduleModel;
    slot: number = 0;
    is_delete: boolean = false;
  }
