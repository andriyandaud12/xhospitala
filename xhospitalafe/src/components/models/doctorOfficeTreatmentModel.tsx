import { DoctorTreatmentModel } from "./doctorTreatmentModel";
import { DoctorOfficeModel } from "./doctorOfficeModel";

export class DoctorOfficeTreatmentModel {
    constructor() {
        this.doctorTreatment = new DoctorTreatmentModel;
        this.doctorOffice = new DoctorOfficeModel;
    }
    id: number = 0;
    doctorTreatmentModelId: number = 0;
    doctorTreatment: DoctorTreatmentModel;
    doctorOfficeModelId: number = 0;
    doctorOffice: DoctorOfficeModel;
}
