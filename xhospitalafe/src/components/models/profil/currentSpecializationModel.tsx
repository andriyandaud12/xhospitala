import { SpecializationModel } from "../specializationModel";

export class CurentSpecializationModel {
  constructor() {
    this.specialization = new SpecializationModel();
  }
  id: number = 0;
  doctorId: number = 0;
  specializationId: number = 0;
  is_delete: boolean = false;
  specialization: SpecializationModel;
}
