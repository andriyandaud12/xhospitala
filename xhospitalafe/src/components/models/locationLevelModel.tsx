export class LocationLevelModel {
    id: number = 0;
    name: string = "";
    abbreviation: string = "";
    is_delete: boolean = false;
  }