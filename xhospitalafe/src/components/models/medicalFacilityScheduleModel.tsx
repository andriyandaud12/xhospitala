import { MedicalFacilityModel } from "./medicalFacilityModel";

export class MedicalFacilityScheduleModel {
    constructor() {
        this.medicalFacility= new MedicalFacilityModel;
    }

    id: number = 0;
    medicalFacilityId: number = 0;
    medicalFacility: MedicalFacilityModel;
    day: string = "";
    timeScheduleStart: string = "";
    timeScheduleEnd: string = "";
    is_delete: boolean = false;
  }