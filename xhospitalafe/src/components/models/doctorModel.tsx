import { BiodataModel } from "./biodataModel";

export class DoctorModel {
    constructor() {
        this.biodata = new BiodataModel
    }
    id: number = 0;
    biodata: BiodataModel;
    str: string = "";
}