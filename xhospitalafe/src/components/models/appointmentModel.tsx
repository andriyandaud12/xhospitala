import { CustomerModel } from "./customerModel";
import { DoctorOfficeModel } from "./doctorOfficeModel";
import { DoctorOfficeScheduleModel } from "./doctorOfficeScheduleModel";
import { DoctorOfficeTreatmentModel } from "./doctorOfficeTreatmentModel";

export class AppointmentModel {
    constructor() {
        this.customer = new CustomerModel;
        this.doctorOffice = new DoctorOfficeModel;
        this.doctorOfficeSchedule = new DoctorOfficeScheduleModel;
        this.doctorOfficeTreatment = new DoctorOfficeTreatmentModel;
    }
    id: number = 0;
    customerId: number = 0;
    customer: CustomerModel;
    doctorOfficeId: number = 0;
    doctorOffice: DoctorOfficeModel;
    doctorOfficeScheduleId: number = 0;
    doctorOfficeSchedule: DoctorOfficeScheduleModel;
    doctorOfficeTreatmentId: number = 0;
    doctorOfficeTreatment: DoctorOfficeTreatmentModel;
    appointmentDate: Date | null = null;
}