import { CustomerRelationModel } from "./RelationModel";
import { BiodataModel } from "./biodataModel";
import { BloodGroupModel } from "./bloodGroupModel";
import { CustomerModel } from "./customerModel";

export class PasienModel {
  constructor() {
    this.biodata = new BiodataModel();
    this.bloodGroup = new BloodGroupModel();
    this.customerRelation = new CustomerRelationModel();
    this.customer = new CustomerModel();
  }
  id: number = 0;
  parentBiodataId: number = 0;
  customer: CustomerModel;
  customerId: number = 0;
  biodata: BiodataModel;
  bloodGroup: BloodGroupModel;
  bloodGroupId: number = 0;
  customerRelation: CustomerRelationModel;
  customerRelationId: number = 0;
  is_delete: boolean = false;
  
  // dob: string = "";
  // gender:string=""
  // rhesusType:string=""
  // height:number=0
  // weight:number=0
}
