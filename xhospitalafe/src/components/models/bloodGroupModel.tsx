export class BloodGroupModel {
    id: number = 0;
    code: string = "";
    description: string = "";
    is_delete: boolean = false;
  }
  