import React from 'react';

import { LocationModel } from '../models/locationModel';
import { LocationLevelModel } from '../models/locationLevelModel';
import { Ecommand } from '../enums/eCommand';
import { LocationService } from '../services/locationService'; 


interface IProps {
    locationLevels: LocationLevelModel[];
    locations:  LocationModel[];
    locationByLevels:  LocationModel[];
    location: LocationModel;
    command: Ecommand;
    changeHandler: any;
    errorAlerts: any;
}

interface IState {
}

export default class Form extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    

    render() {
        const { locationLevels, locations, location, command, changeHandler, 
            errorAlerts, locationByLevels } = this.props;
        return (
            
                <form>
                    <div>
                    {command === Ecommand.ChangeStatus ? (
                        <div className="flex items-start mb-6">
                        <div className="flex items-center h-5">
                        <p>Apakah anda yakin ingin menghapus lokasi '{location.name}'</p>
                        {!errorAlerts.Location?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Location?.message}
              </strong>
            ) : null}
                        </div>
                    </div>
                    ) : (<>
                        <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Level Lokasi*</label>
                        <select
                            id="locationLevelId"
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            required
                            value={location.locationLevelId}
                            onChange={
                                changeHandler('locationLevelId')
                              }
                        >
                            <option selected>---Pilih---</option>
                            {locationLevels?.map((o: LocationLevelModel) => (
                                <option value={o.id}>
                                    {o.name} 
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Wilayah</label>
                        <select
                            id="locationId"
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            required
                            value={location.parentId}
                            onChange={changeHandler('parentId')}
                        >
                            <option selected value="null">---Pilih---</option>
                            {locations?.map((o: LocationModel) => (
                                <option value={o.id}>
                                    {o.location?.locationLevel?.abbreviation && o.location?.name ? o.location?.locationLevel?.abbreviation + " " + o.location?.name + ", " : "N/A"}
                                    {o.location?.location?.locationLevel?.abbreviation && o.location?.location?.name ? o.location?.location?.locationLevel?.abbreviation + " " + o.location?.location?.name : ""}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama*</label>
                        <input type="text" id="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required value={location.name} onChange={changeHandler('name')} />
                        {!errorAlerts.Name?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
                    </div>

 

                    </>)}
                    </div>


                               </form>
        )
    }
}