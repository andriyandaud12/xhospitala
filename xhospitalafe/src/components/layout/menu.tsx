import React from "react";
import Cards from "./cards";
import { UserModel } from "../models/userModel";

interface IProps {
  user: UserModel;
}
interface IState {}

export default class Menu extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  render() {
    const { user } = this.props;
    return (
      <div className="text-white bg-black">
        <div>
          <Cards user={user} />
        </div>
      </div>
    );
  }
}
