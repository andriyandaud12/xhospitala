import React from "react";
import Typed from "react-typed";
import Analytics from "./analytics";
import Newsletter from "./newsletter";
import Cards2 from "./cards2";

interface IProps {}
interface IState {}

export default class Hero extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="text-white bg-black">
        <div className="max-w-[800px] mt-[-96px] w-full h-screen mx-auto text-center flex flex-col justify-center">
          <p className="text-[#00df9a] font-bold p-2">
            Personalized Doctor Consultations from Home
          </p>
          <h1 className="md:text-5xl sm:text-4xl text-3xl font-bold md:py-6">
            Empower your well-being.
          </h1>
          <div className="flex justify-center items-center">
            <p className="md:text-xl sm:text-xl text-xs font-bold py-4">
              A new era of healthcare where you can connect with experienced
            </p>
            <Typed
              className="md:text-xl sm:text-xl text-xs font-bold md:pl-4 pl-2"
              strings={["Cardiology", "Dermatology", "Orthopedics"]}
              typeSpeed={120}
              backSpeed={140}
              loop
            />
          </div>
          <p className="md:text-xl text-xl font-bold text-gray-500">
            Take control of your well-being. Start your journey to better health
            today.
          </p>
          <button className="bg-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto py-3 text-black">
            Get Started
          </button>
        </div>
        <div>
          <Cards2 />
        </div>
        {/* <div>
          <Analytics />
        </div> */}
        <div>
          <Newsletter />
        </div>
      </div>
    );
  }
}
