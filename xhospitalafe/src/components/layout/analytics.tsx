import React from "react";
import MedicalBanner from "../../assets/MedicalBanner.jpg";

interface IProps {}
interface IState {}

export default class Analytics extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="w-full bg-white py-16 px-4">
        <div className="max-w-[1240px] mx-auto grid md:grid-cols-2">
          <img className="w-[500px] mx-auto my-4" src={MedicalBanner} alt="/" />
          <div className="flex flex-col justify-center">
            <p className="text-[#00df9a] font-bold ">
              MEDICAL DATA ANALYTICS DASHBOARD
            </p>
            <h1 className="md:text-4xl sm:text-3xl text-2xl font-bold py-2 text-black">
              Medical Data Management Center
            </h1>
            <p className="text-black text-justify">
              Manage Health Information Centrally In an effort to improve
              healthcare, we are dedicated to presenting the Medical Analytics
              Panel. Understanding that every detail matters, we are committed
              to providing well-managed health data. With the latest technology,
              we ensure accurate and reliable information, enabling efficient
              health monitoring and rapid decision making.
            </p>
            <button className="bg-black text-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto md:mx-0 py-3">
              Get Started
            </button>
          </div>
        </div>
      </div>
    );
  }
}
