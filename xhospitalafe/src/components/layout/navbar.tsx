import React from "react";
import { AiOutlineClose, AiOutlineMenu } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";
import { Link, Route, Routes } from "react-router-dom";
import Hero from "./hero";
import Authentiaction from "../auth/authentiaction";
import { Location } from "../location";
import { LocationLevel } from "../locationLevel";
import Pasien from "../pasien/pasien/pasien";
import Relation from "../pasien/relation/relation";
import BloodGroup from "../pasien/bloodGroup/bloodGroup";
import SearchResult from "../doctor/searchResult";
import { SearchResultModel } from "../models/searchDokter/searchResultModel";
import DetailDokter from "../doctor/detailDokter";
import ProfilDoctor from "../doctor/profilDoctor/profilDoctor";
import { ProfilDoctorService } from "../services/profilDoctor/profilDoctorService";
import { withRouter } from "./withRouter";
import { UserModel } from "../models/userModel";
import { AuthService } from "../services/authService/authService";
import Menu from "./menu";
import Header from "./header";
import AuthRegis from "../auth/authRegis";
import FormOtpRegis from "../auth/formOtpRegis";
import FormSearch from "../doctor/formSearch";
import { Ecommand } from "../enums/eCommand";
import { SearchService } from "../services/searchService/searchService";
import { SearchDataModel } from "../models/searchDokter/searchData";
import images from "../../assets/LogoProject.png";
import searchResult from "../doctor/searchResult";
import { TreatmentModel } from "../models/treatmentModel";

interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  user: UserModel;
  navigate: any;
}
interface IState {
  nav: boolean;
  showModal: boolean;
  showModalReg: boolean;
  showModalOTP: boolean;
  showModalSearch: boolean;
  account: SearchResultModel;
  users: UserModel;
  errorAlerts: any;
  treatmentProps: TreatmentModel[];
  searchDataState: SearchDataModel;
  command: Ecommand;
  // isSideBarOpen: boolean;
}
class Navbar extends React.Component<IProps, IState> {
  newSearch: SearchDataModel = {
    location: "",
    doctorName: "",
    specialization: "",
    treatment: "",
    pageNum: 1,
    rows: 6,
    pages: 0,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      nav: false,
      showModal: false,
      showModalSearch: false,
      showModalReg: false,
      showModalOTP: false,
      account: new SearchResultModel(),
      users: new UserModel(),
      errorAlerts: { Name: false },
      searchDataState: this.newSearch,
      command: Ecommand.create,
      treatmentProps: [],
    };
  }
  componentDidMount(): void {
    this.loadImage();
  }
  loadImage = async () => {
    const { user, logged } = this.props;
    await ProfilDoctorService.getDoctorByIdForProfil()
      .then((result) => {
        if (result.success) {
          this.setState({
            account: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  handleNav = () => {
    this.setState({ nav: !this.state.nav });
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
  };
  setShowModalReg = (val: boolean) => {
    this.setState({
      showModalReg: val,
    });
  };
  setShowModalOTP = (val: boolean) => {
    this.setState({
      showModalOTP: val,
    });
  };

  setShowModalSearch = (val: boolean) => {
    this.setState({
      showModalSearch: val,
    });
  };
  handleProfileImageChange = (newImageUrl: string) => {
    this.setState({
      account: {
        ...this.state.account,
        image: newImageUrl,
      },
    });
  };

  logoutBtn = () => {
    AuthService.logout();
    this.props.navigate("/");
    window.location.reload();
  };
  handleDaftarClick = () => {
    // Show the AuthRegis modal
    this.setShowModalReg(true);
  };

  // Function to handle the OTP submission and show FormOtpRegis modal
  handleOtpSubmit = () => {
    // You may perform OTP submission logic here

    // Once OTP is submitted successfully, show the FormOtpRegis modal
    this.setShowModalReg(false); // Close the AuthRegis modal
    this.setShowModalOTP(true); // Show the FormOtpRegis modal
  };

  serachCommand = () => {
    this.setState({
      command: Ecommand.search,
      showModalSearch: true,
    });
  };

  submitSearch = () => {
    this.setState({
      errorAlerts: {
        Name: {
          valid:
            this.state.searchDataState.specialization.trim().length > 0 ||
            this.state.searchDataState.specialization !== "0",
          message: "Spesialisasi tidak boleh kosong",
        },
      },
    });
    if (
      this.state.searchDataState.specialization.trim().length === 0 ||
      this.state.searchDataState.specialization === "0" ||
      this.state.searchDataState.specialization === ""
    ) {
      return;
    }
    if (this.state.command === Ecommand.search) {
      const { searchDataState } = this.state;
      SearchService.saveSearchDataInput(searchDataState);
      this.setState({
        showModalSearch: false,
      });
      this.props.navigate("/CariDokter");
      window.location.reload();
    }
  };
  resetSearchData = () => {
    // Reset the state values
    this.setState({
      searchDataState: this.newSearch,
      errorAlerts: { Name: false },
    });
  };

  handleSearchData = (name: any) => (event: any) => {
    if (name == "specialization") {
      if (event.target.value !== "") {
        SearchService.getAllTreatment(event.target.value).then((result) => {
          this.setState({
            treatmentProps: result.result,
          });
        });
      } else {
        this.setState({
          treatmentProps: [],
        });
      }
    }
    this.setState({
      searchDataState: {
        ...this.state.searchDataState,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  render() {
    const { logged, user, changeLoggedHandler } = this.props;
    const {
      nav,
      showModal,
      account,
      showModalOTP,
      showModalReg,
      showModalSearch,
      errorAlerts,
      searchDataState,
      treatmentProps,
    } = this.state;
    return (
      <div>
        <Header user={user} logged={logged} />
        <div className="bg-black">
          <div className="flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-white">
            {logged ? (
              <h1
                className={`flex items-center w-full text-3xl font-bold text-[#00df9a] ${
                  nav ? `collapse` : `visible`
                }`}
              >
                <Link to={"/Menu"}>Medicare.</Link>
              </h1>
            ) : (
              <h1
                className={`flex items-center w-full text-3xl font-bold text-[#00df9a] ${
                  nav ? `collapse` : `visible`
                }`}
              >
                <Link to={"/"}>Medicare.</Link>
              </h1>
            )}

            <ul className="hidden md:flex flex-nowrap">
              <li className="flex">
                {logged ? (
                  <Link to={"/Menu"} className="p-4">
                    Home
                  </Link>
                ) : (
                  <Link to={"/"} className="p-4">
                    Home
                  </Link>
                )}
              </li>
              <li className="flex p-4">
                <span
                  className="cursor-pointer"
                  onClick={this.serachCommand}
                  style={{ whiteSpace: "nowrap" }}
                >
                  Find Doctor
                </span>
              </li>
            </ul>
            {logged ? (
              <>
                <div className="p-4 cursor-pointer" onClick={this.logoutBtn}>
                  Logout
                </div>
                <div
                  onClick={this.handleNav}
                  className="block cursor-pointer bg-black"
                >
                  {nav ? (
                    <AiOutlineClose size={20} />
                  ) : (
                    <AiOutlineMenu size={20} />
                  )}
                </div>
              </>
            ) : (
              <>
                <div
                  className="p-4 cursor-pointer"
                  onClick={() => this.setShowModal(true)}
                >
                  Login
                </div>
                <div className="p-4" onClick={() => this.setShowModalReg(true)}>
                  Register
                </div>
              </>
            )}
            {logged ? (
              <div className="flex p-6 items-center justify-center flex-shrink-0 ">
                <Link to={`/ProfileDokter/${logged ? user.biodataId : 0}`}>
                  <img
                    src={account.image}
                    alt={`${account.fullName}'s profile`}
                    className="w-10 h-10 object-cover rounded-full border border-blue-700 p-1 mb-2"
                  />
                </Link>
              </div>
            ) : null}

            <div
              className={
                nav
                  ? "fixed left-0 top-0 w-[15%] h-full border-r border-r-gray-900 bg-[#000300] ease-in-out duration-500"
                  : "ease-in-out duration-500 fixed left-[-100%]"
              }
            >
              <h1 className="w-full text-3xl font-bold text-[#00df9a] m-4">
                Medicare.
              </h1>

              {user.menuRole.map((o) => {
                return (
                  <a
                    href="#"
                    className="items-centerspace-x-3 rounded-md block p-4 hover:bg-gray-700"
                  >
                    <span className="text-gray-100">
                      <Link to={`/${o}`}>{o}</Link>
                    </span>
                  </a>
                );
              })}
            </div>
          </div>
        </div>
        {showModal ? (
          <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="relative">
              <IoMdClose
                className="absolute top-[17%] right-[27%] p-2 cursor-pointer z-50 -translate-y-1 -translate-x-1 md:block hidden"
                size={40}
                onClick={() => this.setShowModal(false)}
              />
              <Authentiaction
                loadImage={this.loadImage}
                setShowModal={this.setShowModal}
                changeLoggedHandler={changeLoggedHandler}
                logged={logged}
              />
            </div>
          </div>
        ) : null}
        {showModalReg ? (
          <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-auto outline-none focus:outline-none">
            <div>
              <IoMdClose
                className="absolute top-[15%] right-[22%] p-2 cursor-pointer z-50"
                size={50}
                onClick={() => this.setShowModalReg(false)}
              />
              <AuthRegis
                setShowModal={this.setShowModalReg}
                changeLoggedHandler={changeLoggedHandler}
                logged={logged}
                handleOtpSubmit={this.handleOtpSubmit} // Pass the callback function to AuthRegis
              />
            </div>
          </div>
        ) : null}

        {showModalOTP ? (
          <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-auto outline-none focus:outline-none">
            <div>
              <IoMdClose
                className="absolute top-[15%] right-[22%] p-2 cursor-pointer z-50"
                size={50}
                onClick={() => this.setShowModalOTP(false)}
              />
              <FormOtpRegis
                setShowModal={this.setShowModalOTP}
                changeLoggedHandler={changeLoggedHandler}
                logged={logged}
              />
            </div>
          </div>
        ) : null}
        {showModalSearch ? (
          <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="w-full h-screen flex justify-center items-center">
              <div className="grid grid-cols-1 md:grid-cols-2 m-auto h-[550px] shadow-lg shadow-gray-600 sm:max-w-[900px] bg-[#f5f5f5] rounded-lg">
                {/* Image logo left */}
                <div className="w-full h-[550px] hidden md:block">
                  <img
                    className="w-full h-full rounded-s-lg"
                    src={images}
                    alt="/"
                  />
                </div>
                {/* form search right*/}
                <div className="flex justify-center items-center h-auto px-4 text-center">
                  <div className="w-full max-w-md dark:bg-gray-900 rounded-lg overflow-hidden overflow-y-auto flex flex-col justify-between">
                    {/* Header Form Search */}
                    <div>
                      <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                        <div></div>
                        <h3 className="text-black ms-8 md:text-3xl sm:text-4xl text-3xl font-bold md:py-6text-3xl dark:text-white">
                          Cari Dokter
                        </h3>
                        <button onClick={() => this.setShowModalSearch(false)}>
                          <AiOutlineClose className=" text-black text-3xl" />
                        </button>
                      </div>
                      <div className="text-gray-600 md:text-1xl sm:text-1xl text-1xl font-light md:py-6text-3xl dark:text-white border-b ">
                        Masukkan Minimal 1 kata kunci untuk pencarian dokter
                        anda
                      </div>
                    </div>
                    {/* Form Search */}
                    <div className="p-3 overflow-y-auto max-h-96">
                      <FormSearch
                        treatmentProps={treatmentProps}
                        errorAlerts={errorAlerts}
                        searchResultData={searchDataState}
                        onSearch={this.handleSearchData}
                      />
                    </div>
                    {/* Button */}
                    <div
                      className="flex items-center justify-between border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        onClick={() => this.resetSearchData()}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8  text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-gray-300"
                      >
                        Atur ulang
                      </button>
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                        onClick={() => this.submitSearch()}
                      >
                        Cari
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <Routes>
          <Route path="/" element={<Hero />} />
          <Route path="/Menu" element={<Menu user={user} />} />
          <Route
            path="/Login"
            element={
              <Authentiaction
                user={user}
                changeLoggedHandler={changeLoggedHandler}
                logged={logged}
              />
            }
          />
          <Route path="/CariDokter" element={<SearchResult />} />
          <Route path="/DetailsDokter" element={<DetailDokter />} />
          <Route path="/DetailsDokter/:biodataId" element={<DetailDokter />} />
          {/* <Route
            element={
              <ProtectedRoute
                isAllowed={user.menuRole.indexOf("Location") > -1}
              />
            }
          > */}
          <Route path="/Location" element={<Location />} />
          <Route path="/LocationLevel" element={<LocationLevel />} />
          {/* </Route> */}
          {/* <Route
            element={
              <ProtectedRoute
                isAllowed={user.menuRole.indexOf("DaftarPasien") > -1}
              />
            }
          > */}
          <Route path="/DaftarPasien" element={<Pasien />} />
          {/* </Route> */}
          {/* <Route
            element={
              <ProtectedRoute
                isAllowed={user.menuRole.indexOf("DaftarRelation") > -1}
              />
            }
          > */}
          <Route path="/DaftarRelation" element={<Relation />} />
          {/* </Route> */}
          {/* <Route
            element={
              <ProtectedRoute
                isAllowed={user.menuRole.indexOf("DaftarBloodGroup") > -1}
              />
            }
          > */}
          <Route path="/DaftarBloodGroup" element={<BloodGroup />} />
          {/* </Route> */}
          {/* <Route
            element={
              <ProtectedRoute
                isAllowed={user.menuRole.indexOf("ProfileDokter") > -1}
              />
            }
          > */}
          {/* <Route
              path="/ProfileDokter"
              element={
                <ProfilDoctor
                  onProfileImageChange={this.handleProfileImageChange}
                />
              }
            /> */}
          <Route
            path="/ProfileDokter/:biodataId"
            element={
              <ProfilDoctor
                onProfileImageChange={this.handleProfileImageChange}
              />
            }
          />
          {/* </Route> */}
        </Routes>
      </div>
    );
  }
}

export default withRouter(Navbar);
