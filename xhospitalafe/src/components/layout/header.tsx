import React from "react";
import { UserModel } from "../models/userModel";

interface IProps {
  logged: boolean;
  user: UserModel;
}

interface IState {}

export default class Header extends React.Component<IProps, IState> {
  render() {
    const name = localStorage.getItem("nama");
    const { logged } = this.props;
    return (
      <div>
        <div className="bg-gray-800 p-3 flex flex-col lg:flex-row list-none lg:ml-auto">
          <div className="container mx-auto space-y-3">
            <h2 className="container text-sm text-white">
              Hi, {logged ? name : "Guest"}
            </h2>
          </div>
        </div>
      </div>
    );
  }
}
