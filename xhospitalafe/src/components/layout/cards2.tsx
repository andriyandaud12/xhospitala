import React from "react";
import Gambar1 from "../../assets/CariDokter.png";
import Gambar2 from "../../assets/Obat.png";
import Gambar3 from "../../assets/BMI.jpg";
import { Ecommand } from "../enums/eCommand";
import FormSearch from "../doctor/formSearch";
import { UserModel } from "../models/userModel";
import { AiOutlineClose } from "react-icons/ai";
import { SearchDataModel } from "../models/searchDokter/searchData";
import { SearchService } from "../services/searchService/searchService";
import { SearchResultModel } from "../models/searchDokter/searchResultModel";
import images from "../../assets/LogoProject.png";
import { withRouter } from "./withRouter";
import { TreatmentModel } from "../models/treatmentModel";

interface IProps {
  user: UserModel;
  navigate: any;
}
interface IState {
  showModal: boolean;
  command: Ecommand;
  showModalSearch: boolean;
  errorAlerts: any;
  searchDataState: SearchDataModel;
  resultStates: SearchResultModel[];
  treatmentProps: TreatmentModel[];
}

class Cards extends React.Component<IProps, IState> {
  newSearch: SearchDataModel = {
    location: "",
    doctorName: "",
    specialization: "",
    treatment: "",
    pageNum: 1,
    rows: 6,
    pages: 0,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      showModal: false,
      command: Ecommand.create,
      showModalSearch: false,
      resultStates: [],
      errorAlerts: { Name: false },
      searchDataState: this.newSearch,
      treatmentProps: [],
    };
  }
  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  searchCommand = () => {
    this.setState({
      command: Ecommand.search,
      showModalSearch: true,
    });
  };

  setShowModalSearch = (val: boolean) => {
    this.setState({
      showModalSearch: val,
    });
  };

  submitSearch = () => {
    this.setState({
      errorAlerts: {
        Name: {
          valid:
            this.state.searchDataState.specialization.trim().length > 0 ||
            this.state.searchDataState.specialization !== "0",
          message: "Spesialisasi tidak boleh kosong",
        },
      },
    });
    if (
      this.state.searchDataState.specialization.trim().length === 0 ||
      this.state.searchDataState.specialization === "0" ||
      this.state.searchDataState.specialization === ""
    ) {
      return;
    }
    if (this.state.command === Ecommand.search) {
      const { searchDataState } = this.state;
      SearchService.saveSearchDataInput(searchDataState);
      this.setState({
        showModalSearch: false,
      });
      this.props.navigate("/CariDokter");
    }
  };
  resetSearchData = () => {
    // Reset the state values
    this.setState({
      searchDataState: this.newSearch,
      errorAlerts: { Name: false },
    });
  };

  handleSearchData = (name: any) => (event: any) => {
    if (name == "specialization") {
      if (event.target.value !== "") {
        SearchService.getAllTreatment(event.target.value).then((result) => {
          this.setState({
            treatmentProps: result.result,
          });
        });
      } else {
        this.setState({
          treatmentProps: [],
        });
      }
    }
    this.setState({
      searchDataState: {
        ...this.state.searchDataState,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  render() {
    const {
      showModal,
      showModalSearch,
      searchDataState,
      errorAlerts,
      treatmentProps,
    } = this.state;
    return (
      <div className="relative w-full py-[5rem] px-4 bg-white text-black">
        <p className="font-bold uppercase mb-28 flex justify-center text-2xl">
          Health Service
        </p>
        <div className="max-w-[1240px] mx-auto grid md:grid-cols-3 gap-8">
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300">
            <img
              className="w-20 mx-auto mt-[-3rem] bg-white"
              src={Gambar1}
              alt="/"
            />
            <div>
              <a
                href="#"
                className="items-center space-x-3 rounded-md p-4 flex justify-center"
              >
                <span className="text-black">Find Doctor</span>
              </a>
            </div>
            <button
              className="bg-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3 flex justify-center"
              onClick={this.searchCommand}
            >
              Find Doctor
            </button>
          </div>
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300">
            <img
              className="w-20 mx-auto mt-[-3rem] bg-white"
              src={Gambar2}
              alt="/"
            />
            <div>
              <a
                href="#"
                className="items-center space-x-3 rounded-md p-4 flex justify-center"
              >
                <span className="text-black">Find Medicine</span>
              </a>
            </div>
            <button
              className="bg-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3 flex justify-center"
              onClick={this.searchCommand}
            >
              Find Medicine
            </button>
          </div>
          <div className="w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300">
            <img
              className="w-20 mx-auto mt-[-3rem] bg-white"
              src={Gambar3}
              alt="/"
            />
            <div>
              <a
                href="#"
                className="items-center space-x-3 rounded-md p-4 flex justify-center"
              >
                <span className="text-black">BMI Calendar</span>
              </a>
            </div>
            <button
              className="bg-[#00df9a] w-[200px] rounded-md font-medium my-6 mx-auto px-6 py-3 flex justify-center"
              onClick={this.searchCommand}
            >
              BMI Calendar
            </button>
          </div>
        </div>
        {showModalSearch ? (
          <div className="items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="w-full h-screen flex justify-center items-center">
              <div className="grid grid-cols-1 md:grid-cols-2 m-auto h-[550px] shadow-lg shadow-gray-600 sm:max-w-[900px] bg-[#f5f5f5] rounded-lg">
                {/* Image logo left */}
                <div className="w-full h-[550px] hidden md:block">
                  <img
                    className="w-full h-full rounded-s-lg"
                    src={images}
                    alt="/"
                  />
                </div>
                {/* form search right*/}
                <div className="flex justify-center items-center h-auto px-4 text-center">
                  <div className="w-full max-w-md dark:bg-gray-900 rounded-lg overflow-hidden overflow-y-auto flex flex-col justify-between">
                    {/* Header Form Search */}
                    <div>
                      <div className="flex items-start justify-between p-5 border-gray-300 dark:border-gray-700 rounded-t">
                        <div></div>
                        <h3 className="text-black ms-8 md:text-3xl sm:text-4xl text-3xl font-bold md:py-6text-3xl dark:text-white">
                          Cari Dokter
                        </h3>
                        <button onClick={() => this.setShowModalSearch(false)}>
                          <AiOutlineClose className=" text-black text-3xl" />
                        </button>
                      </div>
                      <div className="text-gray-600 md:text-1xl sm:text-1xl text-1xl font-light md:py-6text-3xl dark:text-white border-b ">
                        Masukkan Minimal 1 kata kunci untuk pencarian dokter
                        anda
                      </div>
                    </div>
                    {/* Form Search */}
                    <div className="p-3 overflow-y-auto max-h-96">
                      <FormSearch
                        treatmentProps={treatmentProps}
                        errorAlerts={errorAlerts}
                        searchResultData={searchDataState}
                        onSearch={this.handleSearchData}
                      />
                    </div>
                    {/* Button */}
                    <div
                      className="flex items-center justify-between border-t border-solid border-blueGray-200 rounded-b"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        onClick={() => this.resetSearchData()}
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8  text-black m:p-6 rounded-lg focus:shadow-outline hover:bg-gray-300"
                      >
                        Atur ulang
                      </button>
                      <button
                        className="md:text-1xl sm:text-1xl text-1xl font-bold px-7 h-8 my-2 mx-8 text-green-100 m:p-6 rounded-lg focus:shadow-outline bg-[#00df9a] hover:bg-green-900"
                        onClick={() => this.submitSearch()}
                      >
                        Cari
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default withRouter(Cards);
