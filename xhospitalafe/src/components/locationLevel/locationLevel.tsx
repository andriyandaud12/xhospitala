import React from 'react';

import { LocationLevelModel } from "../models/locationLevelModel";
import { Ecommand } from "../enums/eCommand";
import { PaginationModel } from "../models/paginationModel";
import { LocationLevelService } from "../services/locationLevelService";
import { config } from "../configurations/config";
import { ValidationResult } from "../../validations/validationResult";
import Form from "./formLocationLevel";

interface IProps {
}

interface IState {
    locationLevels: LocationLevelModel[];
    locationLevel: LocationLevelModel;
    showModal: boolean;
    command: Ecommand;
    pagination: PaginationModel;
    errorAlerts: any;
    showSuccessModal: boolean;
}

export default class LocationLevel extends React.Component<IProps, IState> {
  newPagination: PaginationModel ={
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: '',
    orderBy: '',
    sort: 0,
    pages: 0
  }

    constructor(props: IProps) {
        super(props);
        this.state = {
            locationLevels: [],
            locationLevel: new LocationLevelModel(),
            showModal: false,
            command: Ecommand.create,
            pagination: this.newPagination,
            errorAlerts: { Name: false },
            showSuccessModal: false,
        }
    }

    componentDidMount(): void {
        this.loadLocationLevels();
    }

    loadLocationLevels = async () => {
        const {pagination} = this.state;
        const result = await LocationLevelService.getPagination(pagination);
        // const result = await LocationLevelService.getAll();
        if (result.success) {
            this.setState({
                locationLevels: result.result,
                pagination: {
                    ...this.state.pagination,
                    pages: result.pages
                }
            });
        } else {
            alert('Error: ' + result.result);
        }
    }

    setShowModal = (val: boolean) => {
        this.setState({
            showModal: val
        })
        console.log(this.state.showModal);
    }

    handleInputData = (name: any) => (event: any) => {
      this.setState({
        locationLevel: {
          ...this.state.locationLevel,
          [name]: event.target.value,
        },
      });
      this.setState({
        errorAlerts: {
          ...this.state.errorAlerts,
          [name]: !event.target.value,
        },
      });
    };

    

    changeHandler = (name: any) => (event: any) => {
        this.setState({
            locationLevel: {
                ...this.state.locationLevel,
                [name]: event.target.value
            }
        });
        this.setState({
          errorAlerts: {
            ...this.state.errorAlerts,
            [name]: !event.target.value,
          },
        });
    }

   

    checkBoxHandler = (name: any) => (event: any) => {
        this.setState({
            locationLevel: {
                ...this.state.locationLevel,
                [name]: event.target.checked
            }
        })
    }

    createCommand = () => {
        this.setState({
            showModal: true,
            locationLevel: new LocationLevelModel(),
            command: Ecommand.create,
        })
        // this.setShowModal(true);
    }

    updateCommand = async (id: number) => {
        await LocationLevelService.getById(id)
            .then(result => {
                if (result.success) {
                    this.setState({
                        showModal: true,
                        locationLevel: result.result,
                        command: Ecommand.update
                    })
                } else {
                    alert('Error result ' + result.result);
                }
            })
            .catch(error => {
                alert('Error error' + error);
            })
    }

    changeStatusCommand = async (id: number) => {
        await LocationLevelService.getById(id)
            .then(result => {
                if (result.success) {
                    this.setState({
                        showModal: true,
                        locationLevel: result.result,
                        command: Ecommand.ChangeStatus
                    })
                } else {
                    alert('Error result ' + result.result);
                }
            })
            .catch(error => {
                alert('Error error' + error);
            })
    }

    changeRowsPerPage = (name: any) => (event: any) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                [name]: event.target.value
            }
        })
        new Promise(() => {
            setTimeout(() => {
                this.loadLocationLevels();
            }, 500)
        })
    }

    changeOrder = (column: string) => {
        this.setState({
          pagination: {
            ...this.state.pagination,
            orderBy: column,
          },
        });
        new Promise(() => {
          setTimeout(() => {
            this.loadLocationLevels();
          }, 500);
        });
      };
    

    changePage = (newPage: number) => {
        if (newPage >= 1 && newPage <= this.state.pagination.pages) {
          this.setState(
            {
              pagination: {
                ...this.state.pagination,
                pageNum: newPage,
              },
            },
            () => this.loadLocationLevels()
          );
        }
      };
    
    changeSearch = (name: any) => (event: any) => {
        this.setState({
            pagination: {
                ...this.state.pagination,
                [name]: event.target.value
            }
        })
    }
    

    submitHandler = async (event: any) => {
      const { command, locationLevel } = this.state;
        if (command == Ecommand.create) {
          event.preventDefault();
          this.setState({
            errorAlerts: {
              Name: {
                valid: this.state.locationLevel.name.length > 0,
                message: "Nama tidak boleh kosong",
              },
            },
          });
          if (this.state.locationLevel.name.length === 0) {
            return;
          }
    
          console.log(
            "Lolos ",
            this.state.errorAlerts,
            this.state.locationLevel.name.length
          );
            await LocationLevelService.post(this.state.locationLevel)
                .then(result => {
                    if (result.success) {
                        this.setState({
                            showModal: false,
                            locationLevel: new LocationLevelModel()
                        })
                        this.loadLocationLevels();
                    } else {
                        //alert('Error result ' + result.result);
                        this.setState({
                          errorAlerts: ValidationResult.Validate(result.result),
                        });
                    }
                })
                .catch(error => {
                    alert('Error error' + error);
                })
        } else if (command == Ecommand.update) {
          event.preventDefault();
          this.setState({
            errorAlerts: {
              Name: {
                valid: this.state.locationLevel.name.length > 0,
                message: "Nama tidak boleh kosong",
              },
            },
          });
          if (this.state.locationLevel.name.length === 0) {
            return;
          }
            await LocationLevelService.update(locationLevel.id, locationLevel)
                .then(result => {
                    if (result.success) {
                        this.setState({
                            showModal: false,
                            locationLevel: new LocationLevelModel()
                        })
                        this.loadLocationLevels();
                    } else {
                        alert('Error result ' + result.result);
                    }
                })
                .catch(error => {
                    alert('Error error' + error);
                })
        } else if (command == Ecommand.ChangeStatus) {
            await LocationLevelService.changeStatus(locationLevel.id, true)
            .then(result => {
                if (result.success) {
                    this.setState({
                        showModal: false,
                        locationLevel: locationLevel
                    })
                    this.loadLocationLevels();
                    console.log("error: " + result);
                } else {
                    //alert('Error result ' + result.result);
                    console.log("error: " + result.success + result.result);
                    this.setState({
                      errorAlerts: ValidationResult.Validate(result.result),
                    });
                }
            })
            .catch(error => {
                alert('Error error' + error);
            })
        }
    }

    render() {
        const { locationLevels, locationLevel, showModal, command, pagination, errorAlerts } = this.state;
        const loopPages = () => {
            const { pagination } = this.state;
            const { pageNum, pages } = pagination;
          
            return (
              <div className="flex items-center">
                <button
                  className="mx-1 p-1 bg-gray-200 text-gray-800 rounded-md"
                  onClick={() => this.changePage(pageNum - 1)}
                  hidden={pageNum === 1}
                >
                  Prev
                </button>
                <span className="mx-2 text-gray-800">
                  Page {pageNum} of {pages ? pages : 1}
                </span>
                <button
                  className="mx-1 p-1 bg-gray-200 text-gray-800 rounded-md"
                  onClick={() => this.changePage(pageNum + 1)}
                  hidden={pageNum === pages}
                >
                  Next
                </button>
              </div>
            );
          };
        return (
            <div>
                <div className="text-left text-3xl pt-5">Level Lokasi <span>{JSON.stringify(locationLevel)}</span></div>
                
                <span>{JSON.stringify(pagination)}</span>

  <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 bg-white dark:bg-gray-800 rounded shadow-md">
    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
    <tr className="border-b dark:border-gray-700">
        <th colSpan={1} scope='col' className="ppx-6 py-3 w-14 h-14">
             <input 
             placeholder='cari berdasarkan nama'
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            onChange={this.changeSearch('search')} ></input>
          </th>
        <th scope="col" className="px-6 py-3 w-14 h-14">
        <button
                  className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadLocationLevels()}
                >
                  Filter
                </button>

        </th>
        <th scope="col" className="px-6 py-3 w-14 h-14">
        <button className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800" onClick={() => this.createCommand()}>Create New</button>
</th>
      </tr>
      <tr className="border-b dark:border-gray-700">
      <th
                scope="col"
                className="px-6 py-3 w-14 h-14 "
                
              >
                Nama
              </th>

        <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
              
>
          Nama Singkat
        </th>
        <th scope="col" className="px-6 py-3 w-14 h-14">
          #
        </th>
      </tr>
    </thead>
    <tbody>
      {locationLevels?.map((o: LocationLevelModel) => (
        <tr key={o.id} className="border-b dark:border-gray-800">
          <td className="px-6 py-4">{o.name}</td>
          <td className="px-6 py-4">{o.abbreviation}</td>
         
          <td className="px-4 py-4" colSpan={3}>
  <div className="inline-flex" role="group" aria-label="Button group">
    <button className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800" onClick={() => this.updateCommand(o.id)}>
      Edit
    </button>
    <button className="h-8 px-4 text-blue-100 transition-colors duration-150 bg-red-700 rounded-r-lg focus:shadow-outline hover:bg-red-800" onClick={() => this.changeStatusCommand(o.id)}>Hapus</button>
  </div>
</td>
        </tr>
      ))}
    </tbody>
    <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      <tr className="border-b dark:border-gray-700">
        <th scope="col" className="px-6 py-3 w-14 h-14">
          Rows per page
        </th>
        <th scope="col" className="px-6 py-3 w-14 h-14">
          <select
            id="categoryId"
            
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            onChange={this.changeRowsPerPage('rows')}
          >
            {config.rowsPerPage.map((o: number) => (
              <option value={o} key={o}>
                {o}
              </option>
            ))}
          </select>
        </th>
        <th>    </th>
        <th scope="col" className="px-6 py-3 w-14 h-14">
        {loopPages()}
        </th>
      </tr>
    </tfoot>
  </table>
                {
  showModal ? (
    <div className="fixed inset-0 z-50 overflow-hidden flex items-center justify-center">
      <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
        <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
        <h3 className="text-3xl text-gray-900 dark:text-white">{command === Ecommand.ChangeStatus ? 'Hapus Lokasi' : command.valueOf()}</h3>
          <button
            className="text-black float-right"
            onClick={() => this.setShowModal(false)}
          >
            <span className="text-black opacity-70 h-6 w-6 text-xl block bg-gray-400 dark:bg-gray-700 py-0 rounded-full">×</span>
          </button>
        </div>
        <div className="p-6 overflow-y-auto max-h-96">
          {/* Adjust max-h-96 to the maximum height you want before scrolling starts */}
          <Form
            locationLevel={locationLevel}
            locationLevels={locationLevels}
            command={command}
            errorAlerts={errorAlerts}
            changeHandler={this.changeHandler}
          />
        </div>
        <div className="flex items-center justify-end p-6 border-t border-gray-300 dark:border-gray-700 rounded-b">
          <button className="mx-2 h-10 px-4 text-white transition-colors duration-300 bg-green-600 rounded-lg focus:outline-none hover:bg-green-700" onClick={() => this.setShowModal(false)}>
            Close
          </button>
          <button
                                    className={`mx-2 h-10 px-4 text-white transition-colors duration-300 
                                    ${command === Ecommand.ChangeStatus ? 'bg-red-600 hover:bg-red-700' : 'bg-blue-600 hover:bg-blue-700'}  
                                    rounded-lg focus:outline-non`}
                                    onClick={(event: React.SyntheticEvent) =>
                                      this.submitHandler(event)
                                    }
                                >
                                    {command === Ecommand.ChangeStatus ? 'Hapus' : 'Submit'}
                                </button>
        </div>
      </div>
    </div>
  ) : null
}
            </div>
        )
    }
}