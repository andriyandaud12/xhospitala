import React from 'react';

import { LocationModel } from '../models/locationModel';
import { LocationLevelModel } from '../models/locationLevelModel';
import { Ecommand } from '../enums/eCommand';


interface IProps {
    locationLevels: LocationLevelModel[];
    locationLevel: LocationLevelModel;
    command: Ecommand;
    changeHandler: any;
    errorAlerts: any;
}

interface IState {
}

export default class Form extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    render() {
        const { locationLevels, locationLevel, command, changeHandler, errorAlerts } = this.props;
        return (
                <form>
                    <div>
                    {command === Ecommand.ChangeStatus ? (
                        <div className="flex items-start mb-6">
                        <div className="flex items-center h-5">
                        <p>Apakah anda yakin ingin menghapus level lokasi '{locationLevel.name}'</p>
                        {!errorAlerts.LocationLevel?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.LocationLevel?.message}
              </strong>
            ) : null}
                        </div>
                    </div>
                    ) : (<>
                        
                    <div className="mb-6">
                    <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama*</label>
                        <input type="text" id="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required value={locationLevel.name} onChange={changeHandler('name')} />
                        {!errorAlerts.Name?.valid ? (
              <strong
                className="text-red-600 text-xs font-normal"
                id="title-error"
                role="alert"
              >
                {errorAlerts.Name?.message}
              </strong>
            ) : null}
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama Singkat</label>
                        <input type="text" id="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required value={locationLevel.abbreviation} onChange={changeHandler('abbreviation')} />
                    </div>
                    </>)}
                    </div>
                               </form>
        )
    }
}