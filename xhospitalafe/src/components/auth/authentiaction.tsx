import React from "react";
import images from "../../assets/LogoProject.png";
import { AuthenticationModel } from "../models/authenticationModel";
import { AuthService } from "../services/authService/authService";
import { withRouter } from "../layout/withRouter";
import { ValidationResult } from "../../validations/validationResult";
import { UserModel } from "../models/userModel";
import { AiOutlineEye } from "react-icons/ai";
import { FcGoogle } from "react-icons/fc";

interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  setShowModal: any;
  navigate: any;
  loadImage: () => void;
  user: UserModel;
}
interface IState {
  auth: AuthenticationModel;
  errorAlerts: any;
  message: string;
}

class Authentiaction extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      auth: new AuthenticationModel(),
      errorAlerts: { email: false, password: false },
      message: "",
    };
  }

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      auth: {
        ...this.state.auth,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  componentDidMount(): void {
    AuthService.logout();
  }

  handleSubmit = (event: any) => {
    event.preventDefault();
    this.setState({
      errorAlerts: {
        Email: {
          valid: this.state.auth.email.length > 0,
          message: "Email required",
        },
        Password: {
          valid: this.state.auth.password.length > 0,
          message: "Password required",
        },
      },
    });

    if (
      this.state.auth.email.length === 0 ||
      this.state.auth.password.length === 0
    ) {
      return;
    }

    const { auth } = this.state;
    const { changeLoggedHandler, setShowModal } = this.props;
    AuthService.login(auth)
      .then((result) => {
        if (result.success) {
          this.props.loadImage();
          changeLoggedHandler(true);
          setShowModal(false);
          this.props.navigate("/Menu");
          this.props.user.biodataId = result.result.biodataId;
          this.props.user.email = result.result.email;
          console.log(this.props.user);
        } else {
          this.setState({
            message: result.result.message,
            errorAlerts: ValidationResult.Validate(result.result),
          });
          console.log(this.state.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  render() {
    const { auth, errorAlerts, message } = this.state;
    return (
      <div className="min-h-screen flex items-center justify-center">
        <div className="bg-gray-100 flex rounded-2xl shadow-lg max-w-3xl p-5 items-center">
          <div className="md:w-1/2 px-8 md:px-16">
            <h2 className="font-bold text-2xl text-[#002D74]">Medicare</h2>
            <p className="text-xs mt-4 text-[#002D74]">
              Experience Wellness, Embrace Life: Join Medicare Today!
            </p>
            <form className="flex flex-col gap-4">
              <div className="relative">
                <input
                  className="p-2 mt-8 rounded-xl border w-full"
                  type="text"
                  name="email"
                  placeholder="Email"
                  value={auth.email}
                  onChange={this.changeHandler("email")}
                  maxLength={100}
                />
                <p className="text-red-600 flex mx-auto text-center justify-center text-xs font-light">
                  {!errorAlerts.Email?.valid ? (
                    <strong id="title-error" role="alert">
                      {errorAlerts.Email?.message}
                    </strong>
                  ) : null}
                </p>
              </div>

              <div className="relative">
                <input
                  className="p-2 rounded-xl border w-full"
                  type="password"
                  name="password"
                  placeholder="Password"
                  required
                  value={auth.password}
                  onChange={this.changeHandler("password")}
                  maxLength={255}
                />
                <p className="text-red-600 flex mx-auto text-center justify-center text-xs font-light">
                  {!errorAlerts.Password?.valid ? (
                    <strong id="title-error" role="alert">
                      {errorAlerts.Password?.message}
                    </strong>
                  ) : null}
                  {!errorAlerts.Credential?.valid ? (
                    <strong id="title-error" role="alert">
                      {errorAlerts.Credential?.message
                        ? errorAlerts.Credential?.message
                        : message}
                    </strong>
                  ) : null}
                </p>
                <AiOutlineEye className="absolute top-7 right-3 -translate-y-3 text-gray-600" />
              </div>

              <button
                onClick={this.handleSubmit}
                className="bg-[#002D74] rounded-xl text-white py-2 hover:scale-105 duration-300"
              >
                Sign In
              </button>
            </form>

            <div className="mt-6 grid grid-cols-3 items-center text-gray-400">
              <hr className="border-gray-400" />
              <p className="text-center text-sm">OR</p>
              <hr className="border-gray-400" />
            </div>

            <button className="bg-white border py-2 w-full rounded-xl mt-5 flex justify-center items-center text-sm hover:scale-105 duration-300 text-[#002D74]">
              <FcGoogle className="mr-3" />
              Login With Google
            </button>

            <div className="mt-5 text-xs border-b border-[#002D74] py-4 text-[#002D74]">
              <a href="#">Forgot your password?</a>
            </div>

            <div className="mt-3 text-xs flex justify-between items-center text-[#002D74]">
              <p>Don't have an account?</p>
              <button className="py-2 px-5 bg-white border rounded-xl hover:scale-110 duration-300">
                Register
              </button>
            </div>
          </div>
          <div className="md:block hidden w-1/2">
            <img className="rounded-2xl " src={images} />
          </div>
        </div>
      </div>
      // <div className="w-full h-screen flex justify-center items-center">
      //   <div className="grid grid-cols-1 md:grid-cols-2 m-auto h-[550px] shadow-lg shadow-gray-600 sm:max-w-[900px] bg-[#f5f5f5] rounded-lg">
      //     <div className="w-full h-[550px] hidden md:block">
      //       <img className="w-full h-full rounded-s-lg" src={images} alt="/" />
      //     </div>
      //     <div className="p-4 flex flex-col justify-around rounded-lg">
      //       <form className="text-center">
      //         {/* Tambahkan class text-center di sini */}
      //         <h2 className="text-4xl font-bold mb-8 font">Medicare.</h2>
      //         <div className="justify-center items-center mx-auto w-full">
      //           <input
      //             className="border p-2 w-[75%] rounded-lg"
      //             type="text"
      //             id="email"
      //             placeholder="Email"
      //             value={auth.email}
      //             onChange={this.changeHandler("email")}
      //             maxLength={100}
      //           />
      //           <p className="text-red-600 flex mx-auto text-center justify-center text-xs font-light mt-2 mb-3">
      //             {!errorAlerts.Email?.valid ? (
      //               <strong id="title-error" role="alert">
      //                 {errorAlerts.Email?.message}
      //               </strong>
      //             ) : null}
      //           </p>
      //         </div>
      //         <div className="justify-center items-center mx-auto w-full">
      //           <input
      //             className="border p-2 w-[75%] rounded-lg"
      //             type="password"
      //             placeholder="Password"
      //             id="password"
      //             required
      //             value={auth.password}
      //             onChange={this.changeHandler("password")}
      //             maxLength={255}
      //           />
      //           <p className="text-red-600 flex mx-auto text-center justify-center text-xs font-light mt-2 mb-4">
      //             {!errorAlerts.Password?.valid ? (
      //               <strong id="title-error" role="alert">
      //                 {errorAlerts.Password?.message}
      //               </strong>
      //             ) : null}
      //             {!errorAlerts.Credential?.valid ? (
      //               <strong id="title-error" role="alert">
      //                 {errorAlerts.Credential?.message
      //                   ? errorAlerts.Credential?.message
      //                   : message}
      //               </strong>
      //             ) : null}
      //           </p>
      //         </div>
      //         <button
      //           onClick={this.handleSubmit}
      //           className="w-[75%] flex mx-auto text-center justify-center py-2 text-white rounded-lg bg-blue-700 hover:bg-blue-800"
      //         >
      //           Sign In
      //         </button>
      //         <p className="text-center">Forgot Username or Password?</p>
      //       </form>
      //       <button className="text-center w-[75%] flex mx-auto text-center justify-center py-2 text-white rounded-lg bg-green-600 hover:bg-green-800">
      //         Sign Up
      //       </button>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

export default withRouter(Authentiaction);
