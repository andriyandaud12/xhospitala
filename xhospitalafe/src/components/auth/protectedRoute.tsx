import { Navigate, Outlet } from "react-router-dom";
import { AuthService } from "../services/authService/authService";
import { UserModel } from "../models/userModel";

export const ProtectedRoute = ({ isAllowed, role }: any) => {
  // console.log(isAllowed);
  const user: UserModel = AuthService.getAccount();
  return AuthService.getToken() ? (
    user.menuRole.indexOf(role) > -1 ? (
      <Outlet />
    ) : (
      <Navigate to="/restricted" />
    )
  ) : (
    <Navigate to="/" />
  );
};
