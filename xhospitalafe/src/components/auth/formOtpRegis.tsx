import React from "react";
import images from "../../assets/LogoProject.png";
import { AuthenticationModel } from "../models/authenticationModel";
import { AuthService } from "../services/authService/authService";
import { withRouter } from "../layout/withRouter";
import { ValidationResult } from "../../validations/validationResult";
import { RegisterModel } from "../models/registerModel";
import { OtpModel } from "../models/otpModel";

interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  setShowModal: any;
  navigate: any;
}
interface IState {
  auth: RegisterModel;
  otp: OtpModel;
  errorAlerts: any;
  message: string;
}

class FormOtpRegis extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      auth: new RegisterModel(),
      otp:new OtpModel(),
      errorAlerts: { email: false, password: false },
      message: "",
    };
  }

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      otp: {
        ...this.state.otp,
        [name]: event.target.value,
      },
    });
    this.setState({
      errorAlerts: {
        ...this.state.errorAlerts,
        [name]: !event.target.value,
      },
    });
  };

  componentDidMount(): void {
    AuthService.logout();
  }

  handleSubmit = (event: any) => {
    event.preventDefault();
    this.setState({
      errorAlerts: {
        Email: {
          valid: this.state.otp.email.length > 0,
          message: "Email required",
        },
        // Password: {
        //   valid: this.state.auth.password.length > 0,
        //   message: "Password required",
        // },
      },
    });

    if (
      this.state.otp.email.length === 0 
    //   ||
    //   this.state.auth.password.length === 0
    ) {
      return;
    }

    const { auth,otp } = this.state;
    const { changeLoggedHandler,  setShowModal } = this.props;
    AuthService.otp(otp)
      .then((result) => {
        if (result.success) {
        //   changeLoggedHandler(true);
          setShowModal(false);
        //   this.props.navigate("/");
        } else {
          this.setState({
            message: result.result.message,
            // errorAlerts: ValidationResult.Validate(result.result),
          });
          console.log(this.state.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  render() {
    const { otp,auth, errorAlerts, message } = this.state;
    return (
      <div className="w-full h-screen flex justify-center items-center">
        <div className="grid grid-cols-1 md:grid-cols-2 m-auto h-[550px] shadow-lg shadow-gray-600 sm:max-w-[900px] bg-[#f5f5f5] rounded-lg">
          <div className="w-full h-[550px] hidden md:block">
            <img className="w-full h-full rounded-s-lg" src={images} alt="/" />
          </div>
          <div className="p-4 flex flex-col justify-around rounded-lg">
            <form className="text-center">
              {/* Tambahkan class text-center di sini */}
              <h2 className="text-4xl font-bold mb-8 font">Medicare.</h2>
              <div className="justify-center items-center mx-auto w-full">
                <input
                  className="border p-2 w-[75%] rounded-lg"
                  type="text"
                  id="email"
                  placeholder="Email"
                  value={otp.email}
                  onChange={this.changeHandler("email")}
                  maxLength={100}
                />
                <p className="text-red-600 flex mx-auto text-center justify-center text-xs font-light mt-2 mb-4">
                  {!errorAlerts.Email?.valid ? (
                    <strong id="title-error" role="alert">
                      {errorAlerts.Email?.message}
                    </strong>
                  ) : null}
                </p>
              </div>

              <button
                onClick={this.handleSubmit}
                className="w-[75%] flex mx-auto text-center justify-center py-2 text-white rounded-lg bg-green-600 hover:bg-green-500"
              >
                Konfirmasi Otp
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(FormOtpRegis);
