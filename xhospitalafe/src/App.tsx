import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Navbar from "./components/layout/navbar";
import { BrowserRouter } from "react-router-dom";
import { UserModel } from "./components/models/userModel";
import { AuthService } from "./components/services/authService/authService";

interface IProps {}

interface IState {
  logged: boolean;
  user: UserModel;
}

export default class App extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      logged: false,
      user: new UserModel(),
    };
  }

  componentDidMount = async () => {
    await this.setState({
      logged: AuthService.getToken() ? true : false,
      user: AuthService.getAccount(),
    });
  };

  handleChangeStatus = (val: boolean) => {
    this.setState({
      logged: val,
      user: val ? AuthService.getAccount() : new UserModel(),
    });
  };

  render() {
    return (
      <BrowserRouter>
        <Navbar
          user={this.state.user}
          logged={this.state.logged}
          changeLoggedHandler={this.handleChangeStatus}
        />
      </BrowserRouter>
    );
  }
}
