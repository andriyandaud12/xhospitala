﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace Validations
{
    public class DoctorTreatmentValidator: AbstractValidator<DoctorTreatmentViewModel> { 
    
        public DoctorTreatmentValidator(XHospitalDbContext dbContext)
        {
            RuleFor(x => new { x.Name, x.Id })
                .Must(x =>
                {
                    if ((x.Id != null ? x.Id : 0) != 0)
                        return !dbContext.DoctorTreatments.Where(o => o.Name == x.Name && o.Id != x.Id).Any();
                    else
                        return !dbContext.DoctorTreatments.Where(o => o.Name == o.Name).Any();
                })
                .WithName("Name")
                .WithMessage("Name already exists!");
            RuleFor(x => x.Name).NotNull().Length(1, 10).WithMessage("Length 1 - 50");
        }
    }
}