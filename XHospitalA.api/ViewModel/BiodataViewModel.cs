﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class BiodataViewModel
    {
        public long Id { get; set; }

        [MaxLength(255)]
        public string? FullName { get; set; }

        [MaxLength(15)]
        public string? MobilePhone { get; set; }

        public string? Image { get; set; }

        [MaxLength(255)]
        public string? ImagePath { get; set; }
        public bool Is_delete { get; set; }
    }
}
