﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class LocationViewModel
    {
        public long Id { get; set; }

        [MaxLength(100)]
        public string? Name { get; set; }
        public long? ParentId { get; set; }
        public long? LocationLevelId { get; set; }
        public bool Is_delete { get; set; } = false;
        public LocationViewModel? Location { get; set; }
        public LocationLevelViewModel? LocationLevel { get; set; }

    }

    public class LocationWFViewModel : LocationViewModel
    {
        public LocationViewModel? Location { get; set; }
        public LocationLevelViewModel? LocationLevel { get; set; }

    }
}
