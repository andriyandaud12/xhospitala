﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class LoginViewModel
    {
        //[Required, DataType(DataType.EmailAddress), MaxLength(100)]
        public string Email { get; set; }

        //[Required, MaxLength(255)]
        public string Password { get; set; }
    }
    public class RegisterViewModel 
    {
        public string? Email { get; set; }

        //[Required, MaxLength(255)]
        public string? Password { get; set; }
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string? FullName { get; set; }
        [Required, MaxLength(50)]
        public string? MobilePhone { get; set; }

        public long? RoleId { get; set; }
        public BiodataViewModel? Biodata { get; set; }
        [ MaxLength(6)] // Adjust the length as needed for your OTP
        public string? Otp { get; set; } // Property to capture the entered OTP

    }
    public class UserViewModel
    {
        public UserViewModel()
        {

            Success = true;
        }


        public Boolean Success { get; set; }
        public DateTime? Expire { get; set; }
        public string? Message { get; set; }
        public long Id { get; set; }
        public string? FullName { get; set; }
        public string? MobilePhone { get; set; }

        public long? BiodataId { get; set; }

        public BiodataViewModel? Biodata { get; set; }
        public long? RoleId { get; set; }

        //public RoleViewModel? Role { get; set; }

        public string? Email { get; set; }

        public string? Password { get; set; }

        public int? LoginAttempt { get; set; }

        public bool? IsLocked { get; set; }

        public string? Token { get; set; }

        public DateTime? LastLogin { get; set; }

        public List<string?> MenuRole { get; set; }
        public List<string?> Roles { get; set; }
        public string Otp { get; set; } // Property to capture the entered OTP

    }
    public class UpdateUserViewModel
    {
        public string? FullName { get; set; }
        public string? MobilePhone { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }

        public long? RoleId { get; set; }

    }

}
