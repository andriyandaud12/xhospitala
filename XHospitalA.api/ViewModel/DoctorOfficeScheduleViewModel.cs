﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorOfficeScheduleViewModel
    {
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? MedicalFacilityScheduleId { get; set; }
        public int? Slot { get; set; }
        public bool Is_delete { get; set; }
    }
    public class GetDoctorOfficeScheduleViewModel: DoctorOfficeScheduleViewModel
    {
        public DoctorViewModel? Doctor { get; set; }
        public MedicalFacilityScheduleViewModel? MedicalFacilitySchedule { get; set; }
    }
}
