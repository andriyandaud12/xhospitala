﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class CurrentDoctorSpecializationViewModel
    {
        public long Id { get; set; }

        public long? DoctorId { get; set; }

        public long? SpecializationId { get; set; }
        public bool Is_delete { get; set; }
    }
    public class GetCurrentDoctorSpecializationViewModel: CurrentDoctorSpecializationViewModel
    {
        public DoctorViewModel Doctor { get; set; }
        public SpecializationViewModel? Specialization { get; set; }
    }
}
