﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class OTPViewModel
    {
        public OTPViewModel()
        {
            
        Success = true;
        }


        public Boolean Success { get; set; }
        public DateTime? Expire { get; set; }
        public string Message { get; set; }

    }
}
