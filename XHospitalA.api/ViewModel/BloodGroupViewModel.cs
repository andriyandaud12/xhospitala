﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class BloodGroupViewModel
    {
        public long Id { get; set; }
        [MaxLength(5)]
        public string? Code { get; set; }
        [MaxLength(255)]
        public string? Description { get; set; }
        public bool? Is_delete { get; set; }
    }
}
