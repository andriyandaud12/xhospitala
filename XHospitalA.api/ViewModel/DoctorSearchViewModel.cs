﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorSearchViewModel
    {
        public long? DoctorId { get; set; }
        public long? BiodataId { get; set; }
        public long? CurrentSpecializationId { get; set; }
        public string? FullName { get; set; }
        public string? Specialization { get; set; }
        public string? Pengalaman { get; set; }
        public List<HistoryOfPractice>? HistoryOfPractice { get; set; }
        public string? Image { get; set; }
        public List<DoctorTreatmentViewModel>? Treatment { get; set; }
        public List<DoctorEducationViewModel>? Education { get; set; }
        public List<ScheduleDoctorViewModel> DoctorSchedule { get; set; }
        public bool? IsOnline { get; set; }

    }
    public class HistoryOfPractice
    {
        public long? MedicalFacilityId { get; set; }
        public string? MedicalFacilityName { get; set; }
        public string OfficeSpecialization { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string? MedicalFacilityCategoryName { get; set; }
    }

    public class ScheduleDoctorViewModel
    {
        public long? DoctorId { get; set; }
        public long? MedicalFacilityId { get; set; }
        public int? Slot { get; set; }
        public string? Day { get; set; }
        public string? ScheduleStart { get; set; }
        public string? ScheduleEnd { get; set; }
        public HistoryOfPractice MedicalFacility { get; set; }

    }
}
