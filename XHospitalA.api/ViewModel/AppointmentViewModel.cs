﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace ViewModel
{
    public class AppointmentViewModel
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? DoctorOfficeId { get; set; }
        public long? DoctorOfficeScheduleId { get; set; }
        public long? DoctorOfficeTreatmentId { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public CustomerViewModel? Customer { get; set; }
        public DoctorOfficeViewModel? DoctorOffice { get; set; }
        public DoctorOfficeScheduleViewModel? DoctorOfficeSchedule { get; set; }
        public DoctorOfficeTreatmentViewModel? DoctorOfficeTreatment { get; set; }
    }

}
