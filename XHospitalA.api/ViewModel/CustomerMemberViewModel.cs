﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class CustomerMemberViewModel
    {
        public long Id { get; set; }

        public long? ParentBiodataId { get; set; }

        public long? CustomerId { get; set; }
       

        public bool Is_delete { get; set; }
    }

    public class CustomerMemberViewModelGet:CustomerMemberViewModel
    {
        public string FullName { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }
        public string RhesusType { get; set; } 
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public long BloodGroupId { get; set; }
        public string BloodGroup { get; set; }

        public BiodataViewModel? Biodata { get; set; }
        public CustomerViewModelGet? Customer { get; set; }
        public CustomerRelationViewModel? CustomerRelation { get; set; }
        public long? CustomerRelationId { get; set; }
    }
}
