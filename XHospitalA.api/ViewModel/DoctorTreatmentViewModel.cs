﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorTreatmentViewModel
    {
        public long Id { get; set; }
        public long? DoctorId { get; set; }

        //[Required, MaxLength(50)]
        public string? Name { get; set; }
        public bool Is_delete { get; set; } = false;
    }
    public class GetDoctorTreatmentViewModel: DoctorTreatmentViewModel
    {
        public DoctorViewModel? Doctor { get; set; }
    }
}
