﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class MedicalFacilityCategoryViewModel
    {
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public long? Name { get; set; }
    }
}
