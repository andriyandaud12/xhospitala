﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorEducationViewModel
    {
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? EducationLevelId { get; set; }

        [MaxLength(100)]
        public string? InstitutionName { get; set; }

        [MaxLength(100)]
        public string? Major { get; set; }

        [MaxLength(4)]
        public string? StartYear { get; set; }

        [MaxLength(4)]
        public string? EndYear { get; set; }
        public bool IsLastEducation { get; set; }
        public bool Is_delete { get; set; }
    }
    public class GetDoctorEducationViewModel: DoctorEducationViewModel { 
        public DoctorWFViewModel? Doctor { get; set; }
        public EducationLevelViewModel? EducationLevel { get; set; }
    }
}
