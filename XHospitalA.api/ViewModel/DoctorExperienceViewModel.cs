﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorExperienceViewModel
    {
        public long? DoctorId { get; set; }
        public double? DoctorExperience { get; set; }
    }
}
