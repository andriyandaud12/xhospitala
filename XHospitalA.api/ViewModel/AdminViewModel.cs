﻿using System.ComponentModel.DataAnnotations;

namespace ViewModel
{
    public class AdminViewModel
    {
        public long Id { get; set; }
        public long? BiodataId { get; set; }


        [MaxLength(10)]
        public string? Code { get; set; }
        public BiodataViewModel? Biodata { get; set; }

    }
}