﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorOfficeViewModel
    {
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? MedicalFacilityId { get; set; }

        [Required, MaxLength(100)]
        public string Specialization { get; set; }
        [Required]
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public bool Is_delete { get; set; } = false;
    }
    public class GetDoctorOfficeViewModel : DoctorOfficeViewModel
    {
        public DoctorWFViewModel? Doctor { get; set; }
        public GetMedicalFacilityViewModel? MedicalFacility { get; set; }
    }
}
