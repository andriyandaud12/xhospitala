﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    
    public class CustomerChatViewModel
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? DoctorId { get; set; }
    }
    public class GetCustomerChatViewModel: CustomerChatViewModel
    {
        public CustomerViewModel Customer { get; set; }
        public DoctorViewModel Doctor { get; set; }
    }
}
