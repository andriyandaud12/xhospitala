﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class LocationLevelViewModel
    {
        public long Id { get; set; }

        //[MaxLength(50)]
        public string? Name { get; set; }

        //[MaxLength(50)]
        public string? Abbreviation { get; set; }

        public bool Is_delete { get; set; } = false;
    }
}
