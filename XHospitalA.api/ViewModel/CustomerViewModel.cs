﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class CustomerViewModel
    {
        public long Id { get; set; }
        [Required, MaxLength(50)]
        public string FullName { get; set; }

        public DateTime? Dob { get; set; }
        [MaxLength(1)]
        public string? Gender { get; set; }
        public long BloodGroupId { get; set; }

        [MaxLength(5)]
        public string? RhesusType { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
      
        public bool Is_delete { get; set; }
        public BiodataViewModel? Biodata { get; set; }

        public long? CustomerRelationId { get; set; } // Add this property
    }
    public class CustomerViewModelGet:CustomerViewModel
    {
        //public List<CustomerRelationViewModel> CustomerRelation { get; set; }
        public long BiodataId { get; set; }
        public BiodataViewModel? Biodata { get; set; }
        public BloodGroupViewModel? BloodGroup { get; set; }
        public CustomerRelationViewModel? CustomerRelation { get; set; }
    }
}
