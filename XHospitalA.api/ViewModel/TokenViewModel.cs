﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class TokenViewModel
    {
        public long Id { get; set; }
        public string? Email { get; set; }

        public string? User_Id { get; set; }

        public string? token { get; set; }

        public DateTime? Expired_On { get; set; }

        public bool? Is_Expired { get; set; }
        public string Used_For { get; set; }
    }
}
