﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorOfficeTreatmentPriceViewModel

    {
        public long Id { get; set; }
        public long? DoctorOfficeTreatmentId { get; set; }
        public DoctorOfficeTreatmentViewModel? DoctorOfficeTreatment { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceStartFrom { get; set; }
        public decimal? PriceUntilFrom { get; set; }
    }
}
