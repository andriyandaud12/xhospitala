﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class AppointmentDoneViewModel
    {
        public long Id { get; set; }
        public long? AppoinmentId { get; set; }
        public string Diagnosis { get; set; }
        public CustomerViewModel? Customer { get; set; }
    }
}
