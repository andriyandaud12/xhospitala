﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorOfficeTreatmentViewModel
    {
        public long Id { get; set; }
        public long? DoctorTreatmentId { get; set; }
        public long? DoctorOfficeId { get; set; }        
    }

    public class GetDoctorOfficeTreatmentViewModel : DoctorOfficeTreatmentViewModel
    {
        public DoctorTreatmentViewModel? DoctorTreatment { get; set; }
        public DoctorOfficeViewModel? DoctorOffice { get; set; }
    }
}
