﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class MedicalFacilityScheduleViewModel
    {
        public long Id { get; set; }
        public long? MedicalFacilityId { get; set; }

        [Required, MaxLength(10)]
        public string? Day { get; set; }

        [Required, MaxLength(10)]
        public string? TimeScheduleStart { get; set; }

        [Required, MaxLength(10)]
        public string? TimeScheduleEnd { get; set; }
        public bool Is_delete { get; set; } = false;
    }
    public class GetMedicalFacilityScheduleViewModel: MedicalFacilityScheduleViewModel
    {
        public MedicalFacilityViewModel? MedicalFacility { get; set; }
    }
}
