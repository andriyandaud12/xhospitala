﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class MedicalFacilityViewModel
    {
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string? Name { get; set; }
        public long? MedicalFacilityCategoryId { get; set; }
        public long? LocationId { get; set; }
        public string? FullAddress { get; set; }
        [Required, MaxLength(100)]
        public string? Email { get; set; }
        [Required, MaxLength(10)]
        public string? PhoneCode { get; set; }
        [Required, MaxLength(15)]
        public string? Phone { get; set; }
        [Required, MaxLength(15)]
        public string? Fax { get; set; }
        public bool Is_delete { get; set; } = false;
    }
    public class GetMedicalFacilityViewModel: MedicalFacilityViewModel
    {
        public MedicalFacilityCategoryViewModel? MedicalFacilityCategory { get; set; }
        public LocationViewModel? Location { get; set; }

    }
}
