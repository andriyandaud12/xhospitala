﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class EducationLevelViewModel
    {
        public long Id { get; set; }

        [MaxLength(10)]
        public string? Name { get; set; }
    }
}
