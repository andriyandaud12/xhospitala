﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorViewModel
    {
        public long Id { get; set; }

        public long? BiodataId { get; set; }

        [MaxLength(50)]
        public string? Str { get; set; }

        public bool Is_delete { get; set; } = false;
    }

    public class DoctorWFViewModel: DoctorViewModel
    {
        public BiodataViewModel? Biodata { get; set; }
    }
}
