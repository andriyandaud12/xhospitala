﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DoctorOnlineViewModel
    {
        public long? DoctorId { get; set; }
        public bool? IsOnline { get; set; }
    }
}
