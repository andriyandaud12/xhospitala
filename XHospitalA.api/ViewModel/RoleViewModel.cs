﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    internal class RoleViewModel
    {
        public long Id { get; set; }

        [MaxLength(20)]
        public string? Name { get; set; }

        [MaxLength(20)]
        public string? Code { get; set; }
    }
}
