﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class BiodataAddressViewModel
    {
        public long Id { get; set; }

        public long? BiodataId { get; set; }

        [MaxLength(100)]
        public string? Label { get; set; }

        public string? Recipent { get; set; }

        [MaxLength(15)]
        public string? RecipentPhoneNumber { get; set; }

        public long? LocationId { get; set; }

        [MaxLength(10)]
        public string? PostalCode { get; set; }

        public string? Address { get; set; }
        public BiodataViewModel? Biodata { get; set; }
        public LocationViewModel? Location { get; set; }
    }
}
