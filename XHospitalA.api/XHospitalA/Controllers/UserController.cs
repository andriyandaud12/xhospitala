﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private IValidator<LoginViewModel> _validator;
        private UserRepository _repo;

        public UserController(XHospitalDbContext dbContext, IConfiguration configuration, IValidator<LoginViewModel> validator)
        {
            _repo = new UserRepository(dbContext, configuration);
            _configuration = configuration;
            _validator = validator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(LoginViewModel model)
        {
            ValidationResult valadator = await _validator.ValidateAsync(model);
            if (!valadator.IsValid)
            {
                return BadRequest(valadator.Errors);
            }

            UserViewModel result = new UserViewModel();
            result = _repo.Authentication(model);
            if (result != null)
            {
                if (result.IsLocked == false)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, result.Email),
                        new Claim("Id", result.Id.ToString())
                    };

                    foreach (var role in result.MenuRole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role));
                    }

                    var token = GetToken(claims);
                    result.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    return Ok(result);
                }
            }

            List<object> resultError = new List<object>();
            resultError.Add(new
            {
                propertyName = "Credential",
                errorMessage = "Credential not match!!" + (result?.IsLocked == true ? " Your account is locked." : "")
            });

            return NotFound(resultError);
        }
        [HttpPost("register")]
        public async Task<UserViewModel> Post(RegisterViewModel model)
        {
            return _repo.Authentication(model);
        }


        [HttpPost("sendotp")]
        public async Task<OTPViewModel> Post(string email)
        {
            return _repo.SendOtp(email);
        }
        [HttpPut("updateUserInfo")]
        public async Task<UserViewModel> Put(string email, string otp, [FromBody] UpdateUserViewModel updateModel)
        {
            return _repo.UpdateUserInfo(email, otp, updateModel);
        }
        //[HttpPost("register")]
        //public async Task<OTPViewModel> Post(RegisterViewModel model)
        //{
        //    return _repo.Authentication(model);
        //}

        private JwtSecurityToken GetToken(List<Claim> claims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:Issuer"],
                audience: _configuration["JWT:Audience"],
                expires: DateTime.Now.AddDays(Convert.ToDouble(_configuration["JWT:Expires"])),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            return token;
        }
    }
}
