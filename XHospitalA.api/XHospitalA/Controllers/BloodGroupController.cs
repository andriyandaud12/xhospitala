﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BloodGroupController : ControllerBase
    {//login
        private BloodGroupRepository _repoBloodGroup;
        private IValidator<BloodGroupViewModel> _validator;

        public BloodGroupController(XHospitalDbContext dbContext, IValidator<BloodGroupViewModel> validator)
        {
            _repoBloodGroup = new BloodGroupRepository(dbContext);
            _validator = validator;

        }

        [HttpPost]
        public async Task<IActionResult> Post(BloodGroupViewModel model)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repoBloodGroup.Create(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }
        [HttpGet]
        //[ReadableBodyStream(Roles = "Administrator,DaftarPasien")]
        public async Task<ResponseResult> GetBloodGroup(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repoBloodGroup.Pagination(pageNum, rows, search, orderBy, sort);
        }
        [HttpPut("{id}")]
        //[ReadableBodyStream(Roles = "Administrator,Categories")]
        public async Task<IActionResult> Put(long id, BloodGroupViewModel? model)
        {
            model.Id = id;
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repoBloodGroup.Update(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }
        [HttpPut("changestatus/{id}")]
        //[ReadableBodyStream(Roles = "Administrator,DaftarPasien")]
        public async Task<BloodGroupViewModel> Put(long id, bool status)
        {
            return _repoBloodGroup.ChangeStatus(id, status);
        }
        [HttpGet("{id}")]
        public async Task<BloodGroupViewModel> GetById(int id)
        {
            return _repoBloodGroup.GetById(id);
        }


    }
}
