﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private LocationRepository _repo;
        private IValidator<LocationViewModel> _validator;

        public LocationController(XHospitalDbContext dbcontext, IValidator<LocationViewModel> validator)
        {
            _repo = new LocationRepository(dbcontext);
            _validator = validator;
        }
        [HttpPost]
        public async Task<IActionResult> Post(LocationViewModel model)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repo.Create(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }

        }
        [HttpGet]
        public async Task<ResponseResult> Get(int pageNum, int rows, int? level, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, level, search, orderBy, sort);
        }

        [HttpGet("GetAll")]
        public async Task<List<LocationWFViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<LocationViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<IActionResult> Put(long id, bool status)
        {
            try
            {
                LocationViewModel statusModel = new LocationViewModel { Id = id, Is_delete = status };
                ValidationResult result = await _validator.ValidateAsync(statusModel);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repo.ChangeStatus(id, status));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<LocationViewModel> Put(long id, LocationViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }

        [HttpGet("location/{id}")]
        public async Task<List<LocationViewModel>> GetByParent(long id)
        {
            return _repo.GetByParentId(id);
        }

        [HttpGet("level/{id}")]
        public async Task<List<LocationWFViewModel>> GetAllByLevel(long id)
        {
            return _repo.GetAllByLevel(id);
        }

        [HttpGet("GetAllByLocationLevel5")]
        public async Task<List<LocationWFViewModel>> GetAllByLocationLevel5()
        {
            return _repo.GetAllByLocationLevel5();
        }
        [HttpGet("GetAllByLocationLevel5/{name}")]
        public async Task<List<LocationWFViewModel>> GetAllByLocationLevel5(string name)
        {
            return _repo.GetAllByLocationLevel5(name);
        }
    }
}
