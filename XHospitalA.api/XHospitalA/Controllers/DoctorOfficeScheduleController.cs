﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorOfficeScheduleController : ControllerBase
    {
        private DoctorOfficeScheduleRepository _repo;

        public DoctorOfficeScheduleController(XHospitalDbContext dbContext)
        {
            _repo = new DoctorOfficeScheduleRepository(dbContext);
        }

        [HttpPost]
        public async Task<DoctorOfficeScheduleViewModel> Post(DoctorOfficeScheduleViewModel model)
        {
            return _repo.Create(model);
        }

        [HttpGet("{id}")]
        public async Task<GetDoctorOfficeScheduleViewModel> GetById(long id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<DoctorOfficeScheduleViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<DoctorOfficeScheduleViewModel> Put(long id, DoctorOfficeScheduleViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }

        [HttpGet("GetAll")]
        public async Task<List<GetDoctorOfficeScheduleViewModel>> GetAll()
        {
            return _repo.GetAll();
        }

    }
}
