﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrentSpecializationController : ControllerBase
    {
        private CurrentDoctorSpecializationRepository _repo;

        public CurrentSpecializationController(XHospitalDbContext dbContext)
        {
            _repo = new CurrentDoctorSpecializationRepository(dbContext);
        }

        [HttpPost]
        public async Task<CurrentDoctorSpecializationViewModel> Post(CurrentDoctorSpecializationViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<GetCurrentDoctorSpecializationViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<CurrentDoctorSpecializationViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<CurrentDoctorSpecializationViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<CurrentDoctorSpecializationViewModel> Put(long id, CurrentDoctorSpecializationViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
