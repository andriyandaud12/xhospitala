﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerChatController : ControllerBase
    {
        private CustomerChatRepository _repo;

        public CustomerChatController(XHospitalDbContext dbcontext)
        {
            _repo = new CustomerChatRepository(dbcontext);
        }

        [HttpGet("GetChatByDoctorId/{id}")]
        public async Task<List<GetCustomerChatViewModel>> GetChatByDoctorId(long id)
        {
            return _repo.GetListChatByDoctorId(id);
        }
    }
}
