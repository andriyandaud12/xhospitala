﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[ReadableBodyStream(Roles = "Administrator,Doctor")]

    public class DoctorController : ControllerBase
    {
        private DoctorRepository _repo;

        public DoctorController(XHospitalDbContext dbContext)
        {
            _repo = new DoctorRepository(dbContext);
        }

        [HttpPost]
        [ReadableBodyStream(Roles = "Administrator,Doctor")]
        public async Task<DoctorViewModel> Post(DoctorViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<DoctorWFViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<DoctorViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        [ReadableBodyStream(Roles = "Administrator,Doctor")]
        public async Task<DoctorViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        [ReadableBodyStream(Roles = "Administrator,Doctor")]
        public async Task<DoctorViewModel> Put(long id, DoctorViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
        [HttpGet("Search")]
        //[ReadableBodyStream(Roles = "Administrator,CariDokter")]
        public async Task<ResponseResult> Get(int pageNum, int rows, string? specialization="", string? location = "", string? name = "", string? treatmentName = "")
        {
            return _repo.Pagination(pageNum, rows, location, name, specialization, treatmentName);
        }

        [HttpGet("GetAllFullDescription")]
        public async Task<ResponseResult> GetAllFullDescription(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("GetDoctorById/{Id}")]
        public async Task<DoctorSearchViewModel> GetDoctorById(long Id)
        {
            return _repo.GetDoctorById(Id);
        }

        [HttpGet("GetDoctorPriceById/{Id}")]
        public async Task<DoctorOfficeTreatmentPriceViewModel> GetDoctorPriceById(long Id)
        {
            return _repo.GetDoctorPriceById(Id);
        }

        [HttpGet("GetDoctorExperienceById/{Id}")]
        public async Task<DoctorExperienceViewModel> GetDoctorExperienceById(long Id)
        {
            return _repo.GetDoctorExperienceById(Id);
        }

        [HttpGet("GetDoctorOnline/{Id}")]
        public async Task<DoctorOnlineViewModel> GetDoctorOnline(long Id)
        {
            return _repo.GetDoctorOnline(Id);
        }
    }
}
