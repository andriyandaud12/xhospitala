﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;


namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        private AppointmentRepository _repo;

        public AppointmentController(XHospitalDbContext dbcontext)
        {
            _repo = new AppointmentRepository(dbcontext);
        }
        [HttpPost]
        public async Task<AppointmentViewModel> Post(AppointmentViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetCustomerRelation/{Id}")]
        public async Task<List<CustomerViewModelGet>> GetCustomerRelation(long Id)
        {
            return _repo.GetCustomerRelation(Id);
        }
        [HttpGet("GetMedicalFacility/{Id}")]
        public async Task<List<GetDoctorOfficeViewModel>> GetMedicalFacility(long Id)
        {
            return _repo.GetMedicalFacility(Id);
        }
        [HttpGet("GetSchedule/{Id}")]
        public async Task<List<GetDoctorOfficeScheduleViewModel>> GetSchedule(long Id)
        {
            return _repo.GetSchedule(Id);
        }
        [HttpGet("GetTreatment/{Id}")]
        public async Task<List<GetDoctorOfficeTreatmentViewModel>> GetTreatment(long Id)
        {
            return _repo.GetTreatment(Id);
        }

        [HttpGet("GetAppointmentByDoctorId/{id}")]
        public async Task<List<AppointmentViewModel>> GetAppointmentByDoctorId(long id)
        {
            return _repo.GetAppointmentByDoctorId(id);
        }
    }
}
