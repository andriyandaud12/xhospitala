﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerRelationController : ControllerBase
    {
        private CustomerRelationRepository _repoCustomerRelation;
        private IValidator<CustomerRelationViewModel> _validator;

        public CustomerRelationController(XHospitalDbContext dbContext, IValidator<CustomerRelationViewModel> validator)
        {
            _repoCustomerRelation = new CustomerRelationRepository(dbContext);
            _validator = validator;
        }

        [HttpPost]
        //[ReadableBodyStream(Roles = "Admin")]
        public async Task<IActionResult> Post(CustomerRelationViewModel model)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repoCustomerRelation.Create(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }
        [HttpGet]
        public async Task<ResponseResult> GetBloodGroup(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repoCustomerRelation.Pagination(pageNum, rows, search, orderBy, sort);
        }
        [HttpPut("{id}")]
        //[ReadableBodyStream(Roles = "Administrator,Categories")]
        public async Task<IActionResult> Put(long id, CustomerRelationViewModel? model)
        {
            model.Id = id;
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repoCustomerRelation.Update(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }
        [HttpPut("changestatus/{id}")]
        //[ReadableBodyStream(Roles = "Administrator, Categories")]
        public async Task<CustomerRelationViewModel> Put(long id, bool status)
        {
            return _repoCustomerRelation.ChangeStatus(id, status);
        }

        [HttpGet("{id}")]
        public async Task<CustomerRelationViewModel> GetById(int id)
        {
            return _repoCustomerRelation.GetById(id);
        }
    }
}
