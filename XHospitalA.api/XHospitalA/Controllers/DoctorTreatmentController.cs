﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorTreatmentController : ControllerBase
    {
        private DoctorTreatmentRepository _repo;
        private IValidator<DoctorTreatmentViewModel> _validator;

        public DoctorTreatmentController(XHospitalDbContext dbContext, IValidator<DoctorTreatmentViewModel> validator)
        {
            _repo = new DoctorTreatmentRepository(dbContext);
            _validator = validator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(DoctorTreatmentViewModel model)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repo.Create(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }

        }
        [HttpGet("GetAll")]
        public async Task<List<GetDoctorTreatmentViewModel>> Get()
        {
            return _repo.GetAll();
        }


        [HttpGet("GetAllTreatmentBySpecName/{name}")]
        public async Task<List<GetDoctorTreatmentViewModel>> GetAllTreatmentBySpecName(string name)
        {
            return _repo.GetAllTreatmentBySpecName(name);
        }

        [HttpGet("{id}")]
        public async Task<GetDoctorTreatmentViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<DoctorTreatmentViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<DoctorTreatmentViewModel> Put(long id, DoctorTreatmentViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
