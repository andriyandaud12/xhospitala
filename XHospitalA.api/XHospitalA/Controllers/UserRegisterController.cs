﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRegisterController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private IValidator<TokenViewModel> _validator;
        private RegisterRepository _repo;

        public UserRegisterController(XHospitalDbContext dbContext,IConfiguration configuration,IValidator<TokenViewModel> validator)
        {
            _repo = new RegisterRepository(dbContext, configuration);
            _configuration = configuration;
            _validator = validator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(TokenViewModel model)
        {
            ValidationResult validationResult = await _validator.ValidateAsync(model);
            if (validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);

            }
            try
            {
                TokenViewModel updatedModel = _repo.CreateOrUpdate(model);
                return Ok(updatedModel);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error: {ex.Message}");
            }
        }
    }
}
