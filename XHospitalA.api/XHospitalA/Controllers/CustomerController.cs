﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private CustomerRepository _repo;

        public CustomerController(XHospitalDbContext dbContext)
        {
            _repo = new CustomerRepository(dbContext);
        }

        [HttpPost]
        public async Task<CustomerViewModel> Post(CustomerViewModel model)
        {
            

            // Call the Create method in the repository with customerRelationId
            //var createdCustomer = _repo.Create(model, customerRelationId);
            return _repo.Create(model);

        }
        [HttpGet]
        public async Task<ResponseResult> GetCustomer(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("{id}")]
        public async Task<CustomerViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<CustomerViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<CustomerViewModel> Put(long id, CustomerViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
