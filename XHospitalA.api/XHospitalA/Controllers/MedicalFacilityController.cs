﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalFacilityController : ControllerBase
    {
        private MedicalFacilityRepository _repo;

        public MedicalFacilityController(XHospitalDbContext dbContext)
        {
            _repo = new MedicalFacilityRepository(dbContext);
        }

        [HttpPost]
        public async Task<MedicalFacilityViewModel> Post(MedicalFacilityViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<GetMedicalFacilityViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<GetMedicalFacilityViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<MedicalFacilityViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<MedicalFacilityViewModel> Put(long id, MedicalFacilityViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
