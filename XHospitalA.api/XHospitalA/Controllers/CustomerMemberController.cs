﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerMemberController : ControllerBase
    {
        //[ReadableBodyStream(Roles = "Administrator,Categories")]

        private CustomerMemberRepository _repo;

        public CustomerMemberController(XHospitalDbContext dbContext)
        {
            _repo = new CustomerMemberRepository(dbContext);
        }

        [HttpPost]
        public async Task<CustomerMemberViewModelGet> Post(CustomerMemberViewModelGet model)
        {
            return _repo.Create(model);
        }

        //[HttpGet("GetAll")]
        //public async Task<List<CustomerMemberViewModelGet>> Get()
        //{
        //    return _repo.GetAll();
        //}
        [HttpGet]
        public async Task<ResponseResult> GetBloodGroup(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("{id}")]
        public async Task<CustomerMemberViewModelGet> GetById(int id)
        {
            return _repo.GetById(id);
        }


        [HttpPut("changestatus/{id}")]
        public async Task<CustomerMemberViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<CustomerMemberViewModelGet> Put(long id, CustomerMemberViewModelGet? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
