﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BiodataController : ControllerBase
    {
        private BiodataRepository _repo;

        public BiodataController(XHospitalDbContext dbContext)
        {
            _repo = new BiodataRepository(dbContext);
        }

        [HttpPost]
        public async Task<BiodataViewModel> Post(BiodataViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet]
        public async Task<ResponseResult> GetBiodata(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("{id}")]
        public async Task<BiodataViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<BiodataViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<BiodataViewModel> Put(long id, BiodataViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
        [HttpPut("updateimage/{id}")]
        public async Task<BiodataViewModel> UpdateImage(long id, BiodataViewModel model)
        {
            model.Id = id;
            return _repo.UpdateImage(id, model);
        }

    }
}
