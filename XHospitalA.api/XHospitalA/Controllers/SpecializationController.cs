﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecializationController : ControllerBase
    {
        private SpecializationRepository _repo;

        public SpecializationController(XHospitalDbContext dbContext)
        {
            _repo = new SpecializationRepository(dbContext);
        }

        [HttpPost]
        public async Task<SpecializationViewModel> Post(SpecializationViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<SpecializationViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<SpecializationViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<SpecializationViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<SpecializationViewModel> Put(long id, SpecializationViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
