﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;
using XHospitalA.Security;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationLevelController : ControllerBase
    {
        private LocationLevelRepository _repo;
        private IValidator<LocationLevelViewModel> _validator;

        public LocationLevelController(XHospitalDbContext dbcontext, IValidator<LocationLevelViewModel> validator)
        {
            _repo = new LocationLevelRepository(dbcontext);
            _validator = validator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(LocationLevelViewModel model)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(model);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repo.Create(model));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }

        }

        [HttpGet("GetAll")]
        public async Task<List<LocationLevelViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet]
        public async Task<ResponseResult> Get(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("{id}")]
        public async Task<LocationLevelViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<IActionResult> Put(long id, bool status)
        {
            try
            {
                LocationLevelViewModel statusModel = new LocationLevelViewModel { Id = id, Is_delete = status };
                ValidationResult result = await _validator.ValidateAsync(statusModel);
                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(_repo.ChangeStatus(id, status));
            }
            catch (Exception)
            {
                //throw;
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<LocationLevelViewModel> Put(long id, LocationLevelViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
