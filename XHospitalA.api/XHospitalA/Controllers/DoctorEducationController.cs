﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorEducationController : ControllerBase
    {
        private DoctorEducationRepository _repo;

        public DoctorEducationController(XHospitalDbContext dbContext)
        {
            _repo = new DoctorEducationRepository(dbContext);
        }

        [HttpPost]
        public async Task<DoctorEducationViewModel> Post(DoctorEducationViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<GetDoctorEducationViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<GetDoctorEducationViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<DoctorEducationViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<DoctorEducationViewModel> Put(long id, DoctorEducationViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
