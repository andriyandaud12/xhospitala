﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorOfficeController : ControllerBase
    {
        private DoctorOfficeRepository _repo;

        public DoctorOfficeController(XHospitalDbContext dbContext)
        {
            _repo = new DoctorOfficeRepository(dbContext);
        }

        [HttpPost]
        public async Task<DoctorOfficeViewModel> Post(DoctorOfficeViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("GetAll")]
        public async Task<List<DoctorOfficeViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<DoctorOfficeViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("changestatus/{id}")]
        public async Task<DoctorOfficeViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }

        [HttpPut("{id}")]
        public async Task<DoctorOfficeViewModel> Put(long id, DoctorOfficeViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
    }
}
