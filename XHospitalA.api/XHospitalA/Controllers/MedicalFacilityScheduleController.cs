﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XHospitalA.DataModel;
using XHospitalA.Repositories;

namespace XHospitalA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicalFacilityScheduleController : ControllerBase
    {
        private MedicalFacilityScheduleRepository _repo;

        public MedicalFacilityScheduleController(XHospitalDbContext dbcontext)
        {
            _repo = new MedicalFacilityScheduleRepository(dbcontext);
        }
        [HttpGet("GetPagination")]
        public async Task<ResponseResult> Get(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpGet("GetById/{id}")]
        public async Task<GetMedicalFacilityScheduleViewModel> GetById(long id)
        {
            return _repo.GetByMedFacSchedId(id);
        }

        [HttpPost("Create")]
        public async Task<MedicalFacilityScheduleViewModel> Post(MedicalFacilityScheduleViewModel model)
        {
            return _repo.Create(model);
        }

        [HttpPut("Update/{id}")]
        public async Task<MedicalFacilityScheduleViewModel> Update(long id, MedicalFacilityScheduleViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }

        [HttpGet("MedicalFacilityId/{id}")]
        public async Task<List<MedicalFacilityScheduleViewModel>> GetByMedicalFacilityId(long id)
        {
            return _repo.GetByMedicalFacilityId(id);
        }
    }
}
