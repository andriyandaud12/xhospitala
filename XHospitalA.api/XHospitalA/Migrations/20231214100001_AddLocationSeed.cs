﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class AddLocationSeed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4160));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4163));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4164));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4166));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4168));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4169));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4171));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4172));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4174));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4176));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4177));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4179));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4181));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4182));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4184));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4186));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4187));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4189));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4191));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4098));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4101));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4102));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4104));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4105));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4107));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4110));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4112));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4113));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4115));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4116));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4119));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4122));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4123));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4125));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4126));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4785));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4790));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4792));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4794));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4795));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4798));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4800));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4802));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4804));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4818));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4880));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4882));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4655));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4658));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4659));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4661));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4662));

            migrationBuilder.InsertData(
                table: "MasterLocationLevel",
                columns: new[] { "Id", "Abbreviation", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 1L, "Prov.", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5006), null, null, false, null, null, "Provinsi" },
                    { 2L, "Kota", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5008), null, null, false, null, null, "Kota" },
                    { 3L, "Kab.", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5009), null, null, false, null, null, "Kabupaten" },
                    { 4L, "Kec.", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5011), null, null, false, null, null, "Kecamatan" },
                    { 5L, "Kel.", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5012), null, null, false, null, null, "Kelurahan" },
                    { 6L, "Desa", 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5014), null, null, false, null, null, "Desa" }
                });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4610));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4613));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4617));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4619));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4622));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4624));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4626));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4628));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4630));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4220));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4225));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4227));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4228));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4958));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3770));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3781));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3783));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3784));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3786));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3790));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3792));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3793));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3795));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3796));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3798));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3800));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3801));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3803));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3805));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3807));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4392));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4441));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4464));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4474));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4485));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4496));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4506));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4517));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4527));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4548));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4559));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4570));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4732));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4735));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4736));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4738));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4745));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4756));

            migrationBuilder.InsertData(
                table: "MasterLocation",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "LocationLevelId", "Modified_by", "Modified_on", "Name", "ParentId" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4664), null, null, false, 1L, null, null, "DK Jakarta", null },
                    { 7L, 1L, new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4707), null, null, false, 2L, null, null, "Jakarta Barat", 6L }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6935));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6937));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6938));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6939));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6939));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6941));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6942));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6943));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6944));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6945));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6949));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6950));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6951));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6952));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6954));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6955));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6894));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6896));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6897));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6899));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6902));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6902));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6903));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6904));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6905));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6906));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6909));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6910));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6911));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6912));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6913));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6914));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6915));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7273));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7275));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7276));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7277));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7278));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7279));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7281));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7282));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7284));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7285));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7289));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7290));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7293));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7296));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7297));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7299));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7317));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7318));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7215));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7217));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7218));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7218));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7219));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7188));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7190));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7192));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7193));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7194));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7197));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7198));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7199));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7200));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6978));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6978));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6979));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6980));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6981));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7366));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7368));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7370));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6709));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6722));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6723));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6724));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6725));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6726));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6727));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6729));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6732));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6733));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6734));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6735));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6736));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7083));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7104));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7121));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7127));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7132));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7138));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7143));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7159));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7165));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7238));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7240));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7241));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7243));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7245));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7247));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7248));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7249));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7251));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7252));
        }
    }
}
