﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addSeed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MasterMenu",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Url = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ParentId = table.Column<long>(type: "bigint", nullable: true),
                    BigIcon = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SmallIcon = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterMenu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterMenu_MasterMenu_ParentId",
                        column: x => x.ParentId,
                        principalTable: "MasterMenu",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterRole",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterRole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterMenuRole",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MenuId = table.Column<long>(type: "bigint", nullable: false),
                    RoleId = table.Column<long>(type: "bigint", nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterMenuRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterMenuRole_MasterMenu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "MasterMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MasterMenuRole_MasterRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "MasterRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MasterUser",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BiodataId = table.Column<long>(type: "bigint", nullable: false),
                    RoleId = table.Column<long>(type: "bigint", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    LoginAttempt = table.Column<int>(type: "int", nullable: true),
                    IsLocked = table.Column<bool>(type: "bit", nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterUser_MasterBiodata_BiodataId",
                        column: x => x.BiodataId,
                        principalTable: "MasterBiodata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MasterUser_MasterRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "MasterRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9284));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9285), " https://media.npr.org/assets/news/2010/02/25/doctor-3900f8c9b6c5be80f3b2e509eafe9c41d7ad2671.jpg  " });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9286));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9287));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9288));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9289));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9290));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9291));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9292));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9293));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9294));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9296));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9297));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9298));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9299));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9300));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9301));

            migrationBuilder.InsertData(
                table: "MasterBiodata",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "FullName", "Image", "ImagePath", "Is_delete", "MobilePhone", "Modified_by", "Modified_on" },
                values: new object[] { 18L, 1L, new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9302), null, null, "Gilang Ardi Baskara Alauidin", "https://www.google.com/url?sa=i&url=https%3A%2F%2Ftayostation.com%2Fmeet-tayo-and-friends%2F&psig=AOvVaw2apKnA_qNh9MQrlHYrCwDz&ust=1702434295888000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCPClsI_siIMDFQAAAAAdAAAAABAD", "", false, "081390094000", null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9240));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9241));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9243));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9244));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9245));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9246));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9247));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9253));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9254));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9255));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9256));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9257));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9258));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9259));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9261));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9262));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9263));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9616));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9619));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9620));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9622));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9623));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9624));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9626));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9627));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9628));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9629));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9630));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9633));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9634));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9636));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9637));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9639));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9640));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9660));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9661));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9555));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9556));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9557));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9559));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9560));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9528));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9530));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9531));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9533));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9534));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9535));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9537));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9538));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9539));

            migrationBuilder.InsertData(
                table: "MasterMenu",
                columns: new[] { "Id", "BigIcon", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name", "ParentId", "SmallIcon", "Url" },
                values: new object[,]
                {
                    { 1L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Master", null, null, null },
                    { 2L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Tranasaksi", null, null, null }
                });

            migrationBuilder.InsertData(
                table: "MasterRole",
                columns: new[] { "Id", "Code", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 1L, "ADMIN", 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Admin" },
                    { 2L, "DOKTER", 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Dokter" },
                    { 3L, "PASIEN", 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Pasien" }
                });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9319));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9321));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9321));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9322));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9323));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9098));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9108));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9109));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9110));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9112));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9113));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9114));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9115));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9116));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9117));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9118));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9119));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9120));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9121));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9122));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9123));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9123));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9403));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9416));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9422));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9428));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9434));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9446));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9457));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9462));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9469));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9474));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9480));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9485));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9491));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9496));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9574));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9576));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9578));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9578));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9579));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9581));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9587));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9589));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9590));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9592));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9596));

            migrationBuilder.InsertData(
                table: "MasterMenu",
                columns: new[] { "Id", "BigIcon", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name", "ParentId", "SmallIcon", "Url" },
                values: new object[,]
                {
                    { 3L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "CariDokter", 2L, null, "http://localhost:3000/SearchResult" },
                    { 4L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "DetailDokter", 2L, null, "http://localhost:3000/DetailsDokter" },
                    { 5L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "ProfileDokter", 1L, null, "http://localhost:3000/ProfilDoctor" },
                    { 6L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "Location", 1L, null, "http://localhost:3000/Location" },
                    { 7L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "DaftarPasien", 2L, null, "http://localhost:3000/Pasien" },
                    { 8L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "DaftarRelation", 1L, null, "http://localhost:3000/Relation" },
                    { 9L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "DaftarBloodGroup", 1L, null, "http://localhost:3000/BloodGroup" }
                });

            migrationBuilder.InsertData(
                table: "MasterUser",
                columns: new[] { "Id", "BiodataId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Email", "IsLocked", "Is_delete", "LastLogin", "LoginAttempt", "Modified_by", "Modified_on", "Password", "RoleId" },
                values: new object[,]
                {
                    { 1L, 18L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "gilang@gmail.com", false, false, new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9711), 0, null, null, "ac9689e2272427085e35b9d3e3e8bed88cb3434828b43b86fc0596cad4c6e270", 1L },
                    { 2L, 17L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "andreas.com", false, false, new DateTime(2023, 12, 12, 13, 6, 43, 268, DateTimeKind.Local).AddTicks(9713), 0, null, null, "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd", 2L }
                });

            migrationBuilder.InsertData(
                table: "MasterMenuRole",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "MenuId", "Modified_by", "Modified_on", "RoleId" },
                values: new object[,]
                {
                    { 1L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 3L, null, null, 1L },
                    { 2L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 4L, null, null, 1L },
                    { 3L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 5L, null, null, 1L },
                    { 4L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 6L, null, null, 1L },
                    { 5L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 7L, null, null, 1L },
                    { 6L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 8L, null, null, 1L },
                    { 7L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 9L, null, null, 1L },
                    { 8L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 5L, null, null, 2L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MasterMenu_ParentId",
                table: "MasterMenu",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterMenuRole_MenuId",
                table: "MasterMenuRole",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterMenuRole_RoleId",
                table: "MasterMenuRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterUser_BiodataId",
                table: "MasterUser",
                column: "BiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterUser_RoleId",
                table: "MasterUser",
                column: "RoleId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MasterMenuRole");

            migrationBuilder.DropTable(
                name: "MasterUser");

            migrationBuilder.DropTable(
                name: "MasterMenu");

            migrationBuilder.DropTable(
                name: "MasterRole");

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1991));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1993), " https://media.npr.org/assets/news/2010/02/25/doctor-3900f8c9b6c5be80f3b2e509eafe9c41d7ad2671.jpg" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1995));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1997));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1999));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2001));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2003));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2005));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2007));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2009));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2011));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2013));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2015));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2017));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2019));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2022));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2024));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1923));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1928));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1930));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1932));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1935));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1937));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1940));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1942));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1943));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1945));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1947));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1948));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1950));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1952));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1954));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1955));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1957));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2639));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2641));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2646));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2650));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2652));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2654));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2656));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2661));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2665));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2667));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2669));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2676));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2679));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2684));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2695));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2735));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2524));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2527));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2529));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2531));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2532));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2461));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2464));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2470));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2472));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2483));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2487));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2070));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2073));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2075));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2077));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1531));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1548));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1550));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1552));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1554));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1557));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1559));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1561));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1564));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1566));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1568));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1581));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1583));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1585));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1587));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1589));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2216));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2231));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2247));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2259));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2272));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2286));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2335));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2347));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2396));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2407));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2562));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2565));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2567));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2569));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2571));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2574));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2578));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2590));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2592));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2595));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2597));
        }
    }
}
