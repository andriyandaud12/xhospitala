﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addDbseedEducationLevel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8997));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8999));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9001));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9004));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9005));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9007));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9009));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9013));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9021));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9023));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9027));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9029));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9031));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9033));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9035));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9037));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1942));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1892));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1907));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8798));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8803));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8805));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8807));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8882));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8885));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8887));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8889));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8890));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8901));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8904));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8906));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8920));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8935));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8937));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8939));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8941));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(393));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(398));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(401));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(403));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(406));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(409));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(412));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(417));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(420));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(422));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(425));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(427));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(429));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(431));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(434));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(436));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(439));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(484));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(486));

            migrationBuilder.InsertData(
                table: "m_education_level",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[] { 3L, 1L, new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(488), null, null, false, null, null, "Strata 3" });

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(194));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(197));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(200));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(202));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(221));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(204));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(207));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(210));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(212));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(214));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(216));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(223));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(218));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(225));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(752));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(754));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(756));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(758));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(759));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(761));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(64));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(67));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(70));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(72));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(143));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(147));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(149));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(152));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(155));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1446));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1450));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1452));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1454));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1455));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1457));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1459));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1460));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1462));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1464));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1466));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1468));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1471));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1473));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1475));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1477));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1479));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1481));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1482));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1485));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1487));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1489));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1550));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1552));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1555));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1560));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1562));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1564));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1567));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1569));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1572));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1574));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1576));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1578));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1580));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1583));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1585));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1588));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1590));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1592));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1596));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1598));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1600));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1602));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1605));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1607));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1609));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1612));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1615));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1617));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1619));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1621));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1624));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1626));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1628));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1630));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1633));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1640));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1643));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1646));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9078));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9080));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9081));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9083));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9084));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(598));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(603));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(605));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(672));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(677));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(820));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(833));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8499));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8516));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8518));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8521));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8522));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8524));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8526));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8529));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8531));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8533));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8534));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8537));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8540));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8542));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8551));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8554));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(8556));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1969));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1971));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9244));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9420));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9437));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9521));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9558));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9576));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9636));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9653));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9668));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9684));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9701));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9719));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9736));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9832));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9852));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9870));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 683, DateTimeKind.Local).AddTicks(9886));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(863));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(866));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(869));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(871));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(872));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(874));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(876));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(878));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(880));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(886));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(887));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(889));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(891));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(893));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(895));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(896));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(898));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(900));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1014));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1018));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1020));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1021));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1023));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1025));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1026));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1028));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1030));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1032));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1033));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1036));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1038));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1042));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1043));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1045));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1047));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1049));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1051));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1052));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1054));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1056));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1058));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1060));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1062));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1064));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1067));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1068));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1070));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1074));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1076));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1078));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1080));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1082));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1084));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1086));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1088));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1090));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1092));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1094));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1096));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1098));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1100));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1102));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1104));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1105));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1107));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1170));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1174));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1177));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1179));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1182));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1184));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1186));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1188));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1190));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1192));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1194));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1199));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1201));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1203));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1204));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1206));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1208));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1210));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1212));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1214));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1215));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1217));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1219));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1221));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1223));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1225));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1227));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1230));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1232));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1233));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1235));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1237));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1239));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1241));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1242));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1244));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1247));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1249));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1253));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1255));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1256));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1258));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1260));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1262));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1264));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1265));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1267));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1327));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1330));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1331));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1333));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1335));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1337));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1338));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1340));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1344));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1346));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1779));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1782));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1784));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1786));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1788));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1789));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1792));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1793));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1795));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1797));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1800));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1801));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1803));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(1805));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(265));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(267));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(269));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(270));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(273));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(275));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(277));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(278));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(280));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(282));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(284));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(285));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(288));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(290));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(292));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(293));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 27, 14, 42, 35, 684, DateTimeKind.Local).AddTicks(295));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2808));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2811));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2813));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2815));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2818));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2822));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2824));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2826));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2827));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2829));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2831));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2833));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2838));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2839));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2842));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2846));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5617));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5583));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5593));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2697));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2700));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2702));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2704));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2706));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2709));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2710));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2712));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2714));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2722));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2725));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2726));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2738));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2764));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2766));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2768));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2770));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3965));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3968));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3970));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3973));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3975));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3977));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3980));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3982));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3984));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3987));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3989));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3991));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3993));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3995));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3998));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4000));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4002));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4037));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4039));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3762));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3766));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3768));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3770));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3785));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3772));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3774));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3776));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3777));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3779));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3781));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3788));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3783));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4280));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4283));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4285));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4286));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4288));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4290));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3700));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3706));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3709));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3712));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3715));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3718));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3721));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3723));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3726));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5151));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5154));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5155));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5157));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5159));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5161));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5165));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5166));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5168));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5256));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5258));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5260));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5265));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5273));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5280));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5282));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5284));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5286));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5297));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5299));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5304));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5307));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5312));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5314));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5316));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5318));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5325));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5329));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5331));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5333));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5334));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5338));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5339));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5341));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5348));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5350));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5352));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5354));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5356));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5434));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5436));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5438));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2969));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2972));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2973));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2975));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2976));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4216));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4222));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4224));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4348));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4359));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2304));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2327));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2329));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2331));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2332));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2336));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2338));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2339));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2342));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2344));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2346));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2347));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2469));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2471));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5635));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3127));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3142));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3155));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3167));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3203));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3218));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3230));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3270));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3283));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3295));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3307));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3319));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3333));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3346));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3440));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3455));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3468));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4390));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4392));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4394));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4401));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4403));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4405));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4493));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4495));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4497));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4499));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4500));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4504));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4506));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4508));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4510));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4512));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4513));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4515));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4517));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4519));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4521));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4523));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4525));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4529));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4531));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4533));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4535));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4537));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4539));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4541));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4543));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4544));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4547));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4549));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4551));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4553));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4557));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4559));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4562));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4564));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4566));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4567));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4569));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4572));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4574));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4577));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4579));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4582));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4584));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4586));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4588));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4590));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4777));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4780));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4782));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4784));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4786));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4790));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4792));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4794));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4796));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4798));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4799));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4801));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4804));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4818));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4820));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4825));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4827));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4829));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4831));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4832));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4835));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4837));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4839));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4840));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4842));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4844));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4847));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4850));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4851));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4853));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4855));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4859));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4861));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4864));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4869));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4871));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4873));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4874));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4876));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4958));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4962));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4964));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4966));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4974));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4976));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4979));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4983));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4985));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4988));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4990));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4993));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4995));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4997));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5001));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5003));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5492));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5496));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5497));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5499));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5501));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5503));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5504));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5506));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5508));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5509));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3820));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3822));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3824));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3826));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3827));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3829));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3835));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3917));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3919));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3921));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3924));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3926));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3928));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3929));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3931));
        }
    }
}
