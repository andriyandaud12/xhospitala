﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class seedScheduleDoctor : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9453));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9537));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9540));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9544));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9547));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9550));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9554));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9558));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9561));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9564));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9568));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9571));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9574));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9581));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9584));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9587));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9590));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2871));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2737));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2757));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9313));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9318));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9321));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9325));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9328));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9331));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9334));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9337));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9340));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9355));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9359));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9362));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9371));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9396));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9399));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9403));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9406));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(893));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(900));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(904));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(909));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(915));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(921));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(926));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(933));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(937));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(944));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(948));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(952));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(957));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(961));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(965));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(969));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(973));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1018));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1021));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(663));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(666));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(668));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(671));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(674));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(678));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(682));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1407));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1411));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1414));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1417));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1420));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1423));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(578));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(583));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(587));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(591));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(595));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(600));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(605));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(609));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2281));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2285));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2289), "10.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2292), "10.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2296), "11.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2299));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2303));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2306));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2310));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2314));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2317));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2320));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2324));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2327));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2330));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2337));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2341));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2345));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2348));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2351));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2355), "10.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2361), "10.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2367), "13.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2371));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2374));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2378));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2381));

            migrationBuilder.InsertData(
                table: "MasterMedicalFacilitySchedule",
                columns: new[] { "Id", "Created_by", "Created_on", "Day", "Deleted_by", "Deleted_on", "Is_delete", "MedicalFacilityId", "Modified_by", "Modified_on", "TimeScheduleEnd", "TimeScheduleStart" },
                values: new object[,]
                {
                    { 29L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2384), "Senin", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 30L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2388), "Selasa", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 31L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2392), "Rabu", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 32L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2395), "Kamis", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 33L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2399), "Jum'at", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 34L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2452), "Sabtu", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 35L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2456), "Minggu", null, null, false, 5L, null, null, "19.30", "08:00" },
                    { 36L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2460), "Senin", null, null, false, 6L, null, null, "10.30", "08:00" },
                    { 37L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2463), "Selasa", null, null, false, 6L, null, null, "10.30", "08:00" },
                    { 38L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2467), "Rabu", null, null, false, 6L, null, null, "19.30", "08:00" },
                    { 39L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2471), "Kamis", null, null, false, 6L, null, null, "10.30", "08:00" },
                    { 40L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2474), "Jum'at", null, null, false, 6L, null, null, "19.30", "08:00" },
                    { 41L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2477), "Sabtu", null, null, false, 6L, null, null, "19.30", "08:00" },
                    { 42L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2481), "Minggu", null, null, false, 6L, null, null, "19.30", "08:00" },
                    { 43L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2484), "Senin", null, null, false, 7L, null, null, "10.30", "08:00" },
                    { 44L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2488), "Selasa", null, null, false, 7L, null, null, "15.30", "08:00" },
                    { 45L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2491), "Rabu", null, null, false, 7L, null, null, "19.30", "08:00" },
                    { 46L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2495), "Kamis", null, null, false, 7L, null, null, "19.30", "08:00" },
                    { 47L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2498), "Jum'at", null, null, false, 7L, null, null, "19.30", "08:00" },
                    { 48L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2502), "Sabtu", null, null, false, 7L, null, null, "19.30", "08:00" },
                    { 49L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2505), "Minggu", null, null, false, 7L, null, null, "19.30", "08:00" },
                    { 50L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2508), "Senin", null, null, false, 8L, null, null, "19.30", "08:00" },
                    { 51L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2512), "Selasa", null, null, false, 8L, null, null, "19.30", "08:00" },
                    { 52L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2515), "Rabu", null, null, false, 8L, null, null, "19.30", "08:00" },
                    { 53L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2519), "Kamis", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 54L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2522), "Jum'at", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 55L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2526), "Sabtu", null, null, false, 8L, null, null, "19.30", "08:00" },
                    { 56L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2529), "Minggu", null, null, false, 8L, null, null, "19.30", "08:00" },
                    { 57L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2532), "Senin", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 58L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2536), "Selasa", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 59L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2539), "Rabu", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 60L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2543), "Kamis", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 61L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2546), "Jum'at", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 62L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2550), "Sabtu", null, null, false, 9L, null, null, "19.30", "08:00" },
                    { 63L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2554), "Minggu", null, null, false, 9L, null, null, "10.30", "08:00" }
                });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9652));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9655));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9658));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9661));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9663));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1167));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1178));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1509));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1529));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8958));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8978));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8981));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8984));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8988));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8991));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8996));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9001));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9004));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9008));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9014));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9021));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9037));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9040));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9043));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9854));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9884));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9933));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(20));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(46));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "MedicalFacilityId" },
                values: new object[] { new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(109), 8L });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(134));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(160));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(184));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(209));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(234));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(260));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(401));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1573));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1596));

            migrationBuilder.InsertData(
                table: "TransactionDoctorOfficeSchedule",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "MedicalFacilityScheduleId", "Modified_by", "Modified_on", "Slot" },
                values: new object[,]
                {
                    { 8L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1600), null, null, 2L, false, 8L, null, null, 3 },
                    { 9L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1604), null, null, 2L, false, 9L, null, null, 3 },
                    { 10L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1607), null, null, 2L, false, 10L, null, null, 3 },
                    { 11L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1611), null, null, 2L, false, 11L, null, null, 3 },
                    { 12L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1615), null, null, 2L, false, 12L, null, null, 3 },
                    { 13L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1619), null, null, 2L, false, 13L, null, null, 3 },
                    { 14L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1623), null, null, 2L, false, 14L, null, null, 3 },
                    { 15L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1626), null, null, 3L, false, 15L, null, null, 3 },
                    { 16L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1632), null, null, 3L, false, 16L, null, null, 3 },
                    { 17L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1635), null, null, 3L, false, 17L, null, null, 3 },
                    { 18L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1639), null, null, 3L, false, 18L, null, null, 3 },
                    { 19L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1642), null, null, 3L, false, 19L, null, null, 3 },
                    { 20L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1646), null, null, 3L, false, 20L, null, null, 3 },
                    { 21L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1649), null, null, 3L, false, 21L, null, null, 3 },
                    { 22L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1653), null, null, 4L, false, 22L, null, null, 3 },
                    { 23L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1729), null, null, 4L, false, 23L, null, null, 3 },
                    { 24L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1733), null, null, 4L, false, 24L, null, null, 3 },
                    { 25L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1737), null, null, 4L, false, 25L, null, null, 3 },
                    { 26L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1740), null, null, 4L, false, 26L, null, null, 3 },
                    { 27L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1744), null, null, 4L, false, 27L, null, null, 3 },
                    { 28L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1748), null, null, 4L, false, 28L, null, null, 3 },
                    { 71L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1900), null, null, 11L, false, 8L, null, null, 3 },
                    { 72L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1955), null, null, 11L, false, 9L, null, null, 3 },
                    { 73L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1959), null, null, 11L, false, 10L, null, null, 3 },
                    { 74L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1963), null, null, 11L, false, 11L, null, null, 3 },
                    { 75L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1966), null, null, 11L, false, 12L, null, null, 3 },
                    { 76L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1970), null, null, 11L, false, 13L, null, null, 3 },
                    { 77L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1974), null, null, 11L, false, 14L, null, null, 3 },
                    { 99L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2054), null, null, 15L, false, 15L, null, null, 3 },
                    { 100L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2057), null, null, 15L, false, 16L, null, null, 3 },
                    { 101L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2061), null, null, 15L, false, 17L, null, null, 3 },
                    { 102L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2064), null, null, 15L, false, 18L, null, null, 3 },
                    { 103L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2068), null, null, 15L, false, 19L, null, null, 3 },
                    { 104L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2071), null, null, 15L, false, 20L, null, null, 3 },
                    { 105L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2075), null, null, 15L, false, 21L, null, null, 3 }
                });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2624));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2628));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2641));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(731));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(734));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(751));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(754));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(757));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(761));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(764));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(779));

            migrationBuilder.InsertData(
                table: "TransactionDoctorOfficeSchedule",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "MedicalFacilityScheduleId", "Modified_by", "Modified_on", "Slot" },
                values: new object[,]
                {
                    { 29L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1751), null, null, 5L, false, 29L, null, null, 3 },
                    { 30L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1755), null, null, 5L, false, 30L, null, null, 3 },
                    { 31L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1758), null, null, 5L, false, 31L, null, null, 3 },
                    { 32L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1762), null, null, 5L, false, 32L, null, null, 3 },
                    { 33L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1765), null, null, 5L, false, 33L, null, null, 3 },
                    { 34L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1769), null, null, 5L, false, 34L, null, null, 3 },
                    { 35L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1773), null, null, 5L, false, 35L, null, null, 3 },
                    { 36L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1776), null, null, 6L, false, 36L, null, null, 3 },
                    { 37L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1780), null, null, 6L, false, 37L, null, null, 3 },
                    { 38L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1783), null, null, 6L, false, 38L, null, null, 3 },
                    { 39L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1787), null, null, 6L, false, 39L, null, null, 3 },
                    { 40L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1790), null, null, 6L, false, 40L, null, null, 3 },
                    { 41L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1794), null, null, 6L, false, 41L, null, null, 3 },
                    { 42L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1797), null, null, 6L, false, 42L, null, null, 3 },
                    { 43L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1801), null, null, 7L, false, 43L, null, null, 3 },
                    { 44L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1804), null, null, 7L, false, 44L, null, null, 3 },
                    { 45L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1808), null, null, 7L, false, 45L, null, null, 3 },
                    { 46L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1811), null, null, 7L, false, 46L, null, null, 3 },
                    { 47L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1815), null, null, 7L, false, 47L, null, null, 3 },
                    { 48L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1818), null, null, 7L, false, 48L, null, null, 3 },
                    { 49L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1822), null, null, 7L, false, 49L, null, null, 3 },
                    { 50L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1825), null, null, 8L, false, 50L, null, null, 3 },
                    { 51L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1829), null, null, 8L, false, 51L, null, null, 3 },
                    { 52L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1832), null, null, 8L, false, 52L, null, null, 3 },
                    { 53L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1836), null, null, 8L, false, 53L, null, null, 3 },
                    { 54L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1839), null, null, 8L, false, 54L, null, null, 3 },
                    { 55L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1843), null, null, 8L, false, 55L, null, null, 3 },
                    { 56L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1846), null, null, 8L, false, 56L, null, null, 3 },
                    { 57L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1850), null, null, 9L, false, 57L, null, null, 3 },
                    { 58L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1853), null, null, 9L, false, 58L, null, null, 3 },
                    { 59L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1857), null, null, 9L, false, 59L, null, null, 3 },
                    { 60L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1860), null, null, 9L, false, 60L, null, null, 3 },
                    { 61L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1864), null, null, 9L, false, 61L, null, null, 3 },
                    { 62L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1868), null, null, 9L, false, 62L, null, null, 3 },
                    { 63L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1871), null, null, 9L, false, 63L, null, null, 3 },
                    { 64L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1875), null, null, 10L, false, 57L, null, null, 3 },
                    { 65L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1878), null, null, 10L, false, 58L, null, null, 3 },
                    { 66L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1882), null, null, 10L, false, 59L, null, null, 3 },
                    { 67L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1885), null, null, 10L, false, 60L, null, null, 3 },
                    { 68L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1889), null, null, 10L, false, 61L, null, null, 3 },
                    { 69L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1893), null, null, 10L, false, 62L, null, null, 3 },
                    { 70L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1896), null, null, 10L, false, 63L, null, null, 3 },
                    { 78L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1977), null, null, 12L, false, 43L, null, null, 3 },
                    { 79L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1981), null, null, 12L, false, 44L, null, null, 3 },
                    { 80L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1984), null, null, 12L, false, 45L, null, null, 3 },
                    { 81L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1988), null, null, 12L, false, 46L, null, null, 3 },
                    { 82L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1992), null, null, 12L, false, 47L, null, null, 3 },
                    { 83L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1995), null, null, 12L, false, 48L, null, null, 3 },
                    { 84L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1999), null, null, 12L, false, 49L, null, null, 3 },
                    { 85L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2002), null, null, 13L, false, 36L, null, null, 3 },
                    { 86L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2006), null, null, 13L, false, 37L, null, null, 3 },
                    { 87L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2010), null, null, 13L, false, 38L, null, null, 3 },
                    { 88L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2014), null, null, 13L, false, 39L, null, null, 3 },
                    { 89L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2017), null, null, 13L, false, 40L, null, null, 3 },
                    { 90L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2021), null, null, 13L, false, 41L, null, null, 3 },
                    { 91L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2024), null, null, 13L, false, 42L, null, null, 3 },
                    { 92L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2028), null, null, 14L, false, 29L, null, null, 3 },
                    { 93L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2032), null, null, 14L, false, 30L, null, null, 3 },
                    { 94L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2035), null, null, 14L, false, 31L, null, null, 3 },
                    { 95L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2039), null, null, 14L, false, 32L, null, null, 3 },
                    { 96L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2042), null, null, 14L, false, 33L, null, null, 3 },
                    { 97L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2046), null, null, 14L, false, 34L, null, null, 3 },
                    { 98L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2050), null, null, 14L, false, 35L, null, null, 3 },
                    { 106L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2078), null, null, 16L, false, 50L, null, null, 3 },
                    { 107L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2082), null, null, 16L, false, 51L, null, null, 3 },
                    { 108L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2085), null, null, 16L, false, 52L, null, null, 3 },
                    { 109L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2089), null, null, 16L, false, 53L, null, null, 3 },
                    { 110L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2092), null, null, 16L, false, 54L, null, null, 3 },
                    { 111L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2096), null, null, 16L, false, 55L, null, null, 3 },
                    { 112L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2099), null, null, 16L, false, 56L, null, null, 3 },
                    { 113L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2103), null, null, 17L, false, 50L, null, null, 3 },
                    { 114L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2106), null, null, 17L, false, 51L, null, null, 3 },
                    { 115L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2110), null, null, 17L, false, 52L, null, null, 3 },
                    { 116L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2114), null, null, 17L, false, 53L, null, null, 3 },
                    { 117L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2117), null, null, 17L, false, 54L, null, null, 3 },
                    { 118L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2121), null, null, 17L, false, 55L, null, null, 3 },
                    { 119L, 1L, new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2124), null, null, 17L, false, 56L, null, null, 3 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1808));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1811));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1813));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1814));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1816));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1817));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1819));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1822));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1823));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1824));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1826));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1827));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1829));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1830));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1832));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1833));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1835));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1836));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1837));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2915));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2885));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2896));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1716));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1718));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1720));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1721));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1723));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1724));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1727));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1728));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1730));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1732));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1734));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1735));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1737));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2438));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2440));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2442));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2444));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2446));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2447));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2449));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2451));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2453));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2455));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2458));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2460));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2462));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2464));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2467));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2489));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2492));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2317));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2318));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2320));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2321));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2322));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2324));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2325));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2638));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2640));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2641));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2642));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2644));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2645));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2278));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2280));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2282));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2284));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2286));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2288));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2289));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2291));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2293));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2740));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2742));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2743), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2745), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2747), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2748));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2750));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2751));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2753));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2754));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2756));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2758));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2794));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2795));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2797));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2799));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2800));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2802));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2803));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2805));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2806));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2808), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2809), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                columns: new[] { "Created_on", "TimeScheduleEnd" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2811), "19.30" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2813));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2814));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2816));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1868));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1872));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1873));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1875));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1876));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2589));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2592));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2594));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2679));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2688));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1465));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1476));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1478));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1479));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1481));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1482));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1483));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1485));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1486));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1488));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1490));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1491));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1492));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1494));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1495));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1497));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1498));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2929));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2930));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(1997));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2008));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2019));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2028));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2039));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2049));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "MedicalFacilityId" },
                values: new object[] { new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2058), 7L });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2067));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2077));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2086));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2223));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2232));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2710));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2713));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2715));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2717));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2718));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2720));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2842));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2845));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2848));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2851));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2351));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2353));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2355));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2361));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2362));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2363));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2366));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2367));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2370));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2373));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2374));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2376));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 57, 9, 638, DateTimeKind.Local).AddTicks(2377));
        }
    }
}
