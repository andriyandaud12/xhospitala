﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class memperbaikiJam : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(334));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(337));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(340));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(342));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(345));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(348));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(350));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(353));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(356));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(358));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(361));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(363));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(366));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(368));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(371));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(374));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(376));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(379));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(381));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3735));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3668));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3685));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(165));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(170));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(172));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(175));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(178));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(180));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(182));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(185));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(187));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(201));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(203));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(206));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(223));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(278));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(281));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(286));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1771));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1779));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1782));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1785));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1789));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1792));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1795));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1799));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1802));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1805));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1808));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1812));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1815));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1818));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1821));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1825));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1828));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1869));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1872));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1534));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1537));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1539));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1541));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1543));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1547));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1549));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2189));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2194));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2196));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2199));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2201));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2204));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1462));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1466));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1470));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1473));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1476));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1480));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1483));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1487));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1490));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3143), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3146), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3149), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3152), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3155), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3158), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3161), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3164), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3166), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3169), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3172), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3249), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3253), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3255), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3259), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3265), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3269), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3272), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3276), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3279), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3286), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3292), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3295), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3298), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3306), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3309), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3311), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3316), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3318), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3321), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3324), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3327), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3329), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3332), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3335), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3338), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3341), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3344), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3347), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3349), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3352), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3355), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3357), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3360), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3363), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3365), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3368), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3371), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3373), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3376), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3379), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3382), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3385), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3387), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3390), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3393), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3395), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3398), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3401), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3404), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3406), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3409), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3507), "08.00" });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(446));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(448));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(451));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(453));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(455));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2092));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2096));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2099));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2278));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2290));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9674));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9701));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9704));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9707));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9710));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9712));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9715));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9717));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9720));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9723));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9725));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9728));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9730));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9733));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9750));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9753));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 174, DateTimeKind.Local).AddTicks(9756));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3771));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(818));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(838));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(856));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(930));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(952));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1058));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1076));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1093));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1111));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1130));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1147));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1164));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1380));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1400));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2329));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2413));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2416));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2422));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2424));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2427));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2433));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2436));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2439));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2445));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2448));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2454));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2460));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2463));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2469));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2472));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2478));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2481));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2484));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2487));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2490));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2493));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2495));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2498));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2501));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2504));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2507));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2516));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2519));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2521));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2524));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2527));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2530));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2533));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2536));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2540));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2547));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2550));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2553));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2557));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2559));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2756));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2760));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2763));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2779));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2782));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2785));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2788));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2800));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2803));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2806));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2809));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2812));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2814));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2826));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2829));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2835));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2838));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2852));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2855));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2858));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2861));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2864));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2867));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2879));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2885));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2887));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2890));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2991));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2995));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(2998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3001));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3003));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3006));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3009));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3012));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3015));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3018));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3022));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3025));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3028));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3031));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3034));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3037));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3040));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3043));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3046));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3049));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3052));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3569));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3573));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3575));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3578));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3581));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(3586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1674));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1677));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1679));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1682));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1684));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1687));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1690));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1693));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1697));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1700));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1702));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1705));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1707));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1710));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1715));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 18, 8, 47, 21, 175, DateTimeKind.Local).AddTicks(1717));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8421));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8423));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8424));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8426));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8428));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8429));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8431));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8432));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8434));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8436));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8438));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8439));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8441));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8442));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8444));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8445));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8446));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8448));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8450));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(11));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9978));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9988));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8280));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8282));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8285));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8286));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8288));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8289));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8291));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8292));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8378));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8380));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8382));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8383));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8385));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8386));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8388));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8389));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9013));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9019));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9020));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9022));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9024));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9026));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9028));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9029));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9031));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9033));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9035));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9037));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9038));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9040));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9067));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9068));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8885));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8886));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8887));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8888));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8890));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8891));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8893));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9234));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9235));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9237));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9238));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9240));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9241));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8843));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8847));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8849));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8851));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8853));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8855));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8857));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8859));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9656), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9659), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9660), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9662), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9663), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9665), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9667), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9668), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9670), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9671), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9673), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9675), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9676), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9678), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9679), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9681), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9682), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9684), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9685), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9687), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9729), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9731), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9732), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9734), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9735), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9737), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9739), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9740), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9742), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9743), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9745), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9746), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9748), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9749), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9751), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9754), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9756), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9757), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9759), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9760), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9762), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9763), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9765), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9766), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9768), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9770), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9771), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9773), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9774), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9776), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9778), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9779), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9781), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9782), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9784), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9786), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9787), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9790), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9792), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9793), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9795), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9796), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                columns: new[] { "Created_on", "TimeScheduleStart" },
                values: new object[] { new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9798), "08:00" });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8490));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8491));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8492));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8494));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8495));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9181));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9184));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9186));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9276));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9283));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8042));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8052));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8053));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8055));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8056));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8057));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8059));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8060));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8062));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8063));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8065));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8066));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8068));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8069));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8070));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8072));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8073));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(34));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(35));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8604));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8617));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8629));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8648));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8660));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8670));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8723));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8743));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8772));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8781));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8800));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8810));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9306));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9308));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9310));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9314));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9316));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9317));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9356));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9361));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9362));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9364));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9367));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9374));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9375));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9379));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9380));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9382));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9386));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9387));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9390));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9391));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9393));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9395));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9396));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9399));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9401));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9402));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9404));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9407));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9411));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9412));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9414));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9415));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9417));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9420));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9422));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9424));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9425));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9427));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9428));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9431));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9433));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9471));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9475));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9476));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9478));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9480));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9481));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9483));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9484));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9486));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9487));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9489));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9490));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9492));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9493));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9495));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9497));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9498));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9500));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9503));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9505));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9506));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9508));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9510));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9511));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9513));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9514));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9516));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9518));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9519));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9521));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9522));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9524));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9525));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9527));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9529));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9530));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9532));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9533));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9535));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9536));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9539));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9541));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9542));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9546));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9579));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9583));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9585));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9589));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9594));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9596));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9921));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9923));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9924));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9928));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9929));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9931));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8916));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8919));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8921));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8923));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8924));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8979));
        }
    }
}
