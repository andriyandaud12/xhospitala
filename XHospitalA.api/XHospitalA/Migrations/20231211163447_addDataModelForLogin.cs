﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addDataModelForLogin : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1991));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1993));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1995));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1997));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1999));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2001));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2003));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2005));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2007));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2009));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2011));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2013));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2015));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2017));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2019));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2022));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2024));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1923));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1928));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1930));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1932));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1935));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1937));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1940));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1942));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1943));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1945));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1947));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1948));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1950));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1952));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1954));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1955));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1957));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2639));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2641));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2646));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2650));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2652));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2654));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2656));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2661));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2665));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2667));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2669));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2676));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2679));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2684));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2695));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2735));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2524));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2527));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2529));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2531));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2532));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2461));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2464));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2470));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2472));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2483));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2487));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2070));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2073));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2075));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2077));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1531));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1548));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1550));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1552));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1554));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1557));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1559));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1561));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1564));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1566));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1568));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1581));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1583));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1585));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1587));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1589));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(1591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2216));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2231));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2247));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2259));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2272));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2286));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2335));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2347));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2396));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2407));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2562));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2565));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2567));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2569));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2571));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2574));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2578));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2590));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2592));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2595));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 23, 34, 46, 968, DateTimeKind.Local).AddTicks(2597));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5643));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5645));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5647));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5649));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5652));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5654));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5656));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5660));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5663));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5665));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5666));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5668));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5670));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5672));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5674));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5676));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5731));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5573));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5576));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5578));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5582));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5584));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5586));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5588));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5590));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5592));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5594));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5596));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5597));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5599));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5601));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5603));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5605));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6374));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6377));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6382));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6385));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6394));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6397));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6399));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6402));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6516));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6519));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6521));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6558));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6559));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6261));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6264));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6266));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6267));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6161));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6168));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6171));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6174));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6177));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6179));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6182));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5770));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5772));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5774));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5775));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5778));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5199));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5211));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5213));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5216));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5218));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5219));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5221));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5223));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5294));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5300));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5302));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5933));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5996));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6008));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6032));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6044));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6057));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6070));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6082));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6093));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6106));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6296));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6299));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6301));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6303));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6315));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6317));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6319));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6321));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6325));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6327));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6329));
        }
    }
}
