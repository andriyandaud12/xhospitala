﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addSeedUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2090));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2092));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2093));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2094));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2095));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2097));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2098));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2099));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2100));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2101));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2102));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2103));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2104));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2105));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2107));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2108));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2110));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2111));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2113));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3393));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3369));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3376));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2002));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2005));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2006));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2007));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2008));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2009));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2010));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2011));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2013));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2014));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2015));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2016));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2017));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2062));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2063));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2064));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2065));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2586));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2588));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2589));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2590));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2592));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2593));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2594));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2595));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2596));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2598));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2599));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2601));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2602));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2603));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2605));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2606));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2610));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2628));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2629));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2474));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2477));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2478));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2488));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2479));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2481));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2482));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2483));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2484));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2486));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2489));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2487));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2490));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2750));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2752));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2752));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2753));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2754));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2755));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2441));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2443));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2444));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2445));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2446));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2451));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2452));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2453));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2454));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3135));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3136));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3137));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3138));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3139));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3140));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3141));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3143));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3144));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3145));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3183));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3185));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3186));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3187));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3189));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3190));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3192));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3193));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3194));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3195));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3196));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3199));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3200));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3201));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3202));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3203));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3204));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3207));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3208));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3211));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3213));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3215));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3218));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3219));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3220));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3221));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3222));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3223));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3224));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3225));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3226));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3227));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3228));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3229));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3233));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3234));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3235));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3236));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3237));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3238));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3239));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3240));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3241));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3243));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3243));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3244));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3246));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3247));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3248));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3249));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3250));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3286));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3287));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2138));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2139));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2140));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2141));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2142));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2713));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2715));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2717));

            migrationBuilder.InsertData(
                table: "MasterUser",
                columns: new[] { "Id", "BiodataId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Email", "IsLocked", "Is_delete", "LastLogin", "LoginAttempt", "Modified_by", "Modified_on", "Otp", "OtpExpire", "Password", "RoleId" },
                values: new object[,]
                {
                    { 4L, 1L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "christie@gmail.com", false, false, new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2718), 0, null, null, null, null, "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd", 2L },
                    { 5L, 2L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "hendra@gmail.com", false, false, new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2719), 0, null, null, null, null, "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd", 2L }
                });

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2786));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2792));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1688));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1700));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1701));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1702));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1703));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1704));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1705));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1706));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1707));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1708));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1709));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1711));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1712));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1713));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1714));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1715));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(1717));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3405));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2238));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2259));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2266));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2274));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2280));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2287));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2293));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2364));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2378));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2385));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2391));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2405));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2411));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2847));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2848));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2851));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2852));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2853));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2854));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2855));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2856));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2858));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2859));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2860));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2863));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2864));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2865));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2867));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2874));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2875));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2877));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2879));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2880));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2885));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2888));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2891));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2892));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2894));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2895));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2897));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2899));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2900));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2902));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2904));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2905));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2907));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2908));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2910));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2911));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2958));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2960));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2963));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2965));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2975));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2978));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2979));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2980));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2981));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2983));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2985));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2986));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2987));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2988));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2989));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2991));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2993));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2999));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3000));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3001));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3002));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3003));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3005));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3007));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3008));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3009));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3011));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3012));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3048));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3049));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3051));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3052));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3053));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3054));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3055));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3056));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3057));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3059));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3061));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3063));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3065));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3067));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3069));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3072));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3073));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3077));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3078));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3079));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3080));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3322));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3324));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3325));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3326));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3327));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(3328));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2510));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2512));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2549));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2550));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2552));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2553));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2554));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2556));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2557));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2557));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2558));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2559));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2560));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2561));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2562));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2564));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 16, 23, 16, 445, DateTimeKind.Local).AddTicks(2565));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(842));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(844));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(845));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(846));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(847));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(848));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(849));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(850));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(851));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(852));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(854));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(856));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(857));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(858));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(859));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(860));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(861));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(862));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(863));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(2040));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(2013));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(2019));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(759));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(761));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(762));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(763));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(764));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(765));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(766));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(769));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(771));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(776));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(777));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(778));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(782));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(806));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(808));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(809));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(810));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1390));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1393));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1395));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1396));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1398));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1400));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1402));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1404));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1407));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1408));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1410));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1411));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1412));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1414));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1415));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1417));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1422));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1442));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1444));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1301));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1302));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1303));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1304));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1314));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1305));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1306));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1307));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1308));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1309));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1311));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1317));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1313));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1318));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1555));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1560));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1561));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1562));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1563));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1268));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1270));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1272));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1273));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1275));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1276));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1277));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1278));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1280));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1851));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1853));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1854));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1855));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1856));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1858));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1859));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1860));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1861));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1863));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1864));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1865));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1866));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1867));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1868));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1869));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1870));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1872));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1873));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1874));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1879));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1880));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1881));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1882));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1884));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1885));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1886));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1887));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1888));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1891));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1892));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1893));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1894));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1895));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1897));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1898));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1899));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1900));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1901));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1902));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1903));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1904));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1906));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1907));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1909));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1910));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1911));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1912));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1913));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1914));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1915));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1916));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1917));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1918));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1920));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1921));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1922));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1923));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1924));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1925));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1926));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1927));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1929));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(889));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(891));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(892));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(893));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(894));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1507));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1511));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1512));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1604));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1609));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(590));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(603));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(604));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(605));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(607));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(609));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(610));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(611));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(614));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(615));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(616));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(617));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(618));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(622));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(623));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(624));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(2058));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(2059));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1007));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1016));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1024));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1033));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1055));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1072));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1091));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1098));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1109));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1124));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1131));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1138));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1213));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1228));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1235));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1629));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1630));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1632));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1633));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1634));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1640));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1647));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1648));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1649));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1651));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1657));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1658));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1659));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1660));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1661));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1663));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1665));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1666));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1668));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1669));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1670));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1671));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1672));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1674));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1675));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1676));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1678));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1679));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1680));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1681));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1682));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1683));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1684));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1685));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1687));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1688));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1689));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1691));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1692));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1693));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1694));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1695));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1697));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1698));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1701));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1703));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1706));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1707));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1708));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1709));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1711));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1714));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1715));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1716));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1722));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1724));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1726));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1728));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1730));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1735));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1738));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1740));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1743));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1746));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1749));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1751));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1754));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1756));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1757));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1758));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1760));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1761));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1763));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1764));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1765));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1766));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1768));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1769));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1771));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1772));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1775));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1777));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1779));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1780));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1785));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1788));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1789));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1792));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1795));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1796));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1798));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1799));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1963));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1965));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1341));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1343));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1345));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1346));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1348));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1349));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1350));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1351));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1352));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1360));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1361));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1362));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1364));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 13, 25, 26, 149, DateTimeKind.Local).AddTicks(1367));
        }
    }
}
