﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class ubahNamaTableSemua : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterAdmin_m_biodata_BiodataId",
                table: "MasterAdmin");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterBiodataAddress_MasterLocation_LocationId",
                table: "MasterBiodataAddress");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterBiodataAddress_m_biodata_BiodataId",
                table: "MasterBiodataAddress");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomer_MasterBloodGroup_BloodGroupId",
                table: "MasterCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomer_m_biodata_BiodataId",
                table: "MasterCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomerMember_MasterCustomerRelation_CustomerRelationId",
                table: "MasterCustomerMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomerMember_MasterCustomer_CustomerId",
                table: "MasterCustomerMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomerMember_m_biodata_ParentBiodataId",
                table: "MasterCustomerMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterDoctor_m_biodata_BiodataId",
                table: "MasterDoctor");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterDoctorEducation_MasterDoctor_DoctorId",
                table: "MasterDoctorEducation");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterDoctorEducation_MasterEducationLevel_EducationLevelId",
                table: "MasterDoctorEducation");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterLocation_MasterLocationLevel_LocationLevelId",
                table: "MasterLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterLocation_MasterLocation_ParentId",
                table: "MasterLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMedicalFacility_MasterLocation_LocationId",
                table: "MasterMedicalFacility");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMedicalFacility_MasterMedicalFacilityCategory_MedicalFacilityCategoryId",
                table: "MasterMedicalFacility");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMedicalFacilitySchedule_MasterMedicalFacility_MedicalFacilityId",
                table: "MasterMedicalFacilitySchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMenu_MasterMenu_ParentId",
                table: "MasterMenu");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMenuRole_MasterMenu_MenuId",
                table: "MasterMenuRole");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterMenuRole_MasterRole_RoleId",
                table: "MasterMenuRole");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterUser_MasterRole_RoleId",
                table: "MasterUser");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterUser_m_biodata_BiodataId",
                table: "MasterUser");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinment_MasterCustomer_CustomerId",
                table: "TransactionAppoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOfficeSchedule_DoctorOfficeScheduleId",
                table: "TransactionAppoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOfficeTreatment_DoctorOfficeTreatmentId",
                table: "TransactionAppoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOffice_DoctorOfficeId",
                table: "TransactionAppoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinmentDone_TransactionAppoinment_AppoinmentId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionCurrentDoctorSpecialization_MasterDoctor_DoctorId",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionCurrentDoctorSpecialization_MasterSpecialization_SpecializationId",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionCustomerChat_MasterCustomer_CustomerId",
                table: "TransactionCustomerChat");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionCustomerChat_MasterDoctor_DoctorId",
                table: "TransactionCustomerChat");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorOffice_DoctorOfficeId",
                table: "TransactionDoctorOfficeTreatment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorTreatment_DoctorTreatmentId",
                table: "TransactionDoctorOfficeTreatment");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeTreatmentPrice_TransactionDoctorOfficeTreatment_DoctorOfficeTreatmentId",
                table: "TransactionDoctorOfficeTreatmentPrice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionDoctorTreatment",
                table: "TransactionDoctorTreatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionDoctorOfficeTreatmentPrice",
                table: "TransactionDoctorOfficeTreatmentPrice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionDoctorOfficeTreatment",
                table: "TransactionDoctorOfficeTreatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionDoctorOfficeSchedule",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionDoctorOffice",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionCustomerChat",
                table: "TransactionCustomerChat");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionCurrentDoctorSpecialization",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionAppoinmentDone",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TransactionAppoinment",
                table: "TransactionAppoinment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterUser",
                table: "MasterUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterSpecialization",
                table: "MasterSpecialization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterRole",
                table: "MasterRole");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterMenuRole",
                table: "MasterMenuRole");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterMenu",
                table: "MasterMenu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterMedicalFacilitySchedule",
                table: "MasterMedicalFacilitySchedule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterMedicalFacilityCategory",
                table: "MasterMedicalFacilityCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterMedicalFacility",
                table: "MasterMedicalFacility");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterLocationLevel",
                table: "MasterLocationLevel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterLocation",
                table: "MasterLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterEducationLevel",
                table: "MasterEducationLevel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterDoctorEducation",
                table: "MasterDoctorEducation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterDoctor",
                table: "MasterDoctor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterCustomerRelation",
                table: "MasterCustomerRelation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterCustomerMember",
                table: "MasterCustomerMember");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterCustomer",
                table: "MasterCustomer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterBloodGroup",
                table: "MasterBloodGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterBiodataAddress",
                table: "MasterBiodataAddress");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterAdmin",
                table: "MasterAdmin");

            migrationBuilder.RenameTable(
                name: "TransactionDoctorTreatment",
                newName: "t_doctor_treatment");

            migrationBuilder.RenameTable(
                name: "TransactionDoctorOfficeTreatmentPrice",
                newName: "t_doctor_office_treatment_price");

            migrationBuilder.RenameTable(
                name: "TransactionDoctorOfficeTreatment",
                newName: "t_doctor_office_treatment");

            migrationBuilder.RenameTable(
                name: "TransactionDoctorOfficeSchedule",
                newName: "t_doctor_office_schedule");

            migrationBuilder.RenameTable(
                name: "TransactionDoctorOffice",
                newName: "t_doctor_office");

            migrationBuilder.RenameTable(
                name: "TransactionCustomerChat",
                newName: "t_customer_chat");

            migrationBuilder.RenameTable(
                name: "TransactionCurrentDoctorSpecialization",
                newName: "t_current_doctor_specialization");

            migrationBuilder.RenameTable(
                name: "TransactionAppoinmentDone",
                newName: "t_appointment_done");

            migrationBuilder.RenameTable(
                name: "TransactionAppoinment",
                newName: "t_appoinment");

            migrationBuilder.RenameTable(
                name: "MasterUser",
                newName: "m_user");

            migrationBuilder.RenameTable(
                name: "MasterSpecialization",
                newName: "m_specialization");

            migrationBuilder.RenameTable(
                name: "MasterRole",
                newName: "m_role");

            migrationBuilder.RenameTable(
                name: "MasterMenuRole",
                newName: "m_menu_role");

            migrationBuilder.RenameTable(
                name: "MasterMenu",
                newName: "m_menu");

            migrationBuilder.RenameTable(
                name: "MasterMedicalFacilitySchedule",
                newName: "m_medical_facility_schedule");

            migrationBuilder.RenameTable(
                name: "MasterMedicalFacilityCategory",
                newName: "m_medical_facility_category");

            migrationBuilder.RenameTable(
                name: "MasterMedicalFacility",
                newName: "m_medical_facility");

            migrationBuilder.RenameTable(
                name: "MasterLocationLevel",
                newName: "m_location_level");

            migrationBuilder.RenameTable(
                name: "MasterLocation",
                newName: "m_location");

            migrationBuilder.RenameTable(
                name: "MasterEducationLevel",
                newName: "m_education_level");

            migrationBuilder.RenameTable(
                name: "MasterDoctorEducation",
                newName: "m_doctor_education");

            migrationBuilder.RenameTable(
                name: "MasterDoctor",
                newName: "m_doctor");

            migrationBuilder.RenameTable(
                name: "MasterCustomerRelation",
                newName: "m_customer_relation");

            migrationBuilder.RenameTable(
                name: "MasterCustomerMember",
                newName: "m_customer_member");

            migrationBuilder.RenameTable(
                name: "MasterCustomer",
                newName: "m_customer");

            migrationBuilder.RenameTable(
                name: "MasterBloodGroup",
                newName: "m_blood_group");

            migrationBuilder.RenameTable(
                name: "MasterBiodataAddress",
                newName: "m_biodata_address");

            migrationBuilder.RenameTable(
                name: "MasterAdmin",
                newName: "m_admin");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorTreatment_DoctorId",
                table: "t_doctor_treatment",
                newName: "IX_t_doctor_treatment_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOfficeTreatmentPrice_DoctorOfficeTreatmentId",
                table: "t_doctor_office_treatment_price",
                newName: "IX_t_doctor_office_treatment_price_DoctorOfficeTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOfficeTreatment_DoctorTreatmentId",
                table: "t_doctor_office_treatment",
                newName: "IX_t_doctor_office_treatment_DoctorTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOfficeTreatment_DoctorOfficeId",
                table: "t_doctor_office_treatment",
                newName: "IX_t_doctor_office_treatment_DoctorOfficeId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOfficeSchedule_MedicalFacilityScheduleId",
                table: "t_doctor_office_schedule",
                newName: "IX_t_doctor_office_schedule_MedicalFacilityScheduleId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOfficeSchedule_DoctorId",
                table: "t_doctor_office_schedule",
                newName: "IX_t_doctor_office_schedule_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOffice_MedicalFacilityId",
                table: "t_doctor_office",
                newName: "IX_t_doctor_office_MedicalFacilityId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionDoctorOffice_DoctorId",
                table: "t_doctor_office",
                newName: "IX_t_doctor_office_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionCustomerChat_DoctorId",
                table: "t_customer_chat",
                newName: "IX_t_customer_chat_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionCustomerChat_CustomerId",
                table: "t_customer_chat",
                newName: "IX_t_customer_chat_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionCurrentDoctorSpecialization_SpecializationId",
                table: "t_current_doctor_specialization",
                newName: "IX_t_current_doctor_specialization_SpecializationId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionCurrentDoctorSpecialization_DoctorId",
                table: "t_current_doctor_specialization",
                newName: "IX_t_current_doctor_specialization_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionAppoinmentDone_AppoinmentId",
                table: "t_appointment_done",
                newName: "IX_t_appointment_done_AppoinmentId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeTreatmentId",
                table: "t_appoinment",
                newName: "IX_t_appoinment_DoctorOfficeTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeScheduleId",
                table: "t_appoinment",
                newName: "IX_t_appoinment_DoctorOfficeScheduleId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeId",
                table: "t_appoinment",
                newName: "IX_t_appoinment_DoctorOfficeId");

            migrationBuilder.RenameIndex(
                name: "IX_TransactionAppoinment_CustomerId",
                table: "t_appoinment",
                newName: "IX_t_appoinment_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterUser_RoleId",
                table: "m_user",
                newName: "IX_m_user_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterUser_BiodataId",
                table: "m_user",
                newName: "IX_m_user_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMenuRole_RoleId",
                table: "m_menu_role",
                newName: "IX_m_menu_role_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMenuRole_MenuId",
                table: "m_menu_role",
                newName: "IX_m_menu_role_MenuId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMenu_ParentId",
                table: "m_menu",
                newName: "IX_m_menu_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMedicalFacilitySchedule_MedicalFacilityId",
                table: "m_medical_facility_schedule",
                newName: "IX_m_medical_facility_schedule_MedicalFacilityId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMedicalFacility_MedicalFacilityCategoryId",
                table: "m_medical_facility",
                newName: "IX_m_medical_facility_MedicalFacilityCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterMedicalFacility_LocationId",
                table: "m_medical_facility",
                newName: "IX_m_medical_facility_LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterLocation_ParentId",
                table: "m_location",
                newName: "IX_m_location_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterLocation_LocationLevelId",
                table: "m_location",
                newName: "IX_m_location_LocationLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterDoctorEducation_EducationLevelId",
                table: "m_doctor_education",
                newName: "IX_m_doctor_education_EducationLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterDoctorEducation_DoctorId",
                table: "m_doctor_education",
                newName: "IX_m_doctor_education_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterDoctor_BiodataId",
                table: "m_doctor",
                newName: "IX_m_doctor_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterCustomerMember_ParentBiodataId",
                table: "m_customer_member",
                newName: "IX_m_customer_member_ParentBiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterCustomerMember_CustomerRelationId",
                table: "m_customer_member",
                newName: "IX_m_customer_member_CustomerRelationId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterCustomerMember_CustomerId",
                table: "m_customer_member",
                newName: "IX_m_customer_member_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterCustomer_BloodGroupId",
                table: "m_customer",
                newName: "IX_m_customer_BloodGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterCustomer_BiodataId",
                table: "m_customer",
                newName: "IX_m_customer_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterBiodataAddress_LocationId",
                table: "m_biodata_address",
                newName: "IX_m_biodata_address_LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterBiodataAddress_BiodataId",
                table: "m_biodata_address",
                newName: "IX_m_biodata_address_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_MasterAdmin_BiodataId",
                table: "m_admin",
                newName: "IX_m_admin_BiodataId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_treatment",
                table: "t_doctor_treatment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_office_treatment_price",
                table: "t_doctor_office_treatment_price",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_office_treatment",
                table: "t_doctor_office_treatment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_office_schedule",
                table: "t_doctor_office_schedule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_office",
                table: "t_doctor_office",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_customer_chat",
                table: "t_customer_chat",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_current_doctor_specialization",
                table: "t_current_doctor_specialization",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_appointment_done",
                table: "t_appointment_done",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_appoinment",
                table: "t_appoinment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_user",
                table: "m_user",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_specialization",
                table: "m_specialization",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_role",
                table: "m_role",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_menu_role",
                table: "m_menu_role",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_menu",
                table: "m_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_medical_facility_schedule",
                table: "m_medical_facility_schedule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_medical_facility_category",
                table: "m_medical_facility_category",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_medical_facility",
                table: "m_medical_facility",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_location_level",
                table: "m_location_level",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_location",
                table: "m_location",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_education_level",
                table: "m_education_level",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_doctor_education",
                table: "m_doctor_education",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_doctor",
                table: "m_doctor",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_customer_relation",
                table: "m_customer_relation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_customer_member",
                table: "m_customer_member",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_customer",
                table: "m_customer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_blood_group",
                table: "m_blood_group",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_biodata_address",
                table: "m_biodata_address",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_admin",
                table: "m_admin",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2941));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2944));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2946));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2949));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2951));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2954));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2958));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2961));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2963));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2966));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2968));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2970));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2973));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2989));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2992));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2994));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2996));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2998));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5624));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5574));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5589));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2789));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2793));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2796));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2798));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2801));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2804));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2806));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2808));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2810));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2821));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2826));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2836));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2884));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2886));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2888));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2891));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4284));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4287));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4290));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4295));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4298));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4300));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4303));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4306));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4308));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4311));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4314));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4316));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4319));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4322));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4325));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4328));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4366));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4368));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4128));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4131));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4133));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4135));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4154));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4138));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4140));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4143));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4145));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4147));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4150));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4157));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4152));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4159));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4662));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4666));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4668));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4670));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4672));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4674));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4062));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4066));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4069));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4072));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4074));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4077));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4080));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4083));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4086));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5225));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5229));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5247));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5252));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5254));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5257));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5259));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5274));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5279));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5281));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5286));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5306));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5313));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5315));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5317));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5322));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5324));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5329));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5331));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5334));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5336));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5338));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5341));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5348));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5350));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5353));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5355));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5357));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5360));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5362));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5365));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5379));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5382));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5386));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5388));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5393));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5398));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5403));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3056));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3058));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3060));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3062));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3064));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4551));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4555));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4558));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4561));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4564));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4752));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2410));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2440));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2442));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2444));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2447));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2449));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2477));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2479));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2482));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2484));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2486));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2488));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2490));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2504));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2506));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2508));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5660));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5663));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3306));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3329));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3349));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3367));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3435));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3458));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3475));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3529));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3547));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3564));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3581));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3597));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3614));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3631));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3811));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3849));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3866));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4809));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4817));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4819));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4824));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4827));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4830));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4832));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4835));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4838));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4840));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4842));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4845));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4847));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4850));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4852));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4854));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4859));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4862));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4865));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4867));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4869));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4872));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4874));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4877));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4879));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4882));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4884));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4886));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4889));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4891));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4894));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4896));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4899));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4904));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4906));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4909));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4911));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4914));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4916));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4918));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4921));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4923));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4940));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4943));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4945));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4947));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4950));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4952));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4957));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4960));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4962));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4964));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4967));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4972));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4984));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4992));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4994));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4997));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5002));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5004));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5007));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5009));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5012));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5014));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5017));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5019));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5021));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5024));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5026));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5029));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5031));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5034));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5037));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5039));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5042));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5044));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5047));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5049));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5052));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5054));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5069));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5071));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5074));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5076));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5079));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5081));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5084));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5086));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5089));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5091));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5093));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5099));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5103));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5108));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5111));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5113));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5115));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5118));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5120));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5123));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5125));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5128));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5130));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5132));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5135));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5137));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5140));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5464));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5467));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5469));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5474));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5476));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5478));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5482));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5485));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5493));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4194));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4210));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4212));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4215));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4217));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4221));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4223));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4228));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4230));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4232));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4234));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4236));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4238));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4240));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4242));

            migrationBuilder.AddForeignKey(
                name: "FK_m_admin_m_biodata_BiodataId",
                table: "m_admin",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_biodata_address_m_biodata_BiodataId",
                table: "m_biodata_address",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_biodata_address_m_location_LocationId",
                table: "m_biodata_address",
                column: "LocationId",
                principalTable: "m_location",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_customer_m_biodata_BiodataId",
                table: "m_customer",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_customer_m_blood_group_BloodGroupId",
                table: "m_customer",
                column: "BloodGroupId",
                principalTable: "m_blood_group",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_customer_member_m_biodata_ParentBiodataId",
                table: "m_customer_member",
                column: "ParentBiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_customer_member_m_customer_CustomerId",
                table: "m_customer_member",
                column: "CustomerId",
                principalTable: "m_customer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_customer_member_m_customer_relation_CustomerRelationId",
                table: "m_customer_member",
                column: "CustomerRelationId",
                principalTable: "m_customer_relation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_doctor_m_biodata_BiodataId",
                table: "m_doctor",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_doctor_education_m_doctor_DoctorId",
                table: "m_doctor_education",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_doctor_education_m_education_level_EducationLevelId",
                table: "m_doctor_education",
                column: "EducationLevelId",
                principalTable: "m_education_level",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_location_m_location_ParentId",
                table: "m_location",
                column: "ParentId",
                principalTable: "m_location",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_location_m_location_level_LocationLevelId",
                table: "m_location",
                column: "LocationLevelId",
                principalTable: "m_location_level",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_medical_facility_m_location_LocationId",
                table: "m_medical_facility",
                column: "LocationId",
                principalTable: "m_location",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_medical_facility_m_medical_facility_category_MedicalFacilityCategoryId",
                table: "m_medical_facility",
                column: "MedicalFacilityCategoryId",
                principalTable: "m_medical_facility_category",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_medical_facility_schedule_m_medical_facility_MedicalFacilityId",
                table: "m_medical_facility_schedule",
                column: "MedicalFacilityId",
                principalTable: "m_medical_facility",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_menu_m_menu_ParentId",
                table: "m_menu",
                column: "ParentId",
                principalTable: "m_menu",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_menu_role_m_menu_MenuId",
                table: "m_menu_role",
                column: "MenuId",
                principalTable: "m_menu",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_m_menu_role_m_role_RoleId",
                table: "m_menu_role",
                column: "RoleId",
                principalTable: "m_role",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_m_user_m_biodata_BiodataId",
                table: "m_user",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_m_user_m_role_RoleId",
                table: "m_user",
                column: "RoleId",
                principalTable: "m_role",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_appoinment_m_customer_CustomerId",
                table: "t_appoinment",
                column: "CustomerId",
                principalTable: "m_customer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_appoinment_t_doctor_office_DoctorOfficeId",
                table: "t_appoinment",
                column: "DoctorOfficeId",
                principalTable: "t_doctor_office",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_appoinment_t_doctor_office_schedule_DoctorOfficeScheduleId",
                table: "t_appoinment",
                column: "DoctorOfficeScheduleId",
                principalTable: "t_doctor_office_schedule",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_appoinment_t_doctor_office_treatment_DoctorOfficeTreatmentId",
                table: "t_appoinment",
                column: "DoctorOfficeTreatmentId",
                principalTable: "t_doctor_office_treatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_appointment_done_t_appoinment_AppoinmentId",
                table: "t_appointment_done",
                column: "AppoinmentId",
                principalTable: "t_appoinment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_current_doctor_specialization_m_doctor_DoctorId",
                table: "t_current_doctor_specialization",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_t_current_doctor_specialization_m_specialization_SpecializationId",
                table: "t_current_doctor_specialization",
                column: "SpecializationId",
                principalTable: "m_specialization",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_t_customer_chat_m_customer_CustomerId",
                table: "t_customer_chat",
                column: "CustomerId",
                principalTable: "m_customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_t_customer_chat_m_doctor_DoctorId",
                table: "t_customer_chat",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_m_doctor_DoctorId",
                table: "t_doctor_office",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_m_medical_facility_MedicalFacilityId",
                table: "t_doctor_office",
                column: "MedicalFacilityId",
                principalTable: "m_medical_facility",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_schedule_m_doctor_DoctorId",
                table: "t_doctor_office_schedule",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_schedule_m_medical_facility_schedule_MedicalFacilityScheduleId",
                table: "t_doctor_office_schedule",
                column: "MedicalFacilityScheduleId",
                principalTable: "m_medical_facility_schedule",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_treatment_t_doctor_office_DoctorOfficeId",
                table: "t_doctor_office_treatment",
                column: "DoctorOfficeId",
                principalTable: "t_doctor_office",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_treatment_t_doctor_treatment_DoctorTreatmentId",
                table: "t_doctor_office_treatment",
                column: "DoctorTreatmentId",
                principalTable: "t_doctor_treatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_office_treatment_price_t_doctor_office_treatment_DoctorOfficeTreatmentId",
                table: "t_doctor_office_treatment_price",
                column: "DoctorOfficeTreatmentId",
                principalTable: "t_doctor_office_treatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_treatment_m_doctor_DoctorId",
                table: "t_doctor_treatment",
                column: "DoctorId",
                principalTable: "m_doctor",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_m_admin_m_biodata_BiodataId",
                table: "m_admin");

            migrationBuilder.DropForeignKey(
                name: "FK_m_biodata_address_m_biodata_BiodataId",
                table: "m_biodata_address");

            migrationBuilder.DropForeignKey(
                name: "FK_m_biodata_address_m_location_LocationId",
                table: "m_biodata_address");

            migrationBuilder.DropForeignKey(
                name: "FK_m_customer_m_biodata_BiodataId",
                table: "m_customer");

            migrationBuilder.DropForeignKey(
                name: "FK_m_customer_m_blood_group_BloodGroupId",
                table: "m_customer");

            migrationBuilder.DropForeignKey(
                name: "FK_m_customer_member_m_biodata_ParentBiodataId",
                table: "m_customer_member");

            migrationBuilder.DropForeignKey(
                name: "FK_m_customer_member_m_customer_CustomerId",
                table: "m_customer_member");

            migrationBuilder.DropForeignKey(
                name: "FK_m_customer_member_m_customer_relation_CustomerRelationId",
                table: "m_customer_member");

            migrationBuilder.DropForeignKey(
                name: "FK_m_doctor_m_biodata_BiodataId",
                table: "m_doctor");

            migrationBuilder.DropForeignKey(
                name: "FK_m_doctor_education_m_doctor_DoctorId",
                table: "m_doctor_education");

            migrationBuilder.DropForeignKey(
                name: "FK_m_doctor_education_m_education_level_EducationLevelId",
                table: "m_doctor_education");

            migrationBuilder.DropForeignKey(
                name: "FK_m_location_m_location_ParentId",
                table: "m_location");

            migrationBuilder.DropForeignKey(
                name: "FK_m_location_m_location_level_LocationLevelId",
                table: "m_location");

            migrationBuilder.DropForeignKey(
                name: "FK_m_medical_facility_m_location_LocationId",
                table: "m_medical_facility");

            migrationBuilder.DropForeignKey(
                name: "FK_m_medical_facility_m_medical_facility_category_MedicalFacilityCategoryId",
                table: "m_medical_facility");

            migrationBuilder.DropForeignKey(
                name: "FK_m_medical_facility_schedule_m_medical_facility_MedicalFacilityId",
                table: "m_medical_facility_schedule");

            migrationBuilder.DropForeignKey(
                name: "FK_m_menu_m_menu_ParentId",
                table: "m_menu");

            migrationBuilder.DropForeignKey(
                name: "FK_m_menu_role_m_menu_MenuId",
                table: "m_menu_role");

            migrationBuilder.DropForeignKey(
                name: "FK_m_menu_role_m_role_RoleId",
                table: "m_menu_role");

            migrationBuilder.DropForeignKey(
                name: "FK_m_user_m_biodata_BiodataId",
                table: "m_user");

            migrationBuilder.DropForeignKey(
                name: "FK_m_user_m_role_RoleId",
                table: "m_user");

            migrationBuilder.DropForeignKey(
                name: "FK_t_appoinment_m_customer_CustomerId",
                table: "t_appoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_appoinment_t_doctor_office_DoctorOfficeId",
                table: "t_appoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_appoinment_t_doctor_office_schedule_DoctorOfficeScheduleId",
                table: "t_appoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_appoinment_t_doctor_office_treatment_DoctorOfficeTreatmentId",
                table: "t_appoinment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_appointment_done_t_appoinment_AppoinmentId",
                table: "t_appointment_done");

            migrationBuilder.DropForeignKey(
                name: "FK_t_current_doctor_specialization_m_doctor_DoctorId",
                table: "t_current_doctor_specialization");

            migrationBuilder.DropForeignKey(
                name: "FK_t_current_doctor_specialization_m_specialization_SpecializationId",
                table: "t_current_doctor_specialization");

            migrationBuilder.DropForeignKey(
                name: "FK_t_customer_chat_m_customer_CustomerId",
                table: "t_customer_chat");

            migrationBuilder.DropForeignKey(
                name: "FK_t_customer_chat_m_doctor_DoctorId",
                table: "t_customer_chat");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_m_doctor_DoctorId",
                table: "t_doctor_office");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_m_medical_facility_MedicalFacilityId",
                table: "t_doctor_office");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_schedule_m_doctor_DoctorId",
                table: "t_doctor_office_schedule");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_schedule_m_medical_facility_schedule_MedicalFacilityScheduleId",
                table: "t_doctor_office_schedule");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_treatment_t_doctor_office_DoctorOfficeId",
                table: "t_doctor_office_treatment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_treatment_t_doctor_treatment_DoctorTreatmentId",
                table: "t_doctor_office_treatment");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_office_treatment_price_t_doctor_office_treatment_DoctorOfficeTreatmentId",
                table: "t_doctor_office_treatment_price");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_treatment_m_doctor_DoctorId",
                table: "t_doctor_treatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_treatment",
                table: "t_doctor_treatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_office_treatment_price",
                table: "t_doctor_office_treatment_price");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_office_treatment",
                table: "t_doctor_office_treatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_office_schedule",
                table: "t_doctor_office_schedule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_office",
                table: "t_doctor_office");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_customer_chat",
                table: "t_customer_chat");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_current_doctor_specialization",
                table: "t_current_doctor_specialization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_appointment_done",
                table: "t_appointment_done");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_appoinment",
                table: "t_appoinment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_user",
                table: "m_user");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_specialization",
                table: "m_specialization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_role",
                table: "m_role");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_menu_role",
                table: "m_menu_role");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_menu",
                table: "m_menu");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_medical_facility_schedule",
                table: "m_medical_facility_schedule");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_medical_facility_category",
                table: "m_medical_facility_category");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_medical_facility",
                table: "m_medical_facility");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_location_level",
                table: "m_location_level");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_location",
                table: "m_location");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_education_level",
                table: "m_education_level");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_doctor_education",
                table: "m_doctor_education");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_doctor",
                table: "m_doctor");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_customer_relation",
                table: "m_customer_relation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_customer_member",
                table: "m_customer_member");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_customer",
                table: "m_customer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_blood_group",
                table: "m_blood_group");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_biodata_address",
                table: "m_biodata_address");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_admin",
                table: "m_admin");

            migrationBuilder.RenameTable(
                name: "t_doctor_treatment",
                newName: "TransactionDoctorTreatment");

            migrationBuilder.RenameTable(
                name: "t_doctor_office_treatment_price",
                newName: "TransactionDoctorOfficeTreatmentPrice");

            migrationBuilder.RenameTable(
                name: "t_doctor_office_treatment",
                newName: "TransactionDoctorOfficeTreatment");

            migrationBuilder.RenameTable(
                name: "t_doctor_office_schedule",
                newName: "TransactionDoctorOfficeSchedule");

            migrationBuilder.RenameTable(
                name: "t_doctor_office",
                newName: "TransactionDoctorOffice");

            migrationBuilder.RenameTable(
                name: "t_customer_chat",
                newName: "TransactionCustomerChat");

            migrationBuilder.RenameTable(
                name: "t_current_doctor_specialization",
                newName: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.RenameTable(
                name: "t_appointment_done",
                newName: "TransactionAppoinmentDone");

            migrationBuilder.RenameTable(
                name: "t_appoinment",
                newName: "TransactionAppoinment");

            migrationBuilder.RenameTable(
                name: "m_user",
                newName: "MasterUser");

            migrationBuilder.RenameTable(
                name: "m_specialization",
                newName: "MasterSpecialization");

            migrationBuilder.RenameTable(
                name: "m_role",
                newName: "MasterRole");

            migrationBuilder.RenameTable(
                name: "m_menu_role",
                newName: "MasterMenuRole");

            migrationBuilder.RenameTable(
                name: "m_menu",
                newName: "MasterMenu");

            migrationBuilder.RenameTable(
                name: "m_medical_facility_schedule",
                newName: "MasterMedicalFacilitySchedule");

            migrationBuilder.RenameTable(
                name: "m_medical_facility_category",
                newName: "MasterMedicalFacilityCategory");

            migrationBuilder.RenameTable(
                name: "m_medical_facility",
                newName: "MasterMedicalFacility");

            migrationBuilder.RenameTable(
                name: "m_location_level",
                newName: "MasterLocationLevel");

            migrationBuilder.RenameTable(
                name: "m_location",
                newName: "MasterLocation");

            migrationBuilder.RenameTable(
                name: "m_education_level",
                newName: "MasterEducationLevel");

            migrationBuilder.RenameTable(
                name: "m_doctor_education",
                newName: "MasterDoctorEducation");

            migrationBuilder.RenameTable(
                name: "m_doctor",
                newName: "MasterDoctor");

            migrationBuilder.RenameTable(
                name: "m_customer_relation",
                newName: "MasterCustomerRelation");

            migrationBuilder.RenameTable(
                name: "m_customer_member",
                newName: "MasterCustomerMember");

            migrationBuilder.RenameTable(
                name: "m_customer",
                newName: "MasterCustomer");

            migrationBuilder.RenameTable(
                name: "m_blood_group",
                newName: "MasterBloodGroup");

            migrationBuilder.RenameTable(
                name: "m_biodata_address",
                newName: "MasterBiodataAddress");

            migrationBuilder.RenameTable(
                name: "m_admin",
                newName: "MasterAdmin");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_treatment_DoctorId",
                table: "TransactionDoctorTreatment",
                newName: "IX_TransactionDoctorTreatment_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_treatment_price_DoctorOfficeTreatmentId",
                table: "TransactionDoctorOfficeTreatmentPrice",
                newName: "IX_TransactionDoctorOfficeTreatmentPrice_DoctorOfficeTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_treatment_DoctorTreatmentId",
                table: "TransactionDoctorOfficeTreatment",
                newName: "IX_TransactionDoctorOfficeTreatment_DoctorTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_treatment_DoctorOfficeId",
                table: "TransactionDoctorOfficeTreatment",
                newName: "IX_TransactionDoctorOfficeTreatment_DoctorOfficeId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_schedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                newName: "IX_TransactionDoctorOfficeSchedule_MedicalFacilityScheduleId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_schedule_DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                newName: "IX_TransactionDoctorOfficeSchedule_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_MedicalFacilityId",
                table: "TransactionDoctorOffice",
                newName: "IX_TransactionDoctorOffice_MedicalFacilityId");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_office_DoctorId",
                table: "TransactionDoctorOffice",
                newName: "IX_TransactionDoctorOffice_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_t_customer_chat_DoctorId",
                table: "TransactionCustomerChat",
                newName: "IX_TransactionCustomerChat_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_t_customer_chat_CustomerId",
                table: "TransactionCustomerChat",
                newName: "IX_TransactionCustomerChat_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_t_current_doctor_specialization_SpecializationId",
                table: "TransactionCurrentDoctorSpecialization",
                newName: "IX_TransactionCurrentDoctorSpecialization_SpecializationId");

            migrationBuilder.RenameIndex(
                name: "IX_t_current_doctor_specialization_DoctorId",
                table: "TransactionCurrentDoctorSpecialization",
                newName: "IX_TransactionCurrentDoctorSpecialization_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_t_appointment_done_AppoinmentId",
                table: "TransactionAppoinmentDone",
                newName: "IX_TransactionAppoinmentDone_AppoinmentId");

            migrationBuilder.RenameIndex(
                name: "IX_t_appoinment_DoctorOfficeTreatmentId",
                table: "TransactionAppoinment",
                newName: "IX_TransactionAppoinment_DoctorOfficeTreatmentId");

            migrationBuilder.RenameIndex(
                name: "IX_t_appoinment_DoctorOfficeScheduleId",
                table: "TransactionAppoinment",
                newName: "IX_TransactionAppoinment_DoctorOfficeScheduleId");

            migrationBuilder.RenameIndex(
                name: "IX_t_appoinment_DoctorOfficeId",
                table: "TransactionAppoinment",
                newName: "IX_TransactionAppoinment_DoctorOfficeId");

            migrationBuilder.RenameIndex(
                name: "IX_t_appoinment_CustomerId",
                table: "TransactionAppoinment",
                newName: "IX_TransactionAppoinment_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_m_user_RoleId",
                table: "MasterUser",
                newName: "IX_MasterUser_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_m_user_BiodataId",
                table: "MasterUser",
                newName: "IX_MasterUser_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_m_menu_role_RoleId",
                table: "MasterMenuRole",
                newName: "IX_MasterMenuRole_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_m_menu_role_MenuId",
                table: "MasterMenuRole",
                newName: "IX_MasterMenuRole_MenuId");

            migrationBuilder.RenameIndex(
                name: "IX_m_menu_ParentId",
                table: "MasterMenu",
                newName: "IX_MasterMenu_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_m_medical_facility_schedule_MedicalFacilityId",
                table: "MasterMedicalFacilitySchedule",
                newName: "IX_MasterMedicalFacilitySchedule_MedicalFacilityId");

            migrationBuilder.RenameIndex(
                name: "IX_m_medical_facility_MedicalFacilityCategoryId",
                table: "MasterMedicalFacility",
                newName: "IX_MasterMedicalFacility_MedicalFacilityCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_m_medical_facility_LocationId",
                table: "MasterMedicalFacility",
                newName: "IX_MasterMedicalFacility_LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_m_location_ParentId",
                table: "MasterLocation",
                newName: "IX_MasterLocation_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_m_location_LocationLevelId",
                table: "MasterLocation",
                newName: "IX_MasterLocation_LocationLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_m_doctor_education_EducationLevelId",
                table: "MasterDoctorEducation",
                newName: "IX_MasterDoctorEducation_EducationLevelId");

            migrationBuilder.RenameIndex(
                name: "IX_m_doctor_education_DoctorId",
                table: "MasterDoctorEducation",
                newName: "IX_MasterDoctorEducation_DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_m_doctor_BiodataId",
                table: "MasterDoctor",
                newName: "IX_MasterDoctor_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_m_customer_member_ParentBiodataId",
                table: "MasterCustomerMember",
                newName: "IX_MasterCustomerMember_ParentBiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_m_customer_member_CustomerRelationId",
                table: "MasterCustomerMember",
                newName: "IX_MasterCustomerMember_CustomerRelationId");

            migrationBuilder.RenameIndex(
                name: "IX_m_customer_member_CustomerId",
                table: "MasterCustomerMember",
                newName: "IX_MasterCustomerMember_CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_m_customer_BloodGroupId",
                table: "MasterCustomer",
                newName: "IX_MasterCustomer_BloodGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_m_customer_BiodataId",
                table: "MasterCustomer",
                newName: "IX_MasterCustomer_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_m_biodata_address_LocationId",
                table: "MasterBiodataAddress",
                newName: "IX_MasterBiodataAddress_LocationId");

            migrationBuilder.RenameIndex(
                name: "IX_m_biodata_address_BiodataId",
                table: "MasterBiodataAddress",
                newName: "IX_MasterBiodataAddress_BiodataId");

            migrationBuilder.RenameIndex(
                name: "IX_m_admin_BiodataId",
                table: "MasterAdmin",
                newName: "IX_MasterAdmin_BiodataId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionDoctorTreatment",
                table: "TransactionDoctorTreatment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionDoctorOfficeTreatmentPrice",
                table: "TransactionDoctorOfficeTreatmentPrice",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionDoctorOfficeTreatment",
                table: "TransactionDoctorOfficeTreatment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionDoctorOfficeSchedule",
                table: "TransactionDoctorOfficeSchedule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionDoctorOffice",
                table: "TransactionDoctorOffice",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionCustomerChat",
                table: "TransactionCustomerChat",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionCurrentDoctorSpecialization",
                table: "TransactionCurrentDoctorSpecialization",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionAppoinmentDone",
                table: "TransactionAppoinmentDone",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TransactionAppoinment",
                table: "TransactionAppoinment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterUser",
                table: "MasterUser",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterSpecialization",
                table: "MasterSpecialization",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterRole",
                table: "MasterRole",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterMenuRole",
                table: "MasterMenuRole",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterMenu",
                table: "MasterMenu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterMedicalFacilitySchedule",
                table: "MasterMedicalFacilitySchedule",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterMedicalFacilityCategory",
                table: "MasterMedicalFacilityCategory",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterMedicalFacility",
                table: "MasterMedicalFacility",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterLocationLevel",
                table: "MasterLocationLevel",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterLocation",
                table: "MasterLocation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterEducationLevel",
                table: "MasterEducationLevel",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterDoctorEducation",
                table: "MasterDoctorEducation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterDoctor",
                table: "MasterDoctor",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterCustomerRelation",
                table: "MasterCustomerRelation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterCustomerMember",
                table: "MasterCustomerMember",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterCustomer",
                table: "MasterCustomer",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterBloodGroup",
                table: "MasterBloodGroup",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterBiodataAddress",
                table: "MasterBiodataAddress",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterAdmin",
                table: "MasterAdmin",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8871));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8750));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8764));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5282));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5307));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5309));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5312));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5368));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5371));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5373));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5375));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7034));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7037));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7039));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7042));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7045));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7048));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7051));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7054));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7056));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7059));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7062));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7065));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7073));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7075));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7153));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7201));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7203));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6796));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6800));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6802));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6805));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6893));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6807));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6810));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6812));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6815));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6887));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6896));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6891));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6898));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7459));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7467));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7469));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7471));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7473));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6726));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6734));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6740));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6742));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6745));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6748));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6751));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8340));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8344));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8346));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8349));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8351));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8354));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8356));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8359));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8361));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8364));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8366));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8368));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8371));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8373));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8376));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8378));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8383));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8386));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8388));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8391));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8393));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8396));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8399));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8401));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8403));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8406));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8409));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8411));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8414));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8416));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8419));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8421));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8424));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8426));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8428));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8431));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8433));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8436));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8438));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8440));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8443));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8512));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8515));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8517));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8520));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8522));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8524));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8527));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8529));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8532));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8534));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8536));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8539));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8541));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8544));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8547));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8549));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8551));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8554));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8556));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8558));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8561));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5615));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5619));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5620));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5622));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5624));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7349));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7353));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7356));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7359));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7362));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7607));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7620));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4941));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4973));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4985));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4992));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4994));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4996));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5017));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5019));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5022));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8910));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8914));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5872));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5911));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6038));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6103));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6173));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6190));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6207));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6418));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6435));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7662));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7666));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7668));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7671));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7674));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7676));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7679));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7681));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7683));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7686));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7688));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7691));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7698));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7700));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7703));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7705));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7708));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7710));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7715));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7717));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7720));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7722));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7730));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7732));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7735));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7740));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7849));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7851));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7854));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7856));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7859));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7861));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7864));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7883));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7886));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7888));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7891));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7900));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7903));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7908));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7910));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7912));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7915));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7922));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7925));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7930));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7932));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7935));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7937));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7939));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7942));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7947));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7949));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7952));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8085));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8091));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8094));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8096));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8099));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8101));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8108));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8112));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8117));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8122));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8125));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8127));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8130));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8132));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8135));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8137));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8142));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8145));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8147));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8150));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8153));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8155));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8157));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8160));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8162));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8165));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8167));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8170));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8172));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8175));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8177));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8180));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8182));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8185));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8625));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8631));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8636));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8640));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8643));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8645));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8647));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8649));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8652));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8654));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8656));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8659));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8661));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6953));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6957));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6975));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6977));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5423));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5428));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5516));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5519));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5521));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5524));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5526));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5528));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5530));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5532));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5534));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5537));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5539));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5544));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5549));

            migrationBuilder.AddForeignKey(
                name: "FK_MasterAdmin_m_biodata_BiodataId",
                table: "MasterAdmin",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterBiodataAddress_MasterLocation_LocationId",
                table: "MasterBiodataAddress",
                column: "LocationId",
                principalTable: "MasterLocation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterBiodataAddress_m_biodata_BiodataId",
                table: "MasterBiodataAddress",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomer_MasterBloodGroup_BloodGroupId",
                table: "MasterCustomer",
                column: "BloodGroupId",
                principalTable: "MasterBloodGroup",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomer_m_biodata_BiodataId",
                table: "MasterCustomer",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomerMember_MasterCustomerRelation_CustomerRelationId",
                table: "MasterCustomerMember",
                column: "CustomerRelationId",
                principalTable: "MasterCustomerRelation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomerMember_MasterCustomer_CustomerId",
                table: "MasterCustomerMember",
                column: "CustomerId",
                principalTable: "MasterCustomer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomerMember_m_biodata_ParentBiodataId",
                table: "MasterCustomerMember",
                column: "ParentBiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterDoctor_m_biodata_BiodataId",
                table: "MasterDoctor",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterDoctorEducation_MasterDoctor_DoctorId",
                table: "MasterDoctorEducation",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterDoctorEducation_MasterEducationLevel_EducationLevelId",
                table: "MasterDoctorEducation",
                column: "EducationLevelId",
                principalTable: "MasterEducationLevel",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterLocation_MasterLocationLevel_LocationLevelId",
                table: "MasterLocation",
                column: "LocationLevelId",
                principalTable: "MasterLocationLevel",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterLocation_MasterLocation_ParentId",
                table: "MasterLocation",
                column: "ParentId",
                principalTable: "MasterLocation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMedicalFacility_MasterLocation_LocationId",
                table: "MasterMedicalFacility",
                column: "LocationId",
                principalTable: "MasterLocation",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMedicalFacility_MasterMedicalFacilityCategory_MedicalFacilityCategoryId",
                table: "MasterMedicalFacility",
                column: "MedicalFacilityCategoryId",
                principalTable: "MasterMedicalFacilityCategory",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMedicalFacilitySchedule_MasterMedicalFacility_MedicalFacilityId",
                table: "MasterMedicalFacilitySchedule",
                column: "MedicalFacilityId",
                principalTable: "MasterMedicalFacility",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMenu_MasterMenu_ParentId",
                table: "MasterMenu",
                column: "ParentId",
                principalTable: "MasterMenu",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMenuRole_MasterMenu_MenuId",
                table: "MasterMenuRole",
                column: "MenuId",
                principalTable: "MasterMenu",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterMenuRole_MasterRole_RoleId",
                table: "MasterMenuRole",
                column: "RoleId",
                principalTable: "MasterRole",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterUser_MasterRole_RoleId",
                table: "MasterUser",
                column: "RoleId",
                principalTable: "MasterRole",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterUser_m_biodata_BiodataId",
                table: "MasterUser",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinment_MasterCustomer_CustomerId",
                table: "TransactionAppoinment",
                column: "CustomerId",
                principalTable: "MasterCustomer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOfficeSchedule_DoctorOfficeScheduleId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeScheduleId",
                principalTable: "TransactionDoctorOfficeSchedule",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOfficeTreatment_DoctorOfficeTreatmentId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeTreatmentId",
                principalTable: "TransactionDoctorOfficeTreatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinment_TransactionDoctorOffice_DoctorOfficeId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeId",
                principalTable: "TransactionDoctorOffice",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinmentDone_TransactionAppoinment_AppoinmentId",
                table: "TransactionAppoinmentDone",
                column: "AppoinmentId",
                principalTable: "TransactionAppoinment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionCurrentDoctorSpecialization_MasterDoctor_DoctorId",
                table: "TransactionCurrentDoctorSpecialization",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionCurrentDoctorSpecialization_MasterSpecialization_SpecializationId",
                table: "TransactionCurrentDoctorSpecialization",
                column: "SpecializationId",
                principalTable: "MasterSpecialization",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionCustomerChat_MasterCustomer_CustomerId",
                table: "TransactionCustomerChat",
                column: "CustomerId",
                principalTable: "MasterCustomer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionCustomerChat_MasterDoctor_DoctorId",
                table: "TransactionCustomerChat",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice",
                column: "MedicalFacilityId",
                principalTable: "MasterMedicalFacility",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                column: "MedicalFacilityScheduleId",
                principalTable: "MasterMedicalFacilitySchedule",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorOffice_DoctorOfficeId",
                table: "TransactionDoctorOfficeTreatment",
                column: "DoctorOfficeId",
                principalTable: "TransactionDoctorOffice",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorTreatment_DoctorTreatmentId",
                table: "TransactionDoctorOfficeTreatment",
                column: "DoctorTreatmentId",
                principalTable: "TransactionDoctorTreatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeTreatmentPrice_TransactionDoctorOfficeTreatment_DoctorOfficeTreatmentId",
                table: "TransactionDoctorOfficeTreatmentPrice",
                column: "DoctorOfficeTreatmentId",
                principalTable: "TransactionDoctorOfficeTreatment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");
        }
    }
}
