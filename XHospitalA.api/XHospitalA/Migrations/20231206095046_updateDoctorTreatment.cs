﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class updateDoctorTreatment : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment");

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorTreatment",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment");

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorTreatment",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                table: "TransactionDoctorTreatment",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
