﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class token : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Otp",
                table: "m_user");

            migrationBuilder.DropColumn(
                name: "OtpExpire",
                table: "m_user");

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5378));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5380));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5383));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5385));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5387));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5389));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5393));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5395));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5398));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5402));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5404));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5406));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5408));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5410));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5412));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5496));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8009));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7971));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7982));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5297));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5300));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5311));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5325));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5347));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5349));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6492));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6495));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6497));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6499));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6501));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6504));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6507));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6510));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6512));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6514));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6516));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6518));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6521));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6525));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6527));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6530));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6532));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6561));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6563));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6289));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6293));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6295));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6297));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6312));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6300));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6303));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6304));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6306));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6308));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6314));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6310));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6316));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6810));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6812));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6814));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6815));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6818));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6242));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6246));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6249));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6251));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6254));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6256));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6258));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6260));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7559));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7562));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7564));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7566));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7568));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7570));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7653));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7656));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7657));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7659));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7661));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7663));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7665));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7667));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7669));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7670));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7672));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7675));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7677));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7679));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7681));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7682));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7684));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7686));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7688));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7690));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7691));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7695));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7697));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7699));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7701));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7703));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7705));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7707));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7709));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7711));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7712));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7714));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7716));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7718));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7720));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7722));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7729));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7732));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7734));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7736));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7737));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7739));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7741));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7743));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7745));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7747));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7748));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7751));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7824));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7826));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7828));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7830));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7832));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7834));

            migrationBuilder.UpdateData(
                table: "m_menu",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Name",
                value: "locationlevel");

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5540));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5544));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5545));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5547));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6732));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6739));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6742));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6745));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6866));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6875));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4905));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4926));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4928));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4929));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4931));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4933));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4935));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4936));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5033));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5035));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5037));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5039));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5040));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5048));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5050));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5051));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8026));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8029));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5690));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5702));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5714));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5725));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5761));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5776));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5788));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5830));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5841));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5853));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5865));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5877));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5889));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6016));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6028));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6041));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6904));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6906));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6908));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7064));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7067));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7069));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7071));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7074));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7076));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7080));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7082));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7083));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7085));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7088));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7090));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7092));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7094));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7096));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7097));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7099));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7101));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7103));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7105));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7107));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7111));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7112));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7114));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7116));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7118));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7120));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7122));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7123));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7125));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7127));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7129));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7132));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7135));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7138));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7139));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7141));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7143));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7145));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7147));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7149));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7151));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7154));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7156));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7158));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7160));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7162));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7245));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7247));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7249));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7251));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7252));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7254));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7258));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7260));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7263));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7265));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7267));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7269));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7271));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7273));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7274));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7276));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7278));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7281));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7283));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7285));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7289));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7291));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7293));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7296));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7298));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7300));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7303));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7305));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7306));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7308));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7311));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7313));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7315));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7318));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7320));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7322));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7324));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7327));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7330));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7332));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7333));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7335));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7338));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7344));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7345));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7347));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7350));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7353));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7431));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7433));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7435));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7437));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7439));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7441));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7443));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7445));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7447));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7449));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7451));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7454));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7456));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7457));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7459));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7461));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7463));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7466));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7468));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7470));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7473));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7475));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7476));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7478));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7881));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7883));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7885));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7888));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7890));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7892));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7894));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7896));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7898));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7899));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7903));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7907));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6346));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6348));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6351));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6434));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6436));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6437));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6439));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6441));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6446));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6448));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6449));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6451));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6452));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Otp",
                table: "m_user",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OtpExpire",
                table: "m_user",
                type: "datetime2",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2941));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2944));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2946));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2949));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2951));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2954));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2958));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2961));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2963));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2966));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2968));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2970));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2973));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2989));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2992));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2994));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2996));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2998));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5624));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5574));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5589));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2789));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2793));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2796));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2798));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2801));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2804));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2806));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2808));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2810));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2821));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2823));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2826));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2836));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2884));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2886));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2888));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2891));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4284));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4287));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4290));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4295));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4298));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4300));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4303));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4306));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4308));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4311));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4314));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4316));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4319));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4322));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4325));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4328));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4366));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4368));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4128));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4131));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4133));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4135));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4154));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4138));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4140));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4143));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4145));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4147));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4150));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4157));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4152));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4159));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4662));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4666));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4668));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4670));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4672));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4674));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4062));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4066));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4069));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4072));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4074));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4077));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4080));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4083));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4086));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5225));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5229));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5247));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5252));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5254));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5257));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5259));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5274));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5279));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5281));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5286));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5306));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5313));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5315));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5317));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5322));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5324));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5329));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5331));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5334));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5336));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5338));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5341));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5348));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5350));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5353));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5355));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5357));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5360));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5362));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5365));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5379));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5382));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5386));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5388));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5393));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5398));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5403));

            migrationBuilder.UpdateData(
                table: "m_menu",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Name",
                value: "LevelLocation");

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3056));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3058));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3060));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3062));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3064));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "LastLogin", "Otp", "OtpExpire" },
                values: new object[] { new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4551), null, null });

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "LastLogin", "Otp", "OtpExpire" },
                values: new object[] { new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4555), null, null });

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "LastLogin", "Otp", "OtpExpire" },
                values: new object[] { new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4558), null, null });

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "LastLogin", "Otp", "OtpExpire" },
                values: new object[] { new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4561), null, null });

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "LastLogin", "Otp", "OtpExpire" },
                values: new object[] { new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4564), null, null });

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4752));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2410));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2440));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2442));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2444));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2447));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2449));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2475));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2477));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2479));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2482));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2484));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2486));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2488));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2490));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2504));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2506));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(2508));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5660));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5663));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3306));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3329));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3349));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3367));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3435));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3458));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3475));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3529));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3547));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3564));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3581));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3597));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3614));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3631));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3811));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3849));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(3866));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4809));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4817));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4819));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4824));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4827));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4830));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4832));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4835));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4838));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4840));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4842));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4845));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4847));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4850));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4852));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4854));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4859));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4862));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4865));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4867));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4869));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4872));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4874));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4877));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4879));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4882));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4884));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4886));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4889));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4891));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4894));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4896));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4899));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4904));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4906));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4909));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4911));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4914));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4916));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4918));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4921));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4923));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4940));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4943));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4945));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4947));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4950));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4952));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4957));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4960));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4962));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4964));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4967));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4972));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4984));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4992));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4994));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4997));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5002));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5004));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5007));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5009));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5012));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5014));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5017));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5019));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5021));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5024));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5026));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5029));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5031));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5034));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5037));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5039));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5042));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5044));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5047));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5049));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5052));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5054));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5069));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5071));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5074));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5076));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5079));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5081));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5084));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5086));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5089));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5091));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5093));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5099));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5103));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5106));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5108));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5111));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5113));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5115));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5118));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5120));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5123));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5125));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5128));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5130));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5132));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5135));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5137));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5140));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5464));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5467));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5469));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5474));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5476));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5478));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5482));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5485));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(5493));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4194));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4210));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4212));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4215));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4217));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4221));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4223));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4228));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4230));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4232));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4234));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4236));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4238));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4240));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 15, 1, 19, 508, DateTimeKind.Local).AddTicks(4242));
        }
    }
}
