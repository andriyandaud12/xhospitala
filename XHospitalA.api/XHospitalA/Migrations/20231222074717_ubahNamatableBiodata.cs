﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class ubahNamatableBiodata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterAdmin_MasterBiodata_BiodataId",
                table: "MasterAdmin");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterBiodataAddress_MasterBiodata_BiodataId",
                table: "MasterBiodataAddress");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomer_MasterBiodata_BiodataId",
                table: "MasterCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomerMember_MasterBiodata_ParentBiodataId",
                table: "MasterCustomerMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterDoctor_MasterBiodata_BiodataId",
                table: "MasterDoctor");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterUser_MasterBiodata_BiodataId",
                table: "MasterUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MasterBiodata",
                table: "MasterBiodata");

            migrationBuilder.RenameTable(
                name: "MasterBiodata",
                newName: "m_biodata");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_biodata",
                table: "m_biodata",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8871));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8750));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8764));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5282));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5307));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5309));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5312));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5368));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5371));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5373));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5375));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7034));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7037));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7039));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7042));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7045));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7048));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7051));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7054));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7056));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7059));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7062));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7065));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7073));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7075));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7153));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7201));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7203));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6796));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6800));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6802));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6805));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6893));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6807));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6810));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6812));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6815));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6887));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6896));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6891));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6898));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7459));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7467));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7469));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7471));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7473));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6726));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6734));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6740));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6742));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6745));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6748));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6751));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8340));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8344));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8346));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8349));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8351));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8354));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8356));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8359));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8361));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8364));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8366));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8368));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8371));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8373));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8376));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8378));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8383));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8386));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8388));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8391));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8393));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8396));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8399));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8401));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8403));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8406));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8409));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8411));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8414));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8416));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8419));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8421));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8424));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8426));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8428));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8431));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8433));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8436));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8438));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8440));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8443));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8512));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8515));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8517));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8520));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8522));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8524));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8527));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8529));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8532));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8534));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8536));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8539));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8541));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8544));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8547));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8549));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8551));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8554));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8556));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8558));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8561));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5615));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5619));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5620));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5622));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5624));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7349));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7353));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7356));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7359));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7362));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7607));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7620));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4941));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4973));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4985));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4992));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4994));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4996));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5017));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5019));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5022));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8910));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8914));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5872));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5911));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6038));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6103));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6173));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6190));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6207));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6418));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6435));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7662));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7666));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7668));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7671));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7674));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7676));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7679));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7681));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7683));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7686));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7688));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7691));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7698));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7700));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7703));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7705));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7708));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7710));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7715));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7717));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7720));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7722));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7730));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7732));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7735));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7740));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7849));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7851));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7854));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7856));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7859));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7861));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7864));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7883));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7886));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7888));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7891));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7900));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7903));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7908));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7910));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7912));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7915));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7922));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7925));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7930));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7932));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7935));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7937));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7939));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7942));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7947));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7949));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7952));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(7959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8085));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8091));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8094));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8096));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8099));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8101));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8108));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8112));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8115));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8117));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8122));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8125));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8127));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8130));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8132));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8135));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8137));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8140));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8142));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8145));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8147));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8150));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8153));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8155));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8157));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8160));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8162));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8165));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8167));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8170));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8172));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8175));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8177));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8180));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8182));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8185));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8625));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8631));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8636));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8640));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8643));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8645));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8647));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8649));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8652));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8654));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8656));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8659));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(8661));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6953));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6957));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6975));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(6977));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5423));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5428));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5516));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5519));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5521));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5524));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5526));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5528));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5530));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5532));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5534));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5537));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5539));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5544));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 22, 14, 47, 14, 816, DateTimeKind.Local).AddTicks(5549));

            migrationBuilder.AddForeignKey(
                name: "FK_MasterAdmin_m_biodata_BiodataId",
                table: "MasterAdmin",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterBiodataAddress_m_biodata_BiodataId",
                table: "MasterBiodataAddress",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomer_m_biodata_BiodataId",
                table: "MasterCustomer",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomerMember_m_biodata_ParentBiodataId",
                table: "MasterCustomerMember",
                column: "ParentBiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterDoctor_m_biodata_BiodataId",
                table: "MasterDoctor",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterUser_m_biodata_BiodataId",
                table: "MasterUser",
                column: "BiodataId",
                principalTable: "m_biodata",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterAdmin_m_biodata_BiodataId",
                table: "MasterAdmin");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterBiodataAddress_m_biodata_BiodataId",
                table: "MasterBiodataAddress");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomer_m_biodata_BiodataId",
                table: "MasterCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterCustomerMember_m_biodata_ParentBiodataId",
                table: "MasterCustomerMember");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterDoctor_m_biodata_BiodataId",
                table: "MasterDoctor");

            migrationBuilder.DropForeignKey(
                name: "FK_MasterUser_m_biodata_BiodataId",
                table: "MasterUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_biodata",
                table: "m_biodata");

            migrationBuilder.RenameTable(
                name: "m_biodata",
                newName: "MasterBiodata");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MasterBiodata",
                table: "MasterBiodata",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3473));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3476));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3477));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3479));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3481));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3482));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3484));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3486));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3487));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3489));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3491));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3493));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3494));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3496));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3497));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3499));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3501));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3502));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3504));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5616));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5586));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5596));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3288));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3291));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3293));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3295));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3296));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3298));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3299));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3301));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3302));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3311));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3313));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3315));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3324));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3348));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3349));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3351));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3440));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4419));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4422));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4424));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4426));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4428));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4430));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4432));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4435));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4437));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4476));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4478));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4482));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4484));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4486));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4488));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4490));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4492));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4518));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4522));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4319));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4321));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4323));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4325));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4339));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4327));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4329));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4330));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4332));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4334));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4336));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4341));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4337));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4343));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4712));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4714));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4716));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4718));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4719));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4721));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4203));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4206));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4209));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4211));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4213));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4215));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4217));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4221));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5251));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5253));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5255));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5256));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5258));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5260));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5266));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5273));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5275));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5280));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5282));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5285));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5294));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5299));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5305));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5306));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5347));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5349));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5351));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5352));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5355));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5356));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5358));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5360));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5361));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5363));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5365));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5367));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5368));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5370));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5372));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5374));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5376));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5377));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5379));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5383));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5384));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5386));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5388));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5389));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5393));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5395));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5398));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3541));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3543));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3545));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3546));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3547));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4608));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4610));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4613));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4615));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4617));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4765));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4774));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3073));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3086));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3088));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3089));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3091));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3093));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3095));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3096));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3098));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3100));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3101));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3103));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3105));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3106));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3114));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3116));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3117));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5633));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5635));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3697));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3713));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3801));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3813));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3845));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3858));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3894));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(3961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4055));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4069));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4083));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4795));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4799));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4801));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4803));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4805));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4818));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4820));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4824));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4826));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4828));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4830));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4834));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4836));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4837));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4839));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4843));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4845));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4848));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4941));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4943));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4945));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4947));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4952));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4957));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4963));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4965));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4986));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4991));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4993));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4995));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4996));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5000));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5002));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5005));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5007));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5009));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5011));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5013));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5014));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5016));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5018));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5020));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5022));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5023));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5025));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5073));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5074));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5076));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5078));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5080));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5082));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5084));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5086));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5087));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5091));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5093));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5094));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5098));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5100));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5102));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5104));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5107));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5109));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5111));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5113));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5114));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5118));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5122));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5124));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5125));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5127));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5129));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5131));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5133));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5134));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5136));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5138));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5140));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5141));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5143));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5145));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5147));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5149));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5150));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5152));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5156));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5157));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5436));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5440));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5444));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5446));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5448));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5450));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5454));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5456));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5458));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5512));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(5516));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4366));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4368));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4370));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4373));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4375));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4376));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4378));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4379));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4381));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4383));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4386));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4387));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4389));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4390));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 21, 11, 22, 59, 486, DateTimeKind.Local).AddTicks(4392));

            migrationBuilder.AddForeignKey(
                name: "FK_MasterAdmin_MasterBiodata_BiodataId",
                table: "MasterAdmin",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterBiodataAddress_MasterBiodata_BiodataId",
                table: "MasterBiodataAddress",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomer_MasterBiodata_BiodataId",
                table: "MasterCustomer",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterCustomerMember_MasterBiodata_ParentBiodataId",
                table: "MasterCustomerMember",
                column: "ParentBiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterDoctor_MasterBiodata_BiodataId",
                table: "MasterDoctor",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterUser_MasterBiodata_BiodataId",
                table: "MasterUser",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");
        }
    }
}
