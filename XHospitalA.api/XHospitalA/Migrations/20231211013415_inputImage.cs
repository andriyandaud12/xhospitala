﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class inputImage : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7674), "https://media.istockphoto.com/photos/portrait-of-confident-muslim-doctor-wearing-hijab-standing-at-the-of-picture-id1151233766" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7675), " https://media.npr.org/assets/news/2010/02/25/doctor-3900f8c9b6c5be80f3b2e509eafe9c41d7ad2671.jpg" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7676), "https://thumbs.dreamstime.com/b/portrait-happy-african-doctor-private-clinic-confident-mature-black-consulting-digital-tablet-looking-camera-smiling-164999099.jpg" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7677), "https://jay-harold.com/wp-content/uploads/2015/12/Dollarphotoclub_74864725.jpg" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7678), "https://th.bing.com/th/id/OIP.WPYPa4GubQVLa0kQqXcfvwHaHa?rs=1&pid=ImgDetMain" });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7654));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7656));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7657));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7658));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7659));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7885));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7887));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7888));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7889));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7890));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7904));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7848));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7849));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7850));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7850));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7851));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7829));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7830));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7832));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7833));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7834));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7695));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7697));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7698));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7531));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7541));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7542));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7543));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7780));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7799));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7805));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7811));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7867));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7868));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7869));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1682), "" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1686), "" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1689), "" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1692), "" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "Image" },
                values: new object[] { new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1694), "" });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1629));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1639));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1645));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2267));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2271));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2273));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2275));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2277));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2308));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2311));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2109));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2112));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2115));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2117));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2059));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2067));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2069));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2074));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1733));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1738));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1741));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1097));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1110));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1112));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1114));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1960));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2015));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2153));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2225));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2227));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2229));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2231));
        }
    }
}
