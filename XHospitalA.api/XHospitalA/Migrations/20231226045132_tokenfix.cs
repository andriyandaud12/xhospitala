﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class tokenfix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "t_token",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    User_Id = table.Column<long>(type: "bigint", nullable: true),
                    token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Expired_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_Expired = table.Column<bool>(type: "bit", nullable: true),
                    Used_For = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_token", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_token_m_user_User_Id",
                        column: x => x.User_Id,
                        principalTable: "m_user",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2808));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2811));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2813));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2815));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2818));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2822));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2824));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2826));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2827));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2829));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2831));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2833));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2838));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2839));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2842));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2844));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2846));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5617));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5583));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5593));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2697));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2700));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2702));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2704));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2706));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2709));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2710));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2712));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2714));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2722));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2725));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2726));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2738));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2764));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2766));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2768));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2770));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3965));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3968));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3970));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3973));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3975));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3977));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3980));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3982));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3984));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3987));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3989));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3991));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3993));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3995));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3998));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4000));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4002));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4037));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4039));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3762));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3766));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3768));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3770));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3785));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3772));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3774));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3776));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3777));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3779));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3781));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3788));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3783));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4280));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4283));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4285));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4286));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4288));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4290));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3700));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3706));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3709));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3712));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3715));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3718));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3721));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3723));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3726));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5151));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5154));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5155));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5157));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5159));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5161));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5165));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5166));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5168));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5256));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5258));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5260));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5265));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5273));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5280));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5282));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5284));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5286));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5297));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5299));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5304));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5307));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5312));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5314));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5316));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5318));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5325));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5329));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5331));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5333));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5334));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5338));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5339));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5341));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5348));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5350));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5352));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5354));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5356));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5434));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5436));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5438));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2969));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2972));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2973));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2975));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2976));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4216));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4222));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4224));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4348));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4359));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2304));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2327));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2329));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2331));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2332));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2336));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2338));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2339));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2342));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2344));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2346));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2347));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2457));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2466));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2469));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(2471));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5635));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3127));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3142));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3155));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3167));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3203));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3218));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3230));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3270));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3283));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3295));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3307));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3319));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3333));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3346));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3440));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3455));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3468));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4390));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4392));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4394));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4401));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4403));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4405));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4493));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4495));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4497));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4499));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4500));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4504));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4506));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4508));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4510));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4512));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4513));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4515));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4517));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4519));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4521));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4523));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4525));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4529));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4531));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4533));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4535));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4537));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4539));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4541));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4543));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4544));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4547));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4549));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4551));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4553));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4557));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4559));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4562));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4564));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4566));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4567));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4569));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4572));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4574));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4577));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4579));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4582));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4584));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4586));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4588));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4590));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4777));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4780));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4782));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4784));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4786));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4790));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4792));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4794));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4796));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4798));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4799));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4801));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4804));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4818));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4820));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4825));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4827));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4829));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4831));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4832));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4835));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4837));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4839));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4840));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4842));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4844));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4847));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4850));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4851));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4853));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4855));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4857));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4859));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4861));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4864));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4869));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4871));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4873));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4874));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4876));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4958));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4962));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4964));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4966));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4974));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4976));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4979));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4983));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4985));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4987));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4988));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4990));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4993));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4995));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4997));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5001));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5003));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5487));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5491));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5492));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5496));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5497));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5499));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5501));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5503));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5504));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5506));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5508));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5509));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3820));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3822));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3824));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3826));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3827));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3829));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3835));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3917));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3919));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3921));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3924));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3926));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3928));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3929));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 51, 29, 949, DateTimeKind.Local).AddTicks(3931));

            migrationBuilder.CreateIndex(
                name: "IX_t_token_User_Id",
                table: "t_token",
                column: "User_Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_token");

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5378));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5380));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5383));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5385));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5387));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5389));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5393));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5395));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5396));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5398));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5402));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5404));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5406));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5408));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5410));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5412));

            migrationBuilder.UpdateData(
                table: "m_biodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5496));

            migrationBuilder.UpdateData(
                table: "m_blood_group",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8009));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7971));

            migrationBuilder.UpdateData(
                table: "m_customer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7982));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5287));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5297));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5300));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5310));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5311));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5325));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5343));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5345));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5347));

            migrationBuilder.UpdateData(
                table: "m_doctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5349));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6492));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6495));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6497));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6499));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6501));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6504));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6507));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6510));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6512));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6514));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6516));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6518));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6521));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6525));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6527));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6530));

            migrationBuilder.UpdateData(
                table: "m_doctor_education",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6532));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6561));

            migrationBuilder.UpdateData(
                table: "m_education_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6563));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6289));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6293));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6295));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6297));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6312));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6300));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6303));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6304));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6306));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6308));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6314));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6310));

            migrationBuilder.UpdateData(
                table: "m_location",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6316));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6810));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6812));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6814));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6815));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6817));

            migrationBuilder.UpdateData(
                table: "m_location_level",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6818));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6242));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6246));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6249));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6251));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6254));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6256));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6258));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6260));

            migrationBuilder.UpdateData(
                table: "m_medical_facility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7559));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7562));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7564));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7566));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7568));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7570));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7653));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7656));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7657));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7659));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7661));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7663));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7665));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7667));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7669));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7670));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7672));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7675));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7677));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7679));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7681));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7682));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7684));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7686));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7688));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7690));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7691));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7695));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7697));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7699));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7701));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7703));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7705));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7707));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7709));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7711));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7712));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7714));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7716));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7718));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7720));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7722));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7729));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7732));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7734));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7736));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7737));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7739));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7741));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7743));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7745));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7747));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7748));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7751));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7824));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7826));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7828));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7830));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7832));

            migrationBuilder.UpdateData(
                table: "m_medical_facility_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7834));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5540));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5544));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5545));

            migrationBuilder.UpdateData(
                table: "m_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5547));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6732));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6739));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6742));

            migrationBuilder.UpdateData(
                table: "m_user",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6745));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6866));

            migrationBuilder.UpdateData(
                table: "t_appoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6875));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4905));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4926));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4928));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4929));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4931));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4933));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4935));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4936));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5033));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5035));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5037));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5039));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5040));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5048));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5050));

            migrationBuilder.UpdateData(
                table: "t_current_doctor_specialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5051));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8026));

            migrationBuilder.UpdateData(
                table: "t_customer_chat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(8029));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5690));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5702));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5714));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5725));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5761));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5776));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5788));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5830));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5841));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5853));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5865));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5877));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5889));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6016));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6028));

            migrationBuilder.UpdateData(
                table: "t_doctor_office",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6041));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6904));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6906));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6908));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7064));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7067));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7069));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7071));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7074));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7076));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7080));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7082));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7083));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7085));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7088));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7090));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7092));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7094));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7096));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7097));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7099));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7101));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7103));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7105));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7107));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7111));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7112));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7114));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7116));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7118));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7120));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7122));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7123));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7125));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7127));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7129));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7132));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7135));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7138));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7139));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7141));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7143));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7145));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7147));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7149));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7151));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7154));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7156));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7158));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7160));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7162));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7245));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7247));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7249));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7251));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7252));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7254));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7258));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7260));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7263));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7265));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7267));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7269));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7271));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7273));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7274));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7276));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7278));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7281));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7283));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7285));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7289));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7291));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7293));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7296));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7298));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7300));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7303));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7305));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7306));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7308));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7311));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7313));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7315));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7318));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7320));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7322));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7324));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7327));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7330));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7332));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7333));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7335));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7338));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7344));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7345));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7347));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7350));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7353));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7431));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7433));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7435));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7437));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7439));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7441));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7443));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7445));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7447));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7449));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7451));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7454));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7456));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7457));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7459));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7461));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7463));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7465));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7466));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7468));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7470));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7473));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7475));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7476));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7478));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_schedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7480));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7881));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7883));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7885));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7888));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7890));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7892));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7894));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7896));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7898));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7899));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7901));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7903));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "t_doctor_office_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(7907));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6346));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6348));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6351));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6434));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6436));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6437));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6439));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6441));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6446));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6448));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6449));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6451));

            migrationBuilder.UpdateData(
                table: "t_doctor_treatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 26, 11, 22, 36, 841, DateTimeKind.Local).AddTicks(6452));
        }
    }
}
