﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addseed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "MasterBiodata",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "FullName", "Image", "ImagePath", "Is_delete", "MobilePhone", "Modified_by", "Modified_on" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8994), null, null, "Christie Imelda", "", "", false, "081231231123", null, null },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8998), null, null, "Hendra", "", "", false, "081231231123", null, null },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9002), null, null, "Hery Setyobudi", "", "", false, "081231231123", null, null },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9005), null, null, "Indra Tambunan", "", "", false, "081231231123", null, null },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9009), null, null, "Fenata Bandra", "", "", false, "081231231123", null, null }
                });

            migrationBuilder.InsertData(
                table: "MasterLocation",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "LocationLevelId", "Modified_by", "Modified_on", "Name", "ParentId" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9583), null, null, false, null, null, null, "Slipi", null },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9587), null, null, false, null, null, null, "Kedoya", null },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9590), null, null, false, null, null, null, "Cengkareng Timur", null },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9592), null, null, false, null, null, null, "Pegadungan", null },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9595), null, null, false, null, null, null, "Slipi", null }
                });

            migrationBuilder.InsertData(
                table: "MasterSpecialization",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9059), null, null, false, null, null, "Penyakit Dalam" },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9063), null, null, false, null, null, "Anak" },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9066), null, null, false, null, null, "Saraf" },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9068), null, null, false, null, null, "Kandungan dan ginekologi" },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9071), null, null, false, null, null, "Bedah" }
                });

            migrationBuilder.InsertData(
                table: "MasterDoctor",
                columns: new[] { "Id", "BiodataId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Str" },
                values: new object[,]
                {
                    { 1L, 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8853), null, null, false, null, null, "" },
                    { 2L, 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8939), null, null, false, null, null, "" },
                    { 3L, 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8942), null, null, false, null, null, "" },
                    { 4L, 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8946), null, null, false, null, null, "" },
                    { 5L, 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8949), null, null, false, null, null, "" }
                });

            migrationBuilder.InsertData(
                table: "MasterMedicalFacility",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Email", "Fax", "FullAddress", "Is_delete", "LocationId", "MedicalFacilityCategoryId", "Modified_by", "Modified_on", "Name", "Phone", "PhoneCode" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9469), null, null, "rsab@gmail.com", "5668284", "Jl. Let. Jend. S. Parman, Kav.87, Slipi, Jakarta Barat", false, 1L, null, null, null, "RS Anak dan Bunda Harapan Kita", "5668284", "021" },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9473), null, null, "rskmjeck@gmail.com", "29221000", "Jl. Terusan Arjuna Utara 1, Jakarta Barat", false, 2L, null, null, null, "RS Khusus Mata Jakarta Eye Center Kedoya", "29221000", "021" },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9477), null, null, "rsceng@gmail.com", "54372874", "Jl. Bumi Cengkareng Indah No.1, Cengkareng Timur, Jakarta Barat", false, 3L, null, null, null, "RSUD Cengkareng", "54372874", "021" },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9481), null, null, "rscip@gmail.com", "54372874", "Jl. Satu Maret No.26, Pegadungan, Jakarta Barat", false, 4L, null, null, null, "RSUD Ciputra", "54372874", "021" },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9515), null, null, "rskan@gmail.com", "5681570", "Jl. S Parman Kav.84-86, Slipi, Jakarta Barat", false, 5L, null, null, null, "RS Kanker Dharmis", "5681570", "021" }
                });

            migrationBuilder.InsertData(
                table: "TransactionCurrentDoctorSpecialization",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "Modified_by", "Modified_on", "SpecializationId" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8580), null, null, 1L, false, null, null, 1L },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8595), null, null, 2L, false, null, null, 2L },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8598), null, null, 3L, false, null, null, 3L },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8602), null, null, 4L, false, null, null, 4L },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8605), null, null, 5L, false, null, null, 5L }
                });

            migrationBuilder.InsertData(
                table: "TransactionDoctorOffice",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "EndDate", "Is_delete", "MedicalFacilityId", "Modified_by", "Modified_on", "Specialization", "StartDate" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9276), null, null, 1L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 1L, null, null, "Penyakit Dalam", new DateTime(2023, 1, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9307), null, null, 2L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 2L, null, null, "Anak", new DateTime(2023, 1, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9332), null, null, 3L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 3L, null, null, "Saraf", new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9358), null, null, 4L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 4L, null, null, "THT", new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9409), null, null, 5L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 5L, null, null, "Bedah", new DateTime(2023, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "TransactionDoctorTreatment",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9644), null, null, 1L, false, null, null, "Diagnosis penyakit dalam" },
                    { 2L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9648), null, null, 2L, false, null, null, "Imunisasi" },
                    { 3L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9651), null, null, 3L, false, null, null, "Angiografi" },
                    { 4L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9654), null, null, 4L, false, null, null, "Persalinan" },
                    { 5L, 1L, new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9657), null, null, 5L, false, null, null, "Pembedahan" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L);
        }
    }
}
