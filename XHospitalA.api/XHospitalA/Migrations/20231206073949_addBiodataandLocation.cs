﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addBiodataandLocation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "MasterAdmin",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "MasterBiodata",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    MobilePhone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImagePath = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterBiodata", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterBloodGroup",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterBloodGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterCustomerRelation",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterCustomerRelation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterEducationLevel",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterEducationLevel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterLocationLevel",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Abbreviation = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterLocationLevel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterMedicalFacilityCategory",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterMedicalFacilityCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterSpecialization",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterSpecialization", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterDoctor",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BiodataId = table.Column<long>(type: "bigint", nullable: true),
                    Str = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDoctor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDoctor_MasterBiodata_BiodataId",
                        column: x => x.BiodataId,
                        principalTable: "MasterBiodata",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterCustomer",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BiodataId = table.Column<long>(type: "bigint", nullable: true),
                    Dob = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
                    BloodGroupId = table.Column<long>(type: "bigint", nullable: true),
                    RhesusType = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: true),
                    Height = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Weight = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterCustomer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterCustomer_MasterBiodata_BiodataId",
                        column: x => x.BiodataId,
                        principalTable: "MasterBiodata",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterCustomer_MasterBloodGroup_BloodGroupId",
                        column: x => x.BloodGroupId,
                        principalTable: "MasterBloodGroup",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterLocation",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ParentId = table.Column<long>(type: "bigint", nullable: true),
                    LocationLevelId = table.Column<long>(type: "bigint", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterLocation_MasterLocationLevel_LocationLevelId",
                        column: x => x.LocationLevelId,
                        principalTable: "MasterLocationLevel",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterLocation_MasterLocation_ParentId",
                        column: x => x.ParentId,
                        principalTable: "MasterLocation",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterDoctorEducation",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorId = table.Column<long>(type: "bigint", nullable: true),
                    EducationLevelId = table.Column<long>(type: "bigint", nullable: true),
                    InstitutionName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Major = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StartYear = table.Column<string>(type: "nvarchar(4)", maxLength: 4, nullable: true),
                    EndYear = table.Column<string>(type: "nvarchar(4)", maxLength: 4, nullable: true),
                    IsLastEducation = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterDoctorEducation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterDoctorEducation_MasterDoctor_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "MasterDoctor",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterDoctorEducation_MasterEducationLevel_EducationLevelId",
                        column: x => x.EducationLevelId,
                        principalTable: "MasterEducationLevel",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TransactionCurrentDoctorSpecialization",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorId = table.Column<long>(type: "bigint", nullable: false),
                    SpecializationId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionCurrentDoctorSpecialization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionCurrentDoctorSpecialization_MasterDoctor_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "MasterDoctor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransactionCurrentDoctorSpecialization_MasterSpecialization_SpecializationId",
                        column: x => x.SpecializationId,
                        principalTable: "MasterSpecialization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionDoctorTreatment",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorId = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionDoctorTreatment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorTreatment_MasterDoctor_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "MasterDoctor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MasterCustomerMember",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentBiodataId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerRelationId = table.Column<long>(type: "bigint", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterCustomerMember", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterCustomerMember_MasterBiodata_ParentBiodataId",
                        column: x => x.ParentBiodataId,
                        principalTable: "MasterBiodata",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterCustomerMember_MasterCustomerRelation_CustomerRelationId",
                        column: x => x.CustomerRelationId,
                        principalTable: "MasterCustomerRelation",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterCustomerMember_MasterCustomer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "MasterCustomer",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TransactionAppoinmentDone",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppoinmentId = table.Column<long>(type: "bigint", nullable: true),
                    Diagnosis = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CustomerId = table.Column<long>(type: "bigint", nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionAppoinmentDone", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionAppoinmentDone_MasterCustomer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "MasterCustomer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MasterBiodataAddress",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BiodataId = table.Column<long>(type: "bigint", nullable: true),
                    Label = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Recipent = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RecipentPhoneNumber = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    LocationId = table.Column<long>(type: "bigint", nullable: true),
                    PostalCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterBiodataAddress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterBiodataAddress_MasterBiodata_BiodataId",
                        column: x => x.BiodataId,
                        principalTable: "MasterBiodata",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterBiodataAddress_MasterLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "MasterLocation",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterMedicalFacility",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    MedicalFacilityCategoryId = table.Column<long>(type: "bigint", nullable: true),
                    LocationId = table.Column<long>(type: "bigint", nullable: true),
                    FullAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PhoneCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Fax = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterMedicalFacility", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterMedicalFacility_MasterLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "MasterLocation",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MasterMedicalFacility_MasterMedicalFacilityCategory_MedicalFacilityCategoryId",
                        column: x => x.MedicalFacilityCategoryId,
                        principalTable: "MasterMedicalFacilityCategory",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MasterMedicalFacilitySchedule",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MedicalFacilityId = table.Column<long>(type: "bigint", nullable: true),
                    Day = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    TimeScheduleStart = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    TimeScheduleEnd = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterMedicalFacilitySchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterMedicalFacilitySchedule_MasterMedicalFacility_MedicalFacilityId",
                        column: x => x.MedicalFacilityId,
                        principalTable: "MasterMedicalFacility",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TransactionDoctorOffice",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorId = table.Column<long>(type: "bigint", nullable: false),
                    MedicalFacilityId = table.Column<long>(type: "bigint", nullable: false),
                    Specialization = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionDoctorOffice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "MasterDoctor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                        column: x => x.MedicalFacilityId,
                        principalTable: "MasterMedicalFacility",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionDoctorOfficeSchedule",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorId = table.Column<long>(type: "bigint", nullable: false),
                    MedicalFacilityScheduleId = table.Column<long>(type: "bigint", nullable: false),
                    Slot = table.Column<int>(type: "int", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionDoctorOfficeSchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "MasterDoctor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                        column: x => x.MedicalFacilityScheduleId,
                        principalTable: "MasterMedicalFacilitySchedule",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransactionDoctorOfficeTreatment",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoctorTreatmentId = table.Column<long>(type: "bigint", nullable: true),
                    DoctorOfficeId = table.Column<long>(type: "bigint", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionDoctorOfficeTreatment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorOffice_DoctorOfficeId",
                        column: x => x.DoctorOfficeId,
                        principalTable: "TransactionDoctorOffice",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionDoctorOfficeTreatment_TransactionDoctorTreatment_DoctorTreatmentId",
                        column: x => x.DoctorTreatmentId,
                        principalTable: "TransactionDoctorTreatment",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TransactionAppoinment",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DoctorOfficeId = table.Column<long>(type: "bigint", nullable: true),
                    DoctorOfficeScheduleId = table.Column<long>(type: "bigint", nullable: true),
                    DoctorOfficeTreatmentId = table.Column<long>(type: "bigint", nullable: true),
                    AppointmentDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Created_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: false),
                    Created_on = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Modified_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted_by = table.Column<long>(type: "bigint", maxLength: 50, nullable: true),
                    Deleted_on = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Is_delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionAppoinment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionAppoinment_MasterCustomer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "MasterCustomer",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionAppoinment_TransactionDoctorOfficeSchedule_DoctorOfficeScheduleId",
                        column: x => x.DoctorOfficeScheduleId,
                        principalTable: "TransactionDoctorOfficeSchedule",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionAppoinment_TransactionDoctorOfficeTreatment_DoctorOfficeTreatmentId",
                        column: x => x.DoctorOfficeTreatmentId,
                        principalTable: "TransactionDoctorOfficeTreatment",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionAppoinment_TransactionDoctorOffice_DoctorOfficeId",
                        column: x => x.DoctorOfficeId,
                        principalTable: "TransactionDoctorOffice",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MasterAdmin_BiodataId",
                table: "MasterAdmin",
                column: "BiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterBiodataAddress_BiodataId",
                table: "MasterBiodataAddress",
                column: "BiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterBiodataAddress_LocationId",
                table: "MasterBiodataAddress",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterCustomer_BiodataId",
                table: "MasterCustomer",
                column: "BiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterCustomer_BloodGroupId",
                table: "MasterCustomer",
                column: "BloodGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterCustomerMember_CustomerId",
                table: "MasterCustomerMember",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterCustomerMember_CustomerRelationId",
                table: "MasterCustomerMember",
                column: "CustomerRelationId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterCustomerMember_ParentBiodataId",
                table: "MasterCustomerMember",
                column: "ParentBiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDoctor_BiodataId",
                table: "MasterDoctor",
                column: "BiodataId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDoctorEducation_DoctorId",
                table: "MasterDoctorEducation",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterDoctorEducation_EducationLevelId",
                table: "MasterDoctorEducation",
                column: "EducationLevelId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterLocation_LocationLevelId",
                table: "MasterLocation",
                column: "LocationLevelId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterLocation_ParentId",
                table: "MasterLocation",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterMedicalFacility_LocationId",
                table: "MasterMedicalFacility",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterMedicalFacility_MedicalFacilityCategoryId",
                table: "MasterMedicalFacility",
                column: "MedicalFacilityCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterMedicalFacilitySchedule_MedicalFacilityId",
                table: "MasterMedicalFacilitySchedule",
                column: "MedicalFacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinment_CustomerId",
                table: "TransactionAppoinment",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeScheduleId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeScheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinment_DoctorOfficeTreatmentId",
                table: "TransactionAppoinment",
                column: "DoctorOfficeTreatmentId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinmentDone_CustomerId",
                table: "TransactionAppoinmentDone",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionCurrentDoctorSpecialization_DoctorId",
                table: "TransactionCurrentDoctorSpecialization",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionCurrentDoctorSpecialization_SpecializationId",
                table: "TransactionCurrentDoctorSpecialization",
                column: "SpecializationId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOffice_DoctorId",
                table: "TransactionDoctorOffice",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOffice_MedicalFacilityId",
                table: "TransactionDoctorOffice",
                column: "MedicalFacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOfficeSchedule_DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOfficeSchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                column: "MedicalFacilityScheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOfficeTreatment_DoctorOfficeId",
                table: "TransactionDoctorOfficeTreatment",
                column: "DoctorOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorOfficeTreatment_DoctorTreatmentId",
                table: "TransactionDoctorOfficeTreatment",
                column: "DoctorTreatmentId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionDoctorTreatment_DoctorId",
                table: "TransactionDoctorTreatment",
                column: "DoctorId");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterAdmin_MasterBiodata_BiodataId",
                table: "MasterAdmin",
                column: "BiodataId",
                principalTable: "MasterBiodata",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterAdmin_MasterBiodata_BiodataId",
                table: "MasterAdmin");

            migrationBuilder.DropTable(
                name: "MasterBiodataAddress");

            migrationBuilder.DropTable(
                name: "MasterCustomerMember");

            migrationBuilder.DropTable(
                name: "MasterDoctorEducation");

            migrationBuilder.DropTable(
                name: "TransactionAppoinment");

            migrationBuilder.DropTable(
                name: "TransactionAppoinmentDone");

            migrationBuilder.DropTable(
                name: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropTable(
                name: "MasterCustomerRelation");

            migrationBuilder.DropTable(
                name: "MasterEducationLevel");

            migrationBuilder.DropTable(
                name: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropTable(
                name: "TransactionDoctorOfficeTreatment");

            migrationBuilder.DropTable(
                name: "MasterCustomer");

            migrationBuilder.DropTable(
                name: "MasterSpecialization");

            migrationBuilder.DropTable(
                name: "MasterMedicalFacilitySchedule");

            migrationBuilder.DropTable(
                name: "TransactionDoctorOffice");

            migrationBuilder.DropTable(
                name: "TransactionDoctorTreatment");

            migrationBuilder.DropTable(
                name: "MasterBloodGroup");

            migrationBuilder.DropTable(
                name: "MasterMedicalFacility");

            migrationBuilder.DropTable(
                name: "MasterDoctor");

            migrationBuilder.DropTable(
                name: "MasterLocation");

            migrationBuilder.DropTable(
                name: "MasterMedicalFacilityCategory");

            migrationBuilder.DropTable(
                name: "MasterBiodata");

            migrationBuilder.DropTable(
                name: "MasterLocationLevel");

            migrationBuilder.DropIndex(
                name: "IX_MasterAdmin_BiodataId",
                table: "MasterAdmin");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "MasterAdmin",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);
        }
    }
}
