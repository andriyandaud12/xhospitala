﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class solvingDateString : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8421));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8423));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8424));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8426));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8428));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8429));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8431));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8432));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8434));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8436));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8438));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8439));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8441));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8442));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8444));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8445));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8446));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8448));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8450));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(11));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9978));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9988));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8280));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8282));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8285));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8286));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8288));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8289));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8291));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8292));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8378));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8380));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8382));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8383));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8385));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8386));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8388));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8389));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9013));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9019));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9020));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9022));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9024));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9026));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9028));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9029));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9031));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9033));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9035));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9037));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9038));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9040));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9067));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9068));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8885));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8886));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8887));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8888));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8890));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8891));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8893));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9234));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9235));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9237));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9238));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9240));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9241));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8843));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8847));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8849));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8851));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8853));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8855));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8857));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8859));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9656));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9659));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9660));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9662));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9663));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9665));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9667));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9668));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9670));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9671));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9673));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9675));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9676));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9678));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9679));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9681));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9682));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9684));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9685));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9687));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9729));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9731));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9732));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9734));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9735));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9737));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9739));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9740));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9742));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9743));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9745));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9746));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9748));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9749));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9751));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9754));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9756));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9757));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9759));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9760));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9762));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9763));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9765));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9766));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9768));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9770));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9771));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9773));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9774));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9776));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9778));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9779));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9781));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9782));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9784));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9786));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9787));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9790));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9792));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9793));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9795));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9796));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9798));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8490));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8491));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8492));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8494));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8495));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9181));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9184));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9186));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9276));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9283));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8042));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8052));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8053));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8055));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8056));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8057));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8059));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8060));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8062));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8063));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8065));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8066));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8068));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8069));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8070));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8072));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8073));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(34));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 591, DateTimeKind.Local).AddTicks(35));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8604));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8617));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8629));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8648));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8660));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8670));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8723));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8743));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8772));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8781));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8800));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8810));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9306));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9308));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9310));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9314));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9316));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9317));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9356));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9361));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9362));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9364));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9367));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9374));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9375));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9377));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9379));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9380));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9382));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9386));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9387));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9390));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9391));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9393));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9395));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9396));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9399));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9401));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9402));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9404));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9407));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9411));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9412));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9414));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9415));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9417));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9420));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9422));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9424));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9425));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9427));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9428));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9431));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9433));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9471));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9475));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9476));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9478));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9480));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9481));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9483));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9484));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9486));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9487));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9489));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9490));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9492));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9493));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9495));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9497));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9498));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9500));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9503));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9505));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9506));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9508));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9510));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9511));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9513));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9514));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9516));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9518));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9519));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9521));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9522));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9524));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9525));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9527));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9529));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9530));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9532));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9533));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9535));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9536));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9539));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9541));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9542));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9546));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9579));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9583));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9585));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9589));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9594));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9596));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9921));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9923));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9924));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9928));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9929));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(9931));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8916));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8919));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8921));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8923));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8924));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8972));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 13, 55, 51, 590, DateTimeKind.Local).AddTicks(8979));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9453));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9537));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9540));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9544));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9547));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9550));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9554));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9558));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9561));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9564));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9568));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9571));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9574));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9581));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9584));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9587));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9590));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2871));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2737));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2757));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9313));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9318));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9321));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9325));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9328));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9331));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9334));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9337));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9340));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9355));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9359));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9362));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9371));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9396));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9399));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9403));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9406));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(893));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(900));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(904));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(909));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(915));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(921));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(926));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(933));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(937));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(944));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(948));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(952));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(957));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(961));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(965));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(969));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(973));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1018));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1021));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(663));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(666));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(668));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(671));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(674));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(678));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(682));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1407));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1411));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1414));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1417));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1420));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1423));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(578));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(583));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(587));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(591));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(595));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(600));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(605));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(609));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2281));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2285));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2289));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2292));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2296));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2299));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2303));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2306));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2310));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2314));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2317));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2320));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2324));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2327));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2330));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2337));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2341));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2345));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2348));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2351));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2355));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2361));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2367));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2371));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2374));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2378));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2384));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2388));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2392));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2395));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2399));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2452));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2456));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2460));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2463));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2467));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2471));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2474));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2477));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2481));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2484));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2488));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2491));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2495));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2498));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2502));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2505));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2508));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2512));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2515));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2519));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2522));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2526));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2529));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2532));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2536));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2539));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2543));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2546));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2550));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2554));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9652));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9655));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9658));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9661));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9663));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1167));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1178));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1509));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1529));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8958));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8978));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8981));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8984));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8988));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8991));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(8996));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9001));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9004));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9008));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9014));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9021));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9037));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9040));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9043));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2906));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9854));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9884));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9933));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 126, DateTimeKind.Local).AddTicks(9992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(20));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(46));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(109));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(134));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(160));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(184));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(209));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(234));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(260));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(401));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1573));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1596));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1600));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1604));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1607));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1611));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1615));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1619));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1623));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1626));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1632));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1642));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1646));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1649));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1653));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1740));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1751));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1758));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1765));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1769));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1780));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1783));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1790));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1801));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1804));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1808));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1811));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1815));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1818));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1822));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1825));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1829));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1836));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1839));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1843));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1846));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1850));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1853));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1857));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1860));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1864));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1868));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1875));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1885));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1889));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1900));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1963));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1981));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1988));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1995));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(1999));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2002));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2006));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2010));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2014));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2017));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2021));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2024));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2028));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2032));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2035));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2039));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2042));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2046));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2050));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2054));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2057));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2061));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2064));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2068));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2075));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2078));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2082));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2085));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2092));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2096));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2099));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2103));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2106));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2117));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2121));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2124));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2624));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2628));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(2641));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(731));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(734));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(751));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(754));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(757));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(761));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(764));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 17, 10, 50, 49, 127, DateTimeKind.Local).AddTicks(779));
        }
    }
}
