﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class memperbaikiseedGelar : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5643));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5645));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5647));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5649), "dr. Indra Tambunan, Sp.OG" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5652));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5654), "dr. Ade Chandra, Sp.PD" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5656));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5660), "dr. Adhitya Angga Wardhana, Sp.A" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5663), "dr. Adi Saputra, Sp.PD" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5665), "dr. Adimas Haryanaputra Tjindarbumi, Sp.A" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5666), "dr. Agus Endrawanto, Sp.OG" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5668), "dr. Agustine, Sp.A" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5670));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5672), "dr. Aladin Sampara Johan, Sp.S" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5674), "dr. Amrul Mukminin, Sp.PD" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5676), "dr. Andreas Ariawan, Sp.A" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5731), "dr. Andreas Kurniawan Suwito, Sp.OG" });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5573));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5576));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5578));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5582));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5584));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5586));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5588));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5590));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5592));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5594));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5596));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5597));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5599));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5601));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5603));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5605));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6374));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6377));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6382));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6385));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6394));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6397));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6399));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6402));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6516));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6519));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6521));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6558));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6559));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6261));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6264));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6266));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6267));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6161));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6168));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6171));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6174));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6177));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6179));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6182));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5770));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5772));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5774));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5775));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5778));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5199));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5211));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5213));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5216));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5218));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5219));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5221));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5223));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5294));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5296));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5300));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5302));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5933));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(5996));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6008));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6032));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6044));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6057));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6070));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6082));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6093));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6106));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6296));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6299));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6301));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6303));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6315));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6317));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6319));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6321));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6325));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6327));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 11, 10, 15, 547, DateTimeKind.Local).AddTicks(6329));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6977));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6979));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6981));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6982), "dr. Indra Tambunan, Sp.Og" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6984));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6986), "dr. Ade Chandra, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6987));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6989), "dr. Adhitya Angga Wardhana, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6990), "dr. Adi Saputra, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6992), "dr. Adimas Haryanaputra Tjindarbumi, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6993), "dr. Agus Endrawanto, Sp.B, M.Kes" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6995), "dr. Agustine, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6996));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6998), "dr. Aladin Sampara Johan, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6999), "dr. Amrul Mukminin, Sp.B, Sub.Sp.BE (K)" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7001), "dr. Andreas Ariawan, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7002), "dr. Andreas Kurniawan Suwito, Sp.B" });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6830));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6832));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6834));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6929));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6931));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6932));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6934));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6935));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6937));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6938));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6941));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6943));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6944));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6947));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7573));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7576));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7578));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7579));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7581));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7583));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7585));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7587));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7589));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7591));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7593));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7595));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7598));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7600));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7602));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7604));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7606));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7634));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7636));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7422));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7424));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7425));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7426));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7427));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7379));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7386));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7387));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7389));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7391));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7393));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7396));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7031));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7033));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7034));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7035));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6608));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6617));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6619));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6620));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6621));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6623));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6624));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6626));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6627));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6629));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6630));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6632));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6633));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6634));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6636));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6637));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7146));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7158));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7169));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7179));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7230));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7252));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7262));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7271));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7281));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7290));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7300));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7318));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7328));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7337));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7348));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7456));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7458));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7459));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7533));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7534));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7535));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7537));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7540));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7541));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7542));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7545));
        }
    }
}
