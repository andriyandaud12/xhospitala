﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class seedEducation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1682));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1686));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1689));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1692));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1694));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1629));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1639));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1645));

            migrationBuilder.InsertData(
                table: "MasterEducationLevel",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2308), null, null, false, null, null, "Strata 1" },
                    { 2L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2311), null, null, false, null, null, "Strata 2" }
                });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2109));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2112));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2115));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2117));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2059));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2067));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2069));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2074));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1733));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1738));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1741));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1097));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1110));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1112));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1114));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1960));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(1992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2015));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2153));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2225));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2227));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2229));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2231));

            migrationBuilder.InsertData(
                table: "MasterDoctorEducation",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "EducationLevelId", "EndYear", "InstitutionName", "Is_delete", "Major", "Modified_by", "Modified_on", "StartYear" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2267), null, null, 2L, 2L, null, "Universitas Indonesia", false, "Spesialis Anak", null, null, null },
                    { 2L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2271), null, null, 3L, 2L, null, "Universitas Indonesia", false, "Spesialis Saraf", null, null, null },
                    { 3L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2273), null, null, 1L, 2L, null, "Universitas Indonesia", false, "Spesialis Penyakit Dalam", null, null, null },
                    { 4L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2275), null, null, 4L, 2L, null, "Universitas Indonesia", false, "Spesialis kandungan dan ginekologi", null, null, null },
                    { 5L, 1L, new DateTime(2023, 12, 8, 9, 59, 40, 213, DateTimeKind.Local).AddTicks(2277), null, null, 5L, 2L, null, "Universitas Indonesia", false, "Spesialis Bedah", null, null, null }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8994));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8998));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9002));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9005));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9009));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8853));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8939));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8942));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8946));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8949));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9583));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9587));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9590));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9592));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9595));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9469));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9477));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9481));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9515));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9059));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9063));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9066));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9068));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9071));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8580));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8595));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8598));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8602));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(8605));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9276));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9332));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9644));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9648));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9651));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9654));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 8, 5, 15, 59, 396, DateTimeKind.Local).AddTicks(9657));
        }
    }
}
