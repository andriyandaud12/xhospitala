﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class updateDataModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinmentDone_MasterCustomer_CustomerId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropIndex(
                name: "IX_TransactionAppoinmentDone_CustomerId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.AlterColumn<long>(
                name: "MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "MedicalFacilityId",
                table: "TransactionDoctorOffice",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorOffice",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<long>(
                name: "Created_by",
                table: "TransactionCurrentDoctorSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created_on",
                table: "TransactionCurrentDoctorSpecialization",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "Deleted_by",
                table: "TransactionCurrentDoctorSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted_on",
                table: "TransactionCurrentDoctorSpecialization",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Is_delete",
                table: "TransactionCurrentDoctorSpecialization",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Modified_by",
                table: "TransactionCurrentDoctorSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified_on",
                table: "TransactionCurrentDoctorSpecialization",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Created_by",
                table: "MasterSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created_on",
                table: "MasterSpecialization",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "Deleted_by",
                table: "MasterSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Deleted_on",
                table: "MasterSpecialization",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Is_delete",
                table: "MasterSpecialization",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Modified_by",
                table: "MasterSpecialization",
                type: "bigint",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified_on",
                table: "MasterSpecialization",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinmentDone_AppoinmentId",
                table: "TransactionAppoinmentDone",
                column: "AppoinmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinmentDone_TransactionAppoinment_AppoinmentId",
                table: "TransactionAppoinmentDone",
                column: "AppoinmentId",
                principalTable: "TransactionAppoinment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice",
                column: "MedicalFacilityId",
                principalTable: "MasterMedicalFacility",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                column: "MedicalFacilityScheduleId",
                principalTable: "MasterMedicalFacilitySchedule",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionAppoinmentDone_TransactionAppoinment_AppoinmentId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule");

            migrationBuilder.DropIndex(
                name: "IX_TransactionAppoinmentDone_AppoinmentId",
                table: "TransactionAppoinmentDone");

            migrationBuilder.DropColumn(
                name: "Created_by",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Created_on",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Deleted_by",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Deleted_on",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Is_delete",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Modified_by",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Modified_on",
                table: "TransactionCurrentDoctorSpecialization");

            migrationBuilder.DropColumn(
                name: "Created_by",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Created_on",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Deleted_by",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Deleted_on",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Is_delete",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Modified_by",
                table: "MasterSpecialization");

            migrationBuilder.DropColumn(
                name: "Modified_on",
                table: "MasterSpecialization");

            migrationBuilder.AlterColumn<long>(
                name: "MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "MedicalFacilityId",
                table: "TransactionDoctorOffice",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "DoctorId",
                table: "TransactionDoctorOffice",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CustomerId",
                table: "TransactionAppoinmentDone",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionAppoinmentDone_CustomerId",
                table: "TransactionAppoinmentDone",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionAppoinmentDone_MasterCustomer_CustomerId",
                table: "TransactionAppoinmentDone",
                column: "CustomerId",
                principalTable: "MasterCustomer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterDoctor_DoctorId",
                table: "TransactionDoctorOffice",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOffice_MasterMedicalFacility_MedicalFacilityId",
                table: "TransactionDoctorOffice",
                column: "MedicalFacilityId",
                principalTable: "MasterMedicalFacility",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterDoctor_DoctorId",
                table: "TransactionDoctorOfficeSchedule",
                column: "DoctorId",
                principalTable: "MasterDoctor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionDoctorOfficeSchedule_MasterMedicalFacilitySchedule_MedicalFacilityScheduleId",
                table: "TransactionDoctorOfficeSchedule",
                column: "MedicalFacilityScheduleId",
                principalTable: "MasterMedicalFacilitySchedule",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
