﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class seedsDoctorOfficeTreatmentPrice : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5232));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5233));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5235));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5236));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5237));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5238));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5240));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5241));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5242));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5245));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5246));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5247));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5248));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5251));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5257));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5258));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5259));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6768));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6738));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6744));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5167));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5170));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5171));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5172));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5173));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5174));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5175));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5176));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5177));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5183));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5184));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5185));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5192));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5202));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5203));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5204));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5205));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5858));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5861));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5863));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5864));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5865));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5867));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5870));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5871));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5873));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5875));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5877));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5878));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5879));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5882));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5884));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5885));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5886));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5928));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5930));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5770));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5771));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5773));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5774));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5784));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5775));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5776));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5777));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5779));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5780));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5781));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5785));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5782));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5786));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6244));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6248));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6249));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6250));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6251));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6252));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5731));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5734));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5735));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5738));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5740));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5743));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5744));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5745));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5747));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6559));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6561));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6562));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6563));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6565));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6566));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6567));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6568));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6569));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6570));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6571));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6578));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6579));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6581));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6582));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6584));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6585));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6586));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6587));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6589));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6590));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6591));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6592));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6593));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6594));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6598));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6599));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6600));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6601));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6603));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6604));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6605));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6606));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6607));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6608));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6609));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6610));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6612));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6613));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6615));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6616));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6617));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6618));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6619));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6620));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6621));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6622));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6624));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6625));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6626));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6628));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6629));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6630));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6631));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6632));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6634));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6635));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6636));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6637));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6638));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6639));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6641));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6647));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5289));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6107));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6116));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6118));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6121));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6124));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6304));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4860));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4976));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4978));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4983));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4984));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4996));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4997));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4998));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(4999));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5000));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5001));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5007));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5009));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5010));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6782));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6783));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5408));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5417));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5424));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5432));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5454));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5464));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5501));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5510));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5517));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5524));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5531));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5613));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5630));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6326));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6329));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6331));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6332));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6333));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6334));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6335));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6344));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6346));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6347));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6350));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6351));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6352));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6353));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6356));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6357));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6358));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6363));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6364));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6366));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6370));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6373));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6375));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6376));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6377));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6379));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6381));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6382));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6383));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6384));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6386));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6389));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6391));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6393));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6395));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6396));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6397));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6398));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6399));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6400));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6403));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6404));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6405));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6407));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6408));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6414));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6415));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6416));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6418));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6420));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6422));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6423));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6424));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6425));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6426));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6428));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6429));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6431));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6434));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6435));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6436));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6438));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6441));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6444));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6445));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6446));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6447));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6448));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6450));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6454));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6456));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6457));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6458));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6459));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6460));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6462));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6464));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6465));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6466));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6467));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6469));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6470));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6472));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6473));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6474));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6475));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6482));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6483));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6484));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6485));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6486));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6487));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6489));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6490));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6491));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6492));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6493));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6495));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6496));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6497));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6499));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6500));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6501));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6502));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6503));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6504));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6506));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6679));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6681));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6682));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6683));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6684));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6685));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6686));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6687));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6688));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6689));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6690));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6692));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6693));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(6694));

            migrationBuilder.InsertData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                columns: new[] { "Id", "DoctorOfficeTreatmentId", "Price", "PriceStartFrom", "PriceUntilFrom" },
                values: new object[,]
                {
                    { 8L, 8L, 30000m, 300000m, 400000m },
                    { 9L, 9L, 40000m, 250000m, 300000m },
                    { 10L, 10L, 20000m, 150000m, 200000m },
                    { 11L, 11L, 50000m, 300000m, 500000m },
                    { 12L, 12L, 25000m, 100000m, 200000m },
                    { 13L, 13L, 45000m, 400000m, 450000m },
                    { 14L, 14L, 35000m, 600000m, 800000m }
                });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5806));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5807));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5808));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5809));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5810));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5816));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5817));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5818));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5819));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5820));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5821));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5822));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5829));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5830));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5833));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 39, 14, 811, DateTimeKind.Local).AddTicks(5834));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatmentPrice",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2996));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2999));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3000));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3002));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3004));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3005));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3007));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3008));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3010));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3011));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3013));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3014));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3016));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3018));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3019));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3021));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3022));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3025));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3026));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6828));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6785));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6795));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2384));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2387));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2389));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2390));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2392));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2860));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2872));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2874));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2875));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2889));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2891));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2895));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2903));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2918));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2920));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2921));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2923));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4192));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4195));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4198));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4199));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4201));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4203));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4205));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4208));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4210));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4212));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4214));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4215));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4473));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4480));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4482));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4485));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4487));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4553));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4555));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3798));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3801));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3803));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3805));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4067));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4048));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4056));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4057));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4059));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4061));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4062));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4069));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4064));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4071));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4724));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4727));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4800));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4802));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4803));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4805));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3752));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3757));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3759));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3761));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3763));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3765));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3767));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3769));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3770));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5969));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5972));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5974));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5975));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5977));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5979));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5981));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5982));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5985));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5987));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5988));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5990));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5992));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5995));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5997));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6000));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6002));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6003));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6007));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6009));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6010));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6012));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6014));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6015));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6017));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6019));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6020));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6022));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6024));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6027));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6029));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6031));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6032));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6034));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6036));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6313));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6315));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6316));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6318));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6320));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6322));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6323));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6325));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6326));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6330));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6331));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6333));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6336));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6338));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6340));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6341));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6345));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6347));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6350));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6352));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6354));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6355));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3069));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3071));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3073));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3074));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3075));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4646));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4650));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4654));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 4L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4656));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 5L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4658));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4884));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4892));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2043));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2063));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2065));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2067));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2069));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2071));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2072));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2074));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2076));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2078));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2080));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2081));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2083));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2085));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2093));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2096));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(2098));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6848));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6850));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3220));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3233));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3245));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3334));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3371));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3386));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3397));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3464));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3476));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3487));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3498));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3511));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3523));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3620));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3633));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3645));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(3656));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4928));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4931));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4933));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4936));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4937));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4939));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4940));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4942));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4945));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4947));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4953));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4958));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4974));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4979));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4984));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4986));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5220));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5228));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5230));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5233));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5235));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5241));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5243));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5245));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5248));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5250));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5252));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5255));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5257));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5259));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5261));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5263));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5264));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5266));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5268));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5269));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5271));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5274));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5276));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5278));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5280));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5281));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5285));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5286));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5299));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5301));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5305));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5312));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5319));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5322));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5533));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5541));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5546));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5548));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5549));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5553));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5555));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5557));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5558));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5560));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5562));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5563));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5565));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5567));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5568));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5570));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5572));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5574));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5579));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5584));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5586));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5589));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5591));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5594));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5597));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5599));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5600));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5602));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5604));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5605));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5607));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5609));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5610));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5612));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5615));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5617));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5619));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(5620));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6440));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6444));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6445));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6447));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6448));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6454));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6456));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6458));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6459));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(6461));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4136));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4138));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4139));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4141));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4142));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4145));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4147));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4148));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4149));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4151));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4152));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4155));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4157));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4159));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4161));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 20, 10, 13, 9, 60, DateTimeKind.Local).AddTicks(4162));
        }
    }
}
