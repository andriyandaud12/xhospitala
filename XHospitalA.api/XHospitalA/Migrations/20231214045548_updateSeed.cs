﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class updateSeed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6935));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6937));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6938));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6939));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6939));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6941));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6942));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6943));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6944));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6945));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6949));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6950));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6951));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6952));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6954));

            migrationBuilder.InsertData(
                table: "MasterBiodata",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "FullName", "Image", "ImagePath", "Is_delete", "MobilePhone", "Modified_by", "Modified_on" },
                values: new object[] { 19L, 1L, new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6955), null, null, "Giovanni", "https://www.google.com/url?sa=i&url=https%3A%2F%2Ftayostation.com%2Fmeet-tayo-and-friends%2F&psig=AOvVaw2apKnA_qNh9MQrlHYrCwDz&ust=1702434295888000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCPClsI_siIMDFQAAAAAdAAAAABAD", "", false, "081390094001", null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6894));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6896));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6897));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6899));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6902));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6902));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6903));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6904));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6905));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6906));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6909));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6910));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6911));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6912));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6913));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6914));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6915));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7273));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7275));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7276));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7277));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7278));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7279));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7281));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7282));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7284));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7285));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7289));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7290));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7293));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7296));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7297));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7299));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7317));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7318));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7215));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7217));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7218));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7218));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7219));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7188));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7190));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7192));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7193));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7194));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7197));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7198));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7199));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7200));

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "ProfileDokter", 1L, "http://localhost:3000/ProfilDoctor" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "Location", 1L, "http://localhost:3000/Location" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Name", "Url" },
                values: new object[] { "LevelLocation", "http://localhost:3000/locationlevel" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "DaftarPasien", 2L, "http://localhost:3000/Pasien" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "DaftarRelation", 1L, "http://localhost:3000/Relation" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Name", "Url" },
                values: new object[] { "DaftarBloodGroup", "http://localhost:3000/BloodGroup" });

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 1L,
                column: "MenuId",
                value: 4L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 2L,
                column: "MenuId",
                value: 5L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 3L,
                column: "MenuId",
                value: 7L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 4L,
                column: "MenuId",
                value: 8L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 3L, 2L });

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 6L, 3L });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6978));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6978));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6979));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6980));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6981));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7366));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7368));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6709));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6722));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6723));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6724));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6725));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6726));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6727));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6729));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6731));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6732));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6733));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6734));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6735));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6736));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(6737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7078));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7083));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7089));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7104));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7116));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7121));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7127));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7132));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7138));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7143));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7154));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7159));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7165));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7238));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7240));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7241));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7243));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7245));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7246));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7247));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7248));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7249));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7251));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7252));

            migrationBuilder.InsertData(
                table: "MasterUser",
                columns: new[] { "Id", "BiodataId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Email", "IsLocked", "Is_delete", "LastLogin", "LoginAttempt", "Modified_by", "Modified_on", "Password", "RoleId" },
                values: new object[] { 3L, 19L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "gio@gmail.com", false, false, new DateTime(2023, 12, 14, 11, 55, 47, 684, DateTimeKind.Local).AddTicks(7370), 0, null, null, "d84c1585455fe00e6149765bbdfc7d2f8cded19b5ec17bdc34b4046fa321a622", 3L });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6153));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6154));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6155));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6156));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6158));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6158));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6159));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6160));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6162));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6164));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6165));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6167));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6168));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6174));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6175));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6176));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6122));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6124));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6125));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6126));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6127));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6128));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6128));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6129));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6130));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6131));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6132));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6133));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6134));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6135));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6136));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6465));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6466));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6468));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6469));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6470));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6471));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6472));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6473));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6474));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6475));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6477));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6478));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6483));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6484));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6485));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6486));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6487));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6503));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6504));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6416));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6417));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6418));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6419));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6419));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6389));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6393));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6394));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6395));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6400));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6401));

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "CariDokter", 2L, "http://localhost:3000/SearchResult" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "DetailDokter", 2L, "http://localhost:3000/DetailsDokter" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Name", "Url" },
                values: new object[] { "ProfileDokter", "http://localhost:3000/ProfilDoctor" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "Location", 1L, "http://localhost:3000/Location" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "Name", "ParentId", "Url" },
                values: new object[] { "DaftarPasien", 2L, "http://localhost:3000/Pasien" });

            migrationBuilder.UpdateData(
                table: "MasterMenu",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Name", "Url" },
                values: new object[] { "DaftarRelation", "http://localhost:3000/Relation" });

            migrationBuilder.InsertData(
                table: "MasterMenu",
                columns: new[] { "Id", "BigIcon", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Name", "ParentId", "SmallIcon", "Url" },
                values: new object[] { 9L, null, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, "DaftarBloodGroup", 1L, null, "http://localhost:3000/BloodGroup" });

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 1L,
                column: "MenuId",
                value: 3L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 2L,
                column: "MenuId",
                value: 4L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 3L,
                column: "MenuId",
                value: 5L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 4L,
                column: "MenuId",
                value: 6L);

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 7L, 1L });

            migrationBuilder.UpdateData(
                table: "MasterMenuRole",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 8L, 1L });

            migrationBuilder.InsertData(
                table: "MasterMenuRole",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "MenuId", "Modified_by", "Modified_on", "RoleId" },
                values: new object[] { 8L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 5L, null, null, 2L });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6193));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6194));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6194));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6195));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6196));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6543));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6545));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5969));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5981));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5983));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5985));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5992));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5993));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6000));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6001));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6003));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6003));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6275));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6281));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6287));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6292));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6297));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6314));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6320));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6326));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6332));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6338));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6359));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6433));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6434));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6435));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6436));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6437));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6437));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6438));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6439));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6441));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6442));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6443));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6445));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6447));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6447));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 10, 20, 46, 18, DateTimeKind.Local).AddTicks(6449));

            migrationBuilder.InsertData(
                table: "MasterMenuRole",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "MenuId", "Modified_by", "Modified_on", "RoleId" },
                values: new object[] { 7L, 0L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, 9L, null, null, 1L });
        }
    }
}
