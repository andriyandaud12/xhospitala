﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class seedlocationLevelAndLocation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(716));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(719));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(720));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(722));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(723));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(725));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(726));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(728));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(729));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(731));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(732));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(734));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(735));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(738));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(740));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(741));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(743));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(744));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2333));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2301));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2310));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(607));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(610));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(611));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(615));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(616));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(618));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(619));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(621));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(622));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(624));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(625));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(627));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(677));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(679));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(681));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(682));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1345));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1347));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1349));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1352));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1354));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1355));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1357));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1361));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1363));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1364));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1366));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1368));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1370));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1372));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1373));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1400));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1402));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1204), 5L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1206), 5L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1207), 5L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1209), 5L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "LocationLevelId", "Name" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1222), 5L, "Liwuto" });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1211));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1212));

            migrationBuilder.InsertData(
                table: "MasterLocation",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "LocationLevelId", "Modified_by", "Modified_on", "Name", "ParentId" },
                values: new object[,]
                {
                    { 8L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1214), null, null, false, 4L, null, null, "Palmerah", null },
                    { 9L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1215), null, null, false, 4L, null, null, "Kebon Jeruk", null },
                    { 10L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1217), null, null, false, 4L, null, null, "Cengkareng", null },
                    { 11L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1219), null, null, false, 4L, null, null, "Kalideres", null },
                    { 12L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1224), null, null, false, 1L, null, null, "Sulawesi Tenggara", null },
                    { 13L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1220), null, null, false, 4L, null, null, "Kokalukuna", null },
                    { 14L, 1L, new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1225), null, null, false, 2L, null, null, "BauBau", null }
                });

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1563));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1565));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1566));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1567));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1569));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1160));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1162));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1164));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1165));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1167));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1169));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1170));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1174));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2036));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2038));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2040));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2041));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2043));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2045));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2046));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2048));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2049));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2051));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2052));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2054));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2055));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2057));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2093));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2095));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2096));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2098));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2100));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2101));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2103));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2104));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2106));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2107));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2109));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2110));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2112));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2115));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2117));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2118));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2120));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2121));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2123));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2125));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2126));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2128));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2129));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2131));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2133));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2134));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2136));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2137));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2139));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2140));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2142));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2143));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2145));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2146));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2148));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2149));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2151));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2153));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2154));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2156));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2157));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2159));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2160));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2162));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2165));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2166));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2168));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(780));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(783));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(784));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(786));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(787));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1507));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1511));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1513));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1603));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1611));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(302));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(315));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(316));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(317));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(319));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(320));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(322));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(324));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(325));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(326));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(328));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(329));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(331));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(332));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(334));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(335));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(337));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2352));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(915));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(938));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(949));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(982));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1002));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1012));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1075));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1085));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1115));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1125));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1632));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1719));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1721));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1723));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1726));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1728));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1734));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1745));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1749));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1757));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1758));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1760));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1763));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1765));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1768));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1774));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1778));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1779));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1781));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1783));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1784));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1786));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1789));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1792));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1795));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1798));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1834));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1836));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1837));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1839));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1842));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1845));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1847));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1848));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1850));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1852));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1853));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1855));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1857));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1858));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1860));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1861));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1863));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1865));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1868));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1875));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1879));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1884));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1886));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1888));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1890));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1892));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1895));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1899));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1901));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1903));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1904));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1906));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1907));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1943));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1945));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1951));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1953));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1957));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1962));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2238));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2248));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2250));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1254));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1294));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1295));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1297));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1300));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1301));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1303));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1306));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1310));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1314));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3336));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3341));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3345));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3351));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3355));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3361));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3364));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3368));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3372));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3376));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3379));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3382));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3385));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3388));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3391));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3394));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3397));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3400));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3402));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6218));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6156));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6172));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3198));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3203));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3205));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3208));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3210));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3212));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3215));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3217));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3219));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3233));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3236));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3238));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3245));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3277));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3281));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3284));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3287));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4672));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4677));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4682));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4685));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4689));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4693));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4701));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4708));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4712));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4718));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4721));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4726));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4730));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4735));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4738));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4793));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4837));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4840));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4532), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4535), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4537), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "LocationLevelId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4539), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "LocationLevelId", "Name" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4541), null, "Slipi" });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4544));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4546));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5029));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5033));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5036));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5038));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5041));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5043));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4284));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4288));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4291));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4294));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4297));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4303));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4307));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4311));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4316));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5746));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5751));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5800));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5804));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5807));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5810));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5813));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5816));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5818));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5821));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5823));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5826));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5828));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5831));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5833));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5836));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5838));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5841));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5843));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5845));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5848));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5850));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5852));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5855));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5858));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5860));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5863));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5865));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5867));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5870));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5872));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5875));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5877));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5880));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5882));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5885));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5887));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5890));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5892));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5894));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5900));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5902));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5905));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5907));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5910));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5912));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5915));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5917));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5920));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5922));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5925));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5927));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5970));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5973));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5976));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5978));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5981));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5983));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5986));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5989));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5991));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3516));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3520));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3522));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3524));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3526));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4949));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4953));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4956));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5117));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5181));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2787));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2805));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2808));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2811));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2813));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2815));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2818));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2820));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2822));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2824));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2827));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2829));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2831));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2931));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2943));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2946));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(2948));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6250));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3720));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3740));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3758));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3774));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3825));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3862));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(3988));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4011));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4031));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4050));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4183));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4202));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4219));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5220));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5223));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5226));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5229));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5231));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5234));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5247));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5249));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5252));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5255));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5257));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5260));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5262));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5265));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5267));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5270));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5272));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5275));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5277));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5280));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5283));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5285));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5288));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5293));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5295));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5300));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5303));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5305));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5308));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5316));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5318));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5321));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5365));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5372));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5376));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5379));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5382));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5385));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5388));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5391));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5394));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5397));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5403));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5409));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5413));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5417));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5423));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5426));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5429));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5433));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5437));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5440));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5443));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5448));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5456));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5460));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5465));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5471));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5479));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5482));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5485));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5488));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5491));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5494));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5498));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5501));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5504));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5507));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5510));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5513));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5516));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5519));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5522));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5525));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5529));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5573));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5577));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5582));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5585));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5588));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5590));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5596));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5598));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5601));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5603));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5606));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5608));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5611));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5613));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5615));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5618));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5623));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5626));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5629));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5631));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5634));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5639));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5642));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5644));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5648));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(5651));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6051));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6056));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6058));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6061));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6064));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6067));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(6069));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4587));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4590));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4593));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4595));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4597));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4600));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4602));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4604));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4608));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4610));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4612));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4616));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4618));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4624));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4626));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 12, 33, 177, DateTimeKind.Local).AddTicks(4629));
        }
    }
}
