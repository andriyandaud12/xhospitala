﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class parentIdEachLocation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3780));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3782));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3819));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3821));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3822));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3824));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3825));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3826));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3828));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3829));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3831));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3834));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3836));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3837));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3839));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3840));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3842));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3843));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5320));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5290));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5299));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3731));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3733));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3735));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3736));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3738));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3739));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3740));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3742));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3743));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3745));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3746));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3748));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3749));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3751));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3752));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3754));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3755));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4400));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4402));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4404));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4405));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4407));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4410));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4412));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4414));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4416));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4418));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4419));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4421));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4423));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4425));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4427));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4429));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4431));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4453));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4454));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4267), 8L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4270), 9L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4271), 10L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4273), 11L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4286), 13L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4274));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4276));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4278), 7L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4279), 7L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4281), 7L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4282), 7L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4287));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4284), 14L });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4289), 12L });

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4635));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4637));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4639));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4640));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4642));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4643));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4228));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4230));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4232));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4234));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4236));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4237));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4239));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4241));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5041));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5043));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5044));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5046));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5048));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5050));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5051));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5053));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5055));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5056));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5058));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5060));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5061));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5063));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5064));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5066));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5068));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5069));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5071));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5073));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5074));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5076));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5077));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5079));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5081));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5110));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5112));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5114));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5115));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5117));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5119));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5120));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5122));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5123));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5125));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5126));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5128));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5130));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5131));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5133));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5134));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5136));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5138));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5139));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5141));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5142));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5144));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5145));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5147));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5148));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5150));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5152));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5153));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5155));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5156));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5158));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5160));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5161));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5164));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5166));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5167));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5169));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3881));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3883));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3884));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3886));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3887));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4585));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4587));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4589));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4678));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4684));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3500));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3513));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3514));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3516));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3517));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3519));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3520));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3522));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3523));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3524));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3526));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3527));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3529));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3530));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3532));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3533));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3535));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5338));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5340));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(3998));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4011));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4021));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4031));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4041));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4051));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4061));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4071));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4080));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4090));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4100));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4110));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4120));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4131));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4140));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4150));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4159));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4707));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4709));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4711));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4712));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4714));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4716));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4717));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4719));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4720));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4722));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4724));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4727));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4729));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4730));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4764));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4766));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4769));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4771));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4772));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4774));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4775));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4777));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4778));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4780));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4782));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4783));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4785));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4786));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4789));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4793));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4796));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4798));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4800));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4801));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4803));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4805));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4809));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4811));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4817));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4819));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4820));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4822));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4824));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4825));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4827));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4828));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4830));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4831));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4833));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4835));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4836));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4838));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4867));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4874));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4879));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4884));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4886));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4887));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4889));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4890));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4892));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4895));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4897));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4900));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4901));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4903));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4904));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4906));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4908));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4911));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4913));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4915));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4917));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4918));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4920));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4921));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4923));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4924));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4926));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4929));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4930));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4932));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4934));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4935));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4937));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4938));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4940));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4941));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4943));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4945));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4973));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4976));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4978));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4980));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4983));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5205));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5210));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5211));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5212));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5214));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(5244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4318));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4320));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4321));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4323));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4324));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4326));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4327));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4328));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4330));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4331));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4332));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4334));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4335));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4337));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4338));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4369));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 54, 56, 118, DateTimeKind.Local).AddTicks(4371));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(716));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(719));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(720));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(722));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(723));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(725));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(726));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(728));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(729));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(731));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(732));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(734));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(735));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(737));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(738));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(740));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(741));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(743));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(744));

            migrationBuilder.UpdateData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2333));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2301));

            migrationBuilder.UpdateData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2310));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(607));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(610));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(611));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(613));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(615));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(616));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(618));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(619));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(621));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(622));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(624));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(625));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(627));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(677));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(679));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(681));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(682));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1342));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1345));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1347));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1349));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1352));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1354));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1355));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1357));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1359));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1361));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1363));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1364));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1366));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1368));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1370));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1372));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1373));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1400));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1402));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1204), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1206), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1207), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1209), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1222), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1211));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1212));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1214), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1215), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1217), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 11L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1219), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1224));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 13L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1220), null });

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 14L,
                columns: new[] { "Created_on", "ParentId" },
                values: new object[] { new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1225), null });

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1563));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1565));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1566));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1567));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1569));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1160));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1162));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1164));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1165));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1167));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1169));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1170));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1174));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2036));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2038));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2040));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2041));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2043));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2045));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2046));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2048));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2049));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2051));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2052));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2054));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2055));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2057));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2093));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2095));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2096));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2098));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2100));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2101));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2103));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2104));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2106));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2107));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2109));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2110));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2112));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2114));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2115));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2117));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2118));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2120));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2121));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2123));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2125));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2126));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2128));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2129));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2131));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2133));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2134));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2136));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2137));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2139));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2140));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2142));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2143));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2145));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2146));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2148));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2149));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2151));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2153));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2154));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2156));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2157));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2159));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2160));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2162));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2165));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2166));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2168));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(780));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(783));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(784));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(786));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(787));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1507));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1511));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1513));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1603));

            migrationBuilder.UpdateData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1611));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(302));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(315));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(316));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(317));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(319));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(320));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(322));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(324));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(325));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(326));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(328));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(329));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(331));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(332));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(334));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(335));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(337));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2352));

            migrationBuilder.UpdateData(
                table: "TransactionCustomerChat",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2354));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(915));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(927));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(938));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(949));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(971));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(982));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1002));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1012));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1065));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1075));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1085));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1095));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1105));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1115));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1125));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1632));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1635));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1637));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1638));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1719));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1721));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1723));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1725));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1726));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1728));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1729));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1731));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1734));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1736));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1737));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1745));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1749));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1757));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1758));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1760));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1762));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1763));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1765));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1767));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1768));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1770));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1773));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1774));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1776));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1778));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1779));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1781));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1783));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1784));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1786));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1789));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1791));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1792));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1794));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1795));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1797));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1798));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1834));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1836));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1837));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1839));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1841));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1842));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1844));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1845));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1847));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1848));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1850));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1852));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1853));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1855));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1857));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1858));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1860));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1861));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1863));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1865));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1868));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1870));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1871));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1873));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1875));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1876));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1878));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1879));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1881));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1882));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1884));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1886));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1888));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1890));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1892));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1893));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1895));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1896));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1899));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1901));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1903));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1904));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1906));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1907));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1909));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1943));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1945));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1946));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1948));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1950));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1951));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1953));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1954));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1957));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1959));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1961));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1962));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1964));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1966));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1967));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1969));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1970));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2236));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2238));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2239));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2242));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2244));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2248));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(2250));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1254));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1294));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1295));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1297));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1298));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1300));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1301));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1303));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1304));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1306));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1307));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1309));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1310));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1311));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1313));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 19, 3, 53, 31, 546, DateTimeKind.Local).AddTicks(1314));
        }
    }
}
