﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class seedAppointmentScheduleOfficetreatmentCustomerBloodgroup : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5554));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5556));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5558));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5560));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5561));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5563));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5565));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5567));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5569));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5570));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5572));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5574));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5575));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5577));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5578));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5580));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5582));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5583));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5585));

            migrationBuilder.InsertData(
                table: "MasterBloodGroup",
                columns: new[] { "Id", "Code", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Description", "Is_delete", "Modified_by", "Modified_on" },
                values: new object[] { 1L, "O", 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6971), null, null, "Goldar", false, null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5463));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5465));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5467));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5468));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5470));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5472));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5473));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5475));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5476));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5482));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5484));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5486));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5493));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5521));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5523));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5524));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5526));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6306));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6308));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6310));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6312));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6314));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6316));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6318));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6320));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6322));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6324));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6327));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6329));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6331));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6333));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6335));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6337));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6339));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6449));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6450));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6189));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6191));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6222));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6224));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6225));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6227));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6229));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6596));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6598));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6599));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6601));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6602));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6605));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6148));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6150));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6152));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6154));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6157));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6159));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6161));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6163));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6165));

            migrationBuilder.InsertData(
                table: "MasterMedicalFacilitySchedule",
                columns: new[] { "Id", "Created_by", "Created_on", "Day", "Deleted_by", "Deleted_on", "Is_delete", "MedicalFacilityId", "Modified_by", "Modified_on", "TimeScheduleEnd", "TimeScheduleStart" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6750), "Senin", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 2L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6752), "Selasa", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 3L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6753), "Rabu", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 4L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6755), "Kamis", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 5L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6757), "Jum'at", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 6L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6759), "Sabtu", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 7L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6761), "Minggu", null, null, false, 1L, null, null, "19.30", "08:00" },
                    { 8L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6763), "Senin", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 9L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6765), "Selasa", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 10L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6766), "Rabu", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 11L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6768), "Kamis", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 12L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6770), "Jum'at", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 13L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6772), "Sabtu", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 14L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6773), "Minggu", null, null, false, 2L, null, null, "19.30", "08:00" },
                    { 15L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6775), "Senin", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 16L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6777), "Selasa", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 17L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6779), "Rabu", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 18L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6780), "Kamis", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 19L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6782), "Jum'at", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 20L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6784), "Sabtu", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 21L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6786), "Minggu", null, null, false, 3L, null, null, "19.30", "08:00" },
                    { 22L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6787), "Senin", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 23L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6789), "Selasa", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 24L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6791), "Rabu", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 25L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6793), "Kamis", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 26L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6794), "Jum'at", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 27L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6796), "Sabtu", null, null, false, 4L, null, null, "19.30", "08:00" },
                    { 28L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6798), "Minggu", null, null, false, 4L, null, null, "19.30", "08:00" }
                });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5649));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5651));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5652));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5654));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5655));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6535));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6538));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6540));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5149));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5163));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5164));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5166));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5168));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5169));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5171));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5173));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5174));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5176));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5177));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5179));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5180));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5182));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5189));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5291));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5292));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5792));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5807));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5820));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5832));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5883));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5898));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5932));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5944));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5956));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5968));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5980));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(5992));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6004));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6086));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6101));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6114));

            migrationBuilder.InsertData(
                table: "TransactionDoctorOfficeTreatment",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorOfficeId", "DoctorTreatmentId", "Is_delete", "Modified_by", "Modified_on" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6828), null, null, 1L, 1L, false, null, null },
                    { 2L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6829), null, null, 1L, 2L, false, null, null },
                    { 3L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6831), null, null, 1L, 3L, false, null, null },
                    { 4L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6833), null, null, 1L, 4L, false, null, null },
                    { 5L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6834), null, null, 1L, 5L, false, null, null },
                    { 6L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6836), null, null, 1L, 6L, false, null, null },
                    { 7L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6890), null, null, 1L, 7L, false, null, null }
                });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6255));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6257));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6258));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6260));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6261));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6263));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6264));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6266));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6267));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6269));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6271));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6272));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6274));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6275));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6276));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6278));

            migrationBuilder.InsertData(
                table: "MasterCustomer",
                columns: new[] { "Id", "BiodataId", "BloodGroupId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Dob", "Gender", "Height", "Is_delete", "Modified_by", "Modified_on", "RhesusType", "Weight" },
                values: new object[,]
                {
                    { 1L, 18L, 1L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6935), null, null, new DateTime(1997, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "M", 170m, false, null, null, "RH+", 64m },
                    { 2L, 19L, 1L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6945), null, null, new DateTime(1999, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "M", 170m, false, null, null, "RH+", 64m }
                });

            migrationBuilder.InsertData(
                table: "TransactionDoctorOfficeSchedule",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "MedicalFacilityScheduleId", "Modified_by", "Modified_on", "Slot" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6715), null, null, 1L, false, 1L, null, null, 3 },
                    { 2L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6717), null, null, 1L, false, 2L, null, null, 3 },
                    { 3L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6719), null, null, 1L, false, 3L, null, null, 3 },
                    { 4L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6721), null, null, 1L, false, 4L, null, null, 3 },
                    { 5L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6723), null, null, 1L, false, 5L, null, null, 3 },
                    { 6L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6725), null, null, 1L, false, 6L, null, null, 3 },
                    { 7L, 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6726), null, null, 1L, false, 7L, null, null, 3 }
                });

            migrationBuilder.InsertData(
                table: "TransactionAppoinment",
                columns: new[] { "Id", "AppointmentDate", "Created_by", "Created_on", "CustomerId", "Deleted_by", "Deleted_on", "DoctorOfficeId", "DoctorOfficeScheduleId", "DoctorOfficeTreatmentId", "Is_delete", "Modified_by", "Modified_on" },
                values: new object[,]
                {
                    { 1L, new DateTime(2023, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6653), 1L, null, null, 1L, 1L, 1L, false, null, null },
                    { 2L, new DateTime(2023, 12, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, new DateTime(2023, 12, 16, 21, 11, 12, 153, DateTimeKind.Local).AddTicks(6663), 2L, null, null, 1L, 1L, 1L, false, null, null }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 18L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 19L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 20L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 21L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 22L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 23L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 24L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 25L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 26L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 27L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 28L);

            migrationBuilder.DeleteData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransactionAppoinment",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterCustomer",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeSchedule",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOfficeTreatment",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterBloodGroup",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacilitySchedule",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4160));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4163));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4164));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4166));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4168));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4169));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4171));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4172));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4174));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4176));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4177));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4179));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4181));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4182));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4184));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4186));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4187));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4189));

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4191));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4098));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4101));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4102));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4104));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4105));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4107));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4110));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4112));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4113));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4115));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4116));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4119));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4122));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4123));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4125));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4126));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4785));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4788));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4790));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4792));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4794));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4795));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4798));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4800));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4802));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4804));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4806));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4808));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4810));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4812));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4814));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4818));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4880));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4882));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4655));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4658));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4659));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4661));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4662));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4664));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4707));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5006));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5008));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5009));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5011));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5012));

            migrationBuilder.UpdateData(
                table: "MasterLocationLevel",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(5014));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4610));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4613));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4617));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4619));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4622));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4624));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4626));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4628));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4630));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4220));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4225));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4226));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4227));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4228));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 1L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4955));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 2L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4958));

            migrationBuilder.UpdateData(
                table: "MasterUser",
                keyColumn: "Id",
                keyValue: 3L,
                column: "LastLogin",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4961));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3770));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3781));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3783));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3784));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3786));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3789));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3790));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3792));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3793));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3795));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3796));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3798));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3800));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3801));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3803));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3805));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(3807));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4392));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4406));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4419));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4430));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4441));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4464));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4474));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4485));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4496));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4506));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4517));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4527));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4538));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4548));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4559));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4570));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4732));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4733));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4735));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4736));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4738));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4739));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4741));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4742));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4744));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4745));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4747));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4748));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4750));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4752));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4753));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4755));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Created_on",
                value: new DateTime(2023, 12, 14, 17, 0, 0, 672, DateTimeKind.Local).AddTicks(4756));
        }
    }
}
