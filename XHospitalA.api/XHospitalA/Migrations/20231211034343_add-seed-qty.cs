﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XHospitalA.Migrations
{
    /// <inheritdoc />
    public partial class addseedqty : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6977), "dr. Christie Imelda, Sp.PD" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6979), "dr.Hendra, Sp.A" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6981), "dr. Hery Setyobudi, Sp.S" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6982), "dr. Indra Tambunan, Sp.Og" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6984), "dr. Fenata Bandra, Sp.B" });

            migrationBuilder.InsertData(
                table: "MasterBiodata",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "FullName", "Image", "ImagePath", "Is_delete", "MobilePhone", "Modified_by", "Modified_on" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6986), null, null, "dr. Ade Chandra, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1617003874/image_doctor/dr.%20Ade%20Chandra%2C%20Sp.B.jpg.jpg", "", false, "081231231123", null, null },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6987), null, null, "dr. Ade Dian Anggraini, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1671668091/image_doctor/dr.-Dian-Anggraini-M.Sc.%2C-Sp.A-%28K%29-360490b6-6c04-4735-b307-251ded7d304f.png", "", false, "081231231123", null, null },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6989), null, null, "dr. Adhitya Angga Wardhana, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1552274637/image_doctor/adhitya-angga-wardhana.jpg.jpg", "", false, "081231231123", null, null },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6990), null, null, "dr. Adi Saputra, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1632096172/image_doctor/dr.%20Adi%20Saputra%2C%20Sp.B.png.png", "", false, "081231231123", null, null },
                    { 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6992), null, null, "dr. Adimas Haryanaputra Tjindarbumi, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1529381094/image_doctor/adimas-tjindarbumi.jpg.jpg", "", false, "081231231123", null, null },
                    { 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6993), null, null, "dr. Agus Endrawanto, Sp.B, M.Kes", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1549880972/image_doctor/dr.%20Agus%20Endrawanto.jpg.jpg", "", false, "081231231123", null, null },
                    { 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6995), null, null, "dr. Agustine, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1682668016/image_doctor/dr.-Agustine%2C-Sp.B-02306416-bfa9-4615-9d32-13ce94b08dd0.jpg", "", false, "081231231123", null, null },
                    { 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6996), null, null, "dr. Ahmad Fanani, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1690942162/image_doctor/dr.-Ahmad-Fanani%2C-Sp.B-8d818497-8381-45a0-a7f7-4197fb6386ed.jpg", "", false, "081231231123", null, null },
                    { 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6998), null, null, "dr. Aladin Sampara Johan, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1573461781/image_doctor/dr.%20Aladin%20Sampara%20Johan%2C%20Sp.B.jpg.jpg", "", false, "081231231123", null, null },
                    { 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6999), null, null, "dr. Amrul Mukminin, Sp.B, Sub.Sp.BE (K)", "https://th.bing.com/th/id/OIP.c2kGZtUz0nv7PR-mAGZjigAAAA?rs=1&pid=ImgDetMain", "", false, "081231231123", null, null },
                    { 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7001), null, null, "dr. Andreas Ariawan, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1674012813/image_doctor/dr.-Andreas-Ariawan%2C-Sp.B.JPG-c09389b3-51f3-4e4c-b1d6-b094604373f6.jpg", "", false, "081231231123", null, null },
                    { 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7002), null, null, "dr. Andreas Kurniawan Suwito, Sp.B", "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1678155563/image_doctor/dr.-Andreas-Kurniawan-Suwito%2C-Sp.B-dbf61718-5121-4eb6-ad37-f87abc9f4f2b.jpg", "", false, "081231231123", null, null }
                });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6830));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6832));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6834));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6929));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6931));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7573), "2021", "2019" });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7576), "2018", "2014" });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7578), "2020", "2018" });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7579), "2020", "2017" });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7581), "2021", "2016" });

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7634));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7636));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7422));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7424));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7425));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7426));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7427));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7379));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7381));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7383));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7386));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7387));

            migrationBuilder.InsertData(
                table: "MasterMedicalFacility",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Email", "Fax", "FullAddress", "Is_delete", "LocationId", "MedicalFacilityCategoryId", "Modified_by", "Modified_on", "Name", "Phone", "PhoneCode" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7389), null, null, "corporate.secretary@siloamhospitals.com", "5460075", "Gedung Fak. Kedokteran UPH Lt. 32, Jl. Boulevard Jend. Sudirman No. 15,", false, 1L, null, null, null, "Siloam Hospitals ", "25668000", "021" },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7391), null, null, "swnA@rwna.com", "5460075", "Kec. Torgamba labuhan batu", false, 2L, null, null, null, "RS Awal Bros Bagan Batu", "25668000", "021" },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7393), null, null, "resmitrakeluarga@mitra.com", "5460075", "Jl. Kenjeran 506 60113 Surabaya Jawa Timur", false, 2L, null, null, null, "Rs mitra keluarga kenjeran", "99000880", "021" },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7396), null, null, "corporate.secretary@siloamhospitals.com", "5460075", "Jl. Sultan Hasanuddin No. 58, Baubau", false, 4L, null, null, null, "Siloam Hospitals Buton", "25668000", "021" }
                });

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7031));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7033));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7034));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7035));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6608));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6617));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6619));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6620));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6621));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7146));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7158));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7169));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "Specialization" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7179), "Kandungan" });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "Specialization" },
                values: new object[] { new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7230), "Dokter Bedah" });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7451));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7452));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7453));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7455));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7456));

            migrationBuilder.InsertData(
                table: "MasterDoctor",
                columns: new[] { "Id", "BiodataId", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "Is_delete", "Modified_by", "Modified_on", "Str" },
                values: new object[,]
                {
                    { 6L, 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6932), null, null, false, null, null, "" },
                    { 7L, 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6934), null, null, false, null, null, "" },
                    { 8L, 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6935), null, null, false, null, null, "" },
                    { 9L, 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6937), null, null, false, null, null, "" },
                    { 10L, 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6938), null, null, false, null, null, "" },
                    { 11L, 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6940), null, null, false, null, null, "" },
                    { 12L, 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6941), null, null, false, null, null, "" },
                    { 13L, 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6943), null, null, false, null, null, "" },
                    { 14L, 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6944), null, null, false, null, null, "" },
                    { 15L, 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6946), null, null, false, null, null, "" },
                    { 16L, 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6947), null, null, false, null, null, "" },
                    { 17L, 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6948), null, null, false, null, null, "" }
                });

            migrationBuilder.InsertData(
                table: "MasterDoctorEducation",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "EducationLevelId", "EndYear", "InstitutionName", "Is_delete", "Major", "Modified_by", "Modified_on", "StartYear" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7583), null, null, 6L, 2L, "2019", "Universitas Udayana", false, "Spesialis Penyakit dalam", null, null, "2015" },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7585), null, null, 7L, 2L, "2016", "Universitas Gadjah Mada", false, "Spesialis Bedah", null, null, "2013" },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7587), null, null, 8L, 2L, "2011", "Universitas Mataram", false, "Spesialis Anak", null, null, "2006" },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7589), null, null, 9L, 2L, "2022", "Universitas Andalas", false, "Spesialis Penyakit Dalam", null, null, "2019" },
                    { 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7591), null, null, 10L, 2L, "2019", "Universitas Brawijaya", false, "Spesialis Anak", null, null, "2016" },
                    { 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7593), null, null, 11L, 2L, "2014", "Universitas Syiah Kuala", false, "Spesialis kandungan dan ginekologi", null, null, "2010" },
                    { 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7595), null, null, 12L, 2L, "2022", "Universitas Negeri Lampung", false, "Spesialis Anak", null, null, "2018" },
                    { 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7598), null, null, 13L, 2L, "2021", "Universitas Hasanuddin", false, "Spesialis Bedah", null, null, "2018" },
                    { 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7600), null, null, 14L, 2L, "2019", "Universitas Padjajaran", false, "Spesialis Saraf", null, null, "2016" },
                    { 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7602), null, null, 15L, 2L, "2020", "Universitas Negeri Jakarta", false, "Spesialis Penyakit Dalam", null, null, "2016" },
                    { 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7604), null, null, 16L, 2L, "2022", "Universitas Andalas", false, "Spesialis Anak", null, null, "2019" },
                    { 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7606), null, null, 17L, 2L, "2022", "Universitas Andalas", false, "Spesialis Kandungan", null, null, "2019" }
                });

            migrationBuilder.InsertData(
                table: "TransactionCurrentDoctorSpecialization",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "Modified_by", "Modified_on", "SpecializationId" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6623), null, null, 6L, false, null, null, 1L },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6624), null, null, 7L, false, null, null, 5L },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6626), null, null, 8L, false, null, null, 2L },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6627), null, null, 9L, false, null, null, 1L },
                    { 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6629), null, null, 10L, false, null, null, 2L },
                    { 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6630), null, null, 11L, false, null, null, 4L },
                    { 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6632), null, null, 12L, false, null, null, 2L },
                    { 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6633), null, null, 13L, false, null, null, 5L },
                    { 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6634), null, null, 14L, false, null, null, 3L },
                    { 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6636), null, null, 15L, false, null, null, 1L },
                    { 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6637), null, null, 16L, false, null, null, 2L },
                    { 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(6639), null, null, 17L, false, null, null, 4L }
                });

            migrationBuilder.InsertData(
                table: "TransactionDoctorOffice",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "EndDate", "Is_delete", "MedicalFacilityId", "Modified_by", "Modified_on", "Specialization", "StartDate" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7242), null, null, 6L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 6L, null, null, "Penyakit dalam", new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7252), null, null, 7L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 7L, null, null, "Dokter Bedah", new DateTime(2017, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7262), null, null, 8L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 7L, null, null, "Dokter Anak", new DateTime(2018, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7271), null, null, 9L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 9L, null, null, "Penyakit dalam", new DateTime(2021, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7281), null, null, 10L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 9L, null, null, "Dokter Anak", new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7290), null, null, 11L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 2L, null, null, "Dokter Kandungan", new DateTime(2022, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7300), null, null, 12L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 7L, null, null, "Dokter Anak", new DateTime(2019, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7309), null, null, 13L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 6L, null, null, "Dokter Bedah", new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7318), null, null, 14L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 5L, null, null, "Dokter Saraf", new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7328), null, null, 15L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 3L, null, null, "Penyakit dalam", new DateTime(2019, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7337), null, null, 16L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 8L, null, null, "Dokter Anak", new DateTime(2017, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7348), null, null, 17L, new DateTime(2023, 12, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), false, 8L, null, null, "Dokter Kandungan", new DateTime(2020, 2, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "TransactionDoctorTreatment",
                columns: new[] { "Id", "Created_by", "Created_on", "Deleted_by", "Deleted_on", "DoctorId", "Is_delete", "Modified_by", "Modified_on", "Name" },
                values: new object[,]
                {
                    { 6L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7458), null, null, 6L, false, null, null, "Diagnosis penyakit dalam" },
                    { 7L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7459), null, null, 7L, false, null, null, "Operasi Organ dalam" },
                    { 8L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7533), null, null, 8L, false, null, null, "Konsultasi kesehatan anak" },
                    { 9L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7534), null, null, 9L, false, null, null, "Konsultasi penyakit dalam" },
                    { 10L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7535), null, null, 10L, false, null, null, "Imunisasi Balita" },
                    { 11L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7537), null, null, 11L, false, null, null, "Persalinan" },
                    { 12L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7538), null, null, 12L, false, null, null, "Pengobatan cacing pada anak" },
                    { 13L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7540), null, null, 13L, false, null, null, "Operasi pengangkatan organ" },
                    { 14L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7541), null, null, 14L, false, null, null, "Konsultasi penyakit ayan" },
                    { 15L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7542), null, null, 15L, false, null, null, "Diagnosis penyakit lambung" },
                    { 16L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7544), null, null, 16L, false, null, null, "Pengobatan demam anak" },
                    { 17L, 1L, new DateTime(2023, 12, 11, 10, 43, 43, 151, DateTimeKind.Local).AddTicks(7545), null, null, 17L, false, null, null, "Konsultasi kesehatan janin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 17L);

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7674), "Christie Imelda" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7675), "Hendra" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7676), "Hery Setyobudi" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7677), "Indra Tambunan" });

            migrationBuilder.UpdateData(
                table: "MasterBiodata",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "FullName" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7678), "Fenata Bandra" });

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7654));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7656));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7657));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7658));

            migrationBuilder.UpdateData(
                table: "MasterDoctor",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7659));

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7885), null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7887), null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7888), null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7889), null, null });

            migrationBuilder.UpdateData(
                table: "MasterDoctorEducation",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "EndYear", "StartYear" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7890), null, null });

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7904));

            migrationBuilder.UpdateData(
                table: "MasterEducationLevel",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7905));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7848));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7849));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7850));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7850));

            migrationBuilder.UpdateData(
                table: "MasterLocation",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7851));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7829));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7830));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7832));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7833));

            migrationBuilder.UpdateData(
                table: "MasterMedicalFacility",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7834));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7695));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7696));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7697));

            migrationBuilder.UpdateData(
                table: "MasterSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7698));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7531));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7541));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7542));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7543));

            migrationBuilder.UpdateData(
                table: "TransactionCurrentDoctorSpecialization",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7544));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7780));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7787));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7799));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "Created_on", "Specialization" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7805), "THT" });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorOffice",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "Created_on", "Specialization" },
                values: new object[] { new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7811), "Bedah" });

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7866));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7867));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7868));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7869));

            migrationBuilder.UpdateData(
                table: "TransactionDoctorTreatment",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Created_on",
                value: new DateTime(2023, 12, 11, 8, 34, 14, 859, DateTimeKind.Local).AddTicks(7869));
        }
    }
}
