﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_doctor")]
    public class Doctor: BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? BiodataId { get; set; }

        [MaxLength(50)]
        public string? Str { get; set; }

        [ForeignKey("BiodataId")]
        public virtual Biodata Biodata { get; set; }
    }
}
