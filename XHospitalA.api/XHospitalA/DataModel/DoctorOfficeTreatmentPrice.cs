﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_doctor_office_treatment_price")]
    public class DoctorOfficeTreatmentPrice : BaseProperties    
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorOfficeTreatmentId { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceStartFrom { get; set; }
        public decimal? PriceUntilFrom { get; set; }

        [ForeignKey("DoctorOfficeTreatmentId")]
        public virtual DoctorOfficeTreatment DoctorOfficeTreatment { get; set; }   
    }
}
