﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_user")]
    public class User : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? BiodataId { get; set; }

        public long? RoleId { get; set; }

        [MaxLength(100)]
        public string? Email { get; set; }

        [MaxLength(255)]
        public string? Password { get; set; }

        public int? LoginAttempt { get; set; }

        public bool? IsLocked { get; set; }

        public DateTime? LastLogin { get; set; }

        [ForeignKey("BiodataId")]
        public virtual Biodata Biodata { get; set; }

        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
    }
}
