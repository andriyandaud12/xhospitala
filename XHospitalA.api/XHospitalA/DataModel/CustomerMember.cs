﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XHospitalA.DataModel
{
    [Table("m_customer_member")]
    public class CustomerMember : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? ParentBiodataId { get; set; }
        public long? CustomerId { get; set; }
        public long? CustomerRelationId { get; set; }

        [ForeignKey("ParentBiodataId")]
        public virtual Biodata Biodata { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        [ForeignKey("CustomerRelationId")]
        public virtual CustomerRelation CustomerRelation { get; set; }


    }
}
