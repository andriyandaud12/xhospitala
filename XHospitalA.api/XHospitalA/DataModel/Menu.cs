﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_menu")]
    public class Menu : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(20)]
        public string? Name { get; set; }

        [MaxLength(50)]
        public string? Url { get; set; }

        public long? ParentId { get; set; }

        [MaxLength(100)]
        public string? BigIcon { get; set; }

        [MaxLength(100)]
        public string? SmallIcon { get; set; }

        [ForeignKey("ParentId")]
        public virtual Menu Menus { get; set; }
    }
}
