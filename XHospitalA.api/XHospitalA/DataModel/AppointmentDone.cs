﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_appointment_done")]
    public class AppointmentDone : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? AppoinmentId { get; set; }
        public string Diagnosis { get; set; }

        [ForeignKey("AppoinmentId")]
        public virtual Appointment Appointments { get; set; }
    }
}
