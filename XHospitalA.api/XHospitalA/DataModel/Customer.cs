﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_customer")]
    public class Customer:BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? BiodataId { get; set; }
        public DateTime? Dob { get; set; }
        [MaxLength(1)]
        public string? Gender { get; set; }
        public long? BloodGroupId { get; set; }
        [MaxLength(5)]
        public string? RhesusType { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }

        [ForeignKey("BiodataId")]
        public virtual Biodata Biodata { get; set; }
        [ForeignKey("BloodGroupId")]
        public virtual BloodGroup BloodGroup { get; set; }
        
    }
}
