﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Numerics;

namespace XHospitalA.DataModel
{
    [Table("t_doctor_office_schedule")]
    public class DoctorOfficeSchedule : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? MedicalFacilityScheduleId { get; set; }
        public int? Slot{ get; set; }

        [ForeignKey("DoctorId")]
        public virtual Doctor Doctor { get; set; }

        [ForeignKey("MedicalFacilityScheduleId")]
        public virtual MedicalFacilitySchedule MedicalFacilitySchedule { get; set; }
    }
}
