﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_appoinment")]
    public class Appointment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public long? DoctorOfficeId { get; set; }
        public long? DoctorOfficeScheduleId { get; set; }
        public long? DoctorOfficeTreatmentId { get; set; }
        public DateTime? AppointmentDate{ get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer{ get; set; }

        [ForeignKey("DoctorOfficeId")]
        public virtual DoctorOffice DoctorOffice { get; set; }

        [ForeignKey("DoctorOfficeScheduleId")]
        public virtual DoctorOfficeSchedule DoctorOfficeSchedule { get; set; }

        [ForeignKey("DoctorOfficeTreatmentId")]
        public virtual DoctorOfficeTreatment DoctorOfficeTreatment { get; set; }
    }
}
