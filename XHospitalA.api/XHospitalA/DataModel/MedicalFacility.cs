﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Numerics;

namespace XHospitalA.DataModel
{
    [Table("m_medical_facility")]
    public class MedicalFacility : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string? Name{ get; set; }
        public long? MedicalFacilityCategoryId { get; set; }
        public long? LocationId { get; set; }
        public string? FullAddress { get; set; }
        [Required, MaxLength(100)]
        public string? Email { get; set; }
        [Required, MaxLength(10)]
        public string? PhoneCode { get; set; }
        [Required, MaxLength(15)]
        public string? Phone { get; set; }
        [Required, MaxLength(15)]
        public string? Fax { get; set; }

        [ForeignKey("MedicalFacilityCategoryId")]
        public virtual MedicalFacilityCategory MedicalFacilityCategory { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    }
}
