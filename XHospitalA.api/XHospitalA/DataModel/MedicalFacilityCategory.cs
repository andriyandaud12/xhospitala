﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_medical_facility_category")]
    public class MedicalFacilityCategory : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string? Name { get; set; }
    }
}
    