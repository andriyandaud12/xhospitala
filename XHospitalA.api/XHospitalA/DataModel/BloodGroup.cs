﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_blood_group")]
    public class BloodGroup : BaseProperties
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(5)]
        public string? Code { get; set; }
        [MaxLength(255)]
        public string? Description { get; set; }

    }
}
