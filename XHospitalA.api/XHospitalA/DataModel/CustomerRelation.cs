﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XHospitalA.DataModel
{
    [Table("m_customer_relation")]
    public class CustomerRelation: BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(50)]
        public string? Name { get; set; }
    }
    
}
