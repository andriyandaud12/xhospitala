﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_biodata")]
    public class Biodata : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(255)]
        public string? FullName { get; set; }

        [MaxLength(15)]
        public string? MobilePhone { get; set; }

        public string? Image { get; set; }

        [MaxLength(255)]
        public string? ImagePath { get; set; }
    }
}
