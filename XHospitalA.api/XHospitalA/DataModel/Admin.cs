﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_admin")]
    public class Admin : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? BiodataId { get; set; }

        [MaxLength(10)]
        public string? Code { get; set; }

        [ForeignKey("BiodataId")]
        public virtual Biodata Biodata { get; set; }
    }
}
