﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_doctor_treatment")]
    public class DoctorTreatment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorId { get; set; }

        [Required, MaxLength(50)]
        public string? Name{ get; set; }

        [ForeignKey("DoctorId")]
        public virtual Doctor Doctor{ get; set; }
    }
}
