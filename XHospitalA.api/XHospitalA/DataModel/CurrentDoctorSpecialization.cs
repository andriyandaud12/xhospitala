﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_current_doctor_specialization")]
    public class CurrentDoctorSpecialization : BaseProperties
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? DoctorId { get; set; }

        public long? SpecializationId { get; set; }

        [ForeignKey("DoctorId")]
        public virtual Doctor Doctor { get; set; }

        [ForeignKey("SpecializationId")]
        public virtual Specialization Specialization { get; set; }

    }
}
