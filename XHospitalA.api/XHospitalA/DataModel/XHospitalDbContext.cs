﻿using Microsoft.EntityFrameworkCore;

namespace XHospitalA.DataModel
{
    public class XHospitalDbContext : DbContext
    {
        public XHospitalDbContext(DbContextOptions<XHospitalDbContext> options) : base(options)
        {
        }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentDone> AppointmentDones { get; set; }
        public DbSet<Biodata> Biodata { get; set; }
        public DbSet<BiodataAddress> BiodataAddress { get; set; }
        public DbSet<BloodGroup> BloodGroups { get; set; }
        public DbSet<CurrentDoctorSpecialization> CurrentDoctorSpecializations { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerMember> CustomerMembers { get; set; }
        public DbSet<CustomerRelation> CustomerRelations { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<DoctorEducation> DoctorEducations { get; set; }
        public DbSet<DoctorOffice> DoctorOffices { get; set; }
        public DbSet<DoctorOfficeSchedule> DoctorOfficeSchedules { get; set; }
        public DbSet<DoctorOfficeTreatment> DoctorOfficeTreatments { get; set; }
        public DbSet<DoctorOfficeTreatmentPrice> DoctorOfficeTreatmentPrices { get; set; }
        public DbSet<DoctorTreatment> DoctorTreatments { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LocationLevel> LocationLevels { get; set; }
        public DbSet<MedicalFacility> MedicalFacilities { get; set; }
        public DbSet<MedicalFacilityCategory> medicalFacilityCategories { get; set; }
        public DbSet<MedicalFacilitySchedule> MedicalFacilitySchedules { get; set; }
        public DbSet<Specialization> Specializations { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuRole> MenuRoles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<CustomerChat> CustomerChats { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<BaseProperties>()
            //    .Property(o => o.Is_delete)
            //    .HasDefaultValue(false);
            modelBuilder.Seed();
            modelBuilder.Entity<Admin>()
                .Property(o => o.BiodataId)
                .IsRequired(false);

            modelBuilder.Entity<Appointment>()
               .Property(o => o.CustomerId)
               .IsRequired(false);

            modelBuilder.Entity<Appointment>()
               .Property(o => o.DoctorOfficeId)
               .IsRequired(false);

            modelBuilder.Entity<Appointment>()
               .Property(o => o.DoctorOfficeScheduleId)
               .IsRequired(false);

            modelBuilder.Entity<Appointment>()
               .Property(o => o.DoctorOfficeTreatmentId)
               .IsRequired(false);

            modelBuilder.Entity<AppointmentDone>()
               .Property(o => o.AppoinmentId)
               .IsRequired(false);

            modelBuilder.Entity<BiodataAddress>()
                .Property(o => o.BiodataId)
                .IsRequired(false);

            modelBuilder.Entity<BiodataAddress>()
                .Property(o => o.LocationId)
                .IsRequired(false);

            modelBuilder.Entity<Customer>()
                .Property(o => o.BiodataId)
                .IsRequired(false);

            modelBuilder.Entity<Customer>()
                .Property(o => o.BloodGroupId)
                .IsRequired(false);
            modelBuilder.Entity<User>()
                            .Property(o => o.BiodataId)
                            .IsRequired(false);
            modelBuilder.Entity<User>()
                            .Property(o => o.RoleId)
                            .IsRequired(false);

            modelBuilder.Entity<Customer>()
                .Property(o => o.Weight)
                .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<Customer>()
               .Property(o => o.Height)
               .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<CustomerMember>()
                .Property(o => o.ParentBiodataId)
                .IsRequired(false);

            modelBuilder.Entity<CustomerMember>()
                .Property(o => o.CustomerId)
                .IsRequired(false);

            modelBuilder.Entity<CustomerMember>()
                .Property(o => o.CustomerRelationId)
                .IsRequired(false);

            modelBuilder.Entity<Doctor>()
                .Property(o => o.BiodataId)
                .IsRequired(false);

            modelBuilder.Entity<DoctorEducation>()
                .Property(o => o.DoctorId)
                .IsRequired(false);

            modelBuilder.Entity<DoctorEducation>()
                .Property(o => o.EducationLevelId)
                .IsRequired(false);

            modelBuilder.Entity<DoctorEducation>()
               .Property(o => o.IsLastEducation)
               .HasDefaultValue(false);

            modelBuilder.Entity<Location>()
               .Property(o => o.ParentId)
               .IsRequired(false);

            modelBuilder.Entity<Location>()
               .Property(o => o.LocationLevelId)
               .IsRequired(false);

            modelBuilder.Entity<MedicalFacility>()
               .Property(o => o.MedicalFacilityCategoryId)
               .IsRequired(false);

            modelBuilder.Entity<MedicalFacility>()
               .Property(o => o.LocationId)
               .IsRequired(false);

            modelBuilder.Entity<MedicalFacilitySchedule>()
               .Property(o => o.MedicalFacilityId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOfficeTreatment>()
               .Property(o => o.DoctorTreatmentId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOfficeTreatment>()
               .Property(o => o.DoctorOfficeId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOffice>()
               .Property(o => o.DoctorId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOffice>()
               .Property(o => o.MedicalFacilityId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOfficeTreatmentPrice>()
               .Property(o => o.DoctorOfficeTreatmentId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOfficeTreatmentPrice>()
               .Property(o => o.Price)
               .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<DoctorOfficeTreatmentPrice>()
               .Property(o => o.PriceStartFrom)
               .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<DoctorOfficeTreatmentPrice>()
               .Property(o => o.PriceUntilFrom)
               .HasColumnType("decimal(18,4)");

            modelBuilder.Entity<DoctorOfficeSchedule>()
               .Property(o => o.DoctorId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorOfficeSchedule>()
               .Property(o => o.MedicalFacilityScheduleId)
               .IsRequired(false);

            modelBuilder.Entity<AppointmentDone>()
               .Property(o => o.AppoinmentId)
               .IsRequired(false);

            modelBuilder.Entity<DoctorTreatment>()
               .Property(o => o.DoctorId)
               .IsRequired(false);
        }

    }
    public static class ModelBuilderExtention
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrentDoctorSpecialization>()
                .HasData(
                 new CurrentDoctorSpecialization
                 {
                     Id = 1,
                     Is_delete = false,
                     DoctorId = 1,
                     SpecializationId = 1, //penyakit dalam
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 2,
                     Is_delete = false,
                     DoctorId = 2,
                     SpecializationId = 2, //anak
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 3,
                     Is_delete = false,
                     DoctorId = 3,
                     SpecializationId = 3,//saraf
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 4,
                     Is_delete = false,
                     DoctorId = 4,
                     SpecializationId = 4, //kandungan
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 5,
                     Is_delete = false,
                     DoctorId = 5,
                     SpecializationId = 5, //bedah
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 6,
                     Is_delete = false,
                     DoctorId = 6,
                     SpecializationId = 1, //penyakit dalam
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 7,
                     Is_delete = false,
                     DoctorId = 7,
                     SpecializationId = 5, //bedah
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 8,
                     Is_delete = false,
                     DoctorId = 8,
                     SpecializationId = 2, //anak
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 9,
                     Is_delete = false,
                     DoctorId = 9,
                     SpecializationId =1, //penyakit dalam
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 10,
                     Is_delete = false,
                     DoctorId = 10,
                     SpecializationId = 2, //anak
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 11,
                     Is_delete = false,
                     DoctorId = 11,
                     SpecializationId = 4, //kandungan
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 12,
                     Is_delete = false,
                     DoctorId = 12,
                     SpecializationId = 2, //anak
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 13,
                     Is_delete = false,
                     DoctorId = 13,
                     SpecializationId = 5, //bedah
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 14,
                     Is_delete = false,
                     DoctorId = 14,
                     SpecializationId = 3, //saraf
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 15,
                     Is_delete = false,
                     DoctorId = 15,
                     SpecializationId = 1, //penyakit dalam
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 16,
                     Is_delete = false,
                     DoctorId = 16,
                     SpecializationId = 2, //anak
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new CurrentDoctorSpecialization
                 {
                     Id = 17,
                     Is_delete = false,
                     DoctorId = 17,
                     SpecializationId = 4, //kandungan
                     Created_by = 1,
                     Created_on = DateTime.Now
                 }
                );
            modelBuilder.Entity<Doctor>()
                .HasData(
                 new Doctor
                 {
                     Id = 1,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 1,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 2,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 2,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 3,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 3,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 4,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 4,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 5,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 5,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 6,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 6,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 7,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 7,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 8,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 8,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 9,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 9,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 10,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 10,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 11,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 11,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 12,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 12,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 13,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 13,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 14,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 14,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 15,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 15,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 16,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 16,
                     Str = "",
                 },
                 new Doctor
                 {
                     Id = 17,
                     Is_delete = false,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     BiodataId = 17,
                     Str = "",
                 }
                );
            modelBuilder.Entity<Biodata>()
               .HasData(
                new Biodata
                {
                    Id = 1,
                    Is_delete = false,
                    FullName="dr. Christie Imelda, Sp.PD",
                    Image = "https://media.istockphoto.com/photos/portrait-of-confident-muslim-doctor-wearing-hijab-standing-at-the-of-picture-id1151233766",
                    MobilePhone = "081231231123",
                    ImagePath="",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 2,
                    Is_delete = false,
                    FullName = "dr.Hendra, Sp.A",
                    Image = " https://media.npr.org/assets/news/2010/02/25/doctor-3900f8c9b6c5be80f3b2e509eafe9c41d7ad2671.jpg  ",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 3,
                    Is_delete = false,
                    FullName = "dr. Hery Setyobudi, Sp.S",
                    Image = "https://thumbs.dreamstime.com/b/portrait-happy-african-doctor-private-clinic-confident-mature-black-consulting-digital-tablet-looking-camera-smiling-164999099.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 4,
                    Is_delete = false,
                    FullName = "dr. Indra Tambunan, Sp.OG",
                    Image = "https://jay-harold.com/wp-content/uploads/2015/12/Dollarphotoclub_74864725.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 5,
                    Is_delete = false,
                    FullName = "dr. Fenata Bandra, Sp.B",
                    Image = "https://th.bing.com/th/id/OIP.WPYPa4GubQVLa0kQqXcfvwHaHa?rs=1&pid=ImgDetMain",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 6, 
                    Is_delete = false,
                    FullName = "dr. Ade Chandra, Sp.PD",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1617003874/image_doctor/dr.%20Ade%20Chandra%2C%20Sp.B.jpg.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 7,
                    Is_delete = false,
                    FullName = "dr. Ade Dian Anggraini, Sp.B",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1671668091/image_doctor/dr.-Dian-Anggraini-M.Sc.%2C-Sp.A-%28K%29-360490b6-6c04-4735-b307-251ded7d304f.png",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 8,
                    Is_delete = false,
                    FullName = "dr. Adhitya Angga Wardhana, Sp.A",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1552274637/image_doctor/adhitya-angga-wardhana.jpg.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 9,
                    Is_delete = false,
                    FullName = "dr. Adi Saputra, Sp.PD",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1632096172/image_doctor/dr.%20Adi%20Saputra%2C%20Sp.B.png.png",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 10,
                    Is_delete = false,
                    FullName = "dr. Adimas Haryanaputra Tjindarbumi, Sp.A",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1529381094/image_doctor/adimas-tjindarbumi.jpg.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                }
                ,
                new Biodata
                {
                    Id = 11,
                    Is_delete = false,
                    FullName = "dr. Agus Endrawanto, Sp.OG",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1549880972/image_doctor/dr.%20Agus%20Endrawanto.jpg.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 12,
                    Is_delete = false,
                    FullName = "dr. Agustine, Sp.A",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1682668016/image_doctor/dr.-Agustine%2C-Sp.B-02306416-bfa9-4615-9d32-13ce94b08dd0.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 13,
                    Is_delete = false,
                    FullName = "dr. Ahmad Fanani, Sp.B",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1690942162/image_doctor/dr.-Ahmad-Fanani%2C-Sp.B-8d818497-8381-45a0-a7f7-4197fb6386ed.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 14,
                    Is_delete = false,
                    FullName = "dr. Aladin Sampara Johan, Sp.S",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1573461781/image_doctor/dr.%20Aladin%20Sampara%20Johan%2C%20Sp.B.jpg.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 15,
                    Is_delete = false,
                    FullName = "dr. Amrul Mukminin, Sp.PD",
                    Image = "https://th.bing.com/th/id/OIP.c2kGZtUz0nv7PR-mAGZjigAAAA?rs=1&pid=ImgDetMain",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 16,
                    Is_delete = false,
                    FullName = "dr. Andreas Ariawan, Sp.A",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1674012813/image_doctor/dr.-Andreas-Ariawan%2C-Sp.B.JPG-c09389b3-51f3-4e4c-b1d6-b094604373f6.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 17,
                    Is_delete = false,
                    FullName = "dr. Andreas Kurniawan Suwito, Sp.OG",
                    Image = "https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,h_100,c_thumb,dpr_2.0,f_auto,q_auto/v1678155563/image_doctor/dr.-Andreas-Kurniawan-Suwito%2C-Sp.B-dbf61718-5121-4eb6-ad37-f87abc9f4f2b.jpg",
                    MobilePhone = "081231231123",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 18,
                    Is_delete = false,
                    FullName = "Gilang Ardi Baskara Alauidin",
                    Image = "https://www.google.com/url?sa=i&url=https%3A%2F%2Ftayostation.com%2Fmeet-tayo-and-friends%2F&psig=AOvVaw2apKnA_qNh9MQrlHYrCwDz&ust=1702434295888000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCPClsI_siIMDFQAAAAAdAAAAABAD",
                    MobilePhone = "081390094000",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Biodata
                {
                    Id = 19,
                    Is_delete = false,
                    FullName = "Giovanni",
                    Image = "https://www.google.com/url?sa=i&url=https%3A%2F%2Ftayostation.com%2Fmeet-tayo-and-friends%2F&psig=AOvVaw2apKnA_qNh9MQrlHYrCwDz&ust=1702434295888000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCPClsI_siIMDFQAAAAAdAAAAABAD",
                    MobilePhone = "081390094001",
                    ImagePath = "",
                    Created_by = 1,
                    Created_on = DateTime.Now
                }
               );
            modelBuilder.Entity<Specialization>()
                .HasData(
                 new Specialization
                 {
                     Id = 1,
                     Is_delete = false,
                     Name = "Penyakit Dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new Specialization
                 {
                     Id = 2,
                     Is_delete = false,
                     Name = "Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new Specialization
                 {
                     Id = 3,
                     Is_delete = false,
                     Name = "Saraf",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new Specialization
                 {
                     Id = 4,
                     Is_delete = false,
                     Name = "Kandungan dan ginekologi",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new Specialization
                 {
                     Id = 5,
                     Is_delete = false,
                     Name = "Bedah",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 }
                );
            modelBuilder.Entity<DoctorOffice>()
                .HasData(
                 new DoctorOffice
                 { 
                     Id = 1, 
                     Is_delete = false, 
                     DoctorId = 1, 
                     StartDate = DateTime.Parse("2023-01-07"), 
                     EndDate = DateTime.Parse("2023-12-07"), 
                     MedicalFacilityId = 1,
                     Specialization = "Penyakit Dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 2,
                     Is_delete = false,
                     DoctorId = 2,
                     StartDate = DateTime.Parse("2023-01-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 2,
                     Specialization = "Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 3,
                     Is_delete = false,
                     DoctorId = 3,
                     StartDate = DateTime.Parse("2023-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 3,
                     Specialization = "Saraf",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 { Id = 4, Is_delete = false,
                     DoctorId = 4,
                     StartDate = DateTime.Parse("2023-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 4,
                     Specialization = "Kandungan",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 { Id = 5, Is_delete = false,
                     DoctorId = 5,
                     StartDate = DateTime.Parse("2023-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 5,
                     Specialization = "Dokter Bedah",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 6,
                     Is_delete = false,
                     DoctorId = 6,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 6,
                     Specialization = "Penyakit dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 7,
                     Is_delete = false,
                     DoctorId = 7,
                     StartDate = DateTime.Parse("2017-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 7,
                     Specialization = "Dokter Bedah",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 8,
                     Is_delete = false,
                     DoctorId = 8,
                     StartDate = DateTime.Parse("2018-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 8,
                     Specialization = "Dokter Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 9,
                     Is_delete = false,
                     DoctorId = 9,
                     StartDate = DateTime.Parse("2021-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 9,
                     Specialization = "Penyakit dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 10,
                     Is_delete = false,
                     DoctorId = 10,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 9,
                     Specialization = "Dokter Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 11,
                     Is_delete = false,
                     DoctorId = 11,
                     StartDate = DateTime.Parse("2022-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 2,
                     Specialization = "Dokter Kandungan",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 12,
                     Is_delete = false,
                     DoctorId = 12,
                     StartDate = DateTime.Parse("2019-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 7,
                     Specialization = "Dokter Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 13,
                     Is_delete = false,
                     DoctorId = 13,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 6,
                     Specialization = "Dokter Bedah",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 14,
                     Is_delete = false,
                     DoctorId = 14,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 5,
                     Specialization = "Dokter Saraf",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 15,
                     Is_delete = false,
                     DoctorId = 15,
                     StartDate = DateTime.Parse("2019-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 3,
                     Specialization = "Penyakit dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 16,
                     Is_delete = false,
                     DoctorId = 16,
                     StartDate = DateTime.Parse("2017-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 8,
                     Specialization = "Dokter Anak",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorOffice
                 {
                     Id = 17,
                     Is_delete = false,
                     DoctorId = 17,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 8,
                     Specialization = "Dokter Kandungan",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 //kantor 2 doktorId = 1
                 new DoctorOffice
                 {
                     Id = 18,
                     Is_delete = false,
                     DoctorId = 1,
                     StartDate = DateTime.Parse("2020-02-07"),
                     EndDate = DateTime.Parse("2023-12-07"),
                     MedicalFacilityId = 2,
                     Specialization = "Penyakit Dalam",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 }
                );
            modelBuilder.Entity<DoctorOfficeTreatmentPrice>()
                .HasData(
                new DoctorOfficeTreatmentPrice
                {
                    Id = 1,
                    DoctorOfficeTreatmentId = 1,
                    Price = 30000,
                    PriceStartFrom = 300000,
                    PriceUntilFrom = 400000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 2,
                    DoctorOfficeTreatmentId = 2,
                    Price = 40000,
                    PriceStartFrom = 250000,
                    PriceUntilFrom = 300000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 3,
                    DoctorOfficeTreatmentId = 3,
                    Price = 20000,
                    PriceStartFrom = 150000,
                    PriceUntilFrom = 200000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 4,
                    DoctorOfficeTreatmentId = 4,
                    Price = 50000,
                    PriceStartFrom = 300000,
                    PriceUntilFrom = 500000,
                }, new DoctorOfficeTreatmentPrice
                {
                    Id = 5,
                    DoctorOfficeTreatmentId = 5,
                    Price = 25000,
                    PriceStartFrom = 100000,
                    PriceUntilFrom = 200000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 6,
                    DoctorOfficeTreatmentId = 6,
                    Price = 45000,
                    PriceStartFrom = 400000,
                    PriceUntilFrom = 450000,
                }, new DoctorOfficeTreatmentPrice
                {
                    Id = 7,
                    DoctorOfficeTreatmentId = 7,
                    Price = 35000,
                    PriceStartFrom = 600000,
                    PriceUntilFrom = 800000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 8,
                    DoctorOfficeTreatmentId = 8,
                    Price = 30000,
                    PriceStartFrom = 300000,
                    PriceUntilFrom = 400000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 9,
                    DoctorOfficeTreatmentId = 9,
                    Price = 40000,
                    PriceStartFrom = 250000,
                    PriceUntilFrom = 300000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 10,
                    DoctorOfficeTreatmentId = 10,
                    Price = 20000,
                    PriceStartFrom = 150000,
                    PriceUntilFrom = 200000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 11,
                    DoctorOfficeTreatmentId = 11,
                    Price = 50000,
                    PriceStartFrom = 300000,
                    PriceUntilFrom = 500000,
                }, new DoctorOfficeTreatmentPrice
                {
                    Id = 12,
                    DoctorOfficeTreatmentId = 12,
                    Price = 25000,
                    PriceStartFrom = 100000,
                    PriceUntilFrom = 200000,
                },
                new DoctorOfficeTreatmentPrice
                {
                    Id = 13,
                    DoctorOfficeTreatmentId = 13,
                    Price = 45000,
                    PriceStartFrom = 400000,
                    PriceUntilFrom = 450000,
                }, new DoctorOfficeTreatmentPrice
                {
                    Id = 14,
                    DoctorOfficeTreatmentId = 14,
                    Price = 35000,
                    PriceStartFrom = 600000,
                    PriceUntilFrom = 800000,
                }
                );
            modelBuilder.Entity<MedicalFacility>()
                .HasData(
                 new MedicalFacility
                 {
                     Id = 1,
                     Is_delete = false,
                     Email = "rsab@gmail.com",
                     PhoneCode = "021",
                     Phone = "5668284",
                     Fax = "5668284",
                     LocationId = 1,
                     FullAddress = "Jl. Let. Jend. S. Parman, Kav.87, Slipi, Jakarta Barat",
                     Name = "RS Anak dan Bunda Harapan Kita",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 2,
                     Is_delete = false,
                     Email = "rskmjeck@gmail.com",
                     PhoneCode = "021",
                     Phone = "29221000",
                     Fax = "29221000",
                     LocationId = 2,
                     FullAddress = "Jl. Terusan Arjuna Utara 1, Jakarta Barat",
                     Name = "RS Khusus Mata Jakarta Eye Center Kedoya",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 3,
                     Is_delete = false,
                     Email = "rsceng@gmail.com",
                     PhoneCode = "021",
                     Phone = "54372874",
                     Fax = "54372874",
                     LocationId = 3,
                     FullAddress = "Jl. Bumi Cengkareng Indah No.1, Cengkareng Timur, Jakarta Barat",
                     Name = "RSUD Cengkareng",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 4,
                     Is_delete = false,
                     Email = "rscip@gmail.com",
                     PhoneCode = "021",
                     Phone = "54372874",
                     Fax = "54372874",
                     LocationId = 4,
                     FullAddress = "Jl. Satu Maret No.26, Pegadungan, Jakarta Barat",
                     Name = "RSUD Ciputra",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 5,
                     Is_delete = false,
                     Email = "rskan@gmail.com",
                     PhoneCode = "021",
                     Phone = "5681570",
                     Fax = "5681570",
                     LocationId = 5,
                     FullAddress = "Jl. S Parman Kav.84-86, Slipi, Jakarta Barat",
                     Name = "RS Kanker Dharmis",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 6,
                     Is_delete = false,
                     Email = "corporate.secretary@siloamhospitals.com",
                     PhoneCode = "021",
                     Phone = "25668000",
                     Fax = "5460075",
                     LocationId = 1,
                     FullAddress = "Gedung Fak. Kedokteran UPH Lt. 32, Jl. Boulevard Jend. Sudirman No. 15,",
                     Name = "Siloam Hospitals ",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 7,
                     Is_delete = false,
                     Email = "swnA@rwna.com",
                     PhoneCode = "021",
                     Phone = "25668000",
                     Fax = "5460075",
                     LocationId = 2,
                     FullAddress = "Kec. Torgamba labuhan batu",
                     Name = "RS Awal Bros Bagan Batu",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 8,
                     Is_delete = false,
                     Email = "resmitrakeluarga@mitra.com",
                     PhoneCode = "021",
                     Phone = "99000880",
                     Fax = "5460075",
                     LocationId = 2,
                     FullAddress = "Jl. Kenjeran 506 60113 Surabaya Jawa Timur",
                     Name = "Rs mitra keluarga kenjeran",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new MedicalFacility
                 {
                     Id = 9,
                     Is_delete = false,
                     Email = "corporate.secretary@siloamhospitals.com",
                     PhoneCode = "021",
                     Phone = "25668000",
                     Fax = "5460075",
                     LocationId = 5,
                     FullAddress = "Jl. Sultan Hasanuddin No. 58, Baubau",
                     Name = "Siloam Hospitals Buton",
                     Created_by = 1,
                     Created_on = DateTime.Now
                 }
                );
            modelBuilder.Entity<Location>()
               .HasData(
                new Location
                {
                    Id = 1,
                    Is_delete = false,
                    Name = "Slipi",
                    LocationLevelId = 5,
                    ParentId = 8,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 2,
                    Is_delete = false,
                    Name = "Kedoya",
                    LocationLevelId= 5,
                    ParentId = 9,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 3,
                    Is_delete = false,
                    LocationLevelId=5,
                    ParentId = 10,
                    Name = "Cengkareng Timur",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 4,
                    Is_delete = false,
                    Name = "Pegadungan",
                    LocationLevelId = 5,
                    ParentId = 11,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 6,
                    Is_delete = false,
                    Name = "DK Jakarta",
                    ParentId = null,
                    LocationLevelId = 1,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 7,
                    Is_delete = false,
                    Name = "Jakarta Barat",
                    ParentId = 6,
                    LocationLevelId = 2,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },

                new Location
                {
                    Id = 8,
                    Is_delete = false,
                    Name = "Palmerah",
                    ParentId = 7,
                    LocationLevelId = 4,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },

                new Location
                {
                    Id = 9,
                    Is_delete = false,
                    Name = "Kebon Jeruk",
                    ParentId = 7,
                    LocationLevelId = 4,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },

                new Location
                {
                    Id = 10,
                    Is_delete = false,
                    Name = "Cengkareng",
                    ParentId = 7,
                    LocationLevelId = 4,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },

                new Location
                {
                    Id = 11,
                    Is_delete = false,
                    Name = "Kalideres",
                    ParentId = 7,
                    LocationLevelId = 4,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },


                new Location
                {
                    Id = 13,
                    Is_delete = false,
                    Name = "Kokalukuna",
                    ParentId = 14,
                    LocationLevelId = 4,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },

                new Location
                {
                    Id = 5,
                    Is_delete = false,
                    Name = "Liwuto",
                    ParentId = 13,
                    LocationLevelId = 5,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 12,
                    Is_delete = false,
                    Name = "Sulawesi Tenggara",
                    ParentId = null,
                    LocationLevelId = 1,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new Location
                {
                    Id = 14,
                    Is_delete = false,
                    Name = "BauBau",
                    ParentId = 12,
                    LocationLevelId = 2,
                    Created_by = 1,
                    Created_on = DateTime.Now
                }
                );
           modelBuilder.Entity<DoctorTreatment>()
                .HasData(
                 new DoctorTreatment
                 {
                     Id = 1,
                     Is_delete = false,
                     Name = "Diagnosis penyakit dalam",
                     DoctorId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 2,
                     Is_delete = false,
                     Name = "Imunisasi",
                     DoctorId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 3,
                     Is_delete = false,
                     Name = "Angiografi",
                     DoctorId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 4,
                     Is_delete = false,
                     Name = "Persalinan",
                     DoctorId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 5,
                     Is_delete = false,
                     Name = "Pembedahan",
                     DoctorId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 6,
                     Is_delete = false,
                     Name = "Diagnosis penyakit dalam",
                     DoctorId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 7,
                     Is_delete = false,
                     Name = "Operasi Organ dalam",
                     DoctorId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 8,
                     Is_delete = false,
                     Name = "Konsultasi kesehatan anak",
                     DoctorId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 9,
                     Is_delete = false,
                     Name = "Konsultasi penyakit dalam",
                     DoctorId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 10,
                     Is_delete = false,
                     Name = "Imunisasi Balita",
                     DoctorId = 10,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 11,
                     Is_delete = false,
                     Name = "Persalinan",
                     DoctorId = 11,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 12,
                     Is_delete = false,
                     Name = "Pengobatan cacing pada anak",
                     DoctorId = 12,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 13,
                     Is_delete = false,
                     Name = "Operasi pengangkatan organ",
                     DoctorId = 13,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 14,
                     Is_delete = false,
                     Name = "Konsultasi penyakit ayan",
                     DoctorId = 14,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 15,
                     Is_delete = false,
                     Name = "Diagnosis penyakit lambung",
                     DoctorId = 15,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 16,
                     Is_delete = false,
                     Name = "Pengobatan demam anak",
                     DoctorId = 16,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 },
                 new DoctorTreatment
                 {
                     Id = 17,
                     Is_delete = false,
                     Name = "Konsultasi kesehatan janin",
                     DoctorId = 17,
                     Created_by = 1,
                     Created_on = DateTime.Now
                 }
                );
            modelBuilder.Entity<DoctorEducation>()
                .HasData(
                 new DoctorEducation
                 {
                     Id = 1,
                     Is_delete = false,
                     DoctorId = 2,
                     InstitutionName = "Universitas Indonesia",
                     Major = "Spesialis Anak",
                     EndYear = "2021",
                     StartYear = "2019",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 2,
                     Is_delete = false,
                     DoctorId = 3,
                     InstitutionName = "Universitas Indonesia",
                     Major = "Spesialis Saraf",
                     EndYear = "2018",
                     StartYear = "2014",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 3,
                     Is_delete = false,
                     DoctorId = 1,
                     InstitutionName = "Universitas Indonesia",
                     Major = "Spesialis Penyakit Dalam",
                     EndYear = "2020",
                     StartYear = "2018",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 4,
                     Is_delete = false,
                     DoctorId = 4,
                     InstitutionName = "Universitas Indonesia",
                     Major = "Spesialis kandungan dan ginekologi",
                     EducationLevelId = 2,
                     EndYear = "2020",
                     StartYear = "2017",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 5,
                     Is_delete = false,
                     DoctorId = 5,
                     InstitutionName = "Universitas Indonesia",
                     Major = "Spesialis Bedah",
                     EndYear = "2021",
                     StartYear = "2016",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 6,
                     Is_delete = false,
                     DoctorId = 6,
                     InstitutionName = "Universitas Udayana",
                     Major = "Spesialis Penyakit dalam",
                     EndYear = "2019",
                     StartYear = "2015",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 7,
                     Is_delete = false,
                     DoctorId = 7,
                     InstitutionName = "Universitas Gadjah Mada",
                     Major = "Spesialis Bedah",
                     EndYear = "2016",
                     StartYear = "2013",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 8,
                     Is_delete = false,
                     DoctorId = 8,
                     InstitutionName = "Universitas Mataram",
                     Major = "Spesialis Anak",
                     EndYear = "2011",
                     StartYear = "2006",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 9,
                     Is_delete = false,
                     DoctorId = 9,
                     InstitutionName = "Universitas Andalas",
                     Major = "Spesialis Penyakit Dalam",
                     EndYear = "2022",
                     StartYear = "2019",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 10,
                     Is_delete = false,
                     DoctorId = 10,
                     InstitutionName = "Universitas Brawijaya",
                     Major = "Spesialis Anak",
                     EndYear = "2019",
                     StartYear = "2016",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 11,
                     Is_delete = false,
                     DoctorId = 11,
                     InstitutionName = "Universitas Syiah Kuala",
                     Major = "Spesialis kandungan dan ginekologi",
                     EducationLevelId = 2,
                     EndYear = "2014",
                     StartYear = "2010",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 12,
                     Is_delete = false,
                     DoctorId = 12,
                     InstitutionName = "Universitas Negeri Lampung",
                     Major = "Spesialis Anak",
                     EndYear = "2022",
                     StartYear = "2018",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 13,
                     Is_delete = false,
                     DoctorId = 13,
                     InstitutionName = "Universitas Hasanuddin",
                     Major = "Spesialis Bedah",
                     EndYear = "2021",
                     StartYear = "2018",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 14,
                     Is_delete = false,
                     DoctorId = 14,
                     InstitutionName = "Universitas Padjajaran",
                     Major = "Spesialis Saraf",
                     EndYear = "2019",
                     StartYear = "2016",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 15,
                     Is_delete = false,
                     DoctorId = 15,
                     InstitutionName = "Universitas Negeri Jakarta",
                     Major = "Spesialis Penyakit Dalam",
                     EndYear = "2020",
                     StartYear = "2016",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 16,
                     Is_delete = false,
                     DoctorId = 16,
                     InstitutionName = "Universitas Andalas",
                     Major = "Spesialis Anak",
                     EndYear = "2022",
                     StartYear = "2019",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 },
                 new DoctorEducation
                 {
                     Id = 17,
                     Is_delete = false,
                     DoctorId = 17,
                     InstitutionName = "Universitas Andalas",
                     Major = "Spesialis Kandungan",
                     EndYear = "2022",
                     StartYear = "2019",
                     EducationLevelId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                 }
                );
            modelBuilder.Entity<EducationLevel>()
               .HasData(
                new EducationLevel
                {
                    Id = 1,
                    Is_delete = false,
                    Name = "Strata 1",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new EducationLevel
                {
                    Id = 2,
                    Is_delete = false,
                    Name = "Strata 2",
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new EducationLevel
                {
                    Id = 3,
                    Is_delete = false,
                    Name = "Strata 3",
                    Created_by = 1,
                    Created_on = DateTime.Now
                }
                );
            modelBuilder.Entity<Role>()
                .HasData(
                new Role
                {
                    Id = 1,
                    Name = "Admin",
                    Code = "ADMIN"
                },
                new Role
                {
                    Id = 2,
                    Name = "Dokter",
                    Code = "DOKTER"
                },
                new Role
                {
                    Id = 3,
                    Name = "Pasien",
                    Code = "PASIEN"
                }
                );
            modelBuilder.Entity<Menu>()
                .HasData(
                new Menu
                {
                    Id = 1,
                    Name = "Master",
                },
                new Menu
                {
                    Id = 2,
                    Name = "Tranasaksi",
                },
                new Menu
                {
                    Id = 3,
                    Name = "ProfileDokter",
                    Url = "http://localhost:3000/ProfilDoctor",
                    ParentId = 1,
                },
                new Menu
                {
                    Id = 4,
                    Name = "Location",
                    Url = "http://localhost:3000/Location",
                    ParentId = 1,
                },
                 new Menu
                 {
                     Id = 5,
                     Name = "locationlevel",
                     Url = "http://localhost:3000/locationlevel",
                     ParentId = 1,
                 },
                new Menu
                {
                    Id = 6,
                    Name = "DaftarPasien",
                    Url = "http://localhost:3000/Pasien",
                    ParentId = 2,
                },
                new Menu
                {
                    Id = 7,
                    Name = "DaftarRelation",
                    Url = "http://localhost:3000/Relation",
                    ParentId = 1,
                },
                new Menu
                {
                    Id = 8,
                    Name = "DaftarBloodGroup",
                    Url = "http://localhost:3000/BloodGroup",
                    ParentId = 1,
                }
                );
            modelBuilder.Entity<User>()
                .HasData(
                new User
                {
                    Id = 1,
                    BiodataId = 18,
                    RoleId = 1,
                    Email = "gilang@gmail.com",
                    Password = "ac9689e2272427085e35b9d3e3e8bed88cb3434828b43b86fc0596cad4c6e270",
                    LoginAttempt = 0,
                    IsLocked = false,
                    LastLogin = DateTime.Now
                },
                new User
                {
                Id = 2,
                    BiodataId = 17,
                    RoleId = 2,
                    Email = "andreas@gmail.com",
                    Password = "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd",
                    LoginAttempt = 0,
                    IsLocked = false,
                    LastLogin = DateTime.Now
                },
                new User
                {
                    Id = 3,
                    BiodataId = 19,
                    RoleId = 3,
                    Email = "gio@gmail.com",
                    Password = "d84c1585455fe00e6149765bbdfc7d2f8cded19b5ec17bdc34b4046fa321a622",
                    LoginAttempt = 0,
                    IsLocked = false,
                    LastLogin = DateTime.Now
                }, 
                new User
                {
                    Id = 4,
                    BiodataId = 1,
                    RoleId = 2,
                    Email = "christie@gmail.com",
                    Password = "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd",
                    LoginAttempt = 0,
                    IsLocked = false,
                    LastLogin = DateTime.Now
                },
                new User
                {
                    Id = 5,
                    BiodataId = 2,
                    RoleId = 2,
                    Email = "hendra@gmail.com",
                    Password = "88f77951ecd25ee7b7812bd11ebc8c0cafc95d6d6115f132c6eb61e5c0c1c9fd",
                    LoginAttempt = 0,
                    IsLocked = false,
                    LastLogin = DateTime.Now
                }
                );

            modelBuilder.Entity<MenuRole>()
               .HasData(
               new MenuRole
               {
                   Id = 1,
                   MenuId = 4,
                   RoleId = 1
               },
               new MenuRole
               {
                   Id = 2,
                   MenuId = 5,
                   RoleId = 1
               },
               new MenuRole
               {
                   Id = 3,
                   MenuId = 7,
                   RoleId = 1
               },
               new MenuRole
               {
                   Id = 4,
                   MenuId = 8,
                   RoleId = 1
               },
               new MenuRole
               {
                   Id = 5,
                   MenuId = 3,
                   RoleId = 2
               },
               new MenuRole
               {
                   Id = 6,
                   MenuId = 6,
                   RoleId = 3
               }
               );
            modelBuilder.Entity<LocationLevel>()
                .HasData(
                 new LocationLevel
                 {
                     Id = 1,
                     Name = "Provinsi",
                     Abbreviation = "Prov.",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new LocationLevel
                 {
                     Id = 2,
                     Name = "Kota",
                     Abbreviation = "Kota",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new LocationLevel
                 {
                     Id = 3,
                     Name = "Kabupaten",
                     Abbreviation = "Kab.",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new LocationLevel
                 {
                     Id = 4,
                     Name = "Kecamatan",
                     Abbreviation = "Kec.",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new LocationLevel
                 {
                     Id = 5,
                     Name = "Kelurahan",
                     Abbreviation = "Kel.",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new LocationLevel
                 {
                     Id = 6,
                     Name = "Desa",
                     Abbreviation = "Desa",
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }
                );

            modelBuilder.Entity<Appointment>()
                .HasData(
                //janji dngn doctorId = 1 di doctor office 1
                 new Appointment
                 {
                     Id = 1,
                     AppointmentDate = DateTime.Parse("2023-12-25"),
                     CustomerId = 1,
                     DoctorOfficeId = 1,
                     DoctorOfficeScheduleId = 1,
                     DoctorOfficeTreatmentId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new Appointment
                 {
                     Id = 2,
                     AppointmentDate = DateTime.Parse("2023-12-26"),
                     CustomerId = 2,
                     DoctorOfficeId = 1,
                     DoctorOfficeScheduleId = 1,
                     DoctorOfficeTreatmentId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }
                 );
            modelBuilder.Entity<DoctorOfficeSchedule>()
                .HasData(
                //jadwal dokterId 1 di faskes 1
                 new DoctorOfficeSchedule
                 {
                     Id = 1,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 1, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 2,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 2,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 3,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 3,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 4,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 4,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 5,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 5,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 6,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 6,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 7,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 7,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 2 di faskes 2
                 new DoctorOfficeSchedule
                 {
                     Id = 8,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 8, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 9,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 9,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 10,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 10,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 11,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 11,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 12,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 12,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 13,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 13,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 14,
                     DoctorId = 2,
                     MedicalFacilityScheduleId = 14,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 3 di faskes 3
                 new DoctorOfficeSchedule
                 {
                     Id = 15,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 15, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 16,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 16,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 17,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 17,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 18,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 18,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 19,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 19,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 20,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 20,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 21,
                     DoctorId = 3,
                     MedicalFacilityScheduleId = 21,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 4 di faskes 4
                 new DoctorOfficeSchedule
                 {
                     Id = 22,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 22, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 23,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 23,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 24,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 24,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 25,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 25,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 26,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 26,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 27,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 27,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 28,
                     DoctorId = 4,
                     MedicalFacilityScheduleId = 28,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 5 di faskes 5
                 new DoctorOfficeSchedule
                 {
                     Id = 29,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 29, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 30,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 30,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 31,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 31,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 32,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 32,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 33,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 33,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 34,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 34,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 35,
                     DoctorId = 5,
                     MedicalFacilityScheduleId = 35,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 6 di faskes 6
                 new DoctorOfficeSchedule
                 {
                     Id = 36,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 36, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 37,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 37,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 38,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 38,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 39,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 39,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 40,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 40,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 41,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 41,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 42,
                     DoctorId = 6,
                     MedicalFacilityScheduleId = 42,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 7 di faskes 7
                 new DoctorOfficeSchedule
                 {
                     Id = 43,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 43, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 44,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 44,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 45,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 45,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 46,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 46,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 47,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 47,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 48,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 48,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 49,
                     DoctorId = 7,
                     MedicalFacilityScheduleId = 49,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 8 di faskes 8
                 new DoctorOfficeSchedule
                 {
                     Id = 50,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 50, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 51,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 51,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 52,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 52,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 53,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 53,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 54,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 54,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 55,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 55,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 56,
                     DoctorId = 8,
                     MedicalFacilityScheduleId = 56,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 9 di faskes 9
                 new DoctorOfficeSchedule
                 {
                     Id = 57,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 57, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 58,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 58,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 59,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 59,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 60,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 60,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 61,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 61,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 62,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 62,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 63,
                     DoctorId = 9,
                     MedicalFacilityScheduleId = 63,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 10 di faskes 9
                 new DoctorOfficeSchedule
                 {
                     Id = 64,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 57, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 65,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 58,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 66,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 59,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 67,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 60,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 68,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 61,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 69,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 62,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 70,
                     DoctorId = 10,
                     MedicalFacilityScheduleId = 63,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 11 di faskes 2
                 new DoctorOfficeSchedule
                 {
                     Id = 71,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 8, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 72,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 9,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 73,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 10,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 74,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 11,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 75,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 12,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 76,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 13,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 77,
                     DoctorId = 11,
                     MedicalFacilityScheduleId = 14,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 12 di faskes 7
                 new DoctorOfficeSchedule
                 {
                     Id = 78,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 43, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 79,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 44,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 80,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 45,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 81,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 46,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 82,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 47,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 83,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 48,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 84,
                     DoctorId = 12,
                     MedicalFacilityScheduleId = 49,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 13 di faskes 6
                 new DoctorOfficeSchedule
                 {
                     Id = 85,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 36, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 86,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 37,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 87,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 38,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 88,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 39,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 89,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 40,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 90,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 41,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 91,
                     DoctorId = 13,
                     MedicalFacilityScheduleId = 42,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 14 di faskes 5
                 new DoctorOfficeSchedule
                 {
                     Id = 92,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 29, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 93,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 30,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 94,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 31,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 95,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 32,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 96,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 33,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 97,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 34,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 98,
                     DoctorId = 14,
                     MedicalFacilityScheduleId = 35,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 15 di faskes 3
                 new DoctorOfficeSchedule
                 {
                     Id = 99,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 15, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 100,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 16,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 101,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 17,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 102,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 18,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 103,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 19,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 104,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 20,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 105,
                     DoctorId = 15,
                     MedicalFacilityScheduleId = 21,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 16 di faskes 8
                 new DoctorOfficeSchedule
                 {
                     Id = 106,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 50, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 107,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 51,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 108,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 52,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 109,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 53,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 110,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 54,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 111,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 55,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 112,
                     DoctorId = 16,
                     MedicalFacilityScheduleId = 56,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal dokterId 17 di faskes 8
                 new DoctorOfficeSchedule
                 {
                     Id = 113,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 50, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 114,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 51,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 115,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 52,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 116,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 53,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 117,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 54,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 118,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 55,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 119,
                     DoctorId = 17,
                     MedicalFacilityScheduleId = 56,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },

                 //jadwal dokterId 1 di faskes 2
                 new DoctorOfficeSchedule
                 {
                     Id = 120,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 8, //senin
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 121,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 9,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 122,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 10,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 123,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 11,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 124,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 12,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 125,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 13,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeSchedule
                 {
                     Id = 126,
                     DoctorId = 1,
                     MedicalFacilityScheduleId = 14,
                     Slot = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }
                 );
            modelBuilder.Entity<MedicalFacilitySchedule>()
                .HasData(
                //jadwal faskes 1
                 new MedicalFacilitySchedule
                 {
                     Id = 1,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 2,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 3,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 4,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 5,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "11.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 6,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 7,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 2
                 new MedicalFacilitySchedule
                 {
                     Id = 8,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 9,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 10,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 11,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 12,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 13,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 14,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 3
                 new MedicalFacilitySchedule
                 {
                     Id = 15,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 16,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 17,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 18,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 19,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 20,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 21,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 4
                 new MedicalFacilitySchedule
                 {
                     Id = 22,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 23,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 24,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "13.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 25,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 26,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 27,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 28,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 5
                 new MedicalFacilitySchedule
                 {
                     Id = 29,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 30,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 31,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 32,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 33,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 34,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 35,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 6
                 new MedicalFacilitySchedule
                 {
                     Id = 36,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 37,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 38,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 39,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 40,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 41,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 42,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 7
                 new MedicalFacilitySchedule
                 {
                     Id = 43,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 44,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "15.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 45,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 46,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 47,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 48,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 49,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 8
                 new MedicalFacilitySchedule
                 {
                     Id = 50,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 51,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 52,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 53,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 54,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 55,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }, 
                 new MedicalFacilitySchedule
                 {
                     Id = 56,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 8,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 //jadwal faskes 9
                 new MedicalFacilitySchedule
                 {
                     Id = 57,
                     Day = "Senin",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 58,
                     Day = "Selasa",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 59,
                     Day = "Rabu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 60,
                     Day = "Kamis",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 61,
                     Day = "Jum'at",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 62,
                     Day = "Sabtu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "19.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new MedicalFacilitySchedule
                 {
                     Id = 63,
                     Day = "Minggu",
                     TimeScheduleStart = "08.00",
                     TimeScheduleEnd = "10.30",
                     MedicalFacilityId = 9,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }
                 );
            modelBuilder.Entity<DoctorOfficeTreatment>()
                .HasData(
                 // Treatment doctor office 1
                 new DoctorOfficeTreatment
                 {
                     Id = 1,
                     DoctorTreatmentId = 1,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 2,
                     DoctorTreatmentId = 2,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 3,
                     DoctorTreatmentId = 3,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 4,
                     DoctorTreatmentId = 4,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 5,
                     DoctorTreatmentId = 5,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 6,
                     DoctorTreatmentId = 6,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 7,
                     DoctorTreatmentId = 7,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 // Treatment doctor office 2
                 new DoctorOfficeTreatment
                 {
                     Id = 8,
                     DoctorTreatmentId = 1,
                     DoctorOfficeId = 2,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 9,
                     DoctorTreatmentId = 2,
                     DoctorOfficeId = 3,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 10,
                     DoctorTreatmentId = 3,
                     DoctorOfficeId = 4,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 11,
                     DoctorTreatmentId = 4,
                     DoctorOfficeId = 5,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 12,
                     DoctorTreatmentId = 5,
                     DoctorOfficeId = 6,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 13,
                     DoctorTreatmentId = 6,
                     DoctorOfficeId = 7,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 },
                 new DoctorOfficeTreatment
                 {
                     Id = 14,
                     DoctorTreatmentId = 7,
                     DoctorOfficeId = 1,
                     Created_by = 1,
                     Created_on = DateTime.Now,
                     Is_delete = false
                 }
                 );
            modelBuilder.Entity<Customer>()
                .HasData(
                //customer untuk janji doctor 1
                new Customer
                {
                    Id = 1,
                    BiodataId = 18,
                    Dob = DateTime.Parse("1997-12-12"),
                    Gender = "M",
                    BloodGroupId = 1,
                    RhesusType = "RH+",
                    Height = 170,
                    Weight = 64,
                    Created_by = 1,
                    Created_on = DateTime.Now,
                }, 
                new Customer
                {
                    Id = 2,
                    BiodataId = 19,
                    Dob = DateTime.Parse("1999-12-12"),
                    Gender = "M",
                    BloodGroupId = 1,
                    RhesusType = "RH+",
                    Height = 170,
                    Weight = 64,
                    Created_by = 1,
                    Created_on = DateTime.Now,
                }
                );
            modelBuilder.Entity<BloodGroup>()
                .HasData(
                new BloodGroup
                {
                    Id = 1,
                    Code = "O",
                    Description = "Goldar",
                    Created_by = 1,
                    Created_on = DateTime.Now,
                }
                );

            modelBuilder.Entity<CustomerChat>()
                .HasData(
                //konsultasi chat doctorId 1
                new CustomerChat
                {
                    Id = 1,
                    CustomerId = 1,
                    DoctorId = 1,
                    Created_by = 1,
                    Created_on = DateTime.Now
                },
                new CustomerChat
                {
                    Id = 2,
                    CustomerId = 2,
                    DoctorId = 1,
                    Created_by = 1,
                    Created_on = DateTime.Now
                }
                );
        }
    }
}
