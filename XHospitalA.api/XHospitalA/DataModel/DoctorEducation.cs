﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_doctor_education")]
    public class DoctorEducation: BaseProperties
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? EducationLevelId { get; set; }

        [MaxLength(100)]
        public string? InstitutionName { get; set; }

        [MaxLength(100)]
        public string? Major { get; set; }

        [MaxLength(4)]
        public string? StartYear { get; set; }

        [MaxLength(4)]
        public string? EndYear { get; set; }
        public bool IsLastEducation { get; set; }


        [ForeignKey("DoctorId")]
        public virtual Doctor Doctor { get; set; }

        [ForeignKey("EducationLevelId")]
        public virtual EducationLevel EducationLevel { get; set; }
    }
}
