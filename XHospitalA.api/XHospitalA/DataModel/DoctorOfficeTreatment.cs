﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_doctor_office_treatment")]
    public class DoctorOfficeTreatment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorTreatmentId { get; set; }
        public long? DoctorOfficeId { get; set; }

        [ForeignKey("DoctorTreatmentId")]
        public virtual DoctorTreatment DoctorTreatment { get; set; }

        [ForeignKey("DoctorOfficeId")]
        public virtual DoctorOffice DoctorOffice{ get; set; }
    }
}
