﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Numerics;

namespace XHospitalA.DataModel
{
    [Table("t_doctor_office")]
    public class DoctorOffice : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? DoctorId { get; set; }
        public long? MedicalFacilityId { get; set; }

        [Required, MaxLength(100)]
        public string Specialization { get; set; }
        [Required]
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [ForeignKey("DoctorId")]
        public virtual Doctor Doctor{ get; set; }

        [ForeignKey("MedicalFacilityId")]
        public virtual MedicalFacility MedicalFacility { get; set; }
    }
}
