﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_medical_facility_schedule")]
    public class MedicalFacilitySchedule : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? MedicalFacilityId { get; set; }

        [Required, MaxLength(10)]
        public string? Day { get; set; }

        [Required, MaxLength(10)]
        public string? TimeScheduleStart { get; set; }

        [Required, MaxLength(10)]
        public string? TimeScheduleEnd{ get; set; }

        [ForeignKey("MedicalFacilityId")]
        public virtual MedicalFacility MedicalFacility { get; set; }
    }
}
