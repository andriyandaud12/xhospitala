﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("m_biodata_address")]
    public class BiodataAddress : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long? BiodataId { get; set; }

        [MaxLength(100)]
        public string? Label { get; set; }

        public string? Recipent { get; set; }

        [MaxLength(15)]
        public string? RecipentPhoneNumber { get; set; }

        public long?  LocationId { get; set; }

        [MaxLength(10)]
        public string? PostalCode { get; set; }

        public string? Address { get; set; }

        [ForeignKey("BiodataId")]
        public virtual Biodata Biodata { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
    }
}
