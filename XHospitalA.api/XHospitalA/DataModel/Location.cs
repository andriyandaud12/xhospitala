﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XHospitalA.DataModel
{
    [Table("m_location")]
    public class Location : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(100)]
        public string? Name { get; set; }

        public long? ParentId { get; set; }

        public long? LocationLevelId { get; set; }

        [ForeignKey("ParentId")]
        public virtual Location Locations { get; set; }

        [ForeignKey("LocationLevelId")]
        public virtual LocationLevel LocationLevel { get; set; }
    }
}
