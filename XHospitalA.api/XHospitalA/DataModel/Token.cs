﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHospitalA.DataModel
{
    [Table("t_token")]
    public class Token : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(100)]
        public string? Email { get; set; }

        public long? User_Id { get; set; }

        public string? token { get; set; }

        public DateTime? Expired_On { get; set; }

        public bool? Is_Expired { get; set; }
        public string? Used_For { get; set; }


        [ForeignKey("User_Id")]
        public virtual User User { get; set; }
    }
}
