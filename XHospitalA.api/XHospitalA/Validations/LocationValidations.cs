﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class LocationValidator : AbstractValidator<LocationViewModel>
    {
        public LocationValidator(XHospitalDbContext dbContext)
        {
            RuleFor(x => new { x.Name, x.ParentId })
                .Must(x =>
                {
                    if ((x.ParentId != null ? x.ParentId: 0) != 0)
                        return !dbContext.Locations.Where(o => o.Name == x.Name && o.ParentId == x.ParentId).Any();
                    else
                        return !dbContext.Locations.Where(o => o.Name == x.Name && o.Is_delete == false).Any();
                })
                .WithName("Name")
                .WithMessage("Lokasi dengan parent yang sama sudah ada!");

            RuleFor(x => new { x.Id })
                .Must(x =>
                {
                    if ((x.Id != null ? x.Id : 0) != 0)
                        return !dbContext.Locations.Where(o => o.ParentId == x.Id).Any();
                    else
                        return !dbContext.Locations.Where(o => o.ParentId == x.Id && o.Is_delete == false).Any();
                })
                .WithName("Location")
                .WithMessage("Lokasi masih digunakan!");

        }
    }
}
