﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class TokenValidator:AbstractValidator<TokenViewModel>
    {
        private readonly XHospitalDbContext _dbContext;
        public TokenValidator(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;

            RuleFor(x => x.Email)
               .EmailAddress().WithMessage("Bukan format email, contoh = contoh@gmail.com ")
               .Must(email =>
               {
                   // Memeriksa apakah ada pengguna lain dengan email yang sama
                   return !_dbContext.Users.Any(o => o.Email == email);
               })
               .WithName("email")
               .WithMessage((x, combined) =>
               {
                   // You can customize the error message here based on the input data
                   return $" {x.Email} Sudah Terdaftar";
               });
        } 
    }
}
