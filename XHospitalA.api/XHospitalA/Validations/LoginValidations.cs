﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class LoginValidations : AbstractValidator<LoginViewModel>
    {
        public LoginValidations(XHospitalDbContext dbContext)
        {
            RuleFor(x => x.Email)
                .Must(email =>
                {
                    return dbContext.Users.Where(o => o.Email == email).Any();
                })
                .WithName("Email")
                .WithMessage("Email Not Exist!!");

            RuleFor(x => x.Email).NotNull().Length(1, 50).WithMessage("Length 1 - 50");
        }
    }
}
