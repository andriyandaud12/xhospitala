﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class CustomerRelationValidator : AbstractValidator<CustomerRelationViewModel>
    {
        public CustomerRelationValidator(XHospitalDbContext dbContext)
        {
            RuleFor(x => new { x.Name, x.Id })
                .Must(x =>
                {
                    if ((x.Id != null ? x.Id : 0) != 0)
                        return !dbContext.CustomerRelations.Where(o => o.Name == x.Name && o.Id != x.Id && o.Is_delete == false).Any();
                    else
                        return !dbContext.CustomerRelations.Where(o => o.Name == x.Name && o.Is_delete == false).Any();
                })
                .WithName("Name")
                .WithMessage("Nama Relasi already exists!");
            RuleFor(x => x.Name).NotNull().Length(1, 50).WithMessage("Length 1 - 50");
        }
    }
}
