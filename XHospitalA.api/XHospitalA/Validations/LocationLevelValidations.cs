﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class LocationLevelValidator : AbstractValidator<LocationLevelViewModel>
    {
        public LocationLevelValidator(XHospitalDbContext dbContext)
        {
            RuleFor(x => new { x.Name, x.Id })
                .Must(x =>
                {
                    if ((x.Id != null ? x.Id : 0) != 0)
                        return !dbContext.LocationLevels.Where(o => o.Name == x.Name && o.Id != x.Id).Any();
                    else
                        return !dbContext.LocationLevels.Where(o => o.Name == x.Name && o.Is_delete == false).Any();
                })
                .WithName("Name")
                .WithMessage("Nama Level Lokasi sudah ada!");

            RuleFor(x => new { x.Id })
                .Must(x =>
                {
                    if ((x.Id != null ? x.Id : 0) != 0)
                        return !dbContext.Locations.Where(o => o.LocationLevelId == x.Id).Any();
                    else
                        return !dbContext.Locations.Where(o => o.LocationLevelId == x.Id && o.Is_delete == false).Any();
                })
                .WithName("LocationLevel")
                .WithMessage("Level lokasi masih digunakan!");

        }
    }
}
