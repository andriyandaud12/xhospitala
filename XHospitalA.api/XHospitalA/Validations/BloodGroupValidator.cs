﻿using FluentValidation;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Validations
{
    public class BloodGroupValidator: AbstractValidator<BloodGroupViewModel>
    {
        public BloodGroupValidator(XHospitalDbContext dbContext)
        {
            RuleFor(x => new { x.Code, x.Id })
                 .Must(x =>
                 {
                     if ((x.Id != null ? x.Id : 0) != 0)
                         return !dbContext.BloodGroups.Where(o => o.Code == x.Code && o.Id != x.Id && o.Is_delete == false).Any();
                     else
                         return !dbContext.BloodGroups.Where(o => o.Code == x.Code && o.Is_delete == false).Any();
                 })
                 .WithName("Code")
                 .WithMessage("Golongan Darah already exists!");
            RuleFor(x => x.Code).NotNull().Length(1, 50).WithMessage("Length 1 - 50");
        }
    }
}
