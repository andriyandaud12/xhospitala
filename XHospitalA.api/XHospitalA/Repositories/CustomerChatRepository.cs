﻿using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class CustomerChatRepository
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public CustomerChatRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<GetCustomerChatViewModel> GetListChatByDoctorId(long doctorId)
        {
            List<GetCustomerChatViewModel> model = new List<GetCustomerChatViewModel>();

            model = (from o in _dbContext.CustomerChats
                                   where o.DoctorId == doctorId
                                   select new GetCustomerChatViewModel
                                   {
                                       CustomerId = o.CustomerId,
                                       Id = o.Id,
                                       DoctorId = o.DoctorId,
                                   }
                                   ).ToList();

            return model;
        }
    }
}
