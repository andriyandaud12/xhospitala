﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class DoctorOfficeScheduleRepository : IRepository<DoctorOfficeScheduleViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public DoctorOfficeScheduleRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public DoctorOfficeScheduleViewModel ChangeStatus(long id, bool status)
        {
            DoctorOfficeScheduleViewModel result = new DoctorOfficeScheduleViewModel();
            try
            {
                DoctorOfficeSchedule entity = _dbContext.DoctorOfficeSchedules
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new DoctorOfficeScheduleViewModel()
                    {
                        Is_delete = status,
                        Id = entity.Id,
                        DoctorId = entity.DoctorId,
                        MedicalFacilityScheduleId= entity.MedicalFacilityScheduleId,
                        Slot = entity.Slot,                        
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public DoctorOfficeScheduleViewModel Create(DoctorOfficeScheduleViewModel model)
        {
            try
            {
                DoctorOfficeSchedule entity = new DoctorOfficeSchedule();
                entity.Is_delete = model.Is_delete;
                entity.Slot = model.Slot;
                entity.MedicalFacilityScheduleId = model.MedicalFacilityScheduleId;
                entity.DoctorId = model.DoctorId;
                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;
                _dbContext.DoctorOfficeSchedules.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GetDoctorOfficeScheduleViewModel> GetAll()
        {
            List<GetDoctorOfficeScheduleViewModel> result = new List<GetDoctorOfficeScheduleViewModel>();
            try
            {
                result = _dbContext.DoctorOfficeSchedules
                    .Where(o => o.Is_delete == false)
                    .Select(o => new GetDoctorOfficeScheduleViewModel
                    {
                        Is_delete = o.Is_delete,
                        Id = o.Id,
                        Slot = o.Slot,
                        DoctorId = o.DoctorId,
                        MedicalFacilityScheduleId = o.MedicalFacilityScheduleId,
                        Doctor = new DoctorWFViewModel
                        {
                            Id = o.Doctor.Id,
                            BiodataId = o.Doctor.BiodataId,
                            Is_delete = o.Doctor.Is_delete,
                            Str = o.Doctor.Str,
                            Biodata = new BiodataViewModel
                            {
                                FullName = o.Doctor.Biodata.FullName
                            }
                        },
                        MedicalFacilitySchedule = new GetMedicalFacilityScheduleViewModel
                        {
                            Day = o.MedicalFacilitySchedule.Day,
                            MedicalFacility = new GetMedicalFacilityViewModel
                            {
                                Name = o.MedicalFacilitySchedule.MedicalFacility.Name
                            }
                        }
                    })
                    .ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GetDoctorOfficeScheduleViewModel GetById(long id)
        {
            GetDoctorOfficeScheduleViewModel model = new GetDoctorOfficeScheduleViewModel();
            try
            {
                model = (from o in _dbContext.DoctorOfficeSchedules
                         where o.Id == id
                         select new GetDoctorOfficeScheduleViewModel
                         {
                             Is_delete = o.Is_delete,
                             Id = o.Id,
                             Slot = o.Slot,
                             DoctorId = o.DoctorId,
                             MedicalFacilityScheduleId = o.MedicalFacilityScheduleId,
                             Doctor = new DoctorWFViewModel
                             {
                                 Id = o.Doctor.Id,
                                 BiodataId = o.Doctor.BiodataId,
                                 Is_delete = o.Doctor.Is_delete,
                                 Str = o.Doctor.Str,
                                 Biodata = new BiodataViewModel
                                 {
                                     FullName = o.Doctor.Biodata.FullName
                                 }
                             },
                             MedicalFacilitySchedule = new GetMedicalFacilityScheduleViewModel
                             {
                                 Day = o.MedicalFacilitySchedule.Day,
                                 MedicalFacility = new GetMedicalFacilityViewModel
                                 {
                                     Name = o.MedicalFacilitySchedule.MedicalFacility.Name
                                 }
                             }
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GetDoctorOfficeScheduleViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<DoctorOfficeScheduleViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public DoctorOfficeScheduleViewModel Update(DoctorOfficeScheduleViewModel model)
        {
            try
            {
                DoctorOfficeSchedule entity = _dbContext.DoctorOfficeSchedules
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Slot = model.Slot;
                    entity.DoctorId = model.DoctorId;
                    entity.MedicalFacilityScheduleId = model.MedicalFacilityScheduleId;
                    entity.Is_delete = model.Is_delete;
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }

        List<DoctorOfficeScheduleViewModel> IRepository<DoctorOfficeScheduleViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        DoctorOfficeScheduleViewModel IRepository<DoctorOfficeScheduleViewModel>.GetById(long id)
        {
            throw new NotImplementedException();
        }
    }
}
