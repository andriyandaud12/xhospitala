﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class DoctorOfficeRepository : IRepository<DoctorOfficeViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public DoctorOfficeRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public DoctorOfficeViewModel ChangeStatus(long id, bool status)
        {
            DoctorOfficeViewModel result = new DoctorOfficeViewModel();
            try
            {
                DoctorOffice entity = _dbContext.DoctorOffices
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new DoctorOfficeViewModel()
                    {
                        Is_delete = status,
                        DoctorId = entity.DoctorId,
                        StartDate = entity.StartDate, 
                        EndDate = entity.EndDate,
                        Id = entity.Id,
                        MedicalFacilityId = entity.MedicalFacilityId,
                        Specialization = entity.Specialization,
                    };
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public DoctorOfficeViewModel Create(DoctorOfficeViewModel model)
        {
            try
            {
                DoctorOffice entity = new DoctorOffice();
                entity.Is_delete = model.Is_delete;
                entity.StartDate = model.StartDate;
                entity.EndDate = model.EndDate;
                entity.DoctorId = model.DoctorId;
                entity.MedicalFacilityId = model.MedicalFacilityId;
                entity.Specialization = model.Specialization;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.DoctorOffices.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<DoctorOfficeViewModel> GetAll()
        {
            List<DoctorOfficeViewModel> result = new List<DoctorOfficeViewModel>();
            try
            {
                result = (from o in _dbContext.DoctorOffices
                          select new DoctorOfficeViewModel
                          {
                              Is_delete = o.Is_delete,
                              DoctorId = o.DoctorId,
                              EndDate = o.EndDate,
                              MedicalFacilityId = o.MedicalFacilityId,
                              Id = o.Id,
                              Specialization = o.Specialization,
                              StartDate = o.StartDate,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public DoctorOfficeViewModel GetById(long id)
        {
            DoctorOfficeViewModel model = new DoctorOfficeViewModel();
            try
            {
                model = (from o in _dbContext.DoctorOffices
                         where o.Id == id
                         select new DoctorOfficeViewModel
                         {
                             Is_delete = o.Is_delete,
                             DoctorId = o.DoctorId,
                             EndDate = o.EndDate,
                             MedicalFacilityId = o.MedicalFacilityId,
                             Id = o.Id,
                             Specialization = o.Specialization,
                             StartDate = o.StartDate,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new DoctorOfficeViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<DoctorOfficeViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public DoctorOfficeViewModel Update(DoctorOfficeViewModel model)
        {
            try
            {
                DoctorOffice entity = _dbContext.DoctorOffices
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Id = model.Id;
                    entity.Is_delete = model.Is_delete;
                    entity.DoctorId = model.DoctorId;
                    entity.EndDate = model.EndDate;
                    entity.MedicalFacilityId = model.MedicalFacilityId;
                    entity.StartDate = model.StartDate;
                    entity.Specialization = model.Specialization;
                    

                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
