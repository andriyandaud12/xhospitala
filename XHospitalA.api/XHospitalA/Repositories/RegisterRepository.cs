﻿using System.Net.Mail;
using System.Net;
using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class RegisterRepository
    {
        private XHospitalDbContext _dbContext;
        private IConfiguration _configuration;
        public RegisterRepository(XHospitalDbContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }
        public TokenViewModel CreateOrUpdate(TokenViewModel model)
        {
            try
            {
                Token entity = _dbContext.Tokens.FirstOrDefault(o => o.Email == model.Email);

                if (entity == null)
                {
                    // Email doesn't exist, create a new record

                    Random generator = new Random();
                    String otp = generator.Next(0, 1000000).ToString("D6");
                    entity = new Token();
                    {

                        entity.Id = model.Id;
                        entity.Email = model.Email;

                        entity.token = otp;
                        entity.Expired_On = (DateTime.Now).AddMinutes(10);



                        var smtpClient = new SmtpClient("smtp.gmail.com")
                        {
                            Port = 587,
                            Credentials = new NetworkCredential(_configuration["OTP:UserName"], _configuration["OTP:Password"]),
                            EnableSsl = true,
                        };

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress("winkawinoy@gmail.com"),
                            Subject = "OTP",
                            Body = $"<h1>Hello, your OTP: {otp}</h1>",
                            IsBodyHtml = true,
                        };

                        mailMessage.To.Add(entity.Email);

                        smtpClient.Send(mailMessage);

                        _dbContext.SaveChanges();


                        //entity.password = model.password;
                        //entity.Otp = model.otp;
                        //entity.biodata_id = bio.id;
                        //entity.role_id = model.role_id;
                        entity.Created_by = 001;
                        entity.Created_on = DateTime.Now;
                    };
                    _dbContext.Tokens.Add(entity);
                    _dbContext.SaveChanges();

                    model.Id = entity.Id;

                    return model;
                }
                else
                {
                    // Email already exists, update the existing record

                    Random generator = new Random();
                    String otp = generator.Next(0, 1000000).ToString("D6");

                    entity.token = otp;
                    entity.Expired_On = DateTime.Now.AddMinutes(10);

                    // ... (your existing code for sending OTP email)

                    _dbContext.SaveChanges();

                    var smtpClient = new SmtpClient("smtp.gmail.com")
                    {
                        Port = 587,
                        Credentials = new NetworkCredential(_configuration["OTP:UserName"], _configuration["OTP:Password"]),
                        EnableSsl = true,
                    };

                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress("winkawinoy@gmail.com"),
                        Subject = "OTP",
                        Body = $"<h1>Hello, your OTP: {otp}</h1>",
                        IsBodyHtml = true,
                    };

                    mailMessage.To.Add(entity.Email);

                    smtpClient.Send(mailMessage);

                    _dbContext.SaveChanges();

                    // Update additional fields if needed
                    // entity.password = model.password;
                    // entity.Otp = model.otp;
                    // entity.biodata_id = bio.id;
                    // entity.role_id = model.role_id;
                    entity.Created_by = 001;
                    entity.Created_on = DateTime.Now;

                    _dbContext.SaveChanges();

                    model.Id = entity.Id;

                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
