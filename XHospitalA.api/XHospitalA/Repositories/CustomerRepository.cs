﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class CustomerRepository : IRepository<CustomerViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public CustomerRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public CustomerViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public CustomerViewModel Create(CustomerViewModel model)
        {
            try
            {
                Biodata biodataEntity = new Biodata
                {
                    FullName = model.FullName,
                    // Set other properties as needed
                };
                _dbContext.Biodata.Add(biodataEntity);
                _dbContext.SaveChanges();  // Save changes to generate BiodataId
                Customer entity = new Customer
                {
                    Id = model.Id,
                    BiodataId = biodataEntity.Id,  // Set BiodataId to the generated ID
                    BloodGroupId = model.BloodGroupId,
                    Dob = model.Dob,
                    Gender = model.Gender,
                    RhesusType = model.RhesusType,
                    Height = model.Height,
                    Weight = model.Weight,
                    Is_delete = model.Is_delete,
                    Created_by = 1,
                    Created_on = DateTime.Now
                };
                _dbContext.Customers.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }





        //public List<CustomerViewModelGet> GetAll()
        //{
        //    List<CustomerViewModelGet> result = new List<CustomerViewModelGet>();
        //    try
        //    {
        //        result = (from o in _dbContext.Customers
        //                  join cm in _dbContext.CustomerMembers on o.Id equals cm.CustomerId into customerMembersGroup
        //                  from cm in customerMembersGroup.DefaultIfEmpty()
        //                  join cr in _dbContext.CustomerRelations on cm.CustomerRelationId equals cr.Id into customerRelationsGroup
        //                  from cr in customerRelationsGroup.DefaultIfEmpty()
        //                  select new CustomerViewModelGet
        //                  {
        //                      Id = o.Id,
        //                      BiodataId = o.BiodataId ?? 0,
        //                      Biodata = new BiodataViewModel
        //                      {
        //                          Is_delete = o.Biodata.Is_delete,
        //                          Id = o.Biodata.Id,
        //                          FullName = o.Biodata.FullName,
        //                          Image = o.Biodata.Image,
        //                          MobilePhone = o.Biodata.MobilePhone,
        //                          ImagePath = o.Biodata.ImagePath
        //                      },
        //                      BloodGroupId = o.BloodGroupId??0,
        //                      BloodGroup = new BloodGroupViewModel()
        //                      {
        //                          Id = o.BloodGroup.Id,
        //                          Code = o.BloodGroup.Code,
        //                          Description = o.BloodGroup.Description,
        //                      },
        //                      Dob = o.Dob,
        //                      Gender=o.Gender,
        //                      RhesusType=o.RhesusType,
        //                      Height=o.Height,
        //                      Weight=o.Weight,
        //                      Is_delete = o.Is_delete,

        //                  }).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }

        //    return result;
        //}
        public List<CustomerViewModelGet> GetAll()
        {
            List<CustomerViewModelGet> result = new List<CustomerViewModelGet>();
            try
            {
                result = (from o in _dbContext.Customers
                          join cm in _dbContext.CustomerMembers on o.Id equals cm.CustomerId into customerMembersGroup
                          from cm in customerMembersGroup.DefaultIfEmpty()
                          join cr in _dbContext.CustomerRelations on cm.CustomerRelationId equals cr.Id into customerRelationsGroup
                          from cr in customerRelationsGroup.DefaultIfEmpty()
                          select new CustomerViewModelGet
                          {
                              Id = o.Id,
                              BiodataId = o.BiodataId ?? 0,
                              Biodata = new BiodataViewModel
                              {
                                  Is_delete = o.Biodata.Is_delete,
                                  Id = o.Biodata.Id,
                                  FullName = o.Biodata.FullName,
                                  Image = o.Biodata.Image,
                                  MobilePhone = o.Biodata.MobilePhone,
                                  ImagePath = o.Biodata.ImagePath
                              },
                              BloodGroupId = o.BloodGroupId ?? 0,
                              BloodGroup = new BloodGroupViewModel()
                              {
                                  Id = o.BloodGroup.Id,
                                  Code = o.BloodGroup.Code,
                                  Description = o.BloodGroup.Description,
                              },
                              Dob = o.Dob,
                              Gender = o.Gender,
                              RhesusType = o.RhesusType,
                              Height = o.Height,
                              Weight = o.Weight,
                              Is_delete = o.Is_delete,
                              CustomerRelationId = cr.Id,
                              //CustomerRelation = new CustomerRelationViewModel
                              //{
                              //    Id = cr.Id,
                              //    Name = cr.Name
                              //}
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public CustomerViewModelGet GetById(long id)
        {
            CustomerViewModelGet model = new CustomerViewModelGet();
            try
            {
                model = (from o in _dbContext.Customers
                         where o.Id == id
                         select new CustomerViewModelGet
                         {
                             Id = o.Id,
                             BiodataId = o.BiodataId ?? 0,
                             Biodata = new BiodataViewModel
                             {
                                 Is_delete = o.Biodata.Is_delete,
                                 Id = o.Biodata.Id,
                                 FullName = o.Biodata.FullName,
                                 Image = o.Biodata.Image,
                                 MobilePhone = o.Biodata.MobilePhone,
                                 ImagePath = o.Biodata.ImagePath
                             },
                             BloodGroupId = o.BloodGroupId ?? 0,
                             BloodGroup = new BloodGroupViewModel
                             {
                                 Id = o.BloodGroup.Id,
                                 Code = o.BloodGroup.Code,
                                 Description = o.BloodGroup.Description,
                             },
                             Dob = o.Dob,
                             Gender = o.Gender,
                             RhesusType = o.RhesusType,
                             Height = o.Height,
                             Weight = o.Weight,
                             Is_delete = o.Is_delete,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new CustomerViewModelGet();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;

        }

        public List<CustomerViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<CustomerViewModelGet> result = new List<CustomerViewModelGet>();
            try
            {
                //Console.WriteLine(ClaimsContext.UserName());
                //filter by search
                var query = _dbContext.Customers
                    .Where(o => o.Gender.Contains(search));
                if (query.Count() > 0)
                {
                    switch (orderBy)
                    {
                        case "gender":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Gender) : query.OrderByDescending(o => o.Gender);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Select(o => new CustomerViewModelGet
                        {
                            Id = o.Id,
                            BiodataId = o.BiodataId ?? 0,
                            Biodata = new BiodataViewModel
                            {
                                Is_delete = o.Biodata.Is_delete,
                                Id = o.Biodata.Id,
                                FullName = o.Biodata.FullName,
                                Image = o.Biodata.Image,
                                MobilePhone = o.Biodata.MobilePhone,
                                ImagePath = o.Biodata.ImagePath
                            },
                            BloodGroupId = o.BloodGroupId ?? 0,
                            BloodGroup = new BloodGroupViewModel()
                            {
                                Id = o.BloodGroup.Id,
                                Code = o.BloodGroup.Code,
                                Description = o.BloodGroup.Description,
                            },
                            Dob = o.Dob,
                            Gender = o.Gender,
                            RhesusType = o.RhesusType,
                            Height = o.Height,
                            Weight = o.Weight,
                            Is_delete = o.Is_delete,
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }

            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public CustomerViewModel Update(CustomerViewModel model)
        {
            throw new NotImplementedException();
        }

        List<CustomerViewModel> IRepository<CustomerViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        CustomerViewModel IRepository<CustomerViewModel>.GetById(long id)
        {
            throw new NotImplementedException();
        }
    }
}
