﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class SpecializationRepository : IRepository<SpecializationViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public SpecializationRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public SpecializationViewModel ChangeStatus(long id, bool status)
        {
            SpecializationViewModel result = new SpecializationViewModel();
            try
            {
                Specialization entity = _dbContext.Specializations
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new SpecializationViewModel()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Is_delete = status
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public SpecializationViewModel Create(SpecializationViewModel model)
        {
            try
            {
                Specialization entity = new Specialization();
                entity.Id = model.Id;
                entity.Name = model.Name;
                entity.Is_delete = model.Is_delete;
                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.Specializations.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<SpecializationViewModel> GetAll()
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            try
            {
                result = (from o in _dbContext.Specializations
                          select new SpecializationViewModel
                          {
                              Name = o.Name,
                              Id = o.Id,
                              Is_delete = o.Is_delete

                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public SpecializationViewModel GetById(long id)
        {
            SpecializationViewModel model = new SpecializationViewModel();
            try
            {
                model = (from o in _dbContext.Specializations
                         where o.Id == id
                         select new SpecializationViewModel
                         {
                             Id = o.Id,
                             Is_delete = o.Is_delete,
                             Name = o.Name
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new SpecializationViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<SpecializationViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public SpecializationViewModel Update(SpecializationViewModel model)
        {
            try
            {
                Specialization entity = _dbContext.Specializations
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = model.Is_delete;
                    entity.Id = model.Id;
                    entity.Name = model.Name;

                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
