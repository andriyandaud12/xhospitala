﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;
using XHospitalA.Security;

namespace XHospitalA.Repositories
{
    public class CustomerRelationRepository : IRepository<CustomerRelationViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();

        public CustomerRelationRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CustomerRelationViewModel ChangeStatus(long id, bool status)
        {
            CustomerRelationViewModel result = new CustomerRelationViewModel();
            try
            {
                CustomerRelation entity = _dbContext.CustomerRelations
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    //entity.ModifiedBy = ClaimsContext.UserName();
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new CustomerRelationViewModel()
                    {
                        Id = entity.Id,
                        Is_delete = status,
                        Name = entity.Name
                    };
                }
                // else
                // {
                // _result.Success = false;
                // _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public CustomerRelationViewModel Create(CustomerRelationViewModel model)
        {
            try
            {


                CustomerRelation entity = new CustomerRelation();

                entity.Name = model.Name;
                entity.Created_on= DateTime.Now;
                entity.Created_by = ClaimsContext._Id;
                entity.Modified_by = ClaimsContext._Id;
                entity.Modified_on = DateTime.Now;

                _dbContext.CustomerRelations.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<CustomerRelationViewModel> GetAll()
        {
            List<CustomerRelationViewModel> result = new List<CustomerRelationViewModel>();
            try
            {
                result = (from o in _dbContext.CustomerRelations
                          select new CustomerRelationViewModel
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                             Name=o.Name,
                             
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public CustomerRelationViewModel GetById(long id)
        {
            CustomerRelationViewModel model = new CustomerRelationViewModel();
            try
            {
                model = (from o in _dbContext.CustomerRelations
                         where o.Id == id
                         select new CustomerRelationViewModel
                         {
                             Id = o.Id,
                             Name=o.Name,
                             Is_delete = o.Is_delete,

                         }).FirstOrDefault();
                if (model == null)
                {
                    return new CustomerRelationViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<CustomerRelationViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<CustomerRelationViewModel> result = new List<CustomerRelationViewModel>();
            try
            {
                //Console.WriteLine(ClaimsContext.UserName());
                //filter by search
                var query = _dbContext.CustomerRelations
                    .Where(o => o.Name.Contains(search));
                if (query.Count() > 0)
                {
                    switch (orderBy)
                    {
                        case "name":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Where(o => o.Is_delete == false)
                        .Take(rows)
                        .Select(o => new CustomerRelationViewModel
                        {
                            Id = o.Id,
                            Name = o.Name,
                            Is_delete = o.Is_delete,
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }

            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;

        }

        public CustomerRelationViewModel Update(CustomerRelationViewModel model)
        {
            try
            {
                CustomerRelation entity = _dbContext.CustomerRelations
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Name = model.Name;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
