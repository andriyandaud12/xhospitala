﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;
using XHospitalA.Security;

namespace XHospitalA.Repositories
{
    public class LocationLevelRepository : IRepository<LocationLevelViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();

        public LocationLevelRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public LocationLevelViewModel ChangeStatus(long id, bool status)
        {
            LocationLevelViewModel result = new LocationLevelViewModel();
            try
            {
                LocationLevel entity = _dbContext.LocationLevels
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_by = 1;
                    entity.Deleted_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    result = new LocationLevelViewModel()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        Abbreviation = entity.Abbreviation,
                        Is_delete = status
                    };
                }
            }
            catch (Exception e)
            {
                _result.Success = false;
                _result.Message = e.Message;
            }
            return result;
        }

        public LocationLevelViewModel Create(LocationLevelViewModel model)
        {
            try
            {
                LocationLevel entity = new LocationLevel();
                entity.Name = model.Name;
                entity.Abbreviation = model.Abbreviation;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.LocationLevels.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<LocationLevelViewModel> GetAll()
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            try
            {
                result = (from o in _dbContext.LocationLevels
                          .Where(o => o.Is_delete == false)
                          select new LocationLevelViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              Abbreviation = o.Abbreviation,
                              Is_delete = o.Is_delete
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public LocationLevelViewModel GetById(long id)
        {
            LocationLevelViewModel result = new LocationLevelViewModel();
            try
            {
                result = (from o in _dbContext.LocationLevels
                          where o.Id == id
                          select new LocationLevelViewModel
                          {
                             Id = o.Id,
                             Name = o.Name,
                             Abbreviation = o.Abbreviation,
                             Is_delete = o.Is_delete
                          }).FirstOrDefault();
                if (result == null)
                {
                    return new LocationLevelViewModel();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public List<LocationLevelViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            try
            {
                //Console.WriteLine(ClaimsContext.UserName());
                var query = _dbContext.LocationLevels
                    .Where(o => o.Name.Contains(search));

                int count = query.Count();

                if (count > 0)
                {

                    switch (orderBy)
                    {
                        case "name":
                            query = (sort == Sorting.Ascending) ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                            break;
                        default:
                            query = (sort == Sorting.Ascending) ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }

                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Where(o => o.Is_delete == false)
                        .Select(o => new LocationLevelViewModel
                        {
                            Id = o.Id,
                            Name = o.Name,
                            Abbreviation = o.Abbreviation,
                            Is_delete = o.Is_delete
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)count / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Message = "No Record found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;

        }

        public LocationLevelViewModel Update(LocationLevelViewModel model)
        {
            try
            {
                LocationLevel entity = _dbContext.LocationLevels
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Name = model.Name;
                    entity.Abbreviation = model.Abbreviation;

                    entity.Modified_by= 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                }
            }
            catch (Exception e)
            {
                _result.Success = false;
                _result.Message = e.Message;
            }
            return model;

        }
    }
}
