﻿using System.Globalization;
using System.Numerics;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;
using XHospitalA.Security;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace XHospitalA.Repositories
{
    public class DoctorRepository : IRepository<DoctorViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public DoctorRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public DoctorViewModel ChangeStatus(long id, bool status)
        {
            DoctorViewModel result = new DoctorViewModel();
            try
            {
                Doctor entity = _dbContext.Doctors
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new DoctorViewModel()
                    {
                        Is_delete = status,
                        BiodataId = entity.BiodataId,
                        Id = entity.Id,
                        Str = entity.Str
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public DoctorViewModel Create(DoctorViewModel model)
        {
            try
            {
                Doctor entity = new Doctor();
                entity.BiodataId = model.BiodataId;
                entity.Str = model.Str;
                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.Doctors.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<DoctorWFViewModel> GetAll()
        {
            List<DoctorWFViewModel> result = new List<DoctorWFViewModel>();
            try
            {
                result = (from o in _dbContext.Doctors
                          select new DoctorWFViewModel
                          {
                              Is_delete = o.Is_delete,
                              BiodataId = o.BiodataId,
                              Biodata = new BiodataViewModel
                              {
                                  FullName = o.Biodata.FullName,
                                  MobilePhone = o.Biodata.MobilePhone,
                                  Image = o.Biodata.Image,
                                  ImagePath = o.Biodata.ImagePath,
                              },
                              Id = o.Id,
                              Str = o.Str
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public DoctorViewModel GetById(long id)
        {
            DoctorViewModel model = new DoctorViewModel();
            try
            {
                model = (from o in _dbContext.Doctors
                         where o.Id == id
                         select new DoctorViewModel
                         {
                             Id = o.Id,
                             Is_delete = o.Is_delete,
                             BiodataId = o.BiodataId,
                             Str = o.Str,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new DoctorViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<DoctorViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string location, string name, string specialization, string treatmentName)
        {
            List<DoctorSearchViewModel> result = new List<DoctorSearchViewModel>();
            try
            {
                result = (from mdo in _dbContext.DoctorOffices
                          join tdt in _dbContext.DoctorTreatments on
                          new { mdo.DoctorId } equals new { tdt.DoctorId } into mdoTdt
                          from Tdt in mdoTdt.DefaultIfEmpty()

                          join cds in _dbContext.CurrentDoctorSpecializations on
                          new { mdo.DoctorId } equals new { cds.DoctorId } into mdoCds
                          from Cds in mdoCds.DefaultIfEmpty()

                          join de in _dbContext.DoctorEducations on
                          new { mdo.DoctorId } equals new { de.DoctorId } into mdoDe
                          from De in mdoDe.DefaultIfEmpty()

                          join dos in _dbContext.DoctorOfficeSchedules on
                          new { mdo.DoctorId } equals new { dos.DoctorId } into mdoDos
                          from Dos in mdoDos.DefaultIfEmpty()

                          where (string.IsNullOrEmpty(name) || mdo.Doctor.Biodata.FullName.Contains(name)) &&
                               (string.IsNullOrEmpty(location) || mdo.MedicalFacility.Location.Name.Contains(location)) &&                               
                               (mdo.MedicalFacility.MedicalFacilityCategory.Name != "Online") &&
                               (string.IsNullOrEmpty(treatmentName) || Tdt.Name.Contains(treatmentName)) && 
                               (string.IsNullOrEmpty(specialization) || Cds.Specialization.Name.Contains(specialization))


                          group new { mdo, Tdt, Cds, De, Dos } by mdo.DoctorId into grouped
                          select new DoctorSearchViewModel
                          {
                              DoctorId = grouped.Key,
                              CurrentSpecializationId = grouped.Select(x => x.Cds.Id).FirstOrDefault(),
                              BiodataId = grouped.Select(x => x.mdo.Doctor.BiodataId).FirstOrDefault(),
                              FullName = grouped.Select(x => x.mdo.Doctor.Biodata.FullName).FirstOrDefault(),
                              Image = grouped.Select(x => x.mdo.Doctor.Biodata.Image).FirstOrDefault(),
                              Pengalaman="0",
                              HistoryOfPractice = grouped.Select(x => new HistoryOfPractice
                              {
                                  MedicalFacilityName = $"{x.mdo.MedicalFacility.Name}, ({x.mdo.MedicalFacility.Location.Name}, " +
                                  $"{x.mdo.MedicalFacility.Location.Locations.LocationLevel.Abbreviation} {x.mdo.MedicalFacility.Location.Locations.Name}," +
                                  $" {x.mdo.MedicalFacility.Location.Locations.Locations.LocationLevel.Abbreviation} {x.mdo.MedicalFacility.Location.Locations.Locations.Name})",
                                  StartDate = x.mdo.StartDate,
                                  EndDate = x.mdo.EndDate,
                                  OfficeSpecialization = x.mdo.Specialization,
                                  MedicalFacilityCategoryName = x.mdo.MedicalFacility.MedicalFacilityCategory.Name,
                                  MedicalFacilityId = x.mdo.MedicalFacility.Id
                              }).Distinct().ToList(),
                              Specialization = grouped.Select(x => x.Cds.Specialization.Name).FirstOrDefault(),
                              Treatment = grouped.Select(x => new DoctorTreatmentViewModel
                              {
                                  Id = x.Tdt.Id,
                                  DoctorId = x.mdo.DoctorId,
                                  Name = x.Tdt.Name,
                                  Is_delete = x.Tdt.Is_delete
                              }).Where(o => o.Is_delete == false).Distinct().ToList(),
                              Education = grouped.Select(x => new DoctorEducationViewModel
                              {
                                  Id = x.De.Id,
                                  DoctorId = x.De.DoctorId,
                                  InstitutionName = x.De.InstitutionName,
                                  Major = x.De.Major,
                                  EducationLevelId = x.De.EducationLevelId,
                                  EndYear = x.De.EndYear,
                                  IsLastEducation = x.De.IsLastEducation,
                                  StartYear = x.De.StartYear,
                                  Is_delete = x.De.Is_delete
                              }).Distinct().ToList(),
                              DoctorSchedule = grouped.Select(x => new ScheduleDoctorViewModel
                              {
                                  DoctorId = x.Dos.DoctorId,
                                  MedicalFacilityId = x.Dos.MedicalFacilitySchedule.MedicalFacilityId,
                                  Day = x.Dos.MedicalFacilitySchedule.Day,
                                  Slot = x.Dos.Slot,
                                  ScheduleStart = x.Dos.MedicalFacilitySchedule.TimeScheduleStart,
                                  ScheduleEnd = x.Dos.MedicalFacilitySchedule.TimeScheduleEnd,
                              }).Distinct().ToList(),
                              IsOnline=false,
                          }).ToList();


                CultureInfo indonesiaCulture = new CultureInfo("id-ID");

                // Get the current day and time with Indonesian culture
                DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"));
                string currentDay = currentTime.ToString("dddd", indonesiaCulture);

                foreach (var doctor in result)
                {
                    //doctor.Pengalaman = CalculatePengalaman(doctor.HistoryOfPractice).ToString() + " tahun";
                    TimeSpan currentScheduleTime = currentTime.TimeOfDay;
                    //doctor.IsOnline = doctor.DoctorSchedule.Any(schedule =>
                    //    schedule.Day == (currentDay.ToString() == "Jumat" ? "Jum'at" : currentDay.ToString()) &&
                    //    currentScheduleTime >= ParseScheduleTime(schedule.ScheduleStart) &&
                    //    currentScheduleTime <= ParseScheduleTime(schedule.ScheduleEnd) &&
                    //    doctor.HistoryOfPractice.Select(o => o.MedicalFacilityCategoryName)
                    //    .FirstOrDefault() == "Online"
                    //    );
                }

                _result.Data = result.Skip((pageNum - 1) * rows)
                .Take(rows)
                .ToList();

                _result.Pages = (int)Math.Ceiling((decimal)result.Count() / (decimal)rows);

                if (result.Count() > 0)
                {
                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, location, name, specialization, treatmentName);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch
            {

            }
            return _result;
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<DoctorSearchViewModel> result = new List<DoctorSearchViewModel>();
            try
            {
                result = (from mdo in _dbContext.DoctorOffices
                          join tdt in _dbContext.DoctorTreatments on
                          new { mdo.DoctorId } equals new { tdt.DoctorId } into mdoTdt
                          from Tdt in mdoTdt.DefaultIfEmpty()

                          join cds in _dbContext.CurrentDoctorSpecializations on
                          new { mdo.DoctorId } equals new { cds.DoctorId } into mdoCds
                          from Cds in mdoCds.DefaultIfEmpty()

                          join de in _dbContext.DoctorEducations on
                          new { mdo.DoctorId } equals new { de.DoctorId } into mdoDe
                          from De in mdoDe.DefaultIfEmpty()

                              // where mdo.Doctor.Biodata.FullName == name ||
                              //       mdo.MedicalFacility.Location.Name == location ||
                              //       Tdt.Name == treatmentName ||
                              //       Cds.Specialization.Name == specialization
                          group new { mdo, Tdt, Cds, De } by mdo.DoctorId into grouped
                          select new DoctorSearchViewModel
                          {
                              DoctorId = grouped.Key,
                              CurrentSpecializationId = grouped.Select(x => x.Cds.Id).FirstOrDefault(),
                              BiodataId = grouped.Select(x => x.mdo.Doctor.BiodataId).FirstOrDefault(),
                              FullName = grouped.Select(x => x.mdo.Doctor.Biodata.FullName).FirstOrDefault(),
                              Image = grouped.Select(x => x.mdo.Doctor.Biodata.Image).FirstOrDefault(),
                              HistoryOfPractice = grouped.Select(x => new HistoryOfPractice
                              {
                                  MedicalFacilityName = $"{x.mdo.MedicalFacility.Name}, ({x.mdo.MedicalFacility.Location.Name})",
                                  StartDate = x.mdo.StartDate,
                                  EndDate = x.mdo.EndDate,
                                  OfficeSpecialization = x.mdo.Specialization
                              }).Distinct().ToList(),
                              Specialization = grouped.Select(x => x.Cds.Specialization.Name).FirstOrDefault(),
                              Treatment = grouped.Select(x => new DoctorTreatmentViewModel
                              {
                                  Id = x.Tdt.Id,
                                  DoctorId = x.mdo.DoctorId,
                                  Name = x.Tdt.Name,
                                  Is_delete = x.Tdt.Is_delete
                              }).Where(o => o.Is_delete == false).Distinct().ToList(),
                              Education = grouped.Select(x => new DoctorEducationViewModel
                              {
                                  Id = x.De.Id,
                                  DoctorId = x.De.DoctorId,
                                  InstitutionName = x.De.InstitutionName,
                                  Major = x.De.Major,
                                  EducationLevelId = x.De.EducationLevelId,
                                  EndYear = x.De.EndYear,
                                  IsLastEducation = x.De.IsLastEducation,
                                  StartYear = x.De.StartYear,
                                  Is_delete = x.De.Is_delete
                              }).Distinct().ToList(),
                              //University = grouped.Select(x => x.De.InstitutionName).FirstOrDefault(),
                              //Major = grouped.Select(x => x.De.Major).FirstOrDefault(),
                              //Graduate = grouped.Select(x => x.De.EndYear).FirstOrDefault()
                          }).ToList();
                foreach (var doctor in result)
                {
                    doctor.Pengalaman = CalculatePengalaman(doctor.HistoryOfPractice).ToString("0.00");
                }

                _result.Data = result.Skip((pageNum - 1) * rows)
                .Take(rows)
                .ToList();

                _result.Pages = (int)Math.Ceiling((decimal)result.Count() / (decimal)rows);

                if (result.Count() > 0)
                {
                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch
            {

            }
            return _result;
        }

        public DoctorSearchViewModel GetDoctorById(long Id)
        {
            DoctorSearchViewModel result = new DoctorSearchViewModel();
            try
            {
                result = (from mdo in _dbContext.DoctorOffices
                          join tdt in _dbContext.DoctorTreatments on
                          new { mdo.DoctorId } equals new { tdt.DoctorId } into mdoTdt
                          from Tdt in mdoTdt.DefaultIfEmpty()

                          join cds in _dbContext.CurrentDoctorSpecializations on
                          new { mdo.DoctorId } equals new { cds.DoctorId } into mdoCds
                          from Cds in mdoCds.DefaultIfEmpty()

                          join de in _dbContext.DoctorEducations on
                          new { mdo.DoctorId } equals new { de.DoctorId } into mdoDe
                          from De in mdoDe.DefaultIfEmpty()

                          join dos in _dbContext.DoctorOfficeSchedules on
                          new { mdo.DoctorId } equals new { dos.DoctorId } into mdoDos
                          from Dos in mdoDos.DefaultIfEmpty()

                          where mdo.Doctor.BiodataId == Id

                          group new { mdo, Tdt, Cds, De, Dos, } by mdo.DoctorId into grouped
                          select new DoctorSearchViewModel
                          {
                              DoctorId = grouped.Key,
                              CurrentSpecializationId = grouped.Select(x => x.Cds.Id).FirstOrDefault(),
                              BiodataId = grouped.Select(x => x.mdo.Doctor.BiodataId).FirstOrDefault(),
                              FullName = grouped.Select(x => x.mdo.Doctor.Biodata.FullName).FirstOrDefault(),
                              Image = grouped.Select(x => x.mdo.Doctor.Biodata.Image).FirstOrDefault(),
                              HistoryOfPractice = grouped.Select(x => new HistoryOfPractice
                              {
                                  MedicalFacilityName = $"{x.mdo.MedicalFacility.Name}, ({x.mdo.MedicalFacility.Location.Name}, {x.mdo.MedicalFacility.Location.Locations.Name}, {x.mdo.MedicalFacility.Location.Locations.Locations.Name})",
                                  StartDate = x.mdo.StartDate,
                                  EndDate = x.mdo.EndDate,
                                  OfficeSpecialization = x.mdo.Specialization,
                                  MedicalFacilityId = x.mdo.MedicalFacilityId
                              }).Distinct().ToList(),
                              /*HistoryOfPractice = $"{grouped.Select(x => x.mdo.MedicalFacility.Name).FirstOrDefault()}, ({grouped.Select(x => x.mdo.MedicalFacility.Location.Name).FirstOrDefault()})",*/
                              Pengalaman = "0", // Adjust this based on your logic for calculating experience
                              Specialization = grouped.Select(x => x.Cds.Specialization.Name).FirstOrDefault(),
                              Treatment = grouped.Select(x => new DoctorTreatmentViewModel
                              {
                                  Id = x.Tdt.Id,
                                  DoctorId = x.mdo.DoctorId,
                                  Name = x.Tdt.Name,
                                  Is_delete = x.Tdt.Is_delete
                              }).Where(o => o.Is_delete == false).Distinct().ToList(),
                              Education = grouped.Select(x => new DoctorEducationViewModel
                              {
                                  Id = x.De.Id,
                                  DoctorId = x.De.DoctorId,
                                  InstitutionName = x.De.InstitutionName,
                                  Major = x.De.Major,
                                  EducationLevelId = x.De.EducationLevelId,
                                  EndYear = x.De.EndYear,
                                  IsLastEducation = x.De.IsLastEducation,
                                  StartYear = x.De.StartYear,
                                  Is_delete = x.De.Is_delete
                              }).Distinct().ToList(),
                              DoctorSchedule = grouped.Select(x => new ScheduleDoctorViewModel
                              {
                                  DoctorId = x.Dos.DoctorId,
                                  MedicalFacilityId = x.Dos.MedicalFacilitySchedule.MedicalFacilityId,
                                  Day = x.Dos.MedicalFacilitySchedule.Day,
                                  Slot = x.Dos.Slot,
                                  ScheduleStart = x.Dos.MedicalFacilitySchedule.TimeScheduleStart,
                                  ScheduleEnd = x.Dos.MedicalFacilitySchedule.TimeScheduleEnd,
                              }).Distinct().ToList()
                          }).FirstOrDefault();
                CultureInfo indonesiaCulture = new CultureInfo("id-ID");

                // Get the current day and time with Indonesian culture
                DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"));
                string currentDay = currentTime.ToString("dddd", indonesiaCulture);

                result.Pengalaman = CalculatePengalaman(result.HistoryOfPractice).ToString() + " tahun";
                TimeSpan currentScheduleTime = currentTime.TimeOfDay;
                result.IsOnline = result.DoctorSchedule.Any(schedule =>
                    schedule.Day == currentDay.ToString() &&
                    currentScheduleTime >= ParseScheduleTime(schedule.ScheduleStart) &&
                    currentScheduleTime <= ParseScheduleTime(schedule.ScheduleEnd));
            }
            catch
            {

            }
            return result;
        }

        public DoctorOfficeTreatmentPriceViewModel GetDoctorPriceById(long Id)
        {
            DoctorOfficeTreatmentPriceViewModel result = new DoctorOfficeTreatmentPriceViewModel();
            try
            {
                result = (from o in _dbContext.DoctorOfficeTreatmentPrices
                          where o.DoctorOfficeTreatment.DoctorOffice.Doctor.Id == Id
                          select new DoctorOfficeTreatmentPriceViewModel
                          {
                              Id = o.Id,
                              DoctorOfficeTreatmentId = o.DoctorOfficeTreatmentId,
                              DoctorOfficeTreatment = new GetDoctorOfficeTreatmentViewModel
                              {
                                  Id = o.DoctorOfficeTreatment.Id,
                                  DoctorOfficeId = o.DoctorOfficeTreatment.DoctorOfficeId,
                                  DoctorTreatmentId = o.DoctorOfficeTreatment.Id,
                              },
                              Price = o.Price,
                              PriceStartFrom = o.PriceStartFrom,
                              PriceUntilFrom = o.PriceUntilFrom,
                          }
                          ).FirstOrDefault();
            }
            catch
            {

            }
            return result;
        }

        public DoctorViewModel Update(DoctorViewModel model)
        {
            try
            {
                Doctor entity = _dbContext.Doctors
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Str = model.Str;
                    entity.BiodataId = model.BiodataId;
                    entity.Id = model.Id;
                    entity.Is_delete = model.Is_delete;
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }

        List<DoctorViewModel> IRepository<DoctorViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        private double CalculatePengalaman(List<HistoryOfPractice> historyOfPractice)
        {
            List<double> totalExperience = new List<double>();

            foreach (var practice in historyOfPractice)
            {
                if (practice.StartDate != null && practice.EndDate != null)
                {
                    totalExperience.Add((int)Math.Ceiling(((DateTime.Now) - (DateTime)practice.StartDate).TotalDays / 365));
                }
            }

            return totalExperience.Max();
        }
        private static TimeSpan ParseScheduleTime(string scheduleTime)
        {
            string format = "HH.mm";
            return DateTime.ParseExact(scheduleTime, format, CultureInfo.InvariantCulture).TimeOfDay;
        }

        public DoctorExperienceViewModel GetDoctorExperienceById(long doctorId)
        {
            List<HistoryOfPractice> result = new List<HistoryOfPractice>();
            result = (from o in _dbContext.DoctorOffices
                      where o.DoctorId == doctorId
                      select new HistoryOfPractice
                      {
                          StartDate = o.StartDate,
                          EndDate = o.EndDate,
                          MedicalFacilityId = o.MedicalFacilityId,
                          MedicalFacilityName = o.MedicalFacility.Name,
                          OfficeSpecialization = o.Specialization,
                          MedicalFacilityCategoryName = o.MedicalFacility.MedicalFacilityCategory.Name,
                      }).ToList();
            DoctorExperienceViewModel ouput = new DoctorExperienceViewModel()
            {
                DoctorId= doctorId,
                DoctorExperience = CalculatePengalaman(result)
            };

            return ouput;
        }

        public DoctorOnlineViewModel GetDoctorOnline(long doctorId)
        {
            List<ScheduleDoctorViewModel> schedules = new List<ScheduleDoctorViewModel>();
            schedules = (from o in _dbContext.DoctorOfficeSchedules
                        where o.DoctorId == doctorId
                        select new ScheduleDoctorViewModel
                        {
                            DoctorId = o.DoctorId,
                            MedicalFacilityId = o.MedicalFacilitySchedule.MedicalFacilityId,
                            Day = o.MedicalFacilitySchedule.Day,
                            Slot = o.Slot,
                            ScheduleStart = o.MedicalFacilitySchedule.TimeScheduleStart,
                            ScheduleEnd = o.MedicalFacilitySchedule.TimeScheduleEnd,
                        }).ToList();

            List<HistoryOfPractice> result = new List<HistoryOfPractice>();
            result = (from o in _dbContext.DoctorOffices
                      where o.DoctorId == doctorId
                      select new HistoryOfPractice
                      {
                          StartDate = o.StartDate,
                          EndDate = o.EndDate,
                          MedicalFacilityId = o.MedicalFacilityId,
                          MedicalFacilityName = o.MedicalFacility.Name,
                          OfficeSpecialization = o.Specialization,
                          MedicalFacilityCategoryName = o.MedicalFacility.MedicalFacilityCategory.Name,
                      }).ToList();

            CultureInfo indonesiaCulture = new CultureInfo("id-ID");
            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"));
            string currentDay = currentTime.ToString("dddd", indonesiaCulture);
            TimeSpan currentScheduleTime = currentTime.TimeOfDay;
            DoctorOnlineViewModel output = new DoctorOnlineViewModel();
            foreach (var schedule in schedules)
            {
                output = new DoctorOnlineViewModel()
                {
                    DoctorId = doctorId,
                    IsOnline = schedule.Day == (currentDay.ToString() == "Jumat" ? "Jum'at" : currentDay.ToString()) &&
                            currentScheduleTime >= ParseScheduleTime(schedule.ScheduleStart) &&
                            currentScheduleTime <= ParseScheduleTime(schedule.ScheduleEnd) &&
                            result.Where(o=>o.MedicalFacilityCategoryName=="Online")
                            .Select(o => o.MedicalFacilityCategoryName)
                            .FirstOrDefault() == "Online"

                };
            }
            return output;
        }

    }
}
