﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class LocationRepository : IRepository<LocationViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public LocationRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public LocationViewModel ChangeStatus(long id, bool status)
        {
            LocationViewModel result = new LocationViewModel();
            try
            {
                Location entity = _dbContext.Locations
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_by = 1;
                    entity.Deleted_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    result = new LocationViewModel()
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        ParentId = entity.ParentId,
                        LocationLevelId = entity.LocationLevelId,
                        Is_delete = status
                    };
                }
            }
            catch (Exception e)
            {
                _result.Success = false;
                _result.Message = e.Message;
            }
            return result;
        }

        public LocationViewModel Create(LocationViewModel model)
        {
            try
            {
                Location entity = new Location();
                entity.Name = model.Name;
                entity.ParentId = model.ParentId == 0 ? null : model.ParentId;
                entity.LocationLevelId = model.LocationLevelId;
                entity.Is_delete = model.Is_delete;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.Locations.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<LocationWFViewModel> GetAll()
        {
            List<LocationWFViewModel> result = new List<LocationWFViewModel>();
            try
            {
                result = (from o in _dbContext.Locations
                          select new LocationWFViewModel
                          {
                              Name = o.Name,//1
                              ParentId = o.ParentId,//1
                              Location = new LocationViewModel//1
                              {
                                  Name = o.Locations.Name,//2
                                  ParentId = o.Locations.ParentId,//2
                                  Location = new LocationViewModel//2
                                  {
                                      Name = o.Locations.Locations.Name,//3
                                      ParentId = o.Locations.Locations.ParentId,//3
                                      LocationLevelId = o.Locations.LocationLevelId,//3
                                  },
                                  LocationLevelId = o.LocationLevelId,//2
                                  LocationLevel = new LocationLevelViewModel//2
                                  {
                                      Name = o.Locations.LocationLevel.Name,//3
                                      Abbreviation = o.Locations.LocationLevel.Abbreviation//3
                                  },
                              },
                              LocationLevelId = o.LocationLevelId,//1
                              LocationLevel = new LocationLevelViewModel//1
                              {
                                  Name = o.LocationLevel.Name,
                                  Abbreviation = o.LocationLevel.Abbreviation
                              },
                              Is_delete = o.Is_delete//1
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public LocationViewModel GetById(long id)
        {
            LocationViewModel result = new LocationViewModel();
            try
            {
                result = (from o in _dbContext.Locations
                          where o.Id == id
                          select new LocationViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              ParentId = o.ParentId,
                              LocationLevelId = o.LocationLevelId,
                              Is_delete = o.Is_delete
                          }).FirstOrDefault();
                if (result == null)
                {
                    return new LocationViewModel();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public List<LocationViewModel> GetByParentId(long parentId)
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            try
            {
                result = (from o in _dbContext.Locations
                          where o.ParentId == parentId
                          select new LocationViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              ParentId = o.ParentId,
                              LocationLevelId = o.LocationLevelId,
                              Is_delete = o.Is_delete
                          }).ToList();
            }
            catch (Exception)
            {

                throw;
            }

            return result;

        }

        public ResponseResult Pagination(int pageNum, int rows, int? level, string search, string orderBy, Sorting sort)
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            try
            {
                var query = _dbContext.Locations
                    .Where(o => o.Name.Contains(search));

                if (level != null)
                    query = query.Where(o => o.LocationLevelId == level);

                int count = query.Count();

                if (count > 0)
                {


                    switch (orderBy)
                    {
                        case "name":
                            query = (sort == Sorting.Ascending) ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                            break;
                        default:
                            query = (sort == Sorting.Ascending) ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }

                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Where(o => o.Is_delete == false)
                        .Select(o => new LocationViewModel
                        {
                            Id = o.Id,
                            Name = o.Name,
                            ParentId = o.ParentId,
                            Location = o.Locations != null ? new LocationViewModel()
                            {
                                Id = o.Locations.Id,
                                Name = o.Locations.Name,
                                ParentId = o.Locations.ParentId,
                                Location = o.Locations.Locations != null ? new LocationViewModel()
                                {
                                    Id = o.Locations.Locations.Id,
                                    Name = o.Locations.Locations.Name,
                                    LocationLevelId = o.Locations.Locations.LocationLevelId,
                                    LocationLevel = o.LocationLevel != null ? new LocationLevelViewModel()
                                    {
                                        Id = o.Locations.Locations.LocationLevel.Id,
                                        Name = o.Locations.Locations.LocationLevel.Name,
                                        Abbreviation = o.Locations.Locations.LocationLevel.Abbreviation
                                    } : new LocationLevelViewModel(),
                                    Is_delete = o.Is_delete
                                } : new LocationViewModel(),
                                LocationLevelId = o.Locations.LocationLevelId,
                                LocationLevel = o.Locations.LocationLevel != null ? new LocationLevelViewModel()
                                {
                                    Id = o.Locations.LocationLevel.Id,
                                    Name = o.Locations.LocationLevel.Name,
                                    Abbreviation = o.Locations.LocationLevel.Abbreviation
                                } : new LocationLevelViewModel(),
                                Is_delete = o.Locations.Is_delete
                            } : new LocationViewModel(),
                            LocationLevelId = o.LocationLevelId,
                            LocationLevel = o.LocationLevel != null ? new LocationLevelViewModel()
                            {
                                Id = o.LocationLevel.Id,
                                Name = o.LocationLevel.Name,
                                Abbreviation = o.LocationLevel.Abbreviation
                            } : new LocationLevelViewModel(),
                            Is_delete = o.Is_delete
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, level, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Message = "No Record found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;

        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public LocationViewModel Update(LocationViewModel model)
        {
            try
            {
                Location entity = _dbContext.Locations
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Name= model.Name;
                    entity.ParentId = model.ParentId;
                    entity.LocationLevelId = model.LocationLevelId;

                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                }
            }
            catch (Exception e)
            {
                _result.Success = false;
                _result.Message = e.Message;
            }
            return model;
        }

        List<LocationViewModel> IRepository<LocationViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        public List<LocationWFViewModel> GetAllByLevel(long Id)
        {
            List<LocationWFViewModel> result = new List<LocationWFViewModel>();
            try
            {
                result = (from o in _dbContext.Locations
                          where o.LocationLevelId == Id
                          select new LocationWFViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              ParentId = o.ParentId,
                              Location = o.Locations != null ? new LocationViewModel()
                              {
                                  Id = o.Locations.Id,
                                  Name = o.Locations.Name,
                                  ParentId = o.Locations.ParentId,
                                  Location = o.Locations.Locations != null ? new LocationViewModel()
                                  {
                                      Id = o.Locations.Locations.Id,
                                      Name = o.Locations.Locations.Name,
                                      LocationLevelId = o.Locations.Locations.LocationLevelId,
                                      LocationLevel = o.LocationLevel != null ? new LocationLevelViewModel()
                                      {
                                          Id = o.Locations.Locations.LocationLevel.Id,
                                          Name = o.Locations.Locations.LocationLevel.Name,
                                          Abbreviation = o.Locations.Locations.LocationLevel.Abbreviation
                                      } : new LocationLevelViewModel(),
                                      Is_delete = o.Is_delete
                                  } : new LocationViewModel(),
                                  LocationLevelId = o.Locations.LocationLevelId,
                                  LocationLevel = o.Locations.LocationLevel != null ? new LocationLevelViewModel()
                                  {
                                      Id = o.Locations.LocationLevel.Id,
                                      Name = o.Locations.LocationLevel.Name,
                                      Abbreviation = o.Locations.LocationLevel.Abbreviation
                                  } : new LocationLevelViewModel(),
                                  Is_delete = o.Locations.Is_delete
                              } : new LocationViewModel(),
                              LocationLevelId = o.LocationLevelId,
                              LocationLevel = o.LocationLevel != null ? new LocationLevelViewModel()
                              {
                                  Id = o.LocationLevel.Id,
                                  Name = o.LocationLevel.Name,
                                  Abbreviation = o.LocationLevel.Abbreviation
                              } : new LocationLevelViewModel(),
                              Is_delete = o.Is_delete
                          }).Distinct().ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public List<LocationWFViewModel> GetAllByLocationLevel5()
        {
            List<LocationWFViewModel> result = new List<LocationWFViewModel>();
            try
            {
                result = (from o in _dbContext.Locations
                          where o.LocationLevelId == 5
                          select new LocationWFViewModel
                          {
                              Name = o.Name,//1
                              ParentId = o.ParentId,//1
                              Location = new LocationViewModel//1
                              {
                                  Name = o.Locations.Name,//2
                                  ParentId = o.Locations.ParentId,//2
                                  Location = new LocationViewModel//2
                                  {
                                      Name = o.Locations.Locations.Name,//3
                                      ParentId = o.Locations.Locations.ParentId,//3
                                      LocationLevelId = o.Locations.LocationLevelId,//3
                                  },
                                  LocationLevelId = o.LocationLevelId,//2
                                  LocationLevel = new LocationLevelViewModel//2
                                  {
                                      Name = o.Locations.LocationLevel.Name,//3
                                      Abbreviation = o.Locations.LocationLevel.Abbreviation//3
                                  },
                              },
                              LocationLevelId = o.LocationLevelId,//1
                              LocationLevel = new LocationLevelViewModel//1
                              {
                                  Name = o.LocationLevel.Name,
                                  Abbreviation = o.LocationLevel.Abbreviation
                              },
                              Is_delete = o.Is_delete//1
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public List<LocationWFViewModel> GetAllByLocationLevel5(string name)
        {
            List<LocationWFViewModel> result = new List<LocationWFViewModel>();
            try
            {
                result = (from o in _dbContext.Locations
                          where o.LocationLevelId == 5 && o.Name.Contains(name)
                          select new LocationWFViewModel
                          {
                              Name = o.Name,//1
                              ParentId = o.ParentId,//1
                              Location = new LocationViewModel//1
                              {
                                  Name = o.Locations.Name,//2
                                  ParentId = o.Locations.ParentId,//2
                                  Location = new LocationViewModel//2
                                  {
                                      Name = o.Locations.Locations.Name,//3
                                      ParentId = o.Locations.Locations.ParentId,//3
                                      LocationLevelId = o.Locations.LocationLevelId,//3
                                  },
                                  LocationLevelId = o.LocationLevelId,//2
                                  LocationLevel = new LocationLevelViewModel//2
                                  {
                                      Name = o.Locations.LocationLevel.Name,//3
                                      Abbreviation = o.Locations.LocationLevel.Abbreviation//3
                                  },
                              },
                              LocationLevelId = o.LocationLevelId,//1
                              LocationLevel = new LocationLevelViewModel//1
                              {
                                  Name = o.LocationLevel.Name,
                                  Abbreviation = o.LocationLevel.Abbreviation
                              },
                              Is_delete = o.Is_delete//1
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}
