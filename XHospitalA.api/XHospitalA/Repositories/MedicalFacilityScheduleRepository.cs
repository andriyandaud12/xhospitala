﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class MedicalFacilityScheduleRepository : IRepository<MedicalFacilityScheduleViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public MedicalFacilityScheduleRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public MedicalFacilityScheduleViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public MedicalFacilityScheduleViewModel Create(MedicalFacilityScheduleViewModel model)
        {
            try
            {
                MedicalFacilitySchedule entity = new MedicalFacilitySchedule();
                entity.MedicalFacilityId = model.MedicalFacilityId;
                entity.Day = model.Day;
                entity.TimeScheduleStart = model.TimeScheduleStart;
                entity.TimeScheduleEnd = model.TimeScheduleEnd;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.MedicalFacilitySchedules.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public GetMedicalFacilityScheduleViewModel GetByMedFacSchedId(long id)
        {
            GetMedicalFacilityScheduleViewModel model = new GetMedicalFacilityScheduleViewModel();
            try
            {
                model = (from o in _dbContext.MedicalFacilitySchedules
                         where o.Id == id
                         select new GetMedicalFacilityScheduleViewModel
                         {
                             Id = o.Id,
                             Day = o.Day,
                             MedicalFacilityId = o.MedicalFacilityId,
                             TimeScheduleEnd = o.TimeScheduleEnd,
                             TimeScheduleStart = o.TimeScheduleStart,
                             MedicalFacility = new MedicalFacilityViewModel
                             {
                                 Id=o.MedicalFacility.Id,
                                 Email = o.MedicalFacility.Email,
                                 Fax = o.MedicalFacility.Fax,
                                 FullAddress = o.MedicalFacility.FullAddress,
                                 Name = o.MedicalFacility.Name,
                                 LocationId = o.MedicalFacility.LocationId,
                                 MedicalFacilityCategoryId = o.MedicalFacility.MedicalFacilityCategoryId,
                                 Phone=o.MedicalFacility.Phone,
                                 PhoneCode=o.MedicalFacility.PhoneCode,
                                 Is_delete = o.MedicalFacility.Is_delete,
                             }
                         }).FirstOrDefault();
            }
            catch (Exception ex) 
            {
                throw ex;
            }
            return model;
        }

        public List<MedicalFacilityScheduleViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public MedicalFacilityScheduleViewModel GetById(long id)
        {
            throw new NotImplementedException();
        }

        public List<MedicalFacilityScheduleViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<GetMedicalFacilityScheduleViewModel> result = new List<GetMedicalFacilityScheduleViewModel>();
            try
            {
                var query = _dbContext.MedicalFacilitySchedules
                    .Where(o => o.Day.Contains(search));
                if (query.Count() > 0)
                {
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Select(o => new GetMedicalFacilityScheduleViewModel
                        {
                            Id = o.Id,
                            Day = o.Day,
                            MedicalFacilityId = o.MedicalFacilityId,
                            TimeScheduleEnd = o.TimeScheduleEnd,
                            TimeScheduleStart = o.TimeScheduleStart,
                            MedicalFacility = new MedicalFacilityViewModel
                            {
                                Id = o.MedicalFacility.Id,
                                Email = o.MedicalFacility.Email,
                                Fax = o.MedicalFacility.Fax,
                                FullAddress = o.MedicalFacility.FullAddress,
                                Name = o.MedicalFacility.Name,
                                LocationId = o.MedicalFacility.LocationId,
                                MedicalFacilityCategoryId = o.MedicalFacility.MedicalFacilityCategoryId,
                                Phone = o.MedicalFacility.Phone,
                                PhoneCode = o.MedicalFacility.PhoneCode,
                                Is_delete = o.MedicalFacility.Is_delete,
                            }
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public MedicalFacilityScheduleViewModel Update(MedicalFacilityScheduleViewModel model)
        {
            try
            {
                MedicalFacilitySchedule entity = _dbContext.MedicalFacilitySchedules
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if(entity != null)
                {
                    entity.MedicalFacilityId = model.MedicalFacilityId;
                    entity.Day = model.Day;
                    entity.TimeScheduleStart = model.TimeScheduleStart;
                    entity.TimeScheduleEnd = model.TimeScheduleEnd;
                    entity.Is_delete = model.Is_delete;
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                }
            }
            catch(Exception e){}
            return model;
        }

        public List<MedicalFacilityScheduleViewModel> GetByMedicalFacilityId(long Id)
        {
            List<MedicalFacilityScheduleViewModel> result = new List<MedicalFacilityScheduleViewModel>();
            try
            {
                result = (from o in _dbContext.MedicalFacilitySchedules
                          where o.MedicalFacilityId== Id
                          select new MedicalFacilityScheduleViewModel
                          {
                              Id = o.Id,
                              MedicalFacilityId = o.MedicalFacilityId,
                              Day = o.Day,
                              TimeScheduleStart = o.TimeScheduleStart,
                              TimeScheduleEnd = o.TimeScheduleEnd
                          }).ToList();
            }
            catch (Exception)
            {

                throw;
            }

            return result;

        }
    }
}
