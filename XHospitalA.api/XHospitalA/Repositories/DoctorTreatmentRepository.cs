﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class DoctorTreatmentRepository
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public DoctorTreatmentRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public DoctorTreatmentViewModel ChangeStatus(long id, bool status)
        {
            DoctorTreatmentViewModel result = new DoctorTreatmentViewModel();
            try
            {
                DoctorTreatment entity = _dbContext.DoctorTreatments
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new DoctorTreatmentViewModel()
                    {
                        Is_delete = status,
                        Id = entity.Id,
                        DoctorId = entity.DoctorId,
                        Name = entity.Name
                    };
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public DoctorTreatmentViewModel Create(DoctorTreatmentViewModel model)
        {
            try
            {
                DoctorTreatment entity = new DoctorTreatment();
                entity.Is_delete = model.Is_delete;
                entity.Name = model.Name;
                entity.DoctorId = model.DoctorId;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.DoctorTreatments.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GetDoctorTreatmentViewModel> GetAll()
        {
            List<GetDoctorTreatmentViewModel> result = new List<GetDoctorTreatmentViewModel>();
            try
            {
                result = (from o in _dbContext.DoctorTreatments
                          where o.Is_delete == false
                          select new GetDoctorTreatmentViewModel
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                              Name = o.Name,
                              DoctorId = o.DoctorId,
                              Doctor = new DoctorWFViewModel
                              {
                                  Id = o.Doctor.Id,
                                  BiodataId = o.Doctor.BiodataId,
                                  Is_delete = o.Doctor.Is_delete,
                                  Str = o.Doctor.Str,
                                  Biodata = new BiodataViewModel
                                  {
                                      Is_delete = o.Doctor.Biodata.Is_delete,
                                      Id = o.Doctor.Biodata.Id,
                                      FullName = o.Doctor.Biodata.FullName,
                                      Image = o.Doctor.Biodata.Image,
                                      MobilePhone = o.Doctor.Biodata.MobilePhone,
                                      ImagePath = o.Doctor.Biodata.ImagePath
                                  }
                              }
                          }).Distinct().ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public List<GetDoctorTreatmentViewModel> GetAllTreatmentBySpecName(string name)
        {
            List<GetDoctorTreatmentViewModel> result = new List<GetDoctorTreatmentViewModel>();
            try
            {
                result = (from dt in _dbContext.DoctorTreatments
                          join cds in _dbContext.CurrentDoctorSpecializations on
                          new { dt.DoctorId } equals new { cds.DoctorId } into dtCds
                          from Cds in dtCds.DefaultIfEmpty()

                          where dt.Is_delete == false && Cds.Specialization.Name.Contains(name)
                          select new GetDoctorTreatmentViewModel
                          {
                              Is_delete = dt.Is_delete,
                              Id = dt.Id,
                              Name = dt.Name,
                              DoctorId = dt.DoctorId,
                              Doctor = new DoctorWFViewModel
                              {
                                  Id = dt.Doctor.Id,
                                  BiodataId = dt.Doctor.BiodataId,
                                  Is_delete = dt.Doctor.Is_delete,
                                  Str = dt.Doctor.Str,
                                  Biodata = new BiodataViewModel
                                  {
                                      Is_delete = dt.Doctor.Biodata.Is_delete,
                                      Id = dt.Doctor.Biodata.Id,
                                      FullName = dt.Doctor.Biodata.FullName,
                                      Image = dt.Doctor.Biodata.Image,
                                      MobilePhone = dt.Doctor.Biodata.MobilePhone,
                                      ImagePath = dt.Doctor.Biodata.ImagePath
                                  }
                              }
                          }).Distinct().ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GetDoctorTreatmentViewModel GetById(long id)
        {
            GetDoctorTreatmentViewModel model = new GetDoctorTreatmentViewModel();
            try
            {
                model = (from o in _dbContext.DoctorTreatments
                         where o.Id == id
                         select new GetDoctorTreatmentViewModel
                         {
                             Is_delete = o.Is_delete,
                             Id = o.Id,
                             Name = o.Name,
                             DoctorId = o.DoctorId,
                             Doctor = new DoctorWFViewModel
                             {
                                 Id = o.Doctor.Id,
                                 BiodataId = o.Doctor.BiodataId,
                                 Is_delete = o.Doctor.Is_delete,
                                 Str = o.Doctor.Str,
                                 Biodata = new BiodataViewModel
                                 {
                                     Is_delete = o.Doctor.Biodata.Is_delete,
                                     Id = o.Doctor.Biodata.Id,
                                     FullName = o.Doctor.Biodata.FullName,
                                     Image = o.Doctor.Biodata.Image,
                                     MobilePhone = o.Doctor.Biodata.MobilePhone,
                                     ImagePath = o.Doctor.Biodata.ImagePath
                                 }
                             }
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GetDoctorTreatmentViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<MedicalFacilityViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public DoctorTreatmentViewModel Update(DoctorTreatmentViewModel model)
        {
            try
            {
                DoctorTreatment entity = _dbContext.DoctorTreatments
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = model.Is_delete;
                    entity.Id = model.Id;
                    entity.Name = model.Name;
                    entity.DoctorId = model.DoctorId;

                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;

        }
    }
}
