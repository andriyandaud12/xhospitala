﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class CurrentDoctorSpecializationRepository: IRepository<CurrentDoctorSpecializationViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public CurrentDoctorSpecializationRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CurrentDoctorSpecializationViewModel ChangeStatus(long id, bool status)
        {
            CurrentDoctorSpecializationViewModel result = new CurrentDoctorSpecializationViewModel();
            try
            {
                CurrentDoctorSpecialization entity = _dbContext.CurrentDoctorSpecializations
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new CurrentDoctorSpecializationViewModel()
                    {
                       Id = entity.Id,
                        Is_delete = status,
                       DoctorId = entity.DoctorId,
                       SpecializationId = entity.SpecializationId,
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public CurrentDoctorSpecializationViewModel Create(CurrentDoctorSpecializationViewModel model)
        {
            try
            {
                CurrentDoctorSpecialization entity = new CurrentDoctorSpecialization();
                entity.SpecializationId = model.SpecializationId;
                entity.DoctorId = model.DoctorId;
                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.CurrentDoctorSpecializations.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GetCurrentDoctorSpecializationViewModel> GetAll()
        {
            List<GetCurrentDoctorSpecializationViewModel> result = new List<GetCurrentDoctorSpecializationViewModel>();
            try
            {
                result = (from o in _dbContext.CurrentDoctorSpecializations
                          select new GetCurrentDoctorSpecializationViewModel
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                              Doctor = new DoctorWFViewModel
                              {
                                  Id = o.Doctor.Id,
                                  Biodata = new BiodataViewModel
                                  {
                                      FullName = o.Doctor.Biodata.FullName,
                                      Id = o.Doctor.Biodata.Id,
                                      Image = o.Doctor.Biodata.Image,
                                      ImagePath = o.Doctor.Biodata.ImagePath,
                                      MobilePhone = o.Doctor.Biodata.MobilePhone,
                                      Is_delete = o.Doctor.Biodata.Is_delete
                                  },
                                  BiodataId = o.Doctor.BiodataId,
                                  Is_delete = o.Is_delete,
                                  Str = o.Doctor.Str
                              },
                              Specialization = new SpecializationViewModel
                              {
                                Id = o.Specialization.Id,
                                Is_delete = o.Specialization.Is_delete,
                                Name = o.Specialization.Name
                              },
                              DoctorId = o.Doctor.Id,
                              SpecializationId = o.Specialization.Id
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GetCurrentDoctorSpecializationViewModel GetById(long id)
        {
            GetCurrentDoctorSpecializationViewModel model = new GetCurrentDoctorSpecializationViewModel();
            try
            {
                model = (from o in _dbContext.CurrentDoctorSpecializations
                         where o.Id == id
                         select new GetCurrentDoctorSpecializationViewModel
                         {
                             Id = o.Id,
                             Is_delete = o.Is_delete,
                             Doctor = new DoctorWFViewModel
                             {
                                 BiodataId = o.Doctor.Id,
                                 Id = o.Doctor.Id,
                                 Str = o.Doctor.Str,
                                 Is_delete = o.Doctor.Is_delete
                             },
                             DoctorId = o.DoctorId,
                             Specialization = new SpecializationViewModel
                             {
                                 Id = o.Specialization.Id,
                                 Name = o.Specialization.Name
                             },
                             SpecializationId = o.SpecializationId
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GetCurrentDoctorSpecializationViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<CurrentDoctorSpecializationViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public CurrentDoctorSpecializationViewModel Update(CurrentDoctorSpecializationViewModel model)
        {
            try
            {
                CurrentDoctorSpecialization entity = _dbContext.CurrentDoctorSpecializations
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.DoctorId = model.DoctorId;
                    entity.SpecializationId = model.SpecializationId;
                    entity.Is_delete = model.Is_delete;
                    
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }

        List<CurrentDoctorSpecializationViewModel> IRepository<CurrentDoctorSpecializationViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        CurrentDoctorSpecializationViewModel IRepository<CurrentDoctorSpecializationViewModel>.GetById(long id)
        {
            throw new NotImplementedException();
        }
    }
}
