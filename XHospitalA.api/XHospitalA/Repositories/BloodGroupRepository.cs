﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class BloodGroupRepository : IRepository<BloodGroupViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();

        public BloodGroupRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public BloodGroupViewModel ChangeStatus(long id, bool status)
        {
            BloodGroupViewModel result = new BloodGroupViewModel();
            try
            {
                BloodGroup entity = _dbContext.BloodGroups
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    //entity.ModifiedBy = ClaimsContext.UserName();
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new BloodGroupViewModel()
                    {
                        Id = entity.Id,
                        Is_delete = status,
                        Code = entity.Code,
                        Description = entity.Description
                    };
                }
                // else
                // {
                // _result.Success = false;
                // _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public BloodGroupViewModel Create(BloodGroupViewModel model)
        {
            try
            {
                BloodGroup entity = new BloodGroup();

                entity.Code = model.Code;
                entity.Description = model.Description;

                _dbContext.BloodGroups.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<BloodGroupViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public BloodGroupViewModel GetById(long id)
        {
            BloodGroupViewModel model = new BloodGroupViewModel();
            try
            {
                model = (from o in _dbContext.BloodGroups  
                         where o.Id == id
                         select new BloodGroupViewModel
                         {
                             Id = o.Id,
                             Code=o.Code,
                             Description=o.Description,
                             Is_delete=o.Is_delete,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new BloodGroupViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<BloodGroupViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<BloodGroupViewModel> result = new List<BloodGroupViewModel>();
            try
            {
                //Console.WriteLine(ClaimsContext.UserName());
                //filter by search
                var query = _dbContext.BloodGroups
                    .Where(o => o.Code.Contains(search) );
                if (query.Count() > 0)
                {
                    switch (orderBy)
                    {
                        case "code":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Code) : query.OrderByDescending(o => o.Code);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Where(o => o.Is_delete == false)
                        .Take(rows)
                        .Select(o => new BloodGroupViewModel
                        {
                            Id = o.Id,
                            Code = o.Code,
                            Description = o.Description,
                            Is_delete = o.Is_delete,
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }

            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;

        }

        public BloodGroupViewModel Update(BloodGroupViewModel model)
        {
            try
            {
                BloodGroup entity = _dbContext.BloodGroups
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Code = model.Code;
                    entity.Description = model.Description;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
