﻿using Framework.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;
using XHospitalA.Security;

namespace XHospitalA.Repositories
{
    public class UserRepository
    {
        private XHospitalDbContext _dbContext;
        private IConfiguration _configuration;

        public UserRepository(XHospitalDbContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public UserViewModel Authentication(LoginViewModel model)
        {
            UserViewModel result = new UserViewModel();
            User user = _dbContext.Users
                .Where(o => o.Email == model.Email).FirstOrDefault();
            try
            {
                if (user != null)
                {
                    if (user.Password != Encryption.HashSha256(model.Password))
                    {
                        if (user.LoginAttempt == 3)
                        {
                            user.IsLocked = true;
                        }
                        else
                        {
                            user.LoginAttempt = user.LoginAttempt + 1;
                            _dbContext.SaveChanges();
                            return null;
                        }
                    }
                    result = new UserViewModel
                    {
                        Id = user.Id,
                        BiodataId = user.BiodataId,
                        Email = user.Email,
                        LoginAttempt = user.LoginAttempt,
                        IsLocked = user.IsLocked,

                    };
                    result.MenuRole = Roles((long)user.RoleId);
                    result.Biodata = Biodatas((long)user.BiodataId);
                    user.LastLogin = DateTime.Now;
                    _dbContext.SaveChanges();
                    return result;
                }
                else
                {
                    if (user != null)
                    {
                        user.LastLogin = DateTime.Now;
                        _dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return null;
        }
        public UserViewModel Authentication(RegisterViewModel model)
        {
            UserViewModel result = new UserViewModel();
            try
            {
                // Create MasterBiodata
                Biodata biodataEntity = new Biodata
                {
                    FullName = model.FullName,
                    MobilePhone = model.MobilePhone,
                    // Set other properties as needed
                };

                _dbContext.Biodata.Add(biodataEntity);
                _dbContext.SaveChanges();  // Save changes to generate BiodataId

                // Create MasterUser with Biodata association
                User entity = new User
                {
                    Email = model.Email,
                    BiodataId = biodataEntity.Id,  // Set BiodataId to the generated ID
                    Password = Encryption.HashSha256(model.Password),
                    Created_by = 1,
                    Created_on = DateTime.Now,
                    RoleId = model.RoleId,
                    Biodata = biodataEntity  // Establish the relationship with Biodata
                };

                _dbContext.Users.Add(entity);
                _dbContext.SaveChanges();

                // Populate the result with the created user and roles
                result = (from o in _dbContext.Users
                          where o.Email == model.Email
                          select new UserViewModel
                          {
                              Id = o.Id,
                              Email = o.Email,
                              BiodataId = o.BiodataId,
                              FullName = o.Biodata.FullName,
                              MobilePhone = o.Biodata.MobilePhone,
                              RoleId = o.RoleId
                              // Set other properties as needed
                          }).FirstOrDefault();

                result.Roles = Roles((long)entity.RoleId);
            }
            catch (Exception ex)
            {
                // Log the exception details
                Console.WriteLine($"Exception: {ex.Message}\nStackTrace: {ex.StackTrace}");
                throw;
            }

            return result;
        }

        private List<string> Roles(long roleId)
        {
            List<string> result = new List<string>();
            var list = _dbContext.MenuRoles
                .Where(o => o.RoleId == roleId)
                .Join(
                    _dbContext.Menus,
                    MenuRole => MenuRole.MenuId,
                    menu => menu.Id,
                    (MenuRole, menu) => new
                    {
                        MenuName = menu.Name,
                    }
                )
                .ToList();
            result.AddRange(list.Select(item => item.MenuName));
            //foreach (var group in list)
            //{
            //    result.Add(group.Role);
            //}
            return result;
        }

        private BiodataViewModel Biodatas(long biodataId)
        {
            BiodataViewModel result = new BiodataViewModel();
            result = _dbContext.Biodata
                .Where(o => o.Id == biodataId && o.Is_delete == false)
                .Select(o => new BiodataViewModel
                {
                    Id = o.Id,
                    FullName = o.FullName,
                    Image = o.Image,
                    Is_delete = o.Is_delete,
                })
                .FirstOrDefault();
            return result;
        }



        public UserViewModel UpdateUserInfo(string email, string otp, UpdateUserViewModel updateModel)
        {
            UserViewModel result = new UserViewModel();

            try
            {
                // Verify OTP and retrieve user
                User user = _dbContext.Users
                    .Include(u => u.Biodata)  // Include Biodata in the query to avoid lazy loading
                    .Where(o => o.Email == email)
                    .FirstOrDefault();

                if (user != null)
                {
                    if (user.Biodata == null)
                    {
                        // If Biodata doesn't exist, create a new one
                        Biodata newBiodataEntity = new Biodata
                        {
                            FullName = updateModel.FullName,
                            MobilePhone = updateModel.MobilePhone,
                            // Set other properties as needed
                        };

                        _dbContext.Biodata.Add(newBiodataEntity);
                        _dbContext.SaveChanges();

                        // Associate the new Biodata with the user
                        user.BiodataId = newBiodataEntity.Id;
                    }
                    else
                    {
                        // Update existing Biodata properties
                        user.Biodata.FullName = updateModel.FullName;
                        user.Biodata.MobilePhone = updateModel.MobilePhone;
                        // Update other properties as needed
                    }

                    // Update User properties
                    user.Email = updateModel.Email;
                    user.Password = Encryption.HashSha256(updateModel.Password);
                    user.Created_by = 1;
                    user.Created_on = DateTime.Now;
                    user.RoleId = updateModel.RoleId;

                    // Update other properties as needed

                    _dbContext.SaveChanges();

                    // Populate the result with the updated user and roles
                    result = (from o in _dbContext.Users
                              where o.Email == updateModel.Email
                              select new UserViewModel
                              {
                                  Id = o.Id,
                                  Email = o.Email,
                                  BiodataId = o.BiodataId,
                                  FullName = o.Biodata.FullName,
                                  MobilePhone = o.Biodata.MobilePhone,
                                  RoleId = o.RoleId
                                  // Set other properties as needed
                              }).FirstOrDefault();

                    result.Roles = Roles((long)user.RoleId);
                }
                else
                {
                    result.Success = false;
                    result.Message = "Invalid OTP or OTP has expired.";
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error: {ex.Message}";

                if (ex.InnerException != null)
                {
                    result.Message += $" InnerException: {ex.InnerException.Message}";
                }
            }

            return result;
        }
        public OTPViewModel SendOtp(string email)
        {
            OTPViewModel result = new OTPViewModel();

            try
            {
                User user = _dbContext.Users
                    .Where(o => o.Email == email)
                    .FirstOrDefault();

                if (user == null)
                {
                    Random generator = new Random();
                    string otp = generator.Next(0, 1000000).ToString("D6");

                    // Create a new user since the email is not found
                    user = new User
                    {
                        Email = email,

                        BiodataId = null
                        // Set other properties as needed
                    };

                    _dbContext.Users.Add(user);
                    _dbContext.SaveChanges();

                    result.Success = true; // Set success to true since the OTP is sent

                    var smtpClient = new SmtpClient("smtp.gmail.com")
                    {
                        Port = 587,
                        Credentials = new NetworkCredential(_configuration["OTP:UserName"], _configuration["OTP:Password"]),
                        EnableSsl = true,
                    };

                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress("mangatur.aritonang@gmail.com"),
                        Subject = "OTP",
                        Body = $"<h1>Hello, your OTP: {otp}</h1>",
                        IsBodyHtml = true,
                    };

                    mailMessage.To.Add(user.Email);

                    smtpClient.Send(mailMessage);

                    result.Message = "OTP just sent!";
                }
                else
                {
                    result.Success = false;
                    result.Message = "Email already registered!";
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"Error: {ex.Message}";

                if (ex.InnerException != null)
                {
                    result.Message += $" InnerException: {ex.InnerException.Message}";
                }

            }

            return result;
        }
        public TokenViewModel CreateOrUpdate(TokenViewModel model)
        {
            try
            {
                Token entity = _dbContext.Tokens.FirstOrDefault(o => o.Email == model.Email);

                if (entity == null)
                {
                    // Email doesn't exist, create a new record

                    Random generator = new Random();
                    String otp = generator.Next(0, 1000000).ToString("D6");
                    entity = new Token();
                    {

                        entity.Id = model.Id;
                        entity.Email = model.Email;

                        entity.token = otp;
                        entity.Expired_On = (DateTime.Now).AddMinutes(10);



                        var smtpClient = new SmtpClient("smtp.gmail.com")
                        {
                            Port = 587,
                            Credentials = new NetworkCredential(_configuration["OTP:UserName"], _configuration["OTP:Password"]),
                            EnableSsl = true,
                        };

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress("winkawinoy@gmail.com"),
                            Subject = "OTP",
                            Body = $"<h1>Hello, your OTP: {otp}</h1>",
                            IsBodyHtml = true,
                        };

                        mailMessage.To.Add(entity.Email);

                        smtpClient.Send(mailMessage);

                        _dbContext.SaveChanges();


                        //entity.password = model.password;
                        //entity.Otp = model.otp;
                        //entity.biodata_id = bio.id;
                        //entity.role_id = model.role_id;
                        entity.Created_by = 001;
                        entity.Created_on = DateTime.Now;
                    };
                    _dbContext.Tokens.Add(entity);
                    _dbContext.SaveChanges();

                    model.Id = entity.Id;

                    return model;
                }
                else
                {
                    // Email already exists, update the existing record

                    Random generator = new Random();
                    String otp = generator.Next(0, 1000000).ToString("D6");

                    entity.token = otp;
                    entity.Expired_On = DateTime.Now.AddMinutes(10);

                    // ... (your existing code for sending OTP email)

                    _dbContext.SaveChanges();

                    var smtpClient = new SmtpClient("smtp.gmail.com")
                    {
                        Port = 587,
                        Credentials = new NetworkCredential(_configuration["OTP:UserName"], _configuration["OTP:Password"]),
                        EnableSsl = true,
                    };

                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress("winkawinoy@gmail.com"),
                        Subject = "OTP",
                        Body = $"<h1>Hello, your OTP: {otp}</h1>",
                        IsBodyHtml = true,
                    };

                    mailMessage.To.Add(entity.Email);

                    smtpClient.Send(mailMessage);

                    _dbContext.SaveChanges();

                    // Update additional fields if needed
                    // entity.password = model.password;
                    // entity.Otp = model.otp;
                    // entity.biodata_id = bio.id;
                    // entity.role_id = model.role_id;
                    entity.Created_by = 001;
                    entity.Created_on = DateTime.Now;

                    _dbContext.SaveChanges();

                    model.Id = entity.Id;

                    return model;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
