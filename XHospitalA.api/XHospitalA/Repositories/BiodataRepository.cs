﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;
using XHospitalA.Security;

namespace XHospitalA.Repositories
{
    public class BiodataRepository : IRepository<BiodataViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public BiodataRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public BiodataViewModel ChangeStatus(long id, bool status)
        {
            BiodataViewModel result = new BiodataViewModel();
            try
            {
                Biodata entity = _dbContext.Biodata
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new BiodataViewModel()
                    {
                        Is_delete = status,
                        Id = entity.Id,
                        FullName = entity.FullName,
                        Image = entity.Image,
                        MobilePhone = entity.MobilePhone,
                        ImagePath = entity.ImagePath
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public BiodataViewModel Create(BiodataViewModel model)
        {
            try
            {
                Biodata entity = new Biodata();
                entity.ImagePath = model.ImagePath;
                entity.FullName = model.FullName;
                entity.Id = model.Id;
                entity.Is_delete = model.Is_delete;
                entity.MobilePhone = model.MobilePhone;
                entity.Image = model.Image;
                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;
                _dbContext.Biodata.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<BiodataViewModel> GetAll()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            try
            {
                result = _dbContext.Biodata
                    .Where(o => o.FullName != null) // Filter out records where FullName is null
                    .Select(o => new BiodataViewModel
                    {
                        Is_delete = o.Is_delete,
                        Id = o.Id,
                        FullName = o.FullName,
                        Image = o.Image,
                        MobilePhone = o.MobilePhone,
                        ImagePath = o.ImagePath
                    })
                    .ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public BiodataViewModel GetById(long id)
        {
            BiodataViewModel model = new BiodataViewModel();
            try
            {
                model = (from o in _dbContext.Biodata
                         where o.Id == id
                         select new BiodataViewModel
                         {
                             Id = o.Id,
                             FullName = o.FullName,
                             ImagePath = o.ImagePath,
                             MobilePhone = o.MobilePhone,
                             Image = o.Image,
                             Is_delete = o.Is_delete
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new BiodataViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<BiodataViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            try
            {
                // filter by search
                var query = _dbContext.Biodata
                    .Where(o => o.FullName.Contains(search));

                if (query.Count() > 0)
                {
                    // apply sorting based on orderBy and sort direction
                    switch (orderBy)
                    {
                        case "fullName":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.FullName) : query.OrderByDescending(o => o.FullName);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }

                    // select and project the data
                    var selectedData = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Select(o => new BiodataViewModel
                        {
                            Is_delete = o.Is_delete,
                            Id = o.Id,
                            FullName = o.FullName,
                            Image = o.Image,
                            MobilePhone = o.MobilePhone,
                            ImagePath = o.ImagePath
                        });

                    // create result list based on selected data
                    List<BiodataViewModel> result = selectedData
                        .Select(biodata => new BiodataViewModel
                        {
                            Id = biodata.Id,
                            FullName = biodata.FullName,
                            Image = biodata.Image ?? null,
                            MobilePhone = biodata.MobilePhone ?? null,
                            ImagePath = biodata.ImagePath ?? null
                        })
                        .ToList();

                    // update response result
                    _result.Data = result;
                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }

            return _result;
        }


        public BiodataViewModel Update(BiodataViewModel model)
        {
            try
            {
                Biodata entity = _dbContext.Biodata
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Image = model.Image;
                    entity.FullName = model.FullName;
                    entity.ImagePath = model.ImagePath;
                    entity.MobilePhone = model.MobilePhone;
                    entity.Image = model.Image;
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }

        public BiodataViewModel UpdateImage(long id, BiodataViewModel model)
        {
            try
            {
                Biodata entity = _dbContext.Biodata
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Image = model.Image;
                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model = new BiodataViewModel
                    {
                        FullName = entity.FullName,
                        Id = entity.Id,
                        ImagePath = entity.ImagePath,
                        MobilePhone = entity.MobilePhone,
                        Image = entity.Image,
                        Is_delete = entity.Is_delete,
                    };
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
