﻿using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class AppointmentRepository : IRepository<AppointmentViewModel>
    {
        private XHospitalDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public AppointmentRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public AppointmentViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public AppointmentViewModel Create(AppointmentViewModel model)
        {
            try
            {
                Appointment entity = new Appointment();
                entity.CustomerId = model.CustomerId;
                entity.DoctorOfficeId = model.DoctorOfficeId;
                entity.DoctorOfficeScheduleId = model.DoctorOfficeScheduleId;
                entity.DoctorOfficeTreatmentId = model.DoctorOfficeTreatmentId;
                entity.AppointmentDate = model.AppointmentDate;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.Appointments.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<AppointmentViewModel> GetAppointmentByDoctorId(long id)
        {
            List<AppointmentViewModel> result = new List<AppointmentViewModel>();
            try
            {
                result = (from o in _dbContext.Appointments
                          where o.DoctorOffice.DoctorId == id
                          select new AppointmentViewModel
                          {
                              Id = o.Id,
                              CustomerId = o.CustomerId,
                              DoctorOfficeId = o.DoctorOfficeId,
                              DoctorOfficeScheduleId = o.DoctorOfficeScheduleId,
                              DoctorOfficeTreatmentId = o.DoctorOfficeTreatmentId,
                              AppointmentDate = o.AppointmentDate
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public AppointmentViewModel GetById(long id)
        {
            throw new NotImplementedException();
        }

        public List<AppointmentViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public AppointmentViewModel Update(AppointmentViewModel model)
        {
            throw new NotImplementedException();
        }

        public List<CustomerViewModelGet> GetCustomerRelation(long id)
        {
            List<CustomerViewModelGet> result = new List<CustomerViewModelGet>();
            try
            {
                result = (from c in _dbContext.Customers
                          join cm in _dbContext.CustomerMembers on c.Id equals cm.CustomerId
                          join cr in _dbContext.CustomerRelations on cm.CustomerRelationId equals cr.Id
                          join b in _dbContext.Biodata on c.BiodataId equals b.Id
                          where cm.CustomerId == id
                          select new CustomerViewModelGet
                          {
                              Id = c.Id,
                              Biodata = new BiodataViewModel()
                              {
                                  FullName = b.FullName
                              },
                              CustomerRelation = new CustomerRelationViewModel() { 
                                  Name = cr.Name 
                              },
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<GetDoctorOfficeViewModel> GetMedicalFacility(long id)
        {
            List<GetDoctorOfficeViewModel> result = new List<GetDoctorOfficeViewModel>();
            try
            {
                result = (from dof in _dbContext.DoctorOffices
                          join mf in _dbContext.MedicalFacilities on dof.MedicalFacilityId equals mf.Id
                          where dof.DoctorId == id
                          select new GetDoctorOfficeViewModel
                          {
                              Id = dof.Id,
                              MedicalFacility = new GetMedicalFacilityViewModel()
                              {
                                  Name = mf.Name
                              }
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<GetDoctorOfficeScheduleViewModel> GetSchedule(long id)
        {
            List<GetDoctorOfficeScheduleViewModel> result = new List<GetDoctorOfficeScheduleViewModel>();
            try
            {
                result = (from dos in _dbContext.DoctorOfficeSchedules
                          join mfs in _dbContext.MedicalFacilitySchedules on dos.MedicalFacilityScheduleId equals mfs.Id
                          where dos.DoctorId == id
                          select new GetDoctorOfficeScheduleViewModel
                          {
                              Id = dos.Id,
                              MedicalFacilitySchedule = new MedicalFacilityScheduleViewModel()
                              {
                                  Id = mfs.Id,
                                  Day = mfs.Day,
                                  TimeScheduleStart = mfs.TimeScheduleStart,
                                  TimeScheduleEnd = mfs.TimeScheduleEnd
                              }
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<GetDoctorOfficeTreatmentViewModel> GetTreatment(long id)
        {
            List<GetDoctorOfficeTreatmentViewModel> result = new List<GetDoctorOfficeTreatmentViewModel>();
            try
            {
                result = (from dot in _dbContext.DoctorOfficeTreatments
                          join dt in _dbContext.DoctorTreatments on dot.DoctorTreatmentId equals dt.Id
                          where dt.DoctorId == id
                          select new GetDoctorOfficeTreatmentViewModel
                          {
                              Id = dot.Id,
                              DoctorTreatmentId = dot.DoctorTreatmentId,
                              DoctorTreatment = new DoctorTreatmentViewModel()
                              {
                                  Id = dt.Id,
                                  Name = dt.Name,
                                  DoctorId = dt.DoctorId
                              },
                              DoctorOfficeId = dot.DoctorOfficeId,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<AppointmentViewModel> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
