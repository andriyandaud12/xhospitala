﻿using Microsoft.EntityFrameworkCore;
using System.Numerics;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class MedicalFacilityRepository : IRepository<MedicalFacilityViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public MedicalFacilityRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public MedicalFacilityViewModel ChangeStatus(long id, bool status)
        {
            MedicalFacilityViewModel result = new MedicalFacilityViewModel();
            try
            {
                MedicalFacility entity = _dbContext.MedicalFacilities
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new MedicalFacilityViewModel()
                    {
                        Is_delete = status,
                        Id = entity.Id,
                        Email = entity.Email,
                        Fax = entity.Fax,
                        FullAddress = entity.FullAddress,
                        LocationId = entity.LocationId,
                        MedicalFacilityCategoryId = entity.MedicalFacilityCategoryId,
                        Name = entity.Name,
                        Phone = entity.Phone,
                        PhoneCode = entity.PhoneCode,
                    };
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public MedicalFacilityViewModel Create(MedicalFacilityViewModel model)
        {
            try
            {
                MedicalFacility entity = new MedicalFacility();
                entity.Is_delete = model.Is_delete;
                entity.PhoneCode = model.PhoneCode;
                entity.Name = model.Name;
                entity.Phone = model.Phone;
                entity.Email= model.Email;
                entity.Fax = model.Fax;
                entity.FullAddress = model.FullAddress;
                entity.LocationId = model.LocationId;
                entity.MedicalFacilityCategoryId = model.MedicalFacilityCategoryId;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;

                _dbContext.MedicalFacilities.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GetMedicalFacilityViewModel> GetAll()
        {
            List<GetMedicalFacilityViewModel> result = new List<GetMedicalFacilityViewModel>();
            try
            {
                result = (from o in _dbContext.MedicalFacilities
                          select new GetMedicalFacilityViewModel
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                              Email = o.Email,
                              Fax = o.Fax,
                              FullAddress = o.FullAddress,
                              Location = new LocationViewModel
                              {
                                  Is_delete = o.Location.Is_delete,
                                  Id = o.Location.Id,
                                  LocationLevelId = o.Location.LocationLevelId,
                                  Name = o.Location.Name,
                                  ParentId = o.Location.ParentId,
                                  Location = new LocationViewModel
                                  {
                                      Is_delete = o.Location.Locations.Is_delete,
                                      Id = o.Location.Locations.Id,
                                      LocationLevelId = o.Location.Locations.LocationLevelId,
                                      Name = o.Location.Locations.Name,
                                      ParentId = o.Location.Locations.ParentId,
                                      Location = new LocationViewModel
                                      {
                                          Is_delete = o.Location.Locations.Locations.Is_delete,
                                          Id = o.Location.Locations.Locations.Id,
                                          LocationLevelId = o.Location.Locations.Locations.LocationLevelId,
                                          Name = o.Location.Locations.Locations.Name,
                                          ParentId = o.Location.Locations.Locations.ParentId,
                                      }
                                  }
                              },
                              LocationId = o.LocationId,
                              MedicalFacilityCategoryId = o.MedicalFacilityCategoryId,
                              Name = o.Name,
                              Phone = o.Phone,
                              PhoneCode = o.PhoneCode,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GetMedicalFacilityViewModel GetById(long id)
        {
            GetMedicalFacilityViewModel model = new GetMedicalFacilityViewModel();
            try
            {
                model = (from o in _dbContext.MedicalFacilities
                         where o.Id == id
                         select new GetMedicalFacilityViewModel
                         {
                             Is_delete = o.Is_delete,
                             Id = o.Id,
                             Email = o.Email,
                             Fax = o.Fax,
                             FullAddress = o.FullAddress,
                             LocationId = o.LocationId,
                             MedicalFacilityCategoryId = o.MedicalFacilityCategoryId,
                             Name = o.Name,
                             Phone = o.Phone,
                             PhoneCode = o.PhoneCode,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GetMedicalFacilityViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<MedicalFacilityViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public MedicalFacilityViewModel Update(MedicalFacilityViewModel model)
        {
            try
            {
                MedicalFacility entity = _dbContext.MedicalFacilities
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = model.Is_delete;
                    entity.Id = model.Id;
                    entity.Email = model.Email;
                    entity.Fax = model.Fax;
                    entity.FullAddress = model.FullAddress;
                    entity.LocationId = model.LocationId;
                    entity.MedicalFacilityCategoryId = model.MedicalFacilityCategoryId;
                    entity.Name = model.Name;
                    entity.Phone = model.Phone;
                    entity.PhoneCode = model.PhoneCode;


                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }

        List<MedicalFacilityViewModel> IRepository<MedicalFacilityViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        MedicalFacilityViewModel IRepository<MedicalFacilityViewModel>.GetById(long id)
        {
            throw new NotImplementedException();
        }
    }
}
