﻿using ViewModel;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class DoctorEducationRepository
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public DoctorEducationRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public DoctorEducationViewModel ChangeStatus(long id, bool status)
        {
            DoctorEducationViewModel result = new DoctorEducationViewModel();
            try
            {
                DoctorEducation entity = _dbContext.DoctorEducations
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    entity.Deleted_on = DateTime.Now;
                    entity.Deleted_by = 1;

                    _dbContext.SaveChanges();

                    result = new DoctorEducationViewModel()
                    {
                        Is_delete = status,
                        Id = entity.Id,
                        DoctorId = entity.DoctorId,
                        EducationLevelId = entity.EducationLevelId,
                        EndYear = entity.EndYear,
                        InstitutionName = entity.InstitutionName,
                        IsLastEducation = entity.IsLastEducation,
                        Major = entity.Major,
                        StartYear = entity.StartYear
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public DoctorEducationViewModel Create(DoctorEducationViewModel model)
        {
            try
            {
                DoctorEducation entity = new DoctorEducation();
                entity.Is_delete = model.Is_delete;
                entity.DoctorId = model.DoctorId;
                entity.IsLastEducation = model.IsLastEducation;
                entity.EducationLevelId = model.EducationLevelId;
                entity.Major = model.Major;
                entity.StartYear = model.StartYear;
                entity.EndYear = model.EndYear;
                entity.InstitutionName = model.InstitutionName;
                entity.Is_delete = model.Is_delete;

                entity.Created_by = 1;
                entity.Created_on = DateTime.Now;
                _dbContext.DoctorEducations.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GetDoctorEducationViewModel> GetAll()
        {
            List<GetDoctorEducationViewModel> result = new List<GetDoctorEducationViewModel>();
            try
            {
                result = (from o in _dbContext.DoctorEducations
                          select new GetDoctorEducationViewModel
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                              DoctorId = o.DoctorId,
                              EducationLevelId = o.EducationLevelId,
                              EndYear = o.EndYear,
                              Doctor = new DoctorWFViewModel
                              {
                                  Id = o.Doctor.Id,
                                  BiodataId = o.Doctor.BiodataId,
                                  Str = o.Doctor.Str,
                                  Biodata = new BiodataViewModel
                                  {
                                      FullName = o.Doctor.Biodata.FullName,
                                      Id = o.Doctor.Biodata.Id,
                                      Image = o.Doctor.Biodata.Image,
                                      ImagePath = o.Doctor.Biodata.ImagePath,
                                      MobilePhone = o.Doctor.Biodata.MobilePhone,
                                      Is_delete = o.Doctor.Biodata.Is_delete
                                  }
                              },
                              EducationLevel = new EducationLevelViewModel
                              {
                                  Id = o.EducationLevel.Id,
                                  Name = o.EducationLevel.Name
                              },
                              InstitutionName = o.InstitutionName,
                              IsLastEducation = o.IsLastEducation,
                              Major = o.Major,
                              StartYear = o.StartYear
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GetDoctorEducationViewModel GetById(long id)
        {
            GetDoctorEducationViewModel model = new GetDoctorEducationViewModel();
            try
            {
                model = (from o in _dbContext.DoctorEducations
                         where o.Id == id
                         select new GetDoctorEducationViewModel
                         {
                             Is_delete = o.Is_delete,
                             Id = o.Id,
                             DoctorId = o.DoctorId,
                             EducationLevelId = o.EducationLevelId,
                             EndYear = o.EndYear,
                             Doctor = new DoctorWFViewModel
                             {
                                 Id = o.Doctor.Id,
                                 BiodataId = o.Doctor.BiodataId,
                                 Str = o.Doctor.Str,
                                 Biodata = new BiodataViewModel
                                 {
                                     FullName = o.Doctor.Biodata.FullName,
                                     Id = o.Doctor.Biodata.Id,
                                     Image = o.Doctor.Biodata.Image,
                                     ImagePath = o.Doctor.Biodata.ImagePath,
                                     MobilePhone = o.Doctor.Biodata.MobilePhone,
                                     Is_delete = o.Doctor.Biodata.Is_delete
                                 }
                             },
                             EducationLevel = new EducationLevelViewModel
                             {
                                 Id = o.EducationLevel.Id,
                                 Name = o.EducationLevel.Name
                             },
                             InstitutionName = o.InstitutionName,
                             IsLastEducation = o.IsLastEducation,
                             Major = o.Major,
                             StartYear = o.StartYear
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GetDoctorEducationViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public DoctorEducationViewModel Update(DoctorEducationViewModel model)
        {
            try
            {
                DoctorEducation entity = _dbContext.DoctorEducations
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.StartYear = model.StartYear;
                    entity.EndYear = model.EndYear;
                    entity.Major = model.Major;
                    entity.DoctorId = model.DoctorId;
                    entity.EducationLevelId = model.EducationLevelId;
                    entity.IsLastEducation = model.IsLastEducation;
                    entity.InstitutionName = model.InstitutionName;

                    entity.Modified_by = 1;
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
