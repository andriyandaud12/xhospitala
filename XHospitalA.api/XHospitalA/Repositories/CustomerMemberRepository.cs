﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.Repositories;
using XHospitalA.DataModel;

namespace XHospitalA.Repositories
{
    public class CustomerMemberRepository : IRepository<CustomerMemberViewModel>
    {
        private XHospitalDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public CustomerMemberRepository(XHospitalDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public CustomerMemberViewModelGet ChangeStatus(long id, bool status)
        {
            CustomerMemberViewModelGet result = new CustomerMemberViewModelGet();
            try
            {
                CustomerMember entity = _dbContext.CustomerMembers
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Is_delete = status;

                    //entity.ModifiedBy = ClaimsContext.UserName();
                    entity.Modified_on = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new CustomerMemberViewModelGet()
                    {
                        Id = entity.Id,
                        Is_delete = status,
                    };
                }
                // else
                // {
                // _result.Success = false;
                // _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public CustomerMemberViewModelGet Create(CustomerMemberViewModelGet model)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    // Create Biodata
                    Biodata biodataEntity = new Biodata
                    {
                        FullName = model.FullName,
                        // Set other properties as needed
                    };

                    _dbContext.Biodata.Add(biodataEntity);
                    _dbContext.SaveChanges();  // Save changes to generate BiodataId

                    // Check if the BiodataId exists
                    var existingBiodata = _dbContext.Biodata.Find(biodataEntity.Id);
                    if (existingBiodata == null)
                    {
                        // Handle the case where the Biodata entry does not exist
                        // You may need to log an error, throw an exception, or take appropriate action
                        // Rollback the transaction if necessary
                        transaction.Rollback();
                        return null;
                    }

                    // Create Customer
                    Customer customerEntity = new Customer
                    {
                        Dob = model.Dob,
                        Gender = model.Gender,
                        RhesusType = model.RhesusType,
                        Height = model.Height,
                        Weight = model.Weight,
                        BloodGroupId = model.BloodGroupId,
                        BiodataId = existingBiodata.Id,  // Set BiodataId to the existing ID
                    };

                    _dbContext.Customers.Add(customerEntity);
                    _dbContext.SaveChanges();  // Save changes to generate CustomerId

                    // Create CustomerMember with associated relationships
                    CustomerMember entity = new CustomerMember
                    {
                        Is_delete = model.Is_delete,
                        Created_by = 1,
                        Created_on = DateTime.Now,
                        ParentBiodataId = existingBiodata.Id,
                        CustomerId = model.CustomerId,
                        CustomerRelationId = model.CustomerRelationId,
                        Biodata = existingBiodata,
                        Customer = customerEntity,
                    };

                    _dbContext.CustomerMembers.Add(entity);
                    _dbContext.SaveChanges();

                    // Assign values to the model
                    model.Id = entity.Id;
                    model.CustomerId = entity.CustomerId;
                    model.BloodGroupId = (long)customerEntity.BloodGroupId;  // Retrieve BloodGroupId from the associated Customer

                    transaction.Commit();
                    return model;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }


        public CustomerMemberViewModel Create(CustomerMemberViewModel model)
        {
            throw new NotImplementedException();
        }

        public List<CustomerMemberViewModelGet> GetAll()
        {
            List<CustomerMemberViewModelGet> result = new List<CustomerMemberViewModelGet>();
            try
            {
                result = (from o in _dbContext.CustomerMembers
                          where !o.Is_delete
                          select new CustomerMemberViewModelGet
                          {
                              Is_delete = o.Is_delete,
                              Id = o.Id,
                              ParentBiodataId=o.ParentBiodataId,
                              Biodata = new BiodataViewModel
                              {
                                  Is_delete = o.Biodata.Is_delete,
                                  Id = o.Biodata.Id,
                                  FullName = o.Biodata.FullName,
                                  Image = o.Biodata.Image,
                                  MobilePhone = o.Biodata.MobilePhone,
                                  ImagePath = o.Biodata.ImagePath
                              },
                              CustomerId=o.CustomerId,
                              Customer = new CustomerViewModelGet
                              {
                                  Id = o.Customer.Id,
                                  BiodataId = (long)o.Customer.BiodataId,
                                  BloodGroupId = (long)o.Customer.BloodGroupId,
                                  BloodGroup = new BloodGroupViewModel()
                                  {
                                      Id=o.Customer.BloodGroup.Id,
                                      Code=o.Customer.BloodGroup.Code,
                                      Description= o.Customer.BloodGroup.Description
                                  },
                                  Dob = o.Customer.Dob,
                                  Gender = o.Customer.Gender,
                                  RhesusType = o.Customer.RhesusType,
                                  Height = o.Customer.Height,
                                  Weight = o.Customer.Weight,
                                  Is_delete = o.Customer.Is_delete
                              },
                              CustomerRelationId=o.CustomerRelationId,
                              CustomerRelation = new CustomerRelationViewModel
                              {
                                  Id=o.CustomerRelation.Id,
                                  Name=o.CustomerRelation.Name
                              }
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public CustomerMemberViewModelGet GetById(long id)
        {
            CustomerMemberViewModelGet model = new CustomerMemberViewModelGet();
            try
            {
                model = (from o in _dbContext.CustomerMembers
                         where o.Id == id
                         select new CustomerMemberViewModelGet
                         {
                             Id = o.Id,
                             ParentBiodataId = o.ParentBiodataId ?? 0,
                             Biodata = new BiodataViewModel
                             {
                                 Is_delete = o.Biodata.Is_delete,
                                 Id = o.Biodata.Id,
                                 FullName = o.Biodata.FullName,
                                 Image = o.Biodata.Image,
                                 MobilePhone = o.Biodata.MobilePhone,
                                 ImagePath = o.Biodata.ImagePath
                             },
                             CustomerId=o.CustomerId,
                             CustomerRelationId = o.CustomerRelationId,
                             CustomerRelation = new CustomerRelationViewModel()
                             {
                                 Id = o.CustomerRelation.Id,
                                 Name = o.CustomerRelation.Name
                             },


                             Is_delete = o.Is_delete,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new CustomerMemberViewModelGet();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<CustomerMemberViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<CustomerMemberViewModelGet> result = new List<CustomerMemberViewModelGet>();
            try
            {
                //Console.WriteLine(ClaimsContext.UserName());
                //filter by search
                var query = _dbContext.CustomerMembers
                    .Where(o => o.Biodata.FullName.Contains(search));
                if (query.Count() > 0)
                {
                    switch (orderBy)
                    {
                        case "fullName":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Biodata.FullName) : query.OrderByDescending(o => o.Biodata.FullName);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Where(o => o.Is_delete == false)
                        .Take(rows)
                        .Select(o => new CustomerMemberViewModelGet
                        {
                            Is_delete = o.Is_delete,
                            Id = o.Id,
                            ParentBiodataId = o.ParentBiodataId,
                            Biodata = new BiodataViewModel
                            {
                                Is_delete = o.Biodata.Is_delete,
                                Id = o.Biodata.Id,
                                FullName = o.Biodata.FullName,
                                Image = o.Biodata.Image,
                                MobilePhone = o.Biodata.MobilePhone,
                                ImagePath = o.Biodata.ImagePath
                            },
                            CustomerId = o.CustomerId,
                            Customer = new CustomerViewModelGet
                            {
                                Id = o.Customer.Id,
                                BiodataId = (long)o.Customer.BiodataId,
                                BloodGroupId = (long)o.Customer.BloodGroupId,
                                BloodGroup = new BloodGroupViewModel()
                                {
                                    Id = o.Customer.BloodGroup.Id,
                                    Code = o.Customer.BloodGroup.Code,
                                    Description = o.Customer.BloodGroup.Description
                                },
                                Dob = o.Customer.Dob,
                                Gender = o.Customer.Gender,
                                RhesusType = o.Customer.RhesusType,
                                Height = o.Customer.Height,
                                Weight = o.Customer.Weight,
                                Is_delete = o.Customer.Is_delete
                            },
                            CustomerRelationId = o.CustomerRelationId,
                            CustomerRelation = new CustomerRelationViewModel
                            {
                                Id = o.CustomerRelation.Id,
                                Name = o.CustomerRelation.Name
                            }
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }

            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public CustomerMemberViewModelGet Update(CustomerMemberViewModelGet model)
        {
            try
            {
                CustomerMember entity = _dbContext.CustomerMembers
                    .Include(c => c.Biodata) // Include Biodata to avoid lazy loading
                    .Include(c => c.Customer) // Include Customer to avoid lazy loading
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    if (entity.Biodata != null)
                    {
                        // Update Biodata properties
                        entity.Biodata.FullName = model.Biodata.FullName;
                        // Update other Biodata properties as needed
                    }
                    else
                    {
                        // Handle the case where Biodata is null
                        // You may need to create a new Biodata entity or take appropriate action
                    }

                    if (entity.Customer != null)
                    {
                        // Update Customer properties
                        entity.Customer.Dob = model.Customer.Dob;
                        entity.Customer.Gender = model.Customer.Gender;
                        entity.Customer.RhesusType = model.Customer.RhesusType;
                        entity.Customer.Weight = model.Customer.Weight;
                        entity.Customer.Height = model.Customer.Height;
                        // Update other Customer properties as needed
                    }
                    else
                    {
                        // Handle the case where Customer is null
                        // You may need to create a new Customer entity or take appropriate action
                    }

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // Handle the case where CustomerMember is not found
                    // You may want to set an error message or take appropriate action
                }
            }
            catch (Exception e)
            {
                // Handle the exception
                // You may want to log the exception, set an error message, or take appropriate action
            }

            return model;
        }


        public CustomerMemberViewModel Update(CustomerMemberViewModel model)
        {
            throw new NotImplementedException();
        }

        CustomerMemberViewModel IRepository<CustomerMemberViewModel>.ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        List<CustomerMemberViewModel> IRepository<CustomerMemberViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }

        CustomerMemberViewModel IRepository<CustomerMemberViewModel>.GetById(long id)
        {
            throw new NotImplementedException();
        }
    }
}
