﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;

namespace XHospitalA.Security
{
    public class ReadableBodyStreamAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var email = context.HttpContext.User.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            var id = Convert.ToInt64(context.HttpContext.User.Claims.First(claim => claim.Type == "Id").Value);
            new ClaimsContext(email, id);
            //var id = context.HttpContext.User.Claims.First(claim=>claim.Type==ClaimTypes.id)

        }
    }

    public class ClaimsContext
    {
        private static string _Email;
        public static long _Id;
        public ClaimsContext(string email, long id)
        {
            _Email = email;
            _Id = id;
        }

        public static string Email()
        {
            return _Email;
        }
        public static long Id()
        {
            return _Id;
        }


    }

}
